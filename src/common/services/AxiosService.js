import axios from 'axios';
import {env} from "../configs/EnvironmentConfig";
import {notification} from "antd";
import {onChangeShowPopup} from "../redux/actions";


const axiosServices = axios.create({
  baseURL: env.API_ENDPOINT_URL,
  withCredentials: true
})

if (typeof window !== 'undefined') {
  axiosServices.interceptors.request.use(async (config) => {
      // const accessToken = getCookiesData(ACCESS_TOKEN);
      // const refreshToken = getCookiesData(REFRESH_TOKEN);
      // let access_token = accessToken;
      // if (refreshToken && !accessToken) {
      //   try {
      //     const res = await ApiService.refreshToken(refreshToken);
      //     const {data} = res;
      //     setToken(data);
      //     access_token = res.data.access.token;
      //   } catch (err) {
      //     access_token = '';
      //   }
      // }
      // config.headers[TOKEN_PAYLOAD_KEY] = `Bearer ${access_token}`
      return config
    }, error => {
      // Do something with request error here
      notification.error({
        message: 'Error'
      })
      Promise.reject(error)
    }
  )
}

axiosServices.interceptors.response.use((res) => {
    return {data: res.data, status: res.status};
  },
  err => {
    if (err.message === 'Network Error') {
      localStorage.clear();
      return window.location.replace('/error');
    }
    const {response} = err;
    if (response) {
      const {data} = response;
      const {code, message} = data;
      let notificationParam = {
        message: ''
      }

      if ([403, 400, 500].includes(code) && typeof window !== 'undefined') {
        notificationParam.message = message;
      }

      if (code === 404 && typeof window !== 'undefined') {
        notificationParam.message = message;
      }

      if ([401].includes(code)) {
        onChangeShowPopup(true);
      }
      if(notificationParam.message !== '') {
        notification.error({message: notificationParam.message})
      }
      return Promise.reject(response);
    }
  }
)

export default axiosServices;
