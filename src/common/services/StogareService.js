import {ACCESS_TOKEN, REFRESH_TOKEN} from "../configs/AppConfig";

export const getData = (key, defaultValue = {}) => {
  return new Promise((resolve, reject) => {
    if (typeof window !== "undefined") {
      const data = localStorage.getItem(key);
      if (!data) {
        return resolve(defaultValue)
      } else {
        return resolve(JSON.parse(data))
      }
    }
  })
}

export const setData = (key, data) => {
  return new Promise(resolve => {
    localStorage.setItem(key, data);
    resolve();
  })
}


export const removeData = (key, data) => {
  return new Promise(resolve => {
    localStorage.removeItem(key);
    resolve();
  })
}

export const authDataStorage = (tokens) => {
  return new Promise(async (resolve) => {
    try {
      const {refresh, access} = tokens;
      if (!refresh) resolve(false);
      await setData('ACCESS_TOKEN', JSON.stringify(access));
      await setData('REFRESH_TOKEN', JSON.stringify(refresh));
      resolve(true);
    } catch (err) {
      resolve(false);
    }
  })
}

export const setCookiesData = (cname, cvalue, expireTime) => {
  if (typeof window !== "undefined") {
    const d = new Date();
    d.setTime(expireTime);
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
}

export const getCookiesData = (cname) => {
  if (typeof window !== "undefined") {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return null;
  }
}

export const resetToken = () => {
  if (typeof window !== "undefined") {
    const currentTime = new Date().getTime();
    setCookiesData(ACCESS_TOKEN, '', currentTime - 100);
    setCookiesData(REFRESH_TOKEN, '', currentTime - 100);
  }
}

export const setToken = (tokens) => {
  if (typeof window !== "undefined") {
    const {access, refresh} = tokens;
    setCookiesData(ACCESS_TOKEN, access.token, access.expires);
    setCookiesData(REFRESH_TOKEN, refresh.token, refresh.expires);
  }
}