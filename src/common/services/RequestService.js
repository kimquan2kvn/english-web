import axiosServices from "./AxiosService";
import Utils from "../utils";
import {onChangeShowPopup} from "../redux/actions";
import store from "../redux/store";

const sendRequest = (url, method, data, config = {}) => {
  const state = store.getState();
  const { user } = state;
  if(!user.hasOwnProperty('_id') || user._id === '') {
    onChangeShowPopup(true);
    return Promise.reject('error');
  }
  return new Promise(async (resolve, reject) => {
    const URL = method.toUpperCase() === 'GET' ? url + Utils.getParams(data) : url;
    const DATA = method.toUpperCase() === 'GET' ? {} : data;
    axiosServices({
      url: URL,
      method: method.toUpperCase(),
      data: DATA,
      headers: config
    }).then(res => resolve(res)).catch(err => reject(err));
  })
}

export default sendRequest;