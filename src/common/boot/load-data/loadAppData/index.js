import useSWR from 'swr';
import ApiService from "../../../services/ApiService";
import {updateUser} from "../../../redux/actions/User";

const loadUserData = () => {
  return new Promise(async resolve => {
    try {
      const res = await ApiService.refreshToken();
      updateUser(res.data);
      resolve(res.data);
    } catch (err) {
      console.log('err', err)
      updateUser({});
      resolve(null);
    }
  })
}

// export const loadComTags = () => {
//   return new Promise(async resolve => {
//     try {
//       const res = await ApiService.getTags({limit: 10, page: 1});
//       const { results, page, totalPages } = res.data;
//       updateTags(results);
//     }
//     catch(err) {
//       updateTags([]);
//     }
//     resolve([]);
//   })
// }

export const loadData = () => {
  return new Promise(async (resolve, reject) => {
    const user = await loadUserData();
    resolve({
      userData: {},
    })
  })
}

export default function useLoadUserData() {
  const {data, mutate, error} = useSWR("/", loadData);
  const loading = !data && !error;
  return {
    loading,
  }
}