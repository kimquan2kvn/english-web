import React, {useEffect} from 'react';
import useLoadUserData from "../loadAppData";
import {LoadingOutlined} from "@ant-design/icons";
import {useRouter} from "next/router";

const LoadData = ({children}) => {
  const { loading } = useLoadUserData();

  return (
    <>
      {
        loading
          ?
          <div
            style={{height: '100vh', display: 'flex'}}
          >
            <LoadingOutlined
              style={{fontSize: 30, margin: 'auto'}}
            />
          </div>
          :
          children
      }
    </>
  )
}

export default LoadData;