import enLang from './entries/en_US';
import ViLang from "./entries/vi_VN";

const AppLocale = {
    en: enLang,
    vi: ViLang
};

export default AppLocale;
