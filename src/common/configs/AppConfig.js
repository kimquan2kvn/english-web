import {SIDE_NAV_LIGHT, NAV_TYPE_SIDE, DIR_LTR, NAV_TYPE_TOP} from 'common/constants/ThemeConstant';
import { env } from './EnvironmentConfig'

export const APP_NAME = 'English or Foolish';
export const API_BASE_URL = env.API_ENDPOINT_URL
export const APP_PREFIX_PATH = '';
export const AUTH_PREFIX_PATH = '/auth';
export const GOOGLE_CLIENT_ID = '452366200975-22psnkcojje78kgqnhp6sqfcrbd029il.apps.googleusercontent.com';
export const FACEBOOK_APP_ID = '1055271015013176';
export const ACCESS_TOKEN = 'access_token';
export const REFRESH_TOKEN = 'refresh_token';

export const THEME_CONFIG = {
	navCollapsed: false,
	sideNavTheme: SIDE_NAV_LIGHT,
	locale: 'en',
	navType: NAV_TYPE_TOP,
	topNavColor: '#3e82f7',
	headerNavColor: '',
	mobileNav: false,
	currentTheme: 'light',
	direction: DIR_LTR,
	showPopup: false,
};
