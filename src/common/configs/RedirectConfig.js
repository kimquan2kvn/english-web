const RedirectConfig = (code) => {
  switch (code) {
    case 401 : {
      return {
        destination: '/auth/login',
        permanent: false,
      }
    }
    case 403: {
      return {
        destination: '/error/403',
        permanent: false,
      }
    }
    case 400: {
      return {
        destination: '/error/400',
        permanent: false,
      }
    }
    case 404: {
      return {
        destination: '/error/400',
        permanent: false,
      }
    }
    default:
      return ;
  }
}

export default RedirectConfig;
