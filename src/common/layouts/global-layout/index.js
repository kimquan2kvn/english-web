import React from 'react';
import {Provider, useSelector} from 'react-redux';
import {ThemeSwitcherProvider} from "react-css-theme-switcher";
import {THEME_CONFIG} from 'common/configs/AppConfig';
import AppLocale from "common/lang";
import {IntlProvider} from "react-intl";
import {ConfigProvider} from 'antd';
import useBodyClass from 'common/hooks/useBodyClass';

const themes = {
  dark: `/css/dark-theme.css`,
  light: `/css/light-theme.css`,
};

const ConfigApp = props => {
  const {locale, direction} = useSelector(state => state.theme);
  const currentAppLocale = AppLocale[locale];
  console.log("theme", locale)

  useBodyClass(`dir-${direction}`);
  return (
    <IntlProvider
      locale={currentAppLocale.locale}
      messages={currentAppLocale.messages}>
      <ConfigProvider locale={currentAppLocale.antd} direction={direction}>
        {props.children}
      </ConfigProvider>
    </IntlProvider>
  );
};

const GlobalLayout = props => {
  return (
    <div className="App">
      <ThemeSwitcherProvider
        themeMap={themes}
        defaultTheme={THEME_CONFIG.currentTheme}
        insertionPoint="styles-insertion-point"
      >
      <ConfigApp>
        {props.children}
      </ConfigApp>
    </ThemeSwitcherProvider>
</div>
)
  ;
};

export function getServerSideProps(context) {
  return {
    props: {
      data: 'redf'
    },
  }
}

export default GlobalLayout;
