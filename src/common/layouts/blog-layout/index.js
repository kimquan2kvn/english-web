import React, {useEffect, useState} from "react";
import {Col, notification, Row, Spin} from "antd";
import BlogSideContent from "common/components/blog-components/blog-side-content";
import {AppLayout} from "../app-layout";
import ApiService from "../../services/ApiService";
import BlogList from "../../components/blog-components/blog-list";
import {useRouter} from "next/router";
import Utils from "../../utils";
import {useSelector} from "react-redux";

const Blogs = (props) => {
	const router = useRouter();
	const {slug} = router.query;
	const {propsBlogs, children} = props;
	const [searchBlogData, setSearchBlogData] = useState([]);
	const [keyword, setKeyword] = useState('');
	const [totalResultSearch, setTotalResultSearch] = useState(1);
	const [type, setType] = useState('all');
	const [loadedPageSearch, setLoadedPageSearch] = useState([]);
	const [currentPageSearch, setCurrentPageSearch] = useState(1);
	const [totalBlogSearch, setTotalBlogSearch] = useState(0);
	const [searchLoading, setSearchLoading] = useState(false);
	const ref = React.useRef(null);
	const [resize, setResize] = useState(false);
	const {locale} = useSelector(state => state.theme)


	useEffect(() => {
		if (keyword === '') {
			setType('all');
		} else setType('search');
		setCurrentPageSearch(1);
		setLoadedPageSearch([]);
		setSearchBlogData([]);
	}, [keyword])

	const searchBlogs = async (nextPage, value = keyword) => {
		window.scrollTo(top);
		if (value === '') return;
		setSearchLoading(true);
		try {
			const res = await ApiService.searchBlogs({slug: Utils.slugify(value), limit: 4, page: nextPage});
			const {results, totalResults} = res.data;
			setSearchBlogData(results);
			setTotalResultSearch(totalResults);
			setTotalBlogSearch(totalResults);
		} catch (err) {
			setKeyword('');
			notification.error({message: Utils.translate("message.search.news.failed")});
		}
		setSearchLoading(false);
	}

	useEffect(() => {
		window.addEventListener('resize', () => {
			setResize(state => !state);
		})
		return () => {
			window.removeEventListener('resize', () => {
			}, true);
		}
	}, []);

	return (
		<AppLayout>
			<div className="blog-layout">
				<Row gutter={16}>
					<Col xs={24} sm={24} md={24} lg={18} xl={18} xxl={18}>
						{
							slug && keyword === ''
								?
								children
								:
								<Spin
									spinning={propsBlogs?.loading || searchLoading}
									tip={Utils.translate("label.loading")}
								>
									<BlogList
										searchBlogData={searchBlogData}
										{...propsBlogs}
										type={type}
										totalResultSearch={totalResultSearch}
										currentPageSearch={currentPageSearch}
										loadedPageSearch={loadedPageSearch}
										setCurrentPageSearch={setCurrentPageSearch}
										searchBlogs={searchBlogs}
										totalBlogSearch={totalBlogSearch}
									/>
								</Spin>
						}
					</Col>
					<div ref={ref}
							 className='ant-col ant-col-xs-24 ant-col-sm-24 ant-col-md-24 ant-col-lg-6 ant-col-xl-6 ant-col-xxl-6'
							 style={{paddingLeft: 8, paddingRight: 8}}>
						<BlogSideContent
							parentElm={ref}
							setTotalResultSearch={setTotalResultSearch}
							setCurrentPageSearch={setCurrentPageSearch}
							keyword={keyword}
							setKeyword={setKeyword}
							searchBlogs={searchBlogs}
							{...props}
						/>
					</div>
				</Row>
			</div>
		</AppLayout>
	)
}
export default Blogs;
