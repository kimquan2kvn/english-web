import React from "react";
import {Menu} from "antd";
import {LockOutlined, UserOutlined, FacebookOutlined} from "@ant-design/icons";
import {AppLayout} from "../app-layout";
import InnerAppLayout from "../inner-app-layout";
import Link from "next/link"
import {useRouter} from "next/router";

const SettingOption = (props) => {
  const router = useRouter();
  return (
    <Menu
      defaultSelectedKeys={router.pathname}
      mode="inline"
      selectedKeys={router.pathname}
    >
      <Menu.Item key={`/account-setting/edit-profile`}>
        <Link href={'edit-profile'}>
          <a style={{
            display: 'block',
            height: '100%'
          }}>
            <UserOutlined/>
            <span>Edit Profile</span>
          </a>
        </Link>
      </Menu.Item>
      <Menu.Item key={`/account-setting/change-password`}>
        <Link href={'change-password'}>
          <a style={{
            display: 'block',
            height: '100%'
          }}>
            <LockOutlined/>
            <span>Change Password</span>
          </a>
        </Link>
      </Menu.Item>
      <Menu.Item key={`/account-setting/sso-login`}>
        <Link href={`sso-login`}>
          <a style={{
            display: 'block',
            height: '100%'
          }}>
            <FacebookOutlined/>
            <span>SSO Login</span>
          </a>
        </Link>
      </Menu.Item>
    </Menu>
  );
};

const AccountSettingLayout = props => {
  return (
    <AppLayout>
      <InnerAppLayout
        sideContentWidth={320}
        sideContent={<SettingOption {...props}/>}
        mainContent={props.children}
      />
    </AppLayout>
  )
}
export default AccountSettingLayout
