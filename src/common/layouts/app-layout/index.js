import React from 'react';
import {connect, useSelector} from 'react-redux';
import SideNav from 'common/components/layout-components/SideNav';
import MobileNav from 'common/components/layout-components/MobileNav'
import HeaderNav from 'common/components/layout-components/HeaderNav';
import PageHeader from 'common/components/layout-components/PageHeader';
import Footer from 'common/components/layout-components/Footer';
import Image from 'next/image';
import {
  Layout,
  Grid,
  Modal, Button, BackTop
} from "antd";

import navigationConfig from "common/configs/NavigationConfig";
import {
  SIDE_NAV_WIDTH,
  SIDE_NAV_COLLAPSED_WIDTH,
  NAV_TYPE_SIDE,
  NAV_TYPE_TOP,
  DIR_RTL,
  DIR_LTR
} from 'common/constants/ThemeConstant';
import utils from 'common/utils';
import {useThemeSwitcher} from "react-css-theme-switcher";
import {useRouter} from "next/router";
import {LoginForm} from "../../components/auth-components/LoginForm";
import {onChangeShowPopup} from "../../redux/actions";
import {ArrowUpOutlined} from "@ant-design/icons";

const {Content} = Layout;
const {useBreakpoint} = Grid;

export const AppLayout = (props) => {
  const {navCollapsed, navType, locale, direction} = useSelector(state => state.theme);
  const router = useRouter();
  const currentRouteInfo = utils.getRouteInfo(navigationConfig, router.pathname);
  const screens = utils.getBreakPoint(useBreakpoint());
  const isMobile = !screens.includes('lg');
  const isNavSide = navType === NAV_TYPE_SIDE;
  const isNavTop = navType === NAV_TYPE_TOP;
  const showPopup = useSelector(state => state.theme.showPopup);
  const getLayoutGutter = () => {
    if (isNavTop || isMobile) {
      return 0
    }
    return navCollapsed ? SIDE_NAV_COLLAPSED_WIDTH : SIDE_NAV_WIDTH
  }
  const {status} = useThemeSwitcher();

  // if (loading) {
  //   return <Loading cover="page"/>;
  // }

  const getLayoutDirectionGutter = () => {
    if (direction === DIR_LTR) {
      return {paddingLeft: getLayoutGutter()}
    }
    if (direction === DIR_RTL) {
      return {paddingRight: getLayoutGutter()}
    }
    return {paddingLeft: getLayoutGutter()}
  }

  return (
    <Layout>
      <HeaderNav setShowListQuestion={props.setShowListQuestion} timer={props.timer} isMobile={isMobile}/>
      {/*{(isNavTop && !isMobile) ? <TopNav routeInfo={currentRouteInfo}/> : null}*/}
      <Layout className="app-container">
        {(isNavSide && !isMobile) ? <SideNav routeInfo={currentRouteInfo}/> : null}
        <Layout className="app-layout" style={getLayoutDirectionGutter()}>
          <div className={`app-content ${isNavTop ? 'layout-top-nav' : ''}`}>
            <PageHeader display={currentRouteInfo?.breadcrumb} title={currentRouteInfo?.title}/>
            <Content>
              {props.children}
            </Content>
          </div>
          <Footer/>
        </Layout>
      </Layout>
      {isMobile && <MobileNav/>}
      <Modal
        width={450}
        visible={showPopup}
        footer={false}
        onCancel={() => onChangeShowPopup(false)}
      >
        <div className='d-flex justify-content-center align-items-center flex-column'>
          <Image src='/img/logo.png' height={95} width={120}/>
          <h3>
            Welcome to our community
          </h3>
        </div>
        <div>
          <LoginForm isPopup={true}/>
          <div
            className='d-flex float-bottom'
          >
            <Button
              className='m-auto'
              type='link'
              onClick={() => {
                onChangeShowPopup(false);
                router.push('/auth/register').then(_ => {});
              }}
            >
              You do not have an account?
            </Button>
          </div>
        </div>
      </Modal>
      <BackTop>
        <Button
          shape='circle'
          type={'primary'}
        >
          < ArrowUpOutlined/>
        </Button>
      </BackTop>
    </Layout>
  )
}

export default React.memo(AppLayout);
