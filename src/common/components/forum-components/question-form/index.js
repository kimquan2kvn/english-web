import React, {useEffect, useState, useRef} from "react";
import {Button, Divider, Form, Input, notification, Select, Spin} from "antd";
import {PlusCircleOutlined} from "@ant-design/icons";
import ApiService from "../../../services/ApiService";
import Utils from "../../../utils";
import EditorWithForwardedRef from "../../util-components/DynamicImport/dynamic-editor";
import Editor from "../../util-components/Editor";

const QuestionForm = props => {
  const {questionData, setQuestionData, setQuestions, setVisible, visible} = props;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [tags, setTags] = useState([]);
  const [uploadImage, setUploadImage] = useState(false);
  const refEditor = useRef(null);

  useEffect(() => {
    (async () => {
      try {
        const res = await ApiService.getTags({limit: 10, page: 1});
        const {results, page, totalPages} = res.data;
        setTags(results);
      } catch (err) {
        console.log(err);
      }
    })()
  }, []);

  useEffect(() => {
    if (questionData) {
      const data = {
        title: Utils.decodeHtml(questionData.title),
        question: Utils.decodeHtml(questionData.content),
        tags: questionData.tags.map(x => x._id),
        privacy: questionData?.privacy ?? 'public',
      }
      form.setFieldsValue(data);
    }
  }, [questionData])

  const onSubmit = async (values) => {
    setLoading(true);
    try {
      const data = {
        title: values.title,
        content: refEditor.current.getInstance().getMarkdown().replaceAll(/(<br\s*\/*>|\n)/gi, "\n\n"),
        tags: values.tags,
        privacy: values.privacy,
      }
    const res = questionData ? await ApiService.updateQuestion(questionData.slug, data) : await ApiService.addQuestion(data);
    if (res.data) {
      notification.success({message: questionData ?  Utils.translate("message.edit.question.success") : Utils.translate("message.create.question.success")})
    }
    if (questionData) {
      const question = res.data.question;
      setQuestions(state => {
        return state.map(x => {
          if (x._id === question._id) return {...question, tags: tags.filter(x => data.tags.includes(x._id))};
          else return x;
        })
      })
    }
    } catch (err) {
      console.log(err)
      notification.error({message: questionData ?  Utils.translate("message.edit.question.failed") :  Utils.translate("message.create.question.failed")})
    }
    if (questionData) {
      setQuestionData(null);
      setVisible(false);
    }
    form.resetFields();
    setLoading(false);
  }

  return (
    <Spin
      spinning={loading}
      tip={Utils.translate("message.create.question")}
    >
      <Form
        onFinish={onSubmit}
        form={form}
        layout="vertical"
      >
        <div className="d-flex justify-content-between mb-3">
          {questionData ? <h3>{Utils.translate("label.edit.question")}</h3> : <h3>{Utils.translate("label.add.question")}</h3>}
          {
            questionData ? <></> : <Button onClick={() => form.submit()} type="primary" size="small"
                                           icon={<PlusCircleOutlined/>}>{Utils.translate("label.add")}</Button>
          }
        </div>
        <Divider type="dashed" className="mb-3"/>
        <div>
          <Form.Item
            name="title"
            label={Utils.translate("label.question")}
            rules={[{required: true, message: Utils.translate("message.required.title")}]}
          >
            <Input placeholder={Utils.translate("placeholder.title")}/>
          </Form.Item>
          <Spin
            spinning={uploadImage}
            tip={Utils.translate("message.upload.image")}
          >
            <Editor
              refEditor={refEditor}
              setUploadImage={setUploadImage}
              type='create-image'
              onCreateQuestion={onsubmit}
            />

          </Spin>
          <Form.Item
            name="tags"
            label={Utils.translate("label.tag")}
          >
            <Select placeholder={Utils.translate("label.choose.tag")} mode='multiple' optionLabelProp="label">
              {
                tags?.map(x => {
                  return (
                    // eslint-disable-next-line react/jsx-key
                    <Select.Option key={x._id} value={x._id} label={x.title}>{x.title}</Select.Option>
                  )
                })
              }
            </Select>
          </Form.Item>
          {/*<Form.Item*/}
          {/*  name="privacy"*/}
          {/*  label="Privacy"*/}
          {/*  rules={[{required: true, message: "Privacy is required!"}]}*/}
          {/*>*/}
          {/*  <Select placeholder="Choose status...">*/}
          {/*    <Select.Option value="public">Public</Select.Option>*/}
          {/*    <Select.Option value="private">Private</Select.Option>*/}
          {/*  </Select>*/}
          {/*</Form.Item>*/}
          {
            questionData &&
            <div className='d-flex justify-content-end'>
              <Button type='primary' onClick={() => form.submit()}>
                Edit
              </Button>
            </div>
          }
        </div>
      </Form>
    </Spin>
  )
}

export default QuestionForm;
