import React from "react";
import {Avatar, Button, Card, Divider, Input} from "antd";
import {HeartOutlined, UserOutlined} from "@ant-design/icons";
// import "../../custom.scss"
import CommentCard from "common/components/util-components/comment-card";
const {TextArea} = Input;
const QuestionOverview = props => {
  return (
    <div className="question-overview">
      <Card
        bordered={false}
      >
        <div className="header-overview mb-4">
          <h3>How to use Metronic with Laravel Framework ?</h3>
          <div className="description-question">
            <p>Hi Keenthemes,</p>

            <p>I’ve been doing some ajax request, to populate a inside drawer, the content of that drawer has a sub
              menu, that you are using in list and all card toolbar.</p>

            <p>But they are not displaying, since it is an ajax, I tried “KTApp.init();” but didn’t work (worked for
              Tooltips sure, but not Menu).</p>
            <p>Anyway to Re-init those https://ibb.co/gysPGpx Menu. ?</p>

            <p>Thank you.</p>
          </div>
        </div>
        <div className="question-footer d-flex justify-content-between align-items-center mb-4">
          <div className="author d-flex align-items-center">
            <div className="author-avatar mr-2">
              <Avatar icon={<UserOutlined/>} shape="square"/>
            </div>
            <div className="author-information d-flex flex-column">
              <span className="m-0" style={{fontSize: ".9rem"}}>Nguyen Thu Thuy</span>
              <span className="m-0 font-size-sm font-weight-semibold"
                    style={{fontSize: ".8rem"}}>02 Jan 2021</span>
            </div>
          </div>
          <div>
            <Button type="default" size="small" className="mr-2" style={{borderStyle: "dashed"}}>16
              Answers</Button>
            <Button icon={<HeartOutlined/>} size="small"/>
          </div>
        </div>
        <Divider type="dashed" className="mb-3"/>
        <div className="comment mb-4">
          <TextArea className="mb-2"/>
          <Button className="float-right" type="primary" size="small">Submit</Button>
        </div>
        <div>
          <CommentCard title="Replies(24)" hasInput={true}/>
        </div>
      </Card>
    </div>
  )
}
export default QuestionOverview;
