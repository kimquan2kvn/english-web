import React from "react";
import {Button, Card, Divider, List} from "antd";
import {PlusCircleOutlined} from "@ant-design/icons";
import {Link, useHistory} from "react-router-dom";
import QuestionList from "common/components/forum-components/question-list";

const Tags = props => {
  const history = useHistory()
  return (
    <Card bordered={false}>
      <div className="header-content mb-2">
        <div className="d-flex justify-content-between mb-4">
          <h2 className="mr-2 mb-0">Tag: Exams</h2>
          <Button type="primary" size="small" icon={<PlusCircleOutlined/>} onClick={() => (
            history.replace("/app/forum/questions/add")
          )}>Ask question</Button>
        </div>
        <Divider type="dashed"/>
      </div>
      <div>
        <QuestionList {...props} />
      </div>
    </Card>
  )
}
export default Tags;
