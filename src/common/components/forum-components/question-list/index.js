import React, {useState} from "react";
import {Avatar, Button, Divider, List, notification, Spin, Tag, Tooltip, Grid} from "antd";
import Link from 'next/link';
import {HeartFilled, HeartOutlined} from "@ant-design/icons";
import moment from "moment";
import ApiService from "../../../services/ApiService";
import Utils from "../../../utils";
import ReactMarkdown from "react-markdown";


const {useBreakpoint} = Grid;

const QuestionList = props => {
  const {questions, totalResults, loadQuestions, loading, setQuestions, currentPage, setCurrentPage} = props;
  const [loadingData, setLoadingData] = useState(false);
  const isMobile = !Utils.getBreakPoint(useBreakpoint()).includes('md')
  return (
    <Spin
      spinning={loading}
      tip={Utils.translate("label.loading.question")}
    >
      <List
        pagination={{
          current: currentPage,
          pageSize: 10, total: totalResults, onChange: (page) => {
            window.scrollTo(top);
            setCurrentPage(page);
            loadQuestions(page).then(_ => {
            });
          }
        }}
        split={false}
        itemLayout="horizontal"
        dataSource={questions}
        renderItem={item => (
          <>
            <List.Item className="px-0 d-block">
              <div className="question-list">
                <div className="question-content w-100">
                  <div className="mb-2">
                    <h2>
                      <a href={`/forum/question/${item.slug}`}>
                        <h4 style={{fontWeight: 600}}>{Utils.decodeHtml(item?.title ?? '')}</h4>
                      </a>
                    </h2>
                  </div>
                  <div className="question-description mb-3">
                    <p>
                      <ReactMarkdown>
                        {item?.content ?? ''}
                      </ReactMarkdown>
                    </p>
                  </div>
                  <div className='mb-3'>
                    {item?.tags.map(x => {
                      return (
                        <Tag className='mb-2' key={x.title} color="blue">{x.title}</Tag>
                      )
                    })}
                  </div>
                  <div className="question-footer d-flex justify-content-between align-items-center flex-wrap">
                    <div className="author d-flex align-items-center mt-3" style={isMobile ? {order:2} : {}}>
                      <div className="author-avatar mr-2">
                        <Link href={'/profile/' + item?.author?.username ?? ''}>
                          <a>
                            <Avatar src={item?.author?.avatar ?? ''} shape="square"/>
                          </a>
                        </Link>
                      </div>
                      <div className="author-information d-flex flex-column">
                        <Link href={'/profile/' + item?.author?.username ?? ''}>
                          <a>
                            <span className="m-0"
                                  style={{fontWeight: 600, color: '#1a3353'}}>{item?.author?.displayName ?? ''}</span>
                          </a>
                        </Link>
                        <span className="m-0 font-size-sm font-weight-semibold"
                              style={{
                                fontSize: ".8rem",
                                opacity: .8
                              }}>{item.createdAt ? moment(item.createdAt).format('MMMM Do YYYY') : ''}</span>
                      </div>
                    </div>
                    <div style={{order: 1}}>
                      <Link href={'/forum/question/' + item.slug}>
                        <a>
                          <Button
                            type="default"
                            size="small"
                            className="mr-2"
                            style={{borderStyle: "dashed"}}
                          >
                            {item.commentCount} {Utils.translate("label.answer.question")}
                          </Button>
                        </a>
                      </Link>
                      {
                        <Tooltip
                          title={!item.isSaved ? Utils.translate("label.saved.question") :  Utils.translate("label.un.saved.question")}
                        >
                          <Button
                            type="default"
                            size="small"
                            className="mr-2"
                            style={{borderStyle: "dashed", color: item.isSaved ? '#3e79f7' : ''}}
                            onClick={async () => {
                              try {
                                if (loadingData) return;
                                setLoadingData(true);
                                setQuestions(state => {
                                  return state.map(x => {
                                    if (x._id === item._id) return {...x, isSaved: !item.isSaved};
                                    else return x;
                                  })
                                })
                                const res = await ApiService.saveQuestion(item.slug);
                                const {message} = res.data;
                                notification.success({message})
                              } catch (err) {
                                setQuestions(state => {
                                  return state.map(x => {
                                    if (x._id === item._id) return {...x, isSaved: item.isSaved};
                                    else return x;
                                  })
                                })
                                console.log(err);
                              }
                              setLoadingData(false);
                            }}
                          >
                            {item.isSaved ? Utils.translate("label.saved") : Utils.translate("label.save")}
                          </Button>
                        </Tooltip>
                      }
                      {
                        item.userReaction
                          ?
                          <Tooltip
                            title={Utils.translate("label.un.like")}
                          >
                            <Button
                              onClick={async () => {
                                try {
                                  setQuestions(state => {
                                    return state.map(x => {
                                      if (x._id === item._id) {
                                        return {...x, userReaction: false};
                                      } else return x;
                                    });
                                  })
                                  await ApiService.react(item._id, {model: 'CommunityQuestion', delete: true});
                                } catch (err) {
                                  setQuestions(state => {
                                    return state.map(x => {
                                      if (x._id === item._id) {
                                        return {...x, userReaction: 'love'};
                                      } else return x;
                                    });
                                  })
                                }
                              }}
                              icon={
                                <HeartFilled
                                  style={{color: 'red'}}
                                />
                              }
                              size="small"/>
                          </Tooltip>
                          :
                          <Tooltip
                            title={Utils.translate("label.like")}
                          >
                            <Button
                              onClick={async () => {
                                try {
                                  setQuestions(state => {
                                    return state.map(x => {
                                      if (x._id === item._id) {
                                        return {...x, userReaction: 'love'};
                                      } else return x;
                                    });
                                  })
                                  await ApiService.react(item._id, {
                                    model: 'CommunityQuestion',
                                    active: 'true',
                                    type: 'love'
                                  });
                                } catch (err) {
                                  setQuestions(state => {
                                    return state.map(x => {
                                      if (x._id === item._id) {
                                        return {...x, userReaction: false};
                                      } else return x;
                                    });
                                  })
                                }
                              }}
                              icon={<HeartOutlined/>}
                              size="small"
                            />
                          </Tooltip>
                      }
                    </div>
                  </div>
                </div>
              </div>
            </List.Item>
            <Divider dashed/>
          </>
        )}
      />
    </Spin>
  )
}
export default QuestionList;
