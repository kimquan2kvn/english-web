import React, {useEffect, useRef, useState} from 'react';
import {Card, Button, Menu, Typography, Divider, Tooltip, notification, Modal} from "antd";
import {
  CaretDownOutlined,
  CaretUpOutlined, FlagOutlined, LoadingOutlined, SendOutlined
} from "@ant-design/icons";
import Link from 'next/link';
import moment from 'moment';
import EllipsisDropdown from "../../shared-components/EllipsisDropdown";
import ReactMarkdown from 'react-markdown';
import Editor from "../../util-components/Editor";
import ApiService from "../../../services/ApiService";
import Utils from "../../../utils";
import {useSelector} from "react-redux";

const dataFake = [
  {
    "_id": "6215be123ffc58967a727",
    "author": {
      "_id": "61c188ebc1b723da1d2d0779",
      "username": "admin",
      "email": "admin@default.vn",
      "fullName": "admin",
      "displayName": "Hello bạn nhỏ",
      "avatar": "https://storage.googleapis.com/download/storage/v1/b/english-or-foolish/o/89c9fa0bddc24a6cf19a58b00_1644655441595_57734290.jpeg?generation=1644655442196092&alt=media"
    },
    "content": "# fadsfas \n ![image](https://pngquant.org/Ducati_side_shadow.png)",
    "level": 1,
    "category": "62076846f6c5c7e1bafe4bad",
    "createdAt": 1645592082164,
    "reactions": {},
    "subCommentCount": 0,
    "reactionCount": 0,
    "userReaction": false
  },
  {
    "_id": "6215be123ffc5896750728",
    "author": {
      "_id": "61c188ebc1b723da1d2d0779",
      "username": "admin",
      "email": "admin@default.vn",
      "fullName": "admin",
      "displayName": "Hello bạn nhỏ",
      "avatar": "https://storage.googleapis.com/download/storage/v1/b/english-or-foolish/o/89c9fa0bddc24a6cf19a58b00_1644655441595_57734290.jpeg?generation=1644655442196092&alt=media"
    },
    "content": "# fadsfas \n ![image](https://pngquant.org/Ducati_side_shadow.png)",
    "level": 1,
    "category": "62076846f6c5c7e1bafe4bad",
    "createdAt": 1645592082164,
    "reactions": {},
    "subCommentCount": 0,
    "reactionCount": 0,
    "userReaction": false
  }
]


const VoteOption = ({userReaction, reactions, commentId}) => {
  const [vote, setVote] = useState(getVote(reactions));
  const [myReaction, setMyReaction] = useState(userReaction);

  function getVote(reactions) {
    const hasUseful = reactions.hasOwnProperty('useful');
    const hasNotUseful = reactions.hasOwnProperty('notUseful');
    if (hasUseful && hasNotUseful) {
      return reactions.useful - reactions.notUseful;
    } else if (hasUseful && !hasNotUseful) {
      return reactions.useful;
    } else if (hasNotUseful && !hasUseful) {
      return -reactions.notUseful;
    } else {
      return 0
    }
  }

  const handleVote = async (reaction) => {
    try {
      if (reaction === 'useful') {
        setVote(state => {
          const count = myReaction === 'notUseful' ? 2 : 1;
          return state + count;
        })
      } else {
        setVote(state => {
          const count = myReaction === 'useful' ? 2 : 1;
          return state - count;
        })
      }
      setMyReaction(reaction);
      await ApiService.react(commentId, {
        model: 'Comment',
        type: reaction,
        active: 'true',
        reactTo: 'CommunityQuestion'
      });
    } catch (err) {
      setMyReaction(myReaction);
      setVote(vote);
    }
  }

  const handleCancelVote = async () => {
    try {
      if (myReaction === 'useful') {
        setMyReaction(false);
        setVote(state => state - 1);
      } else {
        setMyReaction(false);
        setVote(state => state + 1);
      }
      await ApiService.react(commentId, {model: 'Comment', delete: true});
    } catch (err) {
      setMyReaction(myReaction);
      setVote(vote);
    }
  }

  return (
    <div
      className='d-flex flex-column align-items-center'
    >
      <Tooltip
        title='This answer is useful'
      >
        <Button
          size='small'
          shape='circle'
          style={{border: 'none'}}
          icon={<CaretUpOutlined
            style={{fontSize: 18, color: myReaction === 'useful' ? '#3e79f7' : 'rgba(0, 0, 0, 0.5)'}}/>}
          onClick={() => {
            if (myReaction === 'useful') {
              handleCancelVote().then(_ => {
              });
            } else {
              handleVote('useful').then(_ => {
              });
            }
          }}
        />
      </Tooltip>
      <div
        className='font-weight-semibold'
        style={{
          fontSize: 16
        }}
      >
        {
          vote
        }
      </div>
      <Tooltip
        title='This answer is not useful'
      >
        <Button
          size='small'
          shape='circle'
          style={{border: 'none'}}
          icon={<CaretDownOutlined
            style={{fontSize: 18, color: myReaction === 'notUseful' ? '#3e79f7' : 'rgba(0, 0, 0, 0.5)'}}/>}
          onClick={() => {
            if (myReaction === 'notUseful') {
              handleCancelVote().then(_ => {
              });
            } else {
              handleVote('notUseful').then(_ => {
              });
            }
          }}
        />
      </Tooltip>
    </div>
  )
}

const AnswerItemReply = ({setTotalReply, data, setOpenItem, setCurrentAnswerReply, setReplyAnswerData}) => {
  const [open, setOpen] = useState(true);
  const user = useSelector(state => state.user);

  const onDeleteAnswer = async () => {
    try {
      const res = await ApiService.deleteComment(data._id, {model: 'CommunityQuestion'})
      const { comment } = res.data;
      setReplyAnswerData(state => state.filter(x => x._id !== comment._id));
      notification.success({message: 'Delete answer successfully'});
      setTotalReply(state => state - 1);
    }
    catch(err) {
      console.log(err);
      notification.error({message: 'Failed to delete answer'});
    }
  }

  return (
    <Card>
      <div className='d-flex'>
        <div style={{width: 36}}>
          {
            open &&
            <VoteOption
              commentId={data._id}
              reactions={data.reactions}
              userReaction={data.userReaction}
            />
          }
        </div>
        <div className='px-2 flex-grow-1'>
          <div className='d-flex justify-content-between'>
            <div className='d-flex align-items-center'>
              <Typography.Text
                style={{fontWeight: 600}}
                className='cursor-pointer mr-2'
                onClick={() => {
                  if (!open) {
                    setOpen(true);
                    setOpenItem(data._id);
                    setCurrentAnswerReply('');
                  } else {
                    setOpen(false);
                    setOpenItem('');
                    setCurrentAnswerReply('');
                  }
                }}
              >
                [{
                open ? '-' : '+'
              }]
              </Typography.Text>
              <Link href={''}>
                <a className='mr-2'>
                  <Typography.Text
                    style={{fontWeight: 600}}
                  >
                    {data?.author?.displayName ?? ''}
                  </Typography.Text>
                </a>
              </Link>
              <span>
            {moment(data?.createdAt).fromNow()}
          </span>
            </div>
            <div>
              <EllipsisDropdown
                menu={
                  <Menu>
                    {
                      user._id === data.author._id &&
                      <Menu.Item
                        onClick={() => {
                          Modal.confirm({
                            title: 'Are you sure delete answer?',
                            onOk: () => {
                              onDeleteAnswer().then(_ => {
                              });
                            },
                          })
                        }}
                      >
                        Delete
                      </Menu.Item>
                    }
                    <Menu.Item>
                      Report
                    </Menu.Item>
                  </Menu>
                }
              />
            </div>
          </div>
          {
            open &&
            <>
              <div className='markdown-wrap'>
                <ReactMarkdown>
                  {data?.content ?? ''}
                </ReactMarkdown>
              </div>
              <Button
                className='pl-0'
                type='link'
                icon={<SendOutlined/>}
                onClick={() => setCurrentAnswerReply(data._id)}
              >
                reply
              </Button>
            </>
          }
        </div>
      </div>

    </Card>
  )
}

const InputCommentReply = ({
                             refEditor,
                             questionId,
                             replyFor,
                             setReplyAnswerData,
                             setCurrentAnswerReply,
                             setTotalReply
                           }) => {
  const [loading, setLoading] = useState(false);
  const onSubmitComment = async () => {
    setLoading(true);
    const content = refEditor.current.getInstance().getMarkdown().replaceAll(/(<br\s*\/*>|\n)/gi, "\n\n");
    try {
      const res = await Utils.comment({mainId: questionId, content, replyFor, model: 'CommunityQuestion'});
      const {comment} = res;
      setReplyAnswerData(state => {
        return [
          {
            ...comment,
            reactionCount: 0,
            reactions: {},
            subCommentCount: 0,
            userReaction: false
          },
          ...state,
        ]
      })
      setTotalReply(state => state + 1);
    } catch (err) {
      notification.error({message: 'Failed to comment'})
    }
    setLoading(false);
    setCurrentAnswerReply('');
  }

  return (
    <div className='mx-auto'>
      <Editor
        refEditor={refEditor}
        type='answer'
      />
      <div className='d-flex justify-content-end mb-2'>
        <Button
          loading={loading}
          type={'primary'}
          size='small'
          onClick={onSubmitComment}
        >
          Submit
        </Button>
      </div>
    </div>
  )
}

const AnswerItem = (props) => {
  const {data, questionId, setAnswer} = props;
  const user = useSelector(state => state.user);
  const [showInfo, setShowInfo] = useState(true);
  const refEditor = useRef(null);
  const [nextPage, setNextPage] = useState(1);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [currentAnswerReply, setCurrentAnswerReply] = useState('');
  const [openItem, setOpenItem] = useState('');
  const [loadMoreReply, setLoadMoreReply] = useState(false);
  const [replyAnswerData, setReplyAnswerData] = useState([]);
  const [totalReply, setTotalReply] = useState(data.subCommentCount);

  const loadMoreReplyAnswer = async () => {
    setLoadMoreReply(true);
    try {
      const res = await ApiService.loadComment({replyFor: data._id, limit: 5, page: nextPage, sortBy: '-createdAt'});
      const {results, page, totalPages} = res.data;
      if (nextPage === 1) {
        setReplyAnswerData(results);
      } else setReplyAnswerData(state => [...state, ...results]);
      setNextPage(page + 1);
      setHasNextPage(page < totalPages);
    } catch (err) {

    }
    setLoadMoreReply(false);
  }

  const onDeleteAnswer = async () => {
    try {
      const res = await ApiService.deleteComment(data._id, {model: 'CommunityQuestion'})
      const {comment} = res.data;
      setAnswer(state => state.filter(x => x._id !== comment._id));
      notification.success({message: 'Delete answer successfully'});
    } catch (err) {
      console.log(err);
      notification.error({message: 'Failed to delete answer'});
    }
  }

  const handleReport = async () => {
    try {
      const res = await ApiService
    } catch (err) {

    }
  }

  return (
    <Card>
      <div className='d-flex flex-row'>
        <div style={{width: 36}}>
          {
            showInfo &&
            <VoteOption
              commentId={data._id}
              reactions={data.reactions}
              userReaction={data.userReaction}
            />
          }
        </div>
        <div className='flex-grow-1 mw-100'>
          <div className='w-100 px-2'>
            <div className='d-flex flex-row flex-grow-1 justify-content-between pr-2 py-2'
                 style={{marginBottom: showInfo ? 10 : 0}}>
              <div className='d-flex flex-grow-1'>
                <a href='#' className='pr-2' onClick={e => {
                  e.stopPropagation();
                  e.preventDefault();
                  setShowInfo(state => !state);
                  setCurrentAnswerReply('');
                  setOpenItem('');
                }}>
                  <Typography.Text
                    style={{fontWeight: 600}}
                  >
                    [{showInfo ? '-' : '+'}]
                  </Typography.Text>
                </a>

                <Link href={''}>
                  <a className='mr-2'>
                    <Typography.Text
                      style={{fontWeight: 600}}
                    >
                      {data?.author?.displayName ?? ''}
                    </Typography.Text>
                  </a>
                </Link>
                <span>
                {moment(data?.createdAt).fromNow()}
              </span>
              </div>
              <div className={"flex-grow-0"} style={{
                width: 40,
                position: 'relative'
              }}>
                <div style={{
                  position: 'absolute',
                  right: 10,
                  top: -4
                }}>
                  <EllipsisDropdown
                    menu={
                      <Menu>
                        {
                          user._id === data.author._id &&
                          <Menu.Item
                            onClick={() => {
                              Modal.confirm({
                                title: 'Are you sure delete answer?',
                                onOk: () => {
                                  onDeleteAnswer().then(_ => {
                                  });
                                },
                              })
                            }}
                          >
                            Delete
                          </Menu.Item>
                        }
                        <Menu.Item
                          onClick={handleReport}
                        >
                          Report
                        </Menu.Item>
                      </Menu>
                    }
                  />
                </div>
              </div>
            </div>
            {
              showInfo &&
              <div style={{width: '100%'}}>
                <div className='markdown-wrap'>
                  <ReactMarkdown>
                    {data?.content ?? ''}
                  </ReactMarkdown>
                </div>
                <div>
                  {
                    currentAnswerReply !== data._id &&
                    <Button
                      type='link'
                      icon={<SendOutlined/>}
                      onClick={() => setCurrentAnswerReply(data._id)}
                      className='pl-0'
                    >
                      reply
                    </Button>
                  }
                </div>
                {/*{*/}
                {/*  showInputComment &&*/}
                {/*  <div className='mx-auto'>*/}
                {/*    <Editor*/}
                {/*      refEditor={refEditor}*/}
                {/*      type='answer'*/}
                {/*    />*/}
                {/*    <div>*/}
                {/*      <Button*/}
                {/*        className='float-right'*/}
                {/*        type={'primary'}*/}
                {/*        size='small'*/}
                {/*        onClick={onSubmitComment}*/}
                {/*      >*/}
                {/*        Submit*/}
                {/*      </Button>*/}
                {/*    </div>*/}
                {/*  </div>*/}
                {/*}*/}
                {
                  currentAnswerReply === data._id &&
                  <InputCommentReply
                    setCurrentAnswerReply={setCurrentAnswerReply}
                    refEditor={refEditor}
                    questionId={questionId}
                    replyFor={data._id}
                    setTotalReply={setTotalReply}
                    setReplyAnswerData={setReplyAnswerData}
                  />
                }
                <div>
                  {
                    replyAnswerData.map((item, i) => {
                      if (currentAnswerReply === item._id) {
                        return (
                          <>
                            <AnswerItemReply
                              setTotalReply={setTotalReply}
                              data={item} openItem={openItem}
                              etOpenItem={setOpenItem}
                              setCurrentAnswerReply={setCurrentAnswerReply}
                              setReplyAnswerData={setReplyAnswerData}
                            />
                            <InputCommentReply
                              setCurrentAnswerReply={setCurrentAnswerReply}
                              refEditor={refEditor}
                              questionId={questionId}
                              replyFor={data._id}
                              setTotalReply={setTotalReply}
                              setReplyAnswerData={setReplyAnswerData}
                            />
                          </>
                        )
                      } else return <AnswerItemReply
                        setTotalReply={setTotalReply}
                        data={item} openItem={openItem}
                        setOpenItem={setOpenItem}
                        setCurrentAnswerReply={setCurrentAnswerReply}
                        setReplyAnswerData={setReplyAnswerData}
                      />
                    })
                  }
                </div>
                <div className='d-flex justify-content-between mt-2'>
                  <div>
                    {
                      data.subCommentCount > 0 && hasNextPage &&
                      <span
                        className='under-line cursor-pointer'
                        onClick={() => {
                          loadMoreReplyAnswer().then(_ => {
                          });
                        }}
                      >
                      {
                        loadMoreReply && <LoadingOutlined className='mr-2'/>
                      }
                        View more comment(s)
                    </span>
                    }
                  </div>
                  {
                    replyAnswerData.length > 0 &&
                    <span>
                      {replyAnswerData.length} / {totalReply}
                    </span>
                  }
                </div>
              </div>
            }
          </div>
        </div>
      </div>
    </Card>
  )
}

export default AnswerItem;