import React from "react";
import {Button, Card, Divider, Form, Input, Select, Switch} from "antd";
import QuestionForm from "common/components/forum-components/question-form";

const AddQuestion = props => {
  return (
    <div className="add-question">
      <Card bordered={false}>
        <QuestionForm/>
      </Card>
    </div>
  );
}
export default AddQuestion;
