import React, {useCallback, useEffect} from "react";
import CustomModal from "../../util-components/CustomModal";
import {Button, Result} from "antd";
import {setLoadingState} from "../../../redux/actions/Exam";
import {useDispatch} from "react-redux";

const Countdown = ({duration, isSubmit, paused, submitExam, warning, setWarning}) => {
  const [over, setOver] = React.useState(false);
  const [time, setTime] = React.useState({
    hours: parseInt(duration['hours'], 10),
    minutes: parseInt(duration["minutes"], 10),
    seconds: parseInt(duration["seconds"], 10)
  });
  const dispatch = useDispatch();

  const tick = useCallback(() => {
    if (paused || over) return;

    // Time up
    if (time.hours === 0 && time.minutes === 0 && time.seconds === 0) {
      setOver(true);
      setWarning(false)
      dispatch(setLoadingState(true))
      submitExam({
        hours: 0,
        minutes: 0,
        seconds: 0,
      })
      setTime({
        hours: 0,
        minutes: 0,
        seconds: 0,
      });
    } else if (time.minutes === 0 && time.seconds === 0) {
      setTime({
        hours: time.hours - 1,
        minutes: 59,
        seconds: 59
      });
    } else if (time.seconds === 0) {
      setTime({
        hours: time.hours,
        minutes: time.minutes - 1,
        seconds: 59
      });
    } else {
      setTime({
        hours: time.hours,
        minutes: time.minutes,
        seconds: time.seconds - 1
      });
    }
  }, [time])


  useEffect(() => {
    let timerID = setInterval(() => tick(), 1000);
    if (time.hours === 0 && time.minutes === 1 && time.seconds === 0) {
      setWarning(true)
    }
    return () => clearInterval(timerID);
  });

  useEffect(() => {
    if (isSubmit) {
      submitExam(time)
    }
  }, [isSubmit])


  return (
    <div>
      <h1>{`${time.hours
        .toString()
        .padStart(2, '0')}:${time.minutes
        .toString()
        .padStart(2, '0')}:${time.seconds.toString().padStart(2, '0')}`}</h1>
      <div>{over ? "Time's up!" : ''}</div>
      <CustomModal visible={warning} footer={null} width={500}>
        <Result
          status="warning"
          title="You only have 10 minutes left to finish the exam!"
          extra={
            <div>
              <Button
                className="mr-2"
                type="primary" key="console"
                onClick={() => {
                  setWarning(false)
                }}>
                Continue
              </Button>
              <Button
                type="default" key="console"
                onClick={() => submitExam(time)}
              >
                Submit exam
              </Button>
            </div>
          }
        />
      </CustomModal>
    </div>
  );
}
export default React.memo(Countdown);
