import React from "react";
import {Card, Progress} from "antd";

const ResultChart = ({result}) => {

  return (
    <Card type="inner" title="Result">
      <div style={{textAlign: "center"}}>
        <Progress
          className="mb-3"
          width={80} strokeLinecap="square" type="circle"
          percent={~~((result?.nCorrect / result?.exam?.totalQuestions) * 100)}/>
        <div>
          <h5>{result?.nCorrect} / {result?.exam?.totalQuestions} correct question(s) </h5>
          <p>You finished the exam in {result?.duration > 60 ? ~~(result.duration/60) + " minute(s)" : result?.duration + " second(s)"}</p>
        </div>
      </div>
    </Card>
  )
}
export default ResultChart
