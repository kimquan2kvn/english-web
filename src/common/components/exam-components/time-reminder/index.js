import React, {useCallback, useState} from "react";
import Icon from "@ant-design/icons";
import {reminderTime} from "../../../assets/svg/icon";
import {Button, Card, Modal} from "antd";
import {useDispatch} from "react-redux";
import Countdown from "../countdown";
import Utils from "../../../utils";
import {setLoadingState} from "../../../redux/actions/Exam";


const TimeReminder = ({exam, submitExam, setIsSubmit, isSubmit, warning, setWarning}) => {
  const [paused, setPaused] = useState(false);
  const dispatch = useDispatch()

  const onSubmit = () => {
    setPaused(true)
    Modal.confirm({
      title: 'Do you want to submit this exam?',
      content: "Click OK if you are sure you want to submit. This action can not undo.",
      async onOk() {
        setIsSubmit(true)
        dispatch(setLoadingState(true))
      },
      onCancel() {
        setPaused(false)
      },
    });
  }

  const _renderTimeReminder = useCallback(() => {
    return (
      <Countdown
        duration={Utils.countDownTime(exam.duration)}
        isSubmit={isSubmit} setPaused={setPaused} paused={paused}
        submitExam={submitExam}
        warning={warning}
        setWarning={setWarning}
      />
    )
  },[])

  return (
    <Card type="inner" title="Time reminder">
      <div className="text-center">
        <Icon component={reminderTime} style={{fontSize: 48}} className="mb-2"/>
        {/*{_renderTimeReminder()} */}
      </div>
      <Button type="primary" block onClick={onSubmit}>Submit</Button>
    </Card>
  )
}
export default React.memo(TimeReminder);
