import React from "react";
import Link from "next/link";
import {Button, Col, Row} from "antd";
import Utils from "../../../utils";

const ReportQuestionContent = ({data, more}) => {
  return (
    <div>
      {more && (
        <div>
          <span className="font-weight-semibold">Question {data.id}</span>
          <Link href={`/manage/question/${data._id}/view`} passHref>
            <Button type="link">View more</Button>
          </Link>
        </div>
      )}
      <div>
        <div className="mb-3" dangerouslySetInnerHTML={{__html: data.data?.question}}>
        </div>
        <div className="answer">
          {!(data.parent === 0 && (["doc_hieu", "dien_tu"].includes(data.type))) && (
            <Row gutter={16}>
              {data.data.answers.map((ans, index) => {
                return (
                  <Col span={6} key={index}>
                    <div dangerouslySetInnerHTML={{__html: `${index + 1}. ${Utils.removePTag(ans)}`}}>
                    </div>
                  </Col>
                )
              })}
            </Row>
          )}
        </div>
      </div>
    </div>
  )
}
export default ReportQuestionContent
