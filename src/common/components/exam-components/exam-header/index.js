import React from 'react'
import {Button, Divider, Tag, Tooltip, Grid} from "antd";
import {CalendarOutlined, ClockCircleOutlined, DownloadOutlined, QuestionCircleOutlined} from "@ant-design/icons";
import moment from "moment";
import Utils from "../../../utils";

const {useBreakpoint} = Grid
const ExamHeader = ({exam}) => {
  const isMobile = !Utils.getBreakPoint(useBreakpoint()).includes("sm")
  return (
    <div>
      <div className="d-flex justify-content-between mb-2">
        <h2>{exam?.name}</h2>
        <Tooltip title="Download exam">
          <Button size="small" type="primary" ghost icon={<DownloadOutlined/>}/>
        </Tooltip>
      </div>
        <div className="d-flex">
          <Tooltip title="Total questions" className="mr-3">
            <Tag className="bg-gray-lightest">
              <QuestionCircleOutlined/>
              <span className="ml-1">{exam.totalQuestions ? `${exam.totalQuestions} questions` : "No data"} </span>
            </Tag>
          </Tooltip>
          <Tooltip title="Duration" className="mr-3">
            <Tag className="bg-gray-lightest">
              <ClockCircleOutlined/>
              <span
                className="ml-1">{exam.duration ? moment.utc(moment.duration(parseInt(exam?.duration), "seconds").asMilliseconds()).format("HH:mm:ss") : "No data"}</span>
            </Tag>
          </Tooltip>
          {exam.startDate && (
            <Tooltip title="Start date">
              <Tag className="bg-gray-lightest">
                <CalendarOutlined/>
                <span className="ml-1">{moment(parseInt(exam["startDate"])).format("ll")}</span>
              </Tag>
            </Tooltip>
          )}
        </div>
      <Divider dashed/>
    </div>
  )
}
export default ExamHeader;
