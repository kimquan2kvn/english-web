import React from "react";
import {Avatar, Col, Form, Row, Input, Button, notification} from "antd";
import {ROW_GUTTER} from "../../../constants/ThemeConstant";
import moment from "moment"
import ApiService from "../../../services/ApiService";

const ReportResultCard = ({selectedResult, onCancel, onReport}) => {
  const [form] = Form.useForm();

  const onFinish = async (values) => {
    try {
      const res = await ApiService.addIssue({
        ...values,
        target: selectedResult._id,
        title: `Report result of ${selectedResult?.user?.username}`,
        model: "Result"
      })
      if (res.status === 200) {
        form.resetFields();
        onCancel();
        notification.success({
          message: res.data.message
        })
      }
    } catch (err) {
      console.log(err)
    }
  }

  return (
    <Row gutter={ROW_GUTTER}>
      <Col span={24} className="mb-3">
        <Row gutter={16} className="align-items-center">
          <Col xs={24} sm={8} md={8} lg={8} className="mb-3 user-info d-flex flex-column align-items-center">
            <Avatar src={selectedResult.user.avatar} size={70} className={"mb-1"}/>
            <span>{selectedResult.user.fullName}</span>
          </Col>
          <Col xs={24} sm={16} md={16} lg={16}>
            <div className="result-rank d-flex flex-column align-content-center">
                <span style={{
                  fontWeight: 600,
                  marginBottom: 4
                }}>Result Information</span>
              <div className="exam-result">
                <Row gutter={16}>
                  <Col xs={12} md={12}>
                    <p>Exam: {selectedResult.exam.name}</p>
                    <p
                    >Date: {moment(parseInt(selectedResult.submittedAt)).format("YYYY/MM/DD")} </p>
                  </Col>
                  <Col xs={12} sm={12} md={12}>
                    <p>Correct: {selectedResult.nCorrect}/{selectedResult?.exam?.totalQuestions}</p>
                    <p
                    >Duration: {moment().startOf('day').seconds(selectedResult.duration).format("HH:mm:ss")}</p>
                  </Col>
                </Row>
              </div>
            </div>
          </Col>
        </Row>
      </Col>
      <Col span={24}>
        <Form
          layout="vertical"
          onFinish={onFinish}
          form={form}
        >
          <Form.Item
            name="content"
            label="Result Issue"
            rules={[{required: true, message: 'Please provide issue of this result!'}]}
          >
            <Input.TextArea placeholder="Please input description about the issue!"/>
          </Form.Item>
          <div style={{
            textAlign: "right"
          }}
          >
            <Button onClick={onCancel} style={{
              marginRight: "8px"
            }}>Cancel</Button>
            <Button type="primary" htmlType="submit" danger>Report</Button>
          </div>
        </Form>
      </Col>
    </Row>
  )
}
export default ReportResultCard;
