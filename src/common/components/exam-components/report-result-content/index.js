import React from "react";
import {Avatar, Button, Col, Row} from "antd";
import moment from "moment";
import Link from "next/link";

const ReportResultContent = ({data, more}) => {
  return (
    <div>
      <div>
        <div className="mb-3">
          {more && (
            <div>
              <span className="font-weight-semibold">Information of reported</span>
              <Link href={`/exam/${data?.exam?.code}/ranking`} passHref>
                <Button type="link">View more</Button>
              </Link>
            </div>
          )}
        </div>
        <div>
          <Row gutter={16} className="align-items-center">
            <Col xs={24} sm={8} md={8} lg={8} className="mb-3 user-info d-flex flex-column align-items-center">
              <Avatar src={data.user?.avatar} size={70} className={"mb-1"}/>
              <span>{data.user?.fullName}</span>
            </Col>
            <Col xs={24} sm={16} md={16} lg={16}>
              <div className="result-rank d-flex flex-column align-content-center">
                <span style={{fontWeight: 600, marginBottom: 4}}>Result Information</span>
                <div className="exam-result">
                  <Row gutter={16}>
                    <Col xs={12} md={12}>
                      <p>Exam: {data.exam.name}</p>
                      <p>Date: {moment(parseInt(data.submittedAt)).format("YYYY/MM/DD")} </p>
                    </Col>
                    <Col xs={12} sm={12} md={12}>
                      <p>Correct: {data.nCorrect}/{data.exam?.totalQuestions}</p>
                      <p>Duration: {moment().startOf('day').seconds(data.duration).format("HH:mm:ss")}</p>
                    </Col>
                  </Row>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  )
}
export default ReportResultContent;
