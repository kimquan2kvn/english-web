import React from "react";
import {Card} from "antd";
import QuestionCard from "../question-card";
import {Element} from 'react-scroll'
import ExamHeader from "../exam-header";

const TAKE = "TAKE"
const RESULT = "RESULT"
const ExamContent = ({data, answersData = [], mode = TAKE}) => {
  const {exam = {}, questions = []} = data
  let counter = 0;
  let sequenceCounter = 0;
  return (
    <Card>
      <ExamHeader exam={exam}/>
      <div>
        {questions.map((question, index) => {
          let isParagraphQuestion = false;
          let sectionTitle;
          let titleText = "";
          let result;
          if (!(["doc_hieu", "dien_tu"].includes(question?.type) &&question?.parent === 0)) {
            counter++;
            sequenceCounter++;
          } else {
            isParagraphQuestion = true;
            sequenceCounter = 0;
          }

          if (index === 0 ||question?.type !== questions[index - 1].type) {
            switch (question?.type) {
              case "phat_am":
                titleText = "Mark the letter A, B, C, or D on your answer sheet to indicate the word whose underlined part differs from the other three in pronunciation in each of the following questions.";
                break;
              case "trong_am":
                titleText = "Mark the letter A, B, c, or D on your answer sheet to indicate the word that differs from the other three in the position of primary stress in each of the following questions.";
                break;
              case "dong_nghia":
                titleText = "Mark the letter A, B, c, or D on your answer sheet to indicate the word(s) CLOSEST in meaning to the underlined word(s) in each of the following questions.";
                break;
              case "trai_nghia":
                titleText = "Mark the letter A, B, c, or D on your answer sheet to indicate the word(s) OPPOSITE in meaning to the underlined word(s) in each of the following questions.";
                break;
              case "ngu_phap":
                titleText = "Mark the letter A, B, C, or D on your answer sheet to indicate the correct answer to each of the following questions.";
                break;
              case "giao_tiep":
                titleText = "Mark the letter A, B, c, or D on your answer sheet to indicate the option that best completes each of the following exchanges.";
                break;
              case "dien_tu":
                titleText = "Read the following passage and mark the letter A, B, C, or D on your answer sheet to indicate the correct word or phrase that best fits each of the numbered blanks.";
                break;
              case "doc_hieu":
                titleText = "Read the following passage and mark the letter A, B, C, or D on your answer sheet to indicate the correct answer to each of the below questions.";
                break;
              case "tim_loi_sai":
                titleText = "Mark the letter A, B, c or D on your answer sheet to indicate the underlined part that needs correction ỉn each of the following questions.";
                break;
            }
          }
          sectionTitle = (
            <div style={{marginBottom: 20, marginTop: 20}}>
              <b>{titleText}</b>
            </div>
          )

          let resultForOneQuestion = answersData.find(a => a.question ===question?._id);
          return (
            <div key={question?._id}>
              {sectionTitle}
              <Element name={`question${index + 1}`} style={{
                marginBottom: '20px'
              }}>
                <QuestionCard
                  question={question} id={question?.id} type={question?.type}
                  questionIndex={counter}
                  overrideQuestion={!isParagraphQuestion &&question?.type === "dien_tu"}
                  overrideContent={`(${sequenceCounter})`}
                  isParagraphQuestion={isParagraphQuestion}
                  identifier={question['_id']}
                  result={resultForOneQuestion}
                  mode={mode}
                />
              </Element>
            </div>
          )
        })}
      </div>
    </Card>
  )
}

export default React.memo(ExamContent);
