import React from "react";
import NumberItem from "../number-item";
import {Card} from "antd";
import {Link} from "react-scroll"
import {useSelector} from "react-redux";

const RESULT = "RESULT";
const DO = "DO"
const ListQuestion = ({questions, examId, mode = DO, answersData = [], setShowListQuestion}) => {
  const {questionStateMap} = useSelector(state => state['exam'])
  let counter = 0;
  return (
    <Card type="inner" title="List of question">
      <div style={{
        display: "grid",
        gridTemplateColumns: "repeat(auto-fit, minmax(48px, max-content))",
        gridGap: "8px",
        justifyContent: "center",
      }}>
        {questions.map((question, key) => {
          if ((question.type === "doc_hieu" || question.type === "dien_tu") && question.parent === 0)
            return;
          counter++;
          let result = answersData.find(a => a.question === question._id);
          let status;
          if (mode === RESULT) {
            if (result) {
              status = result.correct === result.answer ? "correct" : "wrong"
            }
          } else {
            status = questionStateMap[question._id]
          }
          return (
            <Link
              onClick={() => {
                if(setShowListQuestion) setShowListQuestion(false)
              }}
              activeClass="active"
              to={`question${key + 1}`}
              key={key}
              offset={-105}
              spy={true}
              smooth={true}>
              <NumberItem state={status} number={counter}/>
            </Link>
          )
        })}
      </div>
    </Card>
  )
}
export default ListQuestion;
