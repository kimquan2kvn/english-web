import React from "react";
import {Button, Col, Form, Row, Input, notification} from "antd";
import Utils from "../../../utils";
import ApiService from "../../../services/ApiService";

const {TextArea} = Input;

const ReportQuestionIssueCard = ({renderChoices, questionHTML, question, onCancel}) => {
  const [form] = Form.useForm();

  const onFinish = async (values) => {
    try {
      const res = await ApiService.addIssue({
        ...values,
        target: question._id,
        title: `Report question ${question.id}`,
        model: "Question",
      })
      if (res.status === 200) {
        form.resetFields();
        onCancel();
        notification.success({
          message: res.data.message
        })
      }
    } catch (err) {
      console.log(err)
    }
  }
  return (
    <div>
      <div className="question-data mb-3">
        <div className="question mb-3" style={!renderChoices ? ({
          textAlign: "justify"
        }) : null}>
          <div className="mb-3" dangerouslySetInnerHTML={{__html: questionHTML}}>
          </div>
          <div className="answer">
            {renderChoices && question && (
              <Row gutter={16}>
                {question.data.answers.map((ans, index) => {
                  return (
                    <Col span={6} key={index}>
                      <div dangerouslySetInnerHTML={{__html: `${index + 1}. ${Utils.removePTag(ans)}`}}>
                      </div>
                    </Col>
                  )
                })}
              </Row>
            )}
          </div>
        </div>
        <Form
          layout="vertical"
          onFinish={onFinish}
          form={form}
        >
          <Form.Item
            name="content"
            label="Question Issue"
            rules={[{required: true, message: 'Please provide issue of this question!'}]}
          >
            <TextArea placeholder="Please input description about the issue!"/>
          </Form.Item>
          <div style={{
            textAlign: "right"
          }}
          >
            <Button onClick={onCancel} style={{
              marginRight: "8px"
            }}>Cancel</Button>
            <Button type="primary" htmlType="submit" danger>Report</Button>
          </div>
        </Form>
      </div>
    </div>
  )
}

export default ReportQuestionIssueCard;
