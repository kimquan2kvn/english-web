import React from "react";
import {Avatar, Card, Tag, Tooltip} from "antd";
import {ClockCircleOutlined, HeartOutlined, QuestionCircleOutlined} from "@ant-design/icons";
import AvatarList from "../../util-components/AvatarList";
import Link from "next/link"
import moment from "moment";


const description = "Unfortunately, not really. Illustrator's support for SVG has always been a little shaky, and, having mucked around in Illustrator's internals, I doubt we'll see much improvement as far as Illustrator is concerned."

const ExamCard = props => {
  const {exam} = props;
  return (
    <Card>
      <div className="d-flex align-items-center mb-3">
        <div>
          <div className="cursor-pointer">
            <Link href={`/exam/${exam.code}`} passHref>
              <a>
                <h4 className="mb-0 short-title">{exam?.name ?? "No data"}</h4>
              </a>
            </Link>
          </div>
          <div className="d-flex mt-2">
            <Tooltip title="Total questions" className="mr-2">
              <Tag>
                <QuestionCircleOutlined/>
                <span className="ml-1">{exam?.totalQuestions ?? "No data"}</span>
              </Tag>
            </Tooltip>
            <Tooltip title="Duration">
              <Tag>
                <ClockCircleOutlined/>
                <span
                  className="ml-1">{moment.utc(moment.duration(exam?.duration, 'seconds').asMilliseconds()).format("HH:mm:ss") || "No data"}</span>
              </Tag>
            </Tooltip>
            {exam?.isDoneBefore ? (
              <Tooltip title="Code">
                <Tag color="cyan">
                  <span
                    className="ml-1">Finished</span>
                </Tag>
              </Tooltip>
            ) : (
              <Tooltip title="Code">
                <Tag>
                  <ClockCircleOutlined/>
                  <span
                    className="ml-1">{exam.code}</span>
                </Tag>
              </Tooltip>
            )}
          </div>
        </div>
      </div>
      <div className="mb-2">
        <p className="m-0 short-description">{exam?.description ?? description}</p>
      </div>
      {exam?.topParticipants.length > 0 ? (
        <div className="d-flex justify-content-between align-items-center">
          <div className="d-flex justify-content-end">
            {exam.topParticipants.map((elm, index) => {
              return (
                <Tooltip title="EOF User" key={"topParticpants" + index}>
                  <Link href={'/profile/' + elm.user.username}>
                    <a>
                      <AvatarList member={elm.user} chain className="cursor-pointer" size={28} index={elm}/>
                    </a>
                  </Link>
                </Tooltip>
              )
            })}
            {exam.topParticipants.length > 5 && (
              <Avatar size={28} className="ml-n2 cursor-pointer bg-white border font-size-sm">
                <span className="text-gray-light font-weight-semibold">+{exam.topParticipants.length - 5}</span>
              </Avatar>
            )}
          </div>
          <Tooltip title="Favourite" className="cursor-pointer">
            {/*<HeartOutlined style={{fontSize: 21}}/>*/}
          </Tooltip>
        </div>
      ) : (
        <Tag className="mb-0">No participants yet!</Tag>
      )}
    </Card>
  )
}
export default ExamCard;
