import React, {useState} from "react";
import {Alert, Col, Radio, Row, Tooltip} from "antd";
import {WarningOutlined, FlagFilled, FlagOutlined, WarningFilled} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import Utils from "../../../utils";
import {setQuestionState, setUserAnswer} from "../../../redux/actions/Exam";
import CustomModal from "../../util-components/CustomModal";
import ReportQuestionIssueCard from "../report-question-issue-card";
import ApiService from "../../../services/ApiService";

const style = {
  radioStyle: {
    marginBottom: ".25rem",
  },
}

const RESULT = "RESULT"

const QuestionCard = ({question, overrideQuestion, questionIndex, identifier, overrideContent, mode, result}) => {
  const dispatch = useDispatch();
  const [visible, setVisible] = useState(false);
  const [isReported, setIsReported] = useState(false);
  const {questionStateMap, userAnswers} = useSelector(state => state["exam"])
  const isParagraph = ["doc_hieu", "dien_tu"].includes(question?.type) && question.parent === 0

  let renderChoices = true;
  let questionHTML = "";
  if (!overrideQuestion) {
    if (["doc_hieu", "dien_tu"].includes(question?.type) && question.parent === 0) {
      renderChoices = false;
      questionHTML = `<div style="text-align: justify">${Utils.removeNbsp(question.data.question)}</div>`;
    } else if (["phat_am", "trong_am"].includes(question.type)) {
      questionHTML = `<b>Question ${questionIndex}: </b>`;
    } else {
      questionHTML = `<b>Question ${questionIndex}: </b>` + Utils.removePTag(question.data.question);
    }
  } else {
    questionHTML = `<b>Question ${questionIndex}: </b>` + overrideContent;
  }

  const onChange = (e, identifier) => {
    dispatch(setQuestionState(identifier, "done"))
    let i = e.target.value
    let value = Utils.convertNumberToAnswer(i)
    const  data = {
      question: identifier,
      answer: value
    }
    const newAnswers = [...userAnswers]
    const index = newAnswers.findIndex(answer => answer['question'] === data['question'])
    if (index !== -1) {
      newAnswers[index] = data
    } else {
      newAnswers.push(data)
    }
    dispatch(setUserAnswer(newAnswers))
  }

  const reportQuestion = () => {
    setVisible(true)
  }

  const questionContent = (
      <div style={{
        position: "relative"
      }}>
        <div style={{
          position: "absolute",
          right: 0,
          top: -20
        }}>
          {mode !== RESULT && (
            <Tooltip placement="top" title={"Flag this question"}>
              <a className={"question-controls"} onClick={() => {
                dispatch(setQuestionState(identifier, "flag"))
              }}>
                {questionStateMap[identifier] === "flag" ? (
                  <FlagFilled/>
                ) : (
                  <FlagOutlined/>
                )}
              </a>
            </Tooltip>
          )}
          <Tooltip placement={"top"}
                   title={renderChoices ? `Report this question ${questionIndex}` : 'Report paragraph'}>
            <a className={"question-controls"} style={{marginLeft: 10}} onClick={reportQuestion}>
              {!isReported ? (
                <WarningOutlined/>
              ) : (
                <WarningFilled/>
              )}
            </a>
          </Tooltip>
        </div>
        <div dangerouslySetInnerHTML={{__html: questionHTML}}>
        </div>
      </div>
    )
  ;

  return (
    <>
      <div className="normal-question">
        <div className="question mb-2">
          {questionContent}
        </div>
        <div className="answer mb-2">
          {mode !== RESULT ? (
            <Radio.Group
              style={{width: '100%'}}
              onChange={(e, index) => onChange(e, identifier)}>
              <Row>
                {renderChoices && question.data.answers && question.data.answers.map((answer, index) => {
                  return (
                    <Col xs={24} sm={24} md={12} lg={6} xl={6} key={index}>
                      <Radio style={style.radioStyle} value={index}>
                        <div dangerouslySetInnerHTML={{
                          __html: `${answer}`
                        }}/>
                      </Radio>
                    </Col>
                  )
                })}
              </Row>
            </Radio.Group>
          ) : (
            <div>
              <Radio.Group
                defaultValue={mode === RESULT ? (result ? Utils.convertAnswerToNumber(result.correct) : null) : 0}
                value={result ? Utils.convertAnswerToNumber(result.answer) : null}
                style={{width: '100%',}}
                onChange={(e, index) => onChange(e, identifier)}>
                <Row>
                  {renderChoices && question.data.answers && question.data.answers.map((answer, index) => {
                    return (
                      <Col xs={24} sm={24} md={12} lg={6} xl={6} key={index}>
                        <Radio
                          className={result ? (result.answer === result.correct && Utils.convertAnswerToNumber(result.correct) === index ? "correct" : "wrong") : ''}
                          style={{
                            ...style.radioStyle,
                          }} value={index}>
                          <div
                            className={`${result ? (result.answer !== result.correct && Utils.convertAnswerToNumber(result.correct) === index ? "correct-answer" : "") : ''}`}
                            dangerouslySetInnerHTML={{
                              __html: `${Utils.removePTag(answer)}`
                            }}>
                          </div>
                        </Radio>
                      </Col>
                    )
                  })}
                </Row>
              </Radio.Group>

              {renderChoices && result && (
                <div style={{
                  marginTop: "10px",
                  paddingRight: "8px"
                }}>
                  {
                    result.answer === result.correct ?
                      <Alert
                        message="Correct"
                        description={<div dangerouslySetInnerHTML={{__html: `${result.explanation}`}}>
                        </div>}
                        type="success"
                        showIcon
                      />
                      :
                      <Alert
                        message="Incorrect"
                        description={<div dangerouslySetInnerHTML={{__html: `${result.explanation}`}}>
                        </div>}
                        type="error"
                        showIcon
                      />
                  }
                </div>
              )}
            </div>
          )}
        </div>
      </div>
      <div>
        <CustomModal
          footer={null}
          title="Report question issue"
          questionHTML={questionHTML} renderChoices={renderChoices} question={question} visible={visible}
          onCancel={() => setVisible(false)}>
          <ReportQuestionIssueCard renderChoices={renderChoices} questionHTML={questionHTML} question={question}
                                   onCancel={() => setVisible(false)}
          />
        </CustomModal>
      </div>
    </>

  )
}
export default QuestionCard;
