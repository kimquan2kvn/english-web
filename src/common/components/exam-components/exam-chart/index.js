import React from "react";
import {Doughnut} from 'react-chartjs-2';
import { COLOR_1, COLOR_2, COLOR_4 } from 'common/constants/ChartConstant';

const ExamChart = props => {
  const data = {
    labels: ['> 40 question', '10-30 ', '< 10 question'],
    datasets: [
      {
        data: [350, 450, 100],
        backgroundColor: [COLOR_1, COLOR_4, COLOR_2],
        pointBackgroundColor : [COLOR_1, COLOR_4, COLOR_2]
      }
    ]
  }
  return (
    <div className="exam-chart">
      <Doughnut data={data}/>
    </div>
  )
}
export default ExamChart;
