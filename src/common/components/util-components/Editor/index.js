import React, {useRef, useState} from 'react';
import {notification, Form, Spin} from "antd";
import EditorWithForwardedRef from "../DynamicImport/dynamic-editor";
import ApiService from "../../../services/ApiService";
import Utils from "../../../utils";

const Editor = (props) => {
  const {refEditor, type} = props;
  const [uploadImage, setUploadImage] = useState(false);

  const uploadBase64 = (base64) => {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await ApiService.uploadToDiscord([{type: 'base64', base64, ext: 'png'}]);
        console.log(res);
        resolve(res.data[0] ?? '');
      } catch (err) {
        reject(err);
      }
      setUploadImage(false);
    })
  }

  return (
    <Spin
      spinning={uploadImage}
      tip={Utils.translate("message.upload.image")}
    >
      <Form.Item
        name="question"
        label={
          type === 'create-question' ? Utils.translate("label.description") : ''
        }
        rules={[{required: true, message: "Question is required!"}]}
      >
        <EditorWithForwardedRef
          editorOptions={{placeholder: "line1 /n line2"}}
          height="40vh"
          placeholder= {Utils.translate("placeholder.description")}
          initialEditType='wysiwyg'
          onChange={async (e) => {
            const regex = /(!\[(.*?)\])\(data:image/;
            const editor = refEditor.current.getInstance();
            let currentValue = editor.getMarkdown();
            if (regex.exec(currentValue)) {
              let startImageBase64 = 0, endImageBase64 = 0;
              try {
                setUploadImage(true);
                const firstResult = regex.exec(currentValue)[0];
                startImageBase64 = regex.exec(currentValue).index;
                endImageBase64 = currentValue.indexOf(')', startImageBase64);
                const base64 = currentValue.slice(startImageBase64 + firstResult.length - 10, endImageBase64);
                const url = await uploadBase64(base64.slice(base64.indexOf(',') + 1));
                const newValue = currentValue.replace(currentValue.slice(startImageBase64, endImageBase64 + 1), `![image](${url})`);
                editor.setMarkdown(newValue);
              } catch (err) {
                console.log(err)
                editor.setMarkdown(currentValue.replace(currentValue.slice(startImageBase64, endImageBase64 + 1), ''));
                notification.error({message: 'Failed to upload image'})
              }
              setUploadImage(false);
            }

          }}
          ref={ref => refEditor.current = ref}
          toolbarItems={
            [
              ['bold', 'italic', 'strike'],
              ['hr', 'quote'],
              ['ul', 'ol', 'task', 'indent', 'outdent'],
              ['code', 'codeblock'],
              ['image']
            ]
          }
        />
      </Form.Item>
    </Spin>
  )
}

export default Editor;