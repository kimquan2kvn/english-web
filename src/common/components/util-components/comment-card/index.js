import React, {createRef, useEffect, useRef, useState} from "react";
import {Avatar, Button, Grid, Input, Menu, Modal, notification, Skeleton, Spin, Tooltip} from "antd";
import {
  ArrowRightOutlined,
  CaretDownOutlined,
  CaretUpOutlined,
  DeleteOutlined,
  EditOutlined,
  LoadingOutlined,
  UserOutlined
} from "@ant-design/icons";
import utils from "../../../utils";
import Utils from "../../../utils";
import moment from 'moment';
import ApiService from "../../../services/ApiService";
import SelectReaction from "../../shared-components/SelectReaction";
import Image from 'next/image';
import {useSelector} from "react-redux";
import EllipsisDropdown from "../../shared-components/EllipsisDropdown";
import Link from 'next/link';
import EditorWithForwardedRef from "../DynamicImport/dynamic-editor";
import ReactMarkdown from 'react-markdown'
import {useRouter} from "next/router";

const ButtonReacted = ({reaction, handleCancelReact}) => {

  const getStyle = () => {
    switch (reaction) {
      case 'like':
        return {
          color: '#2078f4', text: Utils.translate("label.react.like")
        }
      case 'care':
        return {
          color: '#f7b125', text: Utils.translate("label.react.care")
        }
      case 'sad':
        return {
          color: '#f7b125', text: Utils.translate("label.react.sad")
        }
      case 'haha':
        return {
          color: '#f7b125', text:  Utils.translate("label.react.haha")
        }
      case 'angry':
        return {
          color: '#e9710f', text:  Utils.translate("label.react.angry")
        }
      case 'love':
        return {
          color: '#f33e58', text:  Utils.translate("label.react.love")
        }
      case 'wow':
        return {
          color: '#f7b125', text:  Utils.translate("label.react.wow")
        }
    }
  }

  return (<span
    onClick={() => handleCancelReact().then(_ => {
    })}
    className='cursor-pointer button-reaction'
    style={{color: getStyle().color}}
  >
      {getStyle().text}
    </span>)
}

const CommentOptions = ({setComments, data, model, isMd}) => {
  const [visible, setVisible] = useState(false);
  const isMobile = !utils.getBreakPoint(useBreakpoint()).includes('lg');
  const [loading, setLoading] = useState(false);
  const [comment, setComment] = useState(null)
  const refEditor = useRef();
  const {locale} = useSelector(state => state.theme)

  const onSubmit = async () => {
    setLoading(true);
    const content = isMd ? refEditor.current.getInstance().getMarkdown().replaceAll(/(<br\s*\/*>|\n)/gi, "\n\n") : comment
    try {
      const res = await ApiService.updateComment(data._id, {
        model,
        content: content
      });
      const {comment} = res.data;
      setComments(state => {
        return state.map(x => {
          if (x._id === comment._id) return {
            ...x,
            content: content
          }; else return x;
        })
      })
      notification.success({message: Utils.translate("message.edit.comment.success")});
    } catch (err) {
      console.log(err);
    }
    setVisible(false);
    setLoading(false);
  }

  const timeout = (time) => {
    setLoading(true)
    return new Promise(resolve => setTimeout(() => {
      if (isMd) refEditor.current.getInstance().setMarkdown(data?.content ?? '')
      else setComment(data?.content)
      setLoading(false);
      resolve();
    }, time))
  }

  return (<div
    className={`d-flex ${!isMobile && 'comment-option'}`}
  >
    <div className='m-auto ml-2'>
      <EllipsisDropdown
        type='horizontal'
        menu={<Menu>
          <Menu.Item
            onClick={() => {
              Modal.confirm({
                title: Utils.translate("message.confirm.delete.comment"), onOk: async () => {
                  try {
                    const res = await ApiService.deleteComment(data._id, {model});
                    const {comment} = res.data;
                    setComments(state => {
                      return state.filter(x => x._id !== comment._id);
                    })
                    notification.success({message: Utils.translate("message.delete.comment.success")});
                  } catch (err) {
                    console.log(err);
                  }
                }
              })
            }}
          >
            <DeleteOutlined/> Delete
          </Menu.Item>
          <Menu.Item
            onClick={async () => {
              setVisible(true);
              await timeout(300);
            }}
          >
            <EditOutlined/> Edit
          </Menu.Item>
        </Menu>}
      />
    </div>
    <Modal
      title={Utils.translate("label.edit.comment")}
      visible={visible}
      onCancel={() => {
        setVisible(false);
      }}
      footer={false}
    >
      <Spin
        spinning={loading}
        tip={Utils.translate("label.edit.comment")}
      >
        <div className="mb-2">
          {isMd ? (
            <div className="mb-2">
              <EditorWithForwardedRef
                height="20vh"
                placeholder={Utils.translate("placeholder.comment")}
                initialEditType='wysiwyg'
                ref={refEditor}
                toolbarItems={
                  [
                    ['bold', 'italic', 'strike'],
                    ['hr', 'quote'],
                    ['ul', 'ol', 'task', 'indent', 'outdent'],
                    ['code', 'codeblock'],
                  ]
                }
              />
            </div>
          ) : (
            <TextArea
              className="mb-2"
              onChange={(e) => setComment(e.target.value)}
              value={comment}
            />
          )}
        </div>
        <div className='d-flex justify-content-end'>
          <Button
            className='mr-2'
            onClick={() => setVisible(false)}
          >
            Cancel
          </Button>
          <Button
            type='primary'
            onClick={onSubmit}
          >
            Ok
          </Button>
        </div>
      </Spin>
    </Modal>
  </div>)
}

const CommentAction = ({setShowInput, reaction, id, setReactionInfo, model, reactionInfo}) => {
  const [showReaction, setShowReaction] = useState(false);
  const [isReacted, setIsReacted] = useState(reaction);

  const handleReact = async (value) => {
    if (value === isReacted) return;
    try {
      if (isReacted) {
        setReactionInfo(state => {
          const newState = state;
          if (newState.hasOwnProperty(value)) {
            return {...newState, [value]: newState[value] + 1, [isReacted]: newState[isReacted] - 1};
          } else {
            return {...newState, [value]: 1, [isReacted]: newState[isReacted] - 1};
          }
        })
      } else {
        setReactionInfo(state => {
          const newState = state;
          if (newState.hasOwnProperty(value)) {
            return {...newState, [value]: newState[value] + 1};
          } else {
            return {...newState, [value]: 1};
          }
        })
      }
      setIsReacted(value);
      await ApiService.react(id, {model: 'Comment', type: value, active: 'true', reactTo: model});
    } catch (err) {
      setIsReacted(isReacted);
      setReactionInfo(reactionInfo);
      console.log(err)
    }
  }

  const handleCancelReact = async () => {
    setShowReaction(false);
    try {
      setIsReacted(false);
      setReactionInfo(state => {
        const newState = state;
        return {
          ...newState, [isReacted]: newState[isReacted] - 1,
        }
      })
      await ApiService.react(id, {model: 'Comment', delete: true});
    } catch (err) {
      setIsReacted(isReacted);
      setReactionInfo(reactionInfo);
      console.log(err)
    }
  }

  return (<div
    className='d-flex align-items-center my-1 position-relative'
    style={{fontSize: 13, fontWeight: 500}}
  >
    <p
      className='mb-0 button-reply text-center'
      onClick={setShowInput}
      style={{transform: 'translateY(-1px)'}}
    >
      {Utils.translate("label.reply")}
    </p>
    {model !== 'CommunityQuestion' && <div>
      {<div
        className='ml-2'
        onMouseOver={() => {
          if (!showReaction) {
            setShowReaction(true);
          }
        }}
        onMouseOut={() => {
          setShowReaction(false);
        }}
      >
        <div className='wrap-reaction' style={{display: showReaction ? 'flex' : 'none'}}>
          <SelectReaction handleReact={handleReact} setShowReaction={setShowReaction}/>
        </div>
        {isReacted ? <ButtonReacted
          handleCancelReact={handleCancelReact}
          reaction={isReacted}/> : <span
          className='cursor-pointer button-reaction'
          onClick={() => {
            handleReact('like').then(_ => {
            });
          }}
        >
            Like
          </span>}
      </div>}
    </div>}
  </div>)
}

const ReactionInfo = ({reactionInfo}) => {
  const reactionList = Object.keys(reactionInfo);
  const reactionListShow = reactionList.filter(x => reactionInfo[x] > 0);
  const total = reactionList.reduce((total, x) => (total += reactionInfo[x]), 0)
  if (total === 0) return null;
  return (<div
    className='d-flex align-items-end'
  >
    <div className='d-flex ml-2 wrap-reaction-info' style={{width: reactionListShow.length * 20 + 20}}>
      {reactionListShow.map((x, i) => {
        return (<div key={x} style={{zIndex: 10 - i, left: 18 * i}}>
          <Image src={`/img/icon-${x}.png`} width={20} height={20}/>
        </div>)
      })}
      <span className='ml-1' style={{position: 'absolute', right: 8}}>{total !== 0 && total}</span>
    </div>
  </div>)
}

const CommentReply = ({data, mainId, replyFor, updateComment, onInput, setOnInput, model, setComments, isMd}) => {
  const user = useSelector(state => state.user);
  const [reactionInfo, setReactionInfo] = useState(data?.reactions ?? {});
  const [comment, setComment] = useState('');
  const [showInput, setShowInput] = useState(false);
  const {locale} = useSelector(state => state.theme)

  const submitComment = async () => {
    setComment('');
    setOnInput('');
    try {
      const res = await Utils.comment({mainId, replyFor: replyFor, content: comment, model});
      updateComment({...res.comment, reactions: {}, userReaction: false}, data._id);
    } catch (err) {
    }
  }

  const handleShowInput = () => {
    setShowInput(true);
    setOnInput(data._id);
  }

  return (<div className="comment-l2 mb-2 ">
    <div className={`author ${model === 'CommunityQuestion' && 'ml-5'}`}>
      <div className='d-flex flex-grow-1 flex-column align-items-start'>
        <div className='d-flex comment'>
          <span className="comment-content d-flex">
            <div>
            <div className="mb-2 d-flex justify-content-start align-items-center">
              <Link href={'/profile/' + data.author._id}>
                <a>
                  <Avatar shape="circle" src={data?.author?.avatar} size={25} className="mr-2" icon={<UserOutlined/>}/>
                  <span className="mr-2 font-weight-semibold under-line"
                        style={{color: '#1a3353'}}>{data?.author?.displayName ?? ''}</span>
                </a>
              </Link>
              <span className="mb-0  font-size-sm">{data.createdAt ? moment(data.createdAt).fromNow() : ''}</span>
            </div>
            <div className="ant-comment-content-detail" style={{whiteSpace: 'pre-line', minWidth: 250}}>
              {
                <ReactMarkdown>
                  {data?.content ?? ''}
                </ReactMarkdown>
              }
            </div>
            </div>
            {model !== 'CommunityQuestion' && <ReactionInfo reactionInfo={reactionInfo}/>}
          </span>
          {user._id === data.author._id &&
          <CommentOptions isMd={isMd} data={data} setComments={setComments} model={model}/>}
        </div>
        <CommentAction
          reactionInfo={reactionInfo}
          model={model}
          setReactionInfo={setReactionInfo}
          reaction={data.userReaction}
          id={data._id}
          setShowInput={handleShowInput}
        />
        {showInput && data._id === onInput && <div className='align-self-stretch'>
          <TextArea onChange={(e) => setComment(e.target.value)} value={comment} placeholder='Enter comment...'/>
          <Button
            onClick={() => {
              if (comment === '') return;
              submitComment().then(_ => {
              });
            }}
            className='float-right mt-2'
            type='primary'
            size='small'
          >
            Submit
          </Button>
        </div>}
      </div>
    </div>
  </div>)
}

const CommentMain = ({data, mainId, onInput, setOnInput, model, setComments, isMd}) => {
  const user = useSelector(state => state.user);
  const [reactionInfo, setReactionInfo] = useState(data?.reactions ?? {});
  const [comment, setComment] = useState('');
  const [loading, setLoading] = useState(false);
  const [commentsReply, setCommentsReply] = useState([]);
  const [showInput, setShowInput] = useState(false);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [nextPage, setNextPage] = useState(1);
  const [totalReply, setTotalReply] = useState(data?.subCommentCount ?? 0);
  const [vote, setVote] = useState(0);
  const [userReaction, setUserReaction] = useState(null);
  const {locale} = useSelector(state => state.theme)

  useEffect(() => {
    setUserReaction(data.userReaction);
    const reaction = data.reactions;
    const isUseful = reaction.hasOwnProperty('useful');
    const isNotUseFul = reaction.hasOwnProperty('notUseful');
    if (isUseful && isNotUseFul) {
      setVote(reaction['useful'] - reaction['notUseful']);
    } else if (!isUseful && isNotUseFul) {
      setVote(0 - reaction['notUseful']);
    } else if (isUseful && !isNotUseFul) {
      setVote(reaction['useful']);
    }
  }, [])

  const updateComment = (comment, id) => {
    const indexComment = commentsReply.findIndex(x => x._id === id);
    const arr1 = commentsReply.slice(0, indexComment + 1);
    const arr2 = commentsReply.slice(indexComment + 1);
    setTotalReply(state => state + 1);
    setCommentsReply([...arr1, {...comment, reactions: {}, userReaction: false}, ...arr2]);
  }

  const loadMoreReply = async () => {
    setLoading(true);
    try {
      const res = await ApiService.loadComment({replyFor: data._id, limit: 2, page: nextPage});
      const {results, page, totalPages} = res.data;
      setHasNextPage(page < totalPages);
      if (page < totalPages) {
        setNextPage(page + 1);
      }
      setCommentsReply(state => [...state, ...results]);
    } catch (err) {
      console.log(err)
    }
    setLoading(false);
  }

  const submitComment = async () => {
    setComment('');
    setOnInput('');
    try {
      const res = await Utils.comment({mainId, content: comment, replyFor: data._id, model});
      setCommentsReply(state => [{...res.comment, reactions: {}, userReaction: false}, ...state]);
    } catch (err) {
    }
  }

  const handleShowInput = () => {
    setShowInput(true);
    setOnInput(data._id);
  }

  return (<div>
    <div className="comment-l1 mb-4">
      <div className="author d-flex">
        {model === 'CommunityQuestion' && <div className='d-flex flex-column justify-content-start mr-3'>
          <div
            style={{color: userReaction === 'useful' && '#3e79f7'}}
            className='button-vote d-flex cursor-pointer'
            onClick={async () => {
              try {
                if (userReaction === 'useful') {
                  const res = await ApiService.react(data._id, {model: 'Comment', delete: true});
                  if (res.data) {
                    setUserReaction(false);
                    setVote(state => state - 1);
                  }
                } else {
                  const res = await ApiService.react(data._id, {
                    model: 'Comment', type: 'useful', active: 'true', reactTo: model
                  });
                  if (res.data) {
                    setUserReaction('useful');
                    if (userReaction === 'notUseful') {
                      setVote(state => state + 2);
                    } else setVote(state => state + 1);
                  }
                }
              } catch (err) {
              }
            }}
          >
            <div className='m-auto'>
              <Tooltip title='This answer is useful'>
                <CaretUpOutlined/>
              </Tooltip>
            </div>
          </div>
          <div className='text-center'>
            {vote}
          </div>
          <div
            style={{color: userReaction === 'notUseful' && '#3e79f7'}}
            className='button-vote d-flex cursor-pointer'
            onClick={async () => {
              try {
                if (userReaction === 'notUseful') {
                  const res = await ApiService.react(data._id, {model: 'Comment', delete: true});
                  if (res.data) {
                    setUserReaction(false);
                    setVote(state => state + 1);
                  }
                } else {
                  const res = await ApiService.react(data._id, {
                    model: 'Comment', type: 'notUseful', active: 'true', reactTo: model
                  });
                  if (res.data) {
                    setUserReaction('notUseful');
                    if (userReaction === 'useful') {
                      setVote(state => state - 2);
                    } else setVote(state => state - 1);
                  }
                }
              } catch (err) {
              }
            }}
          >
            <div className='m-auto'>
              <Tooltip title='This answer is not useful'>
                <CaretDownOutlined/>
              </Tooltip>
            </div>
          </div>
        </div>}
        <div className='flex-grow-1 d-flex flex-column align-items-start'>
          <div className='d-flex comment'>
            <div className={model === 'CommunityQuestion' ? 'd-flex' : ''}>
              <div className="comment-content d-flex">
                <div>
                  <div className="mb-2 d-flex align-items-center"
                       style={{alignItems: model === 'CommunityQuestion' ? 'center' : ''}}>
                    <Link href={'/profile/' + data.author._id}>
                      <a>
                        <Avatar shape="circle" src={data?.author?.avatar} size={25} className="mr-2"
                                icon={<UserOutlined/>}/>
                        <span className="mr-2 font-weight-semibold under-line"
                              style={{color: '#1a3353'}}>{data?.author?.displayName ?? ''}</span>
                      </a>
                    </Link>
                    <span className="mb-0  font-size-sm">{data.createdAt ? moment(data.createdAt).fromNow() : ''}</span>
                  </div>
                  <div className="ant-comment-content-detail" style={{whiteSpace: 'pre-line', minWidth: 250}}>
                    {isMd ? (
                      <ReactMarkdown>
                        {data?.content ?? ''}
                      </ReactMarkdown>
                    ) : (
                      <> {Utils.decodeHtml(data?.content ?? '')}</>
                    )}
                  </div>
                </div>
                {model !== 'CommunityQuestion' && <ReactionInfo reactionInfo={reactionInfo}/>}
              </div>
            </div>
            {user._id === data.author._id &&
            <CommentOptions isMd={isMd} data={data} setComments={setComments} model={model}/>}
          </div>

          <CommentAction
            reactionInfo={reactionInfo}
            model={model}
            setReactionInfo={setReactionInfo}
            reaction={data.userReaction}
            id={data._id}
            setShowInput={handleShowInput}
          />
          {showInput && data._id === onInput && <div className='align-self-stretch'>
            <TextArea onChange={(e) => setComment(e.target.value)} value={comment} placeholder='Enter comment...'/>
            <Button
              className='float-right mt-2'
              type='primary'
              size='small'
              onClick={() => {
                if (comment === '') return;
                submitComment().then(_ => {
                });
              }}
            >
              Submit
            </Button>
          </div>}
        </div>
      </div>
      {commentsReply.length > 0 && commentsReply.map(comment => {
        return (
          <CommentReply
            isMd={isMd && comment.level === 1 && !comment.hasOwnProperty("replyFor")}
            model={model}
            onInput={onInput}
            setOnInput={setOnInput}
            updateComment={updateComment}
            setComments={setCommentsReply}
            mainId={mainId}
            key={comment._id}
            replyFor={data._id}
            data={comment}/>
        )
      })}
      {
        data.subCommentCount > 0 && hasNextPage &&
        <span
          className='button-reply'
          style={{marginLeft: model === 'CommunityQuestion' ? 54 : 0}}
          onClick={() => loadMoreReply()}
        >
            {loading ? <LoadingOutlined/> : <ArrowRightOutlined/>} {totalReply - commentsReply.length} replies
          </span>
      }
    </div>
  </div>)
}

const {useBreakpoint} = Grid
const {TextArea} = Input
const CommentCard = props => {
  const isMobile = !utils.getBreakPoint(useBreakpoint()).includes('md')
  const {id, model} = props;
  const [comments, setComments] = useState([]);
  const [comment, setComment] = useState('');
  const [onInput, setOnInput] = useState('');
  const [nextPage, setNextPage] = useState(1);
  const [hasNextPage, setHasNextPage] = useState(false);
  const [loading, setLoading] = useState(false);
  const [showSkeleton, setShowSkeleton] = useState(true);
  const ref = createRef();
  const router = useRouter();
  const isMd = router.route !== "/profile/[userId]"
  const {locale} = useSelector(state => state.theme)


  const submitComment = async () => {
    const content = isMd ? ref.current.getInstance().getMarkdown().replaceAll(/(<br\s*\/*>|\n)/gi, "\n\n") : comment
    setComment('');
    try {
      const res = await Utils.comment({mainId: id, content: content, model});
      setComments(state => [{...res.comment, reactions: {}, userReaction: false}, ...state]);
    } catch (err) {
      console.log(err)
    }
  }

  const loadComments = async () => {
    if (nextPage > 1) setLoading(true);
    try {
      const res = await ApiService.loadComment({
        limit: 5,
        page: nextPage,
        category: id,
        level: 1,
        sortBy: model === 'CommunityQuestion' ? '-reactionCount' : '-createdAt'
      });
      const {page, results, totalPages} = res.data;
      setHasNextPage(page < totalPages);
      setNextPage(page + 1);
      setComments(state => [...state, ...results]);
    } catch (err) {
    }
    setShowSkeleton(false);
    setLoading(false);
  }

  useEffect(() => {
    loadComments().then(_ => {
    });
  }, [])

  window.onscroll = function () {
    if (window.scrollY + 3 + document.body.offsetHeight >= document.body.scrollHeight) {
      if (!loading && hasNextPage) {
        loadComments().then(_ => {
        });
      }
    }
  }

  return (
    <div className="comment-list">
      {props.title && (<div className="label mb-4">
        <h4>{props.title}</h4>
      </div>)}
      {props.hasInput && (
        <div className="comment-input">
          <div className="mb-3">
            {isMd ? (
              <div className="mb-2">
                <EditorWithForwardedRef
                  height="10vh"
                  placeholder={Utils.translate("placeholder.comment")}
                  initialEditType='wysiwyg'
                  ref={ref}
                  toolbarItems={[]}
                />
              </div>
            ) : (
              <TextArea
                className="mb-2"
                onChange={(e) => setComment(e.target.value)}
                value={comment}
                placeholder={props?.placeholderInput ?? ''}
              />
            )}
            <div className="float-right" >
              <Button onClick={() => {
                if ((isMd && ref.current.getInstance().getMarkdown()?.replaceAll(/(<br\s*\/*>|\n)/gi, "\n\n") === '') || (!isMd && comment === '')) return;
                submitComment().then(_ => {
                });
              }} type="primary" size="small">{Utils.translate("label.submit")}</Button>
            </div>
          </div>
        </div>)}
      <div
        style={{paddingTop: 50}}
      >
        {comments.map(comment => {
          return (<CommentMain
            isMd={true}
            setComments={setComments}
            model={model}
            setOnInput={setOnInput}
            onInput={onInput}
            key={comment._id}
            data={comment}
            mainId={id}/>)
        })}
        {showSkeleton && <Skeleton active avatar paragraph={{rows: 3}}/>}
      </div>
      {loading &&
      <div style={{maxWidth: 350}}>
        <div className='mb-2'>
          <Skeleton avatar paragraph={{rows: 2}}/>
        </div>
        <Skeleton avatar paragraph={{rows: 2}}/>
      </div>
      }
    </div>
  )
}
export default CommentCard;
