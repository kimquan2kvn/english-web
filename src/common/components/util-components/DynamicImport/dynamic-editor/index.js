import React from "react";
import dynamic from "next/dynamic"
import {Skeleton} from "antd";

const Editor = dynamic(() => import('../../WapperEditor'), { ssr: false, loading: () => <Skeleton /> }
);

const EditorWithForwardedRef = React.forwardRef((props, ref) => (
  <Editor {...props} forwardedRef={ref} />
))


export default EditorWithForwardedRef;
