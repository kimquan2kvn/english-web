import dynamic from "next/dynamic";

const DynamicSelect = dynamic(() => import('antd').then(mod => mod.Select), { ssr: false });

export default DynamicSelect;