import React, {useEffect, useState} from 'react'
import {connect} from 'react-redux'
import {LockOutlined, MailOutlined, UserAddOutlined, UserOutlined} from '@ant-design/icons';
import {Button, Form, Input, Alert, Row, Col, notification} from "antd";
import {signUp} from 'common/redux/actions/Auth';
import {useHistory} from "react-router-dom";
import {motion} from "framer-motion"
import ApiService from "../../services/ApiService";
import {useRouter} from "next/router";

const rules = {
  fullName: [{
    required: true,
    message: "Please input your full name"
  }],
  username: [{
    required: true,
    message: "Please input your username"
  }],
  email: [
    {
      required: true,
      message: 'Please input your email address'
    },
    {
      type: 'email',
      message: 'Please enter a validations email!'
    }
  ],
  password: [
    {
      required: true,
      message: 'Please input your password'
    }
  ],
  confirm: [
    {
      required: true,
      message: 'Please confirm your password!'
    },
    ({getFieldValue}) => ({
      validator(rule, value) {
        if (!value || getFieldValue('password') === value) {
          return Promise.resolve();
        }
        return Promise.reject('Passwords do not match!');
      },
    })
  ]
}

export const RegisterForm = (props) => {
  const {token, redirect, showMessage, hideAuthMessage, allowRedirect} = props
  const [form] = Form.useForm();
  let history = useHistory();
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");
  const onSignUp = (values) => {
    form.validateFields().then(async values => {
      setLoading(true);
      try {
        const res = await ApiService.register({...values})
        if (res) {
          notification.success({
            message: "Register account successfully."
          })
          setTimeout(() => {
            router.replace("/auth/login");
          }, 400)
        }
      } catch (err) {
        console.log(err)
        notification.error({
          message: err.response.data.message
        })
        setLoading(false)
        console.log(err)
      } finally {
        setLoading(false)
      }
    }).catch(info => {
      console.log('Validate Failed:', info);
    });
  }

  useEffect(() => {
    if (token !== null && allowRedirect) {
      history.push(redirect)
    }
    if (showMessage) {
      setTimeout(() => {
        hideAuthMessage();
      }, 3000);
    }
  });

  return (
    <>
      <motion.div
        initial={{opacity: 0, marginBottom: 0}}
        animate={{
          opacity: showMessage ? 1 : 0,
          marginBottom: showMessage ? 20 : 0
        }}>
        <Alert type="error" showIcon message={message}></Alert>
      </motion.div>
      <Form form={form} layout="vertical" name="register-form" onFinish={onSignUp}>
        <Form.Item
          name="fullName"
          label="Full Name"
          rules={rules.fullName}
          hasFeedback
        >
          <Input prefix={<UserOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          name="email"
          label="Email"
          rules={rules.email}
          hasFeedback
        >
          <Input prefix={<MailOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          name="username"
          label="Username"
          rules={rules.password}
          hasFeedback
        >
          <Input prefix={<UserAddOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          name="password"
          label="Password"
          rules={rules.password}
          hasFeedback
        >
          <Input.Password prefix={<LockOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          name="confirm"
          label="ConfirmPassword"
          rules={rules.confirm}
          hasFeedback
        >
          <Input.Password prefix={<LockOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" block loading={loading}>
            Sign Up
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = ({auth}) => {
  const {loading, message, showMessage, token, redirect} = auth;
  return {loading, message, showMessage, token, redirect}
}

const mapDispatchToProps = {
  signUp,
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterForm)
