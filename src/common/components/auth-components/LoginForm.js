import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {Button, Form, Input, Divider, Alert, notification} from "antd";
import {MailOutlined, LockOutlined, FacebookOutlined, ArrowLeftOutlined} from '@ant-design/icons';
import PropTypes from 'prop-types';
import {
  signIn,
  signInWithGoogle,
  signInWithFacebook, authenticated
} from 'common/redux/actions/Auth';
import {useHistory} from "react-router-dom";
import {motion} from "framer-motion"
import ApiService from "../../services/ApiService";
import {getCookiesData, setToken} from "../../services/StogareService";
import {Router, useRouter} from "next/router";
import GoogleLogin from "react-google-login";
import FacebookLogin from "react-facebook-login";
import axios from "axios";
import {updateUser} from "../../redux/actions/User";
import {FACEBOOK_APP_ID, GOOGLE_CLIENT_ID} from "../../configs/AppConfig";
import {onChangeShowPopup} from "../../redux/actions";


export const LoginForm = props => {
  const { isPopup } = props;
  let history = useHistory();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const prePath = getCookiesData('prePath');
  const {
    otherSignIn,
    showForgetPassword,
    hideAuthMessage,
    onForgetPasswordClick,
    signInWithGoogle,
    signInWithFacebook,
    extra,
    token,
    redirect,
    showMessage,
    message,
    allowRedirect
  } = props

  const initialCredential = {
    username: 'admin',
    password: 'admin@123'
  }

  const onLogin = async (values) => {
    setLoading(true)
    try {
      const res = await ApiService.login(values);
      if(res.data.hasOwnProperty('isVerifiedEmail')) {
        return router.replace('/auth/verify-account/' + res.data.email);
      }
      updateUser(res.data.user);
      if(isPopup) {
        window.location.reload();
      } else await router.push(prePath ?? '/').then(_ => {
        notification.success({message: 'Welcome, ' + res.data.user.fullName});
      });
      if(isPopup) onChangeShowPopup(false);
    } catch(err) {
      notification.error({
        message: err.response.data.message
      })
      console.log(err);
    }
    setLoading(false);
  };

  const responseFailureGoogle = async () => {
    notification.error({message: 'Failed to login with google account'});
  }

  const responseSuccessGoogle = async (resGoogle) => {
    try {
      const {accessToken} = resGoogle;
      const res = await axios.post(`/api/auth/login`, {ggAccessToken: accessToken});
      const {tokens, user} = res.data;
      setToken(tokens);
      updateUser(user);
      if(isPopup) {
        window.location.reload();
      } else router.push(prePath ?? '/').then(_ => {
        notification.success({message: 'Welcome ' + user.fullName + ' !!!'});
      });
      if(isPopup) onChangeShowPopup(false);
    } catch (err) {
      console.log('err', err)
      notification.error({message: 'Failed to login with google account'});
    }
  }

  const responseFacebook = async (resFacebook) => {
    if(!resFacebook) return;
    const {accessToken} = resFacebook;
    try {
      const res = await axios.post(`/api/auth/login`, {fbAccessToken: accessToken});
      const {tokens, user} = res.data;
      setToken(tokens);
      updateUser(user);
      if(isPopup) {
        window.location.reload();
      } else await router.push(prePath ?? '/').then(_ => {
        notification.success({message: 'Welcome ' + user.fullName + ' !!!'});
      });
      if(isPopup) onChangeShowPopup(false);
    } catch (err) {
      console.log(err)
      notification.error({message: 'Failed to login with facebook account'});
    }
  }

  useEffect(() => {
    if (token !== null && allowRedirect) {
      history.push(redirect)
    }
    if (showMessage) {
      setTimeout(() => {
        hideAuthMessage();
      }, 3000);
    }
  });

  const renderOtherSignIn = (
    <div>
      <Divider>
        <span className="text-muted font-size-base font-weight-normal">or connect with</span>
      </Divider>
      <div className="d-flex justify-content-between align-items-center w-100">
        <div
          className='flex-grow-1 mr-2'
          style={{borderRadius: 10, overflow: 'hidden', border: '1px solid rgba(0, 0, 0, 0.1)'}}
        >
          <GoogleLogin
            autoLoad={false}
            className='button-google-login flex-grow-1'
            clientId={GOOGLE_CLIENT_ID}
            buttonText="Google"
            onSuccess={responseSuccessGoogle}
            onFailure={responseFailureGoogle}
            cookiePolicy={'single_host_origin'}
          />
        </div>
        <div
          className='flex-grow-1'
        >
          <FacebookLogin
            autoLoad={false}
            cssClass='button-facebook-login'
            appId={FACEBOOK_APP_ID}
            fields="name,email,picture"
            textButton='Facebook'
            onClick={() => {
            }}
            callback={responseFacebook}
            icon={<FacebookOutlined className='mr-2' style={{fontSize: 18}}/>}
          />
        </div>
      </div>
    </div>
  )

  return (
    <>
      <motion.div
        initial={{opacity: 0, marginBottom: 0}}
        animate={{
          opacity: showMessage ? 1 : 0,
          marginBottom: showMessage ? 20 : 0
        }}>
        <Alert type="error" showIcon message={message}/>
      </motion.div>
      <Form
        layout="vertical"
        name="login-form"
        onFinish={onLogin}
      >
        <Form.Item
          name="username"
          label="Username"
          rules={[
            {
              required: true,
              message: 'Please input your username',
            },
          ]}>
          <Input prefix={<MailOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          className='mb-2'
          name="password"
          label={
            <div className={`${showForgetPassword ? 'd-flex justify-content-between w-100 align-items-center' : ''}`}>
              <span>Password</span>
              {
                showForgetPassword &&
                <span
                  onClick={() => onForgetPasswordClick}
                  className="cursor-pointer font-size-sm font-weight-normal text-muted"
                >
									Forget Password?
								</span>
              }
            </div>
          }
          rules={[
            {
              required: true,
              message: 'Please input your password',
            }
          ]}
        >
          <Input.Password prefix={<LockOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          className='mb-2'
        >
          <Button
            className='float-right p-0 text-muted font-size-base font-weight-normal under-line'
            type='link'
            onClick={() => router.push("/auth/reset-password")}
          >
            Forgot password?
          </Button>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" block loading={loading}>
            Sign In
          </Button>
        </Form.Item>
        {
          otherSignIn ? renderOtherSignIn : null
        }
        {extra}
        {
          !isPopup && <div className='d-flex justify-content-center'>
            <Button
              className='p-0 text-gray'
              // icon={<ArrowLeftOutlined />}
              type='link'
              onClick={() => {
              router.push('/')}
              }
            >
              <span className='under-line'>
                Back to home page
              </span>
            </Button>
          </div>
        }
      </Form>
    </>
  )
}

LoginForm.propTypes = {
  otherSignIn: PropTypes.bool,
  showForgetPassword: PropTypes.bool,
  extra: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
};

LoginForm.defaultProps = {
  otherSignIn: true,
  showForgetPassword: false
};

const mapStateToProps = ({auth}) => {
  const {loading, message, showMessage, token, redirect} = auth;
  return {loading, message, showMessage, token, redirect}
}

const mapDispatchToProps = {
  signIn,
  signInWithGoogle,
  signInWithFacebook
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm)
