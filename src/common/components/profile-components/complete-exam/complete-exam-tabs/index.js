import React, {useEffect, useState} from "react";
import {Button, Card, Col, Empty, Row} from "antd";
import CompleteExamCard from "../complete-exam-card";
import ApiService from "../../../../services/ApiService";
import {LoadingOutlined} from "@ant-design/icons";

const CompleteExamTabs = props => {
  const {results, page, totalPages, totalResults} = props.data.result;
  const [completedExam, setCompletedExam] = useState(results ?? []);
  const [hasNextPage, setHasNextPage] = useState((page && totalPages) ? page < totalPages : false);
  const [nextPage, setNextPage] = useState(page ? page + 1 : 0);
  const [loading, setLoading] = useState(false);

  const loadCompletedExams = async () => {
    setLoading(true);
    try {
      const res = await ApiService.getCompletedExams({page: nextPage, limit: 8, sortBy: '-createdAt'});
      const {page, totalPages, results} = res.data.result;
      setHasNextPage(page < totalPages);
      setNextPage(page + 1);
      setCompletedExam(state => [...state, ...results]);
    } catch (err) {

    }
    setLoading(false);
  }

  return (
    <div>
      <Card bordered={false}>
        <h4 className="mb-4">Complete Exam {`(${totalResults})`}</h4>
        {
          completedExam.length > 0
            ?
            <>
              <Row gutter={16}>
                {completedExam.map(eml => (
                  <Col xm={24} sm={8} md={8} lg={6} xl={6} xxl={6} key={eml._id}>
                    <CompleteExamCard data={eml}/>
                  </Col>
                ))}
              </Row>
              {
                hasNextPage && <div
                  onClick={() => {
                    loadCompletedExams().then(_ => {
                    });
                  }}
                >
                  <Button loading={loading}>
                    Show more
                  </Button>
                </div>
              }
            </>
            :
            <Empty/>
        }
      </Card>
    </div>
  )
}
export default CompleteExamTabs;
