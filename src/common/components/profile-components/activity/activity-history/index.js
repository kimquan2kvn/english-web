import React from "react";
import {Alert, Timeline} from "antd";
import {FileTextOutlined, QuestionCircleOutlined, MessageOutlined, FileDoneOutlined} from "@ant-design/icons";

const ActivityHistory = props => {
  return (
    <Timeline>
      <Timeline.Item dot={<FileDoneOutlined style={{ fontSize: '16px' }} />} color="green">
        <p>11:30 02/01/2021</p>
        <Alert message={<p className="mb-0">Completed exam DE THI SO 1 </p>} type="success" />
      </Timeline.Item>
      <Timeline.Item dot={<FileDoneOutlined style={{ fontSize: '16px' }}/>} color="green">
        <p>11:30 02/01/2021</p>
        <Alert message={<p className="mb-0">Completed exam DE THI SO 1 </p>} type="success" />
      </Timeline.Item>
      <Timeline.Item dot={<QuestionCircleOutlined style={{ fontSize: '16px' }}/>} color="red">
        <p>Post a question on the community page at 10:30 on January 2, 2021</p>
      </Timeline.Item>
      <Timeline.Item dot={<QuestionCircleOutlined style={{ fontSize: '16px' }}/>} color="red">
        <p >Post a question on the community page at 10:30 on January 2, 2021</p>
      </Timeline.Item>
      <Timeline.Item dot={<MessageOutlined style={{ fontSize: '16px' }} />} color="blue">
        <p >Reply a question in community</p>
      </Timeline.Item>
      <Timeline.Item dot={<MessageOutlined style={{ fontSize: '16px' }} />} color="blue">
        <p>Reply a question in community</p>
      </Timeline.Item>
    </Timeline>
  )
}
export default ActivityHistory;
