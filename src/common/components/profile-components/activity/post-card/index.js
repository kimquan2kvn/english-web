import React, {useState} from "react";
import {Avatar, Card, Divider, Image, Menu, Modal, notification} from "antd";
import {DeleteOutlined, HeartFilled, MessageFilled, UserOutlined} from "@ant-design/icons";
import CommentCard from "../../../util-components/comment-card";
import moment from "moment";
import EllipsisDropdown from "../../../shared-components/EllipsisDropdown";
import ApiService from "../../../../services/ApiService";
import {useSelector} from "react-redux";

const PostCard = props => {
  const user = useSelector(state => state.user);
  const {data, updatePostList} = props;
  const [userReaction, setUserReaction] = useState(data?.userReaction ?? false);
  const [reactionCount, setReactionCount] = useState(data?.reactionCount ?? 0);
  const [showComment, setShowComment] = useState(false);
  return (
    <>
      <Card bordered={false}>
        <div className="mb-3">
          <div className='d-flex justify-content-between'>
            <div className="author d-flex">
              <div className="avatar mr-2 mb-3">
                <Avatar shape="square" icon={<UserOutlined/>} src={data?.author?.avatar ?? ''}/>
              </div>
              <div className="d-flex flex-column">
                <span className="font-weight-semibold">{data?.author?.displayName ?? ''}</span>
                <span className="font-size-sm">{moment(data.createdAt).fromNow()}</span>
              </div>
            </div>
            <div>
              {
                user._id === data?.author?._id
                &&
                <EllipsisDropdown
                  menu={
                    <Menu>
                      <Menu.Item
                        onClick={async () => {
                          Modal.confirm({
                            title: 'You are sure delete post?',
                            onOk: async () => {
                              try {
                                const res = await ApiService.deletePost(data.slug);
                                const { post } = res.data;
                                updatePostList(state => {
                                  return state.filter(x => x._id !== post._id);
                                })
                                notification.success({message: 'Delete post successfully'})
                              } catch (err) {
                                notification.error({message: 'Failed to delete post'});
                              }
                            }
                          })
                        }}
                      >
                        <DeleteOutlined/> Delete
                      </Menu.Item>
                    </Menu>
                  }
                />
              }
            </div>
          </div>
          <div className="content mb-3">
            {
              data.content
            }
          </div>
          <Image src={data?.thumbnail} style={{width: '100%'}}/>
          <div
            className="button d-flex"
          >
            <div
              className="mr-2 btn-light btn-color-muted btn-active-light-danger btn-sm cursor-pointer d-flex"
              onClick={async () => {
                if (userReaction) {
                  try {
                    setUserReaction(false);
                    const res = await ApiService.react(data._id, {delete: true, model: 'ProfilePost'});
                    const { likeCount } = res.data;
                    setReactionCount(likeCount);
                  } catch (err) {
                    setUserReaction('love');
                  }
                } else {
                  try {
                    setUserReaction('love');
                    const res = await ApiService.react(data._id, {model: 'ProfilePost', active: 'true', type: 'love'});
                    const { likeCount } = res.data;
                    setReactionCount(likeCount);
                  } catch (err) {
                    setUserReaction(false);
                  }
                }
              }}
            >
            <span className={`m-auto ${userReaction && 'text-danger'}`}>
              <HeartFilled className="mr-2" style={{fontSize: ".925rem"}}/>
              <span style={{fontSize: ".825rem"}}>{
                reactionCount
              }</span>
            </span>
            </div>
            <div
              className=" btn-light btn-color-muted btn-active-light-success  btn-sm cursor-pointer d-flex"
              onClick={() => {
                setShowComment(true);
              }}
            >
            <span className='m-auto'>
              <MessageFilled className="mr-2" style={{fontSize: ".925rem"}}/>
              <span style={{fontSize: ".825rem"}}>{
                data?.commentCount ?? 0
              }</span>
            </span>
            </div>
          </div>
        </div>
        <div>
          <Divider type="dashed" className="mb-3"/>
          {showComment && <CommentCard id={data._id} model='ProfilePost' hasInput={true}/>}
        </div>
      </Card>
      {
        props.hasDivider && <Divider/>
      }
    </>
  )
}
export default PostCard
