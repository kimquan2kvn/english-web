import React from 'react'
import {Row, Col, Card, Avatar, Button, Grid, Typography} from 'antd';
import {
  FormOutlined,
MailFilled, UserOutlined,
} from '@ant-design/icons';
import utils from "../../../../utils";
import Flex from "../../../shared-components/Flex";
import Icon from "../../../util-components/Icon";
import StatisticWidgetCard from "../../statistic-widget-card";

const {useBreakpoint} = Grid


const ProfileCard = props => {
  const isMobile = !utils.getBreakPoint(useBreakpoint()).includes('md');
  const { userData, completedExam, totalActions } = props;


  return (
    <Card className={`shadow-md`} bordered={false}
          bodyStyle={{marginBottom: "-4.6875rem", paddingBottom: "4rem"}}>
      <Row justify="center">
        <Col sm={24} md={23}>
          <div className="d-md-flex align-items-center">
            <div className={`text-center ${isMobile ? "mb-3 rounded p-2 bg-white shadow-sm mx-auto" : ""}`}
                 style={isMobile ? {marginTop: "-3.5rem", 'maxWidth': `${props.avatarSize + 16}px`} : {}}>
              <Avatar shape="square" size={props.avatarSize} src={userData.avatar} icon={<UserOutlined/>}/>
            </div>
            <div className="ml-md-4 w-100">
              <Flex alignItems="center" justifyContent="between" mobileFlex={false}
                    className="mb-2 text-md-left text-center">
                <h2 className="mb-0">{userData?.displayName ?? ''}</h2>
                <div className="ml-md-3 mt-3 mt-md-0 mb-3">
                  {/*<Button size="small" type="primary">Add Friend</Button>*/}
                  {/*<Button size="small" className="ml-2">Message</Button>*/}
                </div>
              </Flex>
              {!isMobile && (
                <div className="d-flex flex-column mb-3">
                  <div className="mb-2 mt-2 mt-md-0 mr-5">
                    <span className="text-gray-light">
                      <Icon type={MailFilled} className="font-size-md mr-2"/>
                      <Typography.Text className='font-weight-semibold' copyable>{userData?.email ?? ''}</Typography.Text>
                    </span>
                  </div>
                  <div className="mb-2 mt-2 mt-md-0 ">
                    <span className="text-gray-light">
                      <Icon type={FormOutlined} className="font-size-md mr-2"/>
                      <span className="font-weight-semibold">{userData?.bio ?? '<3'}</span>
                    </span>
                  </div>
                </div>
              )}
              <div className="statistic-card">
                <div>
                  <div>
                    <Row>
                      <Col span={24}>
                        <Row gutter={[16, 16]}>
                          <Col sm={12} md={12} xl={6} lg={6} xxl={6}>
                            <StatisticWidgetCard title="Complete Exam" value={completedExam}/>
                          </Col>
                          <Col sm={12} md={12} xl={6} lg={6} xxl={6}>
                            <StatisticWidgetCard title="Total Actions" value={totalActions ?? 0}/>
                          </Col>
                          <Col sm={12} md={12} xl={6} lg={6} xxl={6}>
                            <StatisticWidgetCard title="Total Discussion" value="No data" status=""/>
                          </Col>
                          <Col sm={12} md={12} xl={6} lg={6} xxl={6}>
                            <StatisticWidgetCard title="Total Starts" value="No data" status=""/>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Card>
  )
}

export default ProfileCard
