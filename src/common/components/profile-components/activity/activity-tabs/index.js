import React, {useEffect, useState} from "react";
import {Button, Card, Col, Empty, List, Progress, Row, Skeleton, Tooltip, Typography} from "antd";
import '@toast-ui/editor/dist/toastui-editor.css';
import PostCard from "../post-card";
import UploadStatusCard from "../upload-status";
import Link from 'next/link'
import Icon, {ArrowRightOutlined} from "@ant-design/icons";
import {examOverview} from "../../../../assets/svg/icon";
import ApiService from "../../../../services/ApiService";

const ActivityTabs = props => {
  const {user, completedExamData} = props.data;
  const {results} = completedExamData.result;
  const [postList, setPostList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [loadMore, setLoadMore] = useState(false);
  const [nextPage, setNextPage] = useState(1);
  const [hasNextPage, setHasNextPage] = useState(true);

  const loadPost = async () => {
    nextPage === 1 && setLoading(true);
    nextPage > 1 && setLoadMore(true);
    try {
      const res = await ApiService.getPosts({slug: user.username, limit: 3, page: nextPage, sortBy: '-createdAt'});
      const {results, page, totalPages} = res.data;
      setHasNextPage(page < totalPages);
      setNextPage(page + 1);
      setPostList(state => [...state, ...results]);
    } catch (err) {
      console.log(err);
    }
    setLoading(false);
    setLoadMore(false);
  }

  window.onscroll = function() {
    if (window.scrollY + 3 + document.body.offsetHeight >= document.body.scrollHeight) {
      if (!loadMore && hasNextPage) {
        loadPost().then(_ => {
        });
      }
    }
  }

  useEffect(() => {
    loadPost().then(_ => {});
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Row gutter={16}>
      <Col xs={24} sm={24} lg={{span: 10, order: 2}} xl={10} xxl={10}>
        <Card bordered={false} style={{
          minHeight: 200,
          backgroundImage: `url(/img/others/img-27.png)`,
          backgroundSize: `70%`,
          backgroundRepeat: `no-repeat`,
          backgroundPosition: `bottom right`,
        }}>
          <div className="d-flex justify-content-between align-items-center mb-3">
            <h4>Membership Card</h4>
          </div>
        </Card>
        <Card bordered={false}>
          <div className="d-flex justify-content-between align-items-center">
            <h4>Recent Completed Exams</h4>
            <Button size="small" type="text" onClick={() => props.setTab('2')}>View all</Button>
          </div>
          <List
            itemLayout="horizontal"
            dataSource={results.slice(0, 5)}
            renderItem={item => (
              <List.Item>
                <div className="w-100">
                  <div className="d-flex align-items-start mb-2 justify-content-between">
                    <div className="icon">
                      <Icon className="mr-2" component={examOverview} style={{fontSize: 32}}/>
                    </div>
                    <div className="flex-grow-1 d-flex">
                      <div className='flex-grow-1 flex-column'>
                        <h5 className="mb-0">{item?.exam?.name ?? ''}</h5>
                        <div className='w-100'>
                          {/*<div style={{*/}
                          {/*  width: "100%",*/}
                          {/*  overflow: "hidden",*/}
                          {/*  textOverflow: "ellipsis",*/}
                          {/*}}>*/}
                          {/*  {item?.exam?.description ?? ''}*/}
                          {/*</div>*/}
                          <Typography.Paragraph
                            ellipsis={{
                              rows: 2,
                            }}
                          >
                            {item?.exam?.description ?? ''}
                          </Typography.Paragraph>
                        </div>
                      </div>
                    </div>
                    <div>
                      <Link href={`/exam/${item?.exam?.code}`} hrefPass>
                        <Button type="text" icon={<ArrowRightOutlined/>} size="small"/>
                      </Link>
                    </div>
                  </div>
                  <div>
                    <div className="description">
                      <div>
                        <Tooltip title={'Correct' + ` ${item?.nCorrect}/ ${item?.exam?.totalQuestions}`}>
                          <Progress percent={(item?.nCorrect / item?.exam?.totalQuestions * 100) ?? 0} size="small"/>
                        </Tooltip>
                      </div>
                    </div>
                  </div>
                </div>
              </List.Item>
            )}
          />
        </Card>
      </Col>
      <Col xs={24} sm={24} lg={{span: 14, order: 1}} xl={14} xxl={14}>
        <UploadStatusCard updatePostList={setPostList}/>
        {
          postList.length > 0
          &&
          <Card
            bordered={false}
            bodyStyle={{
              padding: 0
            }}
          >
            <div
              onScroll={(e) => {
                const { offsetHeight, scrollTop, scrollHeight } = e.target;
                if (Math.ceil(offsetHeight + scrollTop + 1) >= scrollHeight && hasNextPage && !loadMore) {
                  loadPost().then(_ => {});
                }
              }}

              // className='custom-scroll-bar'
              // style={{maxHeight: 730, overflow: 'auto'}}
            >
              {postList.map((elm, i) => (
                <PostCard hasDivider={postList.length > i + 1} updatePostList={setPostList} key={elm._id} data={elm}/>
              ))}
              {loadMore && <>
                <Skeleton avatar paragraph={{rows: 4}} active/>
                <Skeleton avatar paragraph={{rows: 4}} active/>
              </>}
            </div>
          </Card>
        }
        {
          !loading && postList.length === 0
          &&
          <Card
            bordered={false}
          >
            <Empty description='Post not found'/>
          </Card>
        }

        {loading && <Card
          title='Posts'
          bordered={false}
        >
          <Skeleton avatar paragraph={{rows: 4}}/>
          <Skeleton avatar paragraph={{rows: 4}}/>
        </Card>}
      </Col>
    </Row>
  )
}
export default ActivityTabs;
