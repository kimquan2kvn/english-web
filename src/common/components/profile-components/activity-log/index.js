import React, {useState} from "react";
import {Avatar, Button, Card, Divider, Empty, notification, Skeleton, Typography} from "antd";
import {UserOutlined} from "@ant-design/icons";
import ApiService from "../../../services/ApiService";
import Link from 'next/link';
import moment from "moment";
import {useSelector} from "react-redux";

const ActivityHistoryTabs = props => {
  const userData = useSelector(state => state.user);
  const {data, user} = props;
  const [loading, setLoading] = useState(false);
  const [recentActivities, setRecentActivities] = useState(data?.results ?? []);
  const [nextPage, setNextPage] = useState(data?.page + 1 ?? 0);
  const [hasNextPage, setHasNextPage] = useState((data.page && data.totalPages) ? data.page < data.totalPages : false);

  const loadRecentActivities = async () => {
    setLoading(true);
    try {
      const res = await ApiService.getRecentActivities({limit: 10, page: nextPage, sortBy: '-createdAt'});
      const {results, page, totalPages} = res.data;
      setRecentActivities(state => [...state, ...results]);
      setHasNextPage(page < totalPages);
      setNextPage(page + 1);
    } catch (err) {
      notification.error({message: 'Failed to load recent activities'});
    }
    setLoading(false);
  }

  window.onscroll = function () {
    if (window.scrollY + 3 + document.body.offsetHeight >= document.body.scrollHeight) {
      if (!loading && hasNextPage) {
        loadRecentActivities().then(_ => {
        });
      }
    }
  }

  const renderMessage = (text) => {
    return (
      <span>
        <span style={{color: '#333', fontWeight: 600, marginRight: 3}}>
          {userData.displayName}
        </span>
        <span>
          {text.slice(userData.displayName.length)}
        </span>
      </span>
    )
  }

  const getPath = (data) => {
    const {model, target} = data;
    if (!model || !target) return '/';
    switch (model) {
      case 'Blog':
        return '/news/' + target?.slug ?? '';
      case 'CommunityQuestion':
        return '/forum/question/' + target?.slug ?? '';
      case 'Exam':
        return '/exam/' + target?.slug ?? '';
      case 'ProfilePost':
        return '/profile/' + target?.username ?? '';
      default:
        return '/'
    }
  }

  return (
    <div className="history-activity">
      <Card bordered={false} className="px-4">
        <h4 className="mb-4">History Activities</h4>
        {
          recentActivities && recentActivities.length > 0
            ?
            recentActivities.map((elm, i) => (
              <Link href={getPath(elm)} key={elm._id}>
                <a>
                  <div className="activity-history-items cursor-pointer" key={elm._id + "_" + i}>
                    <div className="d-flex  justify-content-between">
                      <div className="d-flex align-items-start" style={{width: '70%'}}>
                        <div className="avatar mr-3">
                          <Avatar icon={<UserOutlined/>} src={user?.avatar} shape="circle"/>
                        </div>
                        <div style={{maxWidth: '90%'}}>
                          <div>
                            <div>
                              <p className="font-weight-semibold mb-0">{renderMessage(elm.message)} : </p>
                              <p
                                style={{
                                  margin: 0,
                                  whiteSpace: 'nowrap',
                                  overflow: 'hidden',
                                  textOverflow: 'ellipsis'
                                }}
                              >
                                {elm.detail}
                              </p>
                            </div>
                            <span className="text-gray-light">{}</span>
                          </div>
                        </div>
                      </div>
                      <div className="mb-2">
                        <p className="text-gray-light mb-0" style={{
                          whiteSpace: 'nowrap',
                          clear: 'both'
                        }}>{moment(elm.createdAt).format('HH:mm:ss a')}</p>
                        <p className="text-gray-light mb-0" style={{
                          whiteSpace: 'nowrap',
                          clear: 'both'
                        }}>{moment(elm.createdAt).format('DD/MM/YYYY')}</p>
                      </div>
                    </div>
                    <Divider type="dashed" className="mb-3"/>
                  </div>
                </a>
              </Link>
            ))
            :
            <Empty/>
        }
        {
          loading &&
          <div>
            <div className='mb-2'>
              <Skeleton avatar paragraph={{rows: 2}}/>
            </div>
            <Skeleton avatar paragraph={{rows: 2}}/>
          </div>
        }
      </Card>
    </div>
  )
}
export default ActivityHistoryTabs;
