import React, {useEffect, useState} from "react";
import {Button, Card, Col, Image, notification, Row, Upload} from "antd";
import {InboxOutlined} from "@ant-design/icons";
import UserForm from "../../ manage-components/user-form";
import axios from "axios";
import {updateUser} from "../../../redux/actions/User";
import {useSelector} from "react-redux";

const EDIT = "EDIT"
const VIEW = "VIEW"


const UserInformationTabs = props => {
  const {user} = props.data;
  const [imagePreview, setImagePreview] = useState(user?.avatar);
  const [fileImage, setFileImage] = useState(null);
  const {user: userData} = useSelector(state => state);
  const [mode, setMode] = useState(EDIT)
  const isView = userData?.username !== user.username;
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (isView) setMode(VIEW)
  }, [])

  const onChange = async (e) => {
    let file = e.file;
    setFileImage(file);
    let src = file.url;
    if (!src) {
      src = await new Promise(resolve => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    setImagePreview(src)
  };

  return (
    <Card className="px-4" bordered={false}>
      <div className="mb-3">
        <h4>Information User</h4>
      </div>
      <Row gutter={32}>
        <Col xs={24} sm={24} md={24} lg={9} xl={9} xxl={9}>
          {userData?.username === user.username ? (
            <div>
              <div className="mb-3">
                <Upload.Dragger
                  className="mb-3"
                  maxCount={1}
                  showUploadList={false}
                  onChange={onChange}
                >
                  {imagePreview ? (
                    <img src={imagePreview} alt="avatar" className="img-fluid"/>
                  ) : (
                    <div>
                      <p className="ant-upload-drag-icon">
                        <InboxOutlined/>
                      </p>
                      <p className="ant-upload-text">Click/drag image file here.</p>
                    </div>
                  )}
                </Upload.Dragger>
                <div className='d-flex justify-content-center'>
                  <Button disabled={!fileImage} loading={loading} className="mr-2" type="primary" onClick={async () => {
                    try {
                      setLoading(true);
                      const formData = new FormData();
                      formData.append('file', fileImage.originFileObj);
                      const res = await axios.post('/api/users/update/' + user.username, formData, {
                        headers: {
                          'Content-Type': 'multipart/form-data',
                        }
                      })
                      if (res.status === 200) {
                        updateUser({...res.data.user})
                        notification.success({message: 'Upload avatar successfully'});
                      }
                    } catch (err) {
                      console.log(err)
                    }
                    setFileImage(null);
                    setLoading(false);
                  }}>Change Avatar</Button>
                </div>
              </div>
            </div>

          ) : (
            <div>
              <Image src={user.avatar} alt="avatar" className="img-fluid" style={{borderRadius: "4px"}}/>
            </div>
          )}
        </Col>
        <Col xs={24} sm={24} md={24} lg={15} xl={15} xxl={15}>
          <UserForm user={user} mode={mode}/>
        </Col>
      </Row>
    </Card>
  )
}
export default UserInformationTabs
