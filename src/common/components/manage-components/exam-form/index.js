import React, {useEffect, useState} from "react";
import {Button, Col, DatePicker, Form, Input, InputNumber, notification, Row, Select, TimePicker} from "antd";
import moment from "moment";
import ApiService from "../../../services/ApiService";
import {QuestionMapConstant} from "../../../constants/QuestionMapConstant";
import Utils from "../../../utils";

const ADD = 'ADD'
const EDIT = 'EDIT'

const initialQuestionMap = QuestionMapConstant.reduce((obj, item) => (obj[item.type] = item.count, obj), {});
const initTotalQuestion = QuestionMapConstant.reduce((previousValue, currentValue) => previousValue + currentValue.count, 0)
const ExamForm = props => {
  const {mode = ADD, param} = props;
  const [loading, setLoading] = useState(false);
  const [exam, setExam] = useState(props.exam || {});
  const [totalQuestion, setTotalQuestion] = useState(initTotalQuestion)
  const [form] = Form.useForm();

  useEffect(() => {
    if (mode === EDIT && JSON.stringify(exam) !== "{}") {
      form.setFieldsValue({
        ...exam,
        startDate: exam.startDate ? moment(parseInt(exam?.startDate)) : undefined,
        endDate: exam.endDate ? moment(parseInt(exam?.endDate)) : undefined,
        duration: moment.utc(moment.duration(exam?.duration, 'seconds').asMilliseconds())
      })
    }
  }, [exam])

  const onSubmit = async (values) => {
    setLoading(true)
    if (mode === ADD) {
      const {questionMap} = values;
      const arr = Object.keys(questionMap);
      const newArr = arr.map((a, index) => ({
        type: arr[index],
        count: questionMap[a]
      }))
      try {
        const res = await ApiService.addExam({
          ...values,
          endDate: values?.endDate?.format("x") ?? null,
          startDate: values?.startDate?.format("x") ?? null,
          duration: moment.duration(values?.duration?.format("HH:mm:ss")).asSeconds(),
          questionMap: newArr
        })
        if (res.status === 200) {
          setLoading(false)
          form.resetFields();
          notification.success({
            message: res.data.message
          })
        }
      } catch (err) {
        setLoading(false)
        console.log(err)
      }
    }
    if (mode === EDIT) {
      try {
        const res = await ApiService.updateExam(exam.code, {
          ...values,
          endDate: values?.endDate?.format("x") ?? null,
          startDate: values?.startDate?.format("x") ?? null,
          duration: moment.duration(values?.duration?.format("HH:mm:ss")).asSeconds(),
        })
        if (res.status === 200) {
          setLoading(false)
          setExam(res.data.exam)
          notification.success({
            message: res.data.message
          })
        }
      } catch (err) {
        setLoading(false);
        console.log(err)
      }
    }
  }

  return (
    <Form
      layout="vertical"
      form={form}
      onFinish={onSubmit}
      initialValues={mode === ADD ? {
        questionMap: {...initialQuestionMap},
        duration: moment('1:00:00', 'HH:mm:ss'),
        privacy: "close"
      } : {}}
      onFieldsChange={(value) => {
        if (value[0].name.includes("questionMap")) {
          let total = 0;
          for (const question in form.getFieldValue("questionMap")) {
            total = total + form.getFieldValue("questionMap")[question]
          }
          setTotalQuestion(total)
        }
      }}
    >
      <Row gutter={16}>
        <Col span={24}>
          <Form.Item
            name="name"
            label={Utils.translate('label.name')}
            rules={[{required: true, message: Utils.translate('label.warn.input.exam.name')}]}
          >
            <Input placeholder={Utils.translate('label.input.exam.name')}/>
          </Form.Item>
        </Col>
      </Row>
      {mode === EDIT && (
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              name="code"
              label={Utils.translate('label.code')}
            >
              <Input disabled={mode === EDIT}/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="totalQuestions"
              label={Utils.translate('label.total.question')}>
              <Input disabled={mode === EDIT}/>
            </Form.Item>
          </Col>
        </Row>
      )}
      <Row gutter={16}>
        <Col span={12}>
          <Form.Item
            name="startDate"
            label={Utils.translate('label.start.date')}>
            <DatePicker showTime placeholder={Utils.translate('label.select.time')} style={{width: "100%"}}/>
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            name="endDate"
            label={Utils.translate('label.end.date')}>
            <DatePicker showTime placeholder={Utils.translate('label.select.time')} style={{width: "100%"}}/>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={12}>
          <Form.Item
            name="duration"
            label={Utils.translate('label.time')}
            rules={[{required: true, message: "Please choose limit time"}]}
          >
            <TimePicker style={{
              width: "100%"
            }}/>
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            name="privacy"
            label={Utils.translate('label.status')}
            rules={[{required: true, message: 'Please choose the privacy'}]}
          >
            <Select defaultValue="open">
              <Select.Option value="open">{Utils.translate('label.open')}</Select.Option>
              <Select.Option value="close">{Utils.translate('label.close')}</Select.Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
      {mode === ADD && (
        <div>
          <div style={{marginBottom: "24px"}}>
            <label style={{
              color: "#1a3353",
              fontSize: "14px",
              fontWeight: "500"
            }}>Choose count of each question type</label>
          </div>

          <Form.List name="questionMap">
            {(fields => (
              <div>
                <Row gutter={16} className="justify-content-between">
                  <Col xs={24} sm={12} md={12} lg={6} xxl={4}>
                    <Form.Item
                      name={"doc_hieu"}
                      label="Reading"
                      rules={[{required: true, message: Utils.translate('label.warn.input.count.question')}]}
                    >
                      <InputNumber min={1} max={14}/>
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={6} xxl={4}>
                    <Form.Item
                      name={"dien_tu"}
                      label="Fill Gap"
                      rules={[{required: true, message: Utils.translate('label.warn.input.count.question')}]}
                    >
                      <InputNumber min={1} max={10}/>
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={6} xxl={4}>
                    <Form.Item
                      name={"trong_am"}
                      label="Word Stress"
                      rules={[{required: true, message: Utils.translate('label.warn.input.count.question')}]}
                    >
                      <InputNumber min={1} max={5}/>
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={6} xxl={4}>
                    <Form.Item
                      name={"phat_am"}
                      label="Pronounce"
                      rules={[{required: true, message: Utils.translate('label.warn.input.count.question')}]}
                    >
                      <InputNumber min={1} max={5}/>
                    </Form.Item>
                  </Col>
                </Row>

                <Row gutter={16} className="justify-content-between">
                  <Col xs={24} sm={12} md={12} lg={6} xxl={4}>
                    <Form.Item
                      name={"ngu_phap"}
                      label={Utils.translate('label.grammar')}
                      rules={[{required: true, message: Utils.translate('label.warn.input.count.question')}]}
                    >
                      <InputNumber min={1} max={30}/>
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={6} xxl={4}>
                    <Form.Item
                      name={["dong_nghia"]}
                      label={Utils.translate('label.synonymous.word')}
                      rules={[{required: true, message: Utils.translate('label.warn.input.count.question')}]}
                    >
                      <InputNumber min={1} max={5}/>
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={6} xxl={4}>
                    <Form.Item
                      name={"trai_nghia"}
                      label={Utils.translate('label.opposite.word')}
                      rules={[{required: true, message: Utils.translate('label.warn.input.count.question')}]}
                    >
                      <InputNumber min={1} max={5}/>
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={6} xxl={4}>
                    <Form.Item
                      name={["giao_tiep"]}
                      label={Utils.translate('label.communication')}
                      rules={[{required: true, message: Utils.translate('label.warn.input.count.question')}]}

                    >
                      <InputNumber min={1} max={5}/>
                    </Form.Item>
                  </Col>
                </Row>
                <Row ggutter={16} className="justify-content-between">
                  <Col xs={24} sm={12} md={12} lg={6} xxl={4}>
                    <Form.Item
                      name={"tim_loi_sai"}
                      label={Utils.translate('label.finding.mistakes')}
                      rules={[{required: true, message: Utils.translate('label.warn.input.count.question')}]}
                    >
                      <InputNumber min={1} max={5}/>
                    </Form.Item>
                  </Col>
                </Row>
              </div>
            ))}
          </Form.List>

          <div style={{marginBottom: "24px"}}>
            <label style={{
              color: "#1a3353",
              fontSize: "14px",
              fontWeight: "500"
            }}>Total Questions: {totalQuestion} </label>
          </div>
        </div>
      )}

      <Row gutter={16}>
        <Col span={24}>
          <Form.Item
            name="description"
            label={Utils.translate('label.description')}
            rules={[
              {
                required: true,
                message: Utils.translate('label.warn.input.url.description')
              },
            ]}
          >
            <Input.TextArea rows={4} placeholder={Utils.translate('label.input.url.description')}/>
          </Form.Item>
        </Col>
      </Row>
      <div className="text-right">
        <Button type="primary" htmlType="submit" loading={loading}>
          {mode === ADD ? Utils.translate('label.add.exam') : Utils.translate('label.save.change')}
        </Button>
      </div>
    </Form>
  )
}
export default ExamForm;
