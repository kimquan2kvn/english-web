import React, {useEffect, useState} from "react";
import {Col, Divider, Form, Input, Radio, Row, Tooltip} from "antd";
import CustomModal from "../../util-components/CustomModal";
import EditorConfig from "../../../configs/EditorConfig";
import dynamic from "next/dynamic";
import QuestionEdit from "../question-edit";
import Utils from "../../../utils";


const ReactQuill = dynamic(() => import("react-quill"), {ssr: false});
const ADD = "ADD";
const EDIT = "EDIT";
const InputQuestion = props => {
  const {mode = ADD, questionData = {}} = props
  const {data} = questionData
  const [visible, setVisible] = useState(false);
  const [title, setTitle] = useState(null);
  const [question, setQuestion] = useState({
    question: "",
    choices: [],
    correct: "",
    explanation: ""
  })

  useEffect(() => {
    if (mode === EDIT) {
      const {answers, correct, ...rest} = data;
      const newQ = {...rest, correct: Utils.convertAnswerToNumber(data?.correct), choices: answers}
      setQuestion(newQ)
    }
  }, [mode, data])

  const chooseAnswer = (correct) => {
    const newQ = {...question, correct: Utils.convertNumberToAnswer(correct)}
    const newQuestion = {...question, correct: correct}
    setQuestion(newQuestion)
    props.onChange(newQ)
  }

  const changeQuestion = (html) => {
    const newQuestion = {...question, question: html}
    setQuestion(newQuestion)
    props.onChange(newQuestion)
  }

  const changeExplanation = (html) => {
    const newQuestion = {...question, explanation: html}
    setQuestion(newQuestion)
    props.onChange(newQuestion)
  }

  const changeAnswer = (index, html) => {
    const {choices} = question
    choices[index] = html;
    const newQuestion = {...question, choices: [...choices]}
    setQuestion(newQuestion)
    props.onChange(newQuestion)
  }

  return (
    <div>
      <div>
        <span
          className="font-weight-bold mb-3 d-block">{props.noChoices ? Utils.translate('label.paragraph') : Utils.translate('label.question') + props?.number ?? ""}</span>
        <Form.Item
          name="paragraph"
          rules={[{required: true, message: Utils.translate('label.warn.input.paragraph')}]}
        >
          <QuestionEdit
            mode={mode}
            title={props.noChoices ? Utils.translate('label.input.paragraph') : Utils.translate('label.input.question')}
            placeholder={mode === "ADD" ? (props.noChoices ? Utils.translate('label.input.paragraph.dot') : Utils.translate('label.input.question.dot')) : data?.question}
            onChange={(html) => changeQuestion(html)}
          />
        </Form.Item>
      </div>

      {!props.noChoices && (
        <div className="question-content">
          <div>
            <span className="font-weight-bold mb-3 d-block">{Utils.translate('label.answers')}</span>
            <Radio.Group className="w-100" value={question.correct}>
              <Row gutter={16}>
                {[0, 1, 2, 3].map(a => (
                  <Col xs={24} lg={12} key={a}>
                    <div className="d-flex mb-3">
                      <Tooltip title="Click to choose correct answer">
                        <Radio value={a} onClick={() => chooseAnswer(a)}/>
                      </Tooltip>
                      <div className="flex-grow-1">
                        <QuestionEdit
                          mode={mode}
                          title={`Answer ${Utils.convertNumberToAnswer(a).toUpperCase()}`}
                          placeholder={(mode === EDIT) ? question.choices[a] : Utils.translate('label.input.answer')}
                          onChange={(html) => changeAnswer(a, html)}
                        />
                      </div>
                    </div>
                  </Col>
                ))}
              </Row>
            </Radio.Group>
          </div>
          <div className="mb-3">
            <span className="font-weight-bold mb-3 d-block">{Utils.translate('label.explanation')}</span>
            <Form.Item
              name="explanation"
              rules={[{required: true, message: Utils.translate('label.warn.input.explanation')}]}
            >
              <QuestionEdit
                mode={mode}
                title={Utils.translate('label.explanation')}
                placeholder={mode === EDIT ? data?.explanation : Utils.translate('label.input.explanation')}
                onChange={(html) => changeExplanation(html)}
              />
            </Form.Item>
          </div>
          <Divider type="dashed" className="mb-3"/>
        </div>
      )}
      <CustomModal footer={null} title={title} visible={visible} onCancel={() => setVisible(false)}>
        <ReactQuill value='' theme="snow" modules={EditorConfig}/>
      </CustomModal>
    </div>
  )
}

export default InputQuestion
