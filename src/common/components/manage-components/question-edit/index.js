import React, {useEffect, useState} from "react";
import EditorConfig from "../../../configs/EditorConfig";
import {Button, Card} from "antd";
import dynamic from "next/dynamic";
import CustomModal from "../../util-components/CustomModal";
import Utils from "../../../utils";

const ReactQuill = dynamic(() => import("react-quill"), {ssr: false});

const ADD = "ADD";
const EDIT = "EDIT";

const QuestionEdit = props => {
  const {mode = ADD, placeholder = ""} = props
  const [showModal, setShowModal] = useState(false);
  const [currentHTML, setCurrentHTML] = useState("");

  useEffect(() => {
    if (mode === EDIT) {
      setCurrentHTML(placeholder)
    }
  }, [placeholder])

  return (
    <div>
      <Card type="inner" title={props.title} extra={
        <div className="d-flex">
          <Button type="primary" style={{display: "inline-block", marginLeft: 8}} onClick={() => {
           setTimeout(()=> {
             setShowModal(true)
           }, 200)
          }} size="small">{Utils.translate('label.edit')}</Button>
        </div>
      }>
        <div
          dangerouslySetInnerHTML={{__html: currentHTML !== "" ? currentHTML :  placeholder}}>
        </div>
      </Card>
      <CustomModal
        title={Utils.translate('label.edit.content')} visible={showModal}
        onCancel={() => {
          setShowModal(false);
        }}
        onOk={() => {

          props.onChange(currentHTML)
          setShowModal(false)
        }}
        width={700}
      >
        <ReactQuill value={currentHTML} theme="snow" modules={EditorConfig}
                    onChange={(value) => setCurrentHTML(value)}/>
      </CustomModal>
    </div>
  )
}
export default QuestionEdit;
