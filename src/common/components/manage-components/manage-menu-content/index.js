import React, {useEffect} from "react";
import {Menu} from "antd";
import {
  ProfileOutlined, MedicineBoxOutlined, UserAddOutlined,
  ContainerOutlined, FileAddOutlined, FileDoneOutlined, UserOutlined,
  QuestionCircleOutlined, PlusCircleOutlined, DeploymentUnitOutlined, WarningOutlined, LockOutlined
} from "@ant-design/icons"
import Link from "next/link"
import {useRouter} from "next/router";
import Utils from "../../../utils";


const {SubMenu} = Menu

const ManageMenuContent = () => {
  const router = useRouter();
  return (
    <Menu
      defaultSelectedKeys={router.pathname}
      mode="inline"
      selectedKeys={router.pathname}
    >
      <SubMenu
        key={`manage/user`}
        title={
          <span>
              <UserOutlined/>
              <span>{Utils.translate('label.manage.user')}</span>
            </span>
        }
      >
        <Menu.Item key={`/manage/user`}>
          <Link href={`/manage/user`} passHref>
            <a className="d-block h-100">
              <UserOutlined/>
              <span>{Utils.translate('label.user.list')}</span>
            </a>
          </Link>
        </Menu.Item>
        <Menu.Item key={`/manage/user/add`} passHref>
          <Link href={`/manage/user/add`} passHref>
           <a className="d-block h-100">
              <UserAddOutlined/>
              <span>{Utils.translate('label.add.user')}</span>
            </a>
          </Link>
        </Menu.Item>
      </SubMenu>
      <Menu.Item key={`/manage/role`}>
        <Link href={`/manage/role`} passHref>
         <a className="d-block h-100">
            <LockOutlined/>
            <span>{Utils.translate('label.manage.role')}</span>
          </a>
        </Link>
      </Menu.Item>
      <SubMenu
        key={`/manage-exam`}
        title={
          <span>
              <FileDoneOutlined/>
              <span>{Utils.translate('label.manage.exam')}</span>
            </span>
        }
      >
        <Menu.Item key={`/manage/exam`}>
          <Link href={`/manage/exam`} passHref>
           <a className="d-block h-100">
              <ContainerOutlined/>
              <span>{Utils.translate('label.exam.list')}</span>
            </a>
          </Link>
        </Menu.Item>
        <Menu.Item key={`/manage/exam/add`}>
          <Link href={`/manage/exam/add`}>
           <a className="d-block h-100">
              <FileAddOutlined/>
              <span>{Utils.translate('label.add.exam')}</span>
            </a>
          </Link>
        </Menu.Item>
      </SubMenu>
      <SubMenu
        key={`manage/question`}
        title={
          <span>
             <QuestionCircleOutlined/>
              <span>{Utils.translate('label.manage.question')}</span>
            </span>
        }
      >
        <Menu.Item key={`/manage/question`}>
          <Link href={`/manage/question`}>
           <a className="d-block h-100">
              <QuestionCircleOutlined/>
              <span>{Utils.translate('label.question.list')}</span>
            </a>
          </Link>
        </Menu.Item>
        <Menu.Item key={`/manage/question/add`}>
          <Link href={`/manage/question/add`}>
           <a className="d-block h-100">
              <PlusCircleOutlined/>
              <span>{Utils.translate('label.add.question.2')}</span>
            </a>
          </Link>
        </Menu.Item>
      </SubMenu>
      <SubMenu
        key={`manage/news`}
        title={
          <span>
             <ProfileOutlined/>
              <span>{Utils.translate('label.manage.blog')}</span>
            </span>
        }
      >
        <Menu.Item key={`/manage/news`}>
          <Link href={`/manage/news`}>
           <a className="d-block h-100">
              <ProfileOutlined/>
              <span>{Utils.translate('label.blog.list')}</span>
            </a>
          </Link>
        </Menu.Item>
        <Menu.Item key={`/manage/news/add`}>
          <Link href={`/manage/news/add`}>
           <a className="d-block h-100">
              <MedicineBoxOutlined/>
              <span>{Utils.translate('label.add.blog')}</span>
            </a>
          </Link>
        </Menu.Item>
      </SubMenu>
      <Menu.Item key={`/manage/community`}>
        <Link href={`/manage/community`}>
         <a className="d-block h-100">
            <DeploymentUnitOutlined/>
            <span>{Utils.translate('label.manage.community')}</span>
          </a>
        </Link>
      </Menu.Item>
      <Menu.Item key={`/manage/issue`}>
        <Link href={`/manage/issue`}>
         <a className="d-block h-100">
            <WarningOutlined/>
            <span>{Utils.translate('label.manage.issue')}</span>
          </a>
        </Link>
      </Menu.Item>
    </Menu>
  )
}
export default ManageMenuContent;
