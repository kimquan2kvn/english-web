import React, {useEffect} from "react";
import {Menu} from "antd";
import navigationConfig from "common/configs/NavigationConfig";
import {connect, useSelector} from "react-redux";
import IntlMessage from "../../util-components/IntlMessage";
import Link from "next/link"
import Utils from "../../../utils";

const {SubMenu} = Menu;

const setLocale = (isLocaleOn, localeKey) =>
  isLocaleOn ? <IntlMessage id={localeKey}/> : localeKey.toString();


const CustomMenuContent = (props) => {
  const {localization} = props;
  const user = useSelector(state => state.user);
  const isAdmin = user?.role?.name === 'admin';
  const {locale} = useSelector(state => state.theme)

  return (
    <Menu mode="horizontal" className="custom-top-nav">
      {(isAdmin ? navigationConfig : navigationConfig.filter(x => x.key !== 'manage')).map((menu, i) =>
        menu.submenu.length > 0 ? (
          <SubMenu
            key={menu.key + `${i}`}
            popupClassName="top-nav-menu"
            selectedKeys={[menu.key]}
            title={
              <span>
                <span>{setLocale(localization, menu.title)}</span>
              </span>
            }
          >
            {menu.submenu.map((subMenuFirst, i) =>
              subMenuFirst.submenu.length > 0 ? (
                <SubMenu
                  selectedKeys={[subMenuFirst.key]}
                  key={subMenuFirst.key + `${i}`}
                  title={setLocale(localization, subMenuFirst.title)}
                >
                  {subMenuFirst.submenu.map((subMenuSecond, i) => (
                    <Menu.Item key={subMenuSecond.key + `${i}`}>
                      <Link href={subMenuSecond.path} passHref>
                        <a style={{display: 'block', height: '100%'}}>
                        <span>
                          {setLocale(localization, subMenuSecond.title)}
                        </span>
                        </a>
                      </Link>
                    </Menu.Item>
                  ))}
                </SubMenu>
              ) : (
                <Menu.Item key={subMenuFirst.key + `${i}`}>
                  <Link href={subMenuFirst.path} passHref>
                    <a>
                      <span>{setLocale(localization, subMenuFirst.title)}</span>
                    </a>
                  </Link>
                </Menu.Item>
              )
            )}
          </SubMenu>
        ) : (
          <li className="ant-menu-item ant-menu-item-only-child" key={menu + + `${i}`}>
            <Link href={menu.path ? menu.path : ""} className="py-3 px-4" prefetch={false} passHref>
              <a
                style={{
                  display: 'block',
                  height: '100%'
                }}
              >
                 <span style={{
                   textTransform: "capitalize",
                   fontSize: 14,
                   fontWeight: 600,
                   lineHeight: "1.6rem"
                 }}>
                   {Utils.translate(menu?.title)}
                   </span>
              </a>
            </Link>
          </li>
        )
      )}
    </Menu>
  );
}


const mapStateToProps = ({theme}) => {
  const {sideNavTheme, topNavColor} = theme;
  return {sideNavTheme, topNavColor};
};

export default  connect(mapStateToProps)(CustomMenuContent);
