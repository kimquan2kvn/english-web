import {
  RESET_QUESTION_STATE,
  RESET_USER_ANSWERS,
  SET_LOADING_STATE,
  SET_QUESTION_STATE,
  SET_USER_ANSWERS
} from "../constants/Exam";

const initialState = {
  loading: false,
  questionStateMap: {},
  userAnswers: []
}

const exam = (state = initialState, action) => {
  let questionMap = state.questionStateMap;
  switch (action.type) {
    case SET_QUESTION_STATE:
      const newQuestionMap = {...questionMap}
      newQuestionMap[action.payload.id] = action.payload.state;
      return {
        ...state,
        questionStateMap: newQuestionMap
      }
    case SET_USER_ANSWERS:
      return {
        ...state,
        userAnswers: action.payload
      }
    case RESET_USER_ANSWERS:
      return {
        ...state,
        userAnswers: []
      }
    case SET_LOADING_STATE:
      return {
        ...state,
        loading: action.payload
      }
    case RESET_QUESTION_STATE:
      return {
        ...state,
      }
    default:
      return {
        ...state,
      }
  }
}

export default exam

