import {combineReducers} from 'redux';
import Auth from './Auth';
import Theme from './Theme';
import User from './User';
import Exam from "./Exam"
import Tags from './Tags';

const reducers = combineReducers({
  theme: Theme,
  auth: Auth,
  user: User,
  exam: Exam,
  tags: Tags,
});

export default reducers;
