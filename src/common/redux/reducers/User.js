import {UPDATE_USER} from "../constants/User";


const initialState = {
  createdAt: 0,
  email: '',
  fullName: '',
  isDeleted: false,
  role: {},
  username: '',
  _id: '',
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case UPDATE_USER:
      return action.payload;

    default: return state;
  }
}

export default reducer;
