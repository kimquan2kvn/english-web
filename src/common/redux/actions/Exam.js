import {RESET_USER_ANSWERS, SET_LOADING_STATE, SET_QUESTION_STATE, SET_USER_ANSWERS} from "../constants/Exam";

export const setQuestionState = (id, state) => {
  return {
    type: SET_QUESTION_STATE,
    payload: {id, state}
  }
}

export const resetQuestionState = () => {
  return {
    type: RESET_USER_ANSWERS
  }
}

export const setUserAnswer = (answers) => {
  return {
    type: SET_USER_ANSWERS,
    payload: answers
  }
}

export const resetUserAnswers = () => {
  return {
    type: RESET_USER_ANSWERS
  }
}

export const setLoadingState = (state) => {
  return {
    type: SET_LOADING_STATE,
    payload: state
  }
}
