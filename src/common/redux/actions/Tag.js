import store from "../store";

export const updateTags = (data) => {
  return store.dispatch({
    type: 'UPDATE_TAG',
    payload: data
  })
}