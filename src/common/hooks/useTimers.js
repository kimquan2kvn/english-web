import {useEffect, useRef, useState} from 'react';

const useTimer = initial => {
  const [timer, setTimer] = useState(initial);
  const [isActive, setIsActive] = useState(false);
  const [isPaused, setIsPaused] = useState(false);
  const [isOver, setIsOver] = useState(false);
  const [isWarning, setIsWarning] = useState(false);
  const countRef = useRef(null);

  useEffect(() => {
    if (initial) {
      setTimer(initial);
    }
    return () => {
      clearInterval(countRef.current);
    };
  }, [initial]);


  useEffect(() => {
    if (timer === 6000) {
      handleWarning();
    }
    if (timer === 0) {
      clearInterval(countRef.current);
      setIsPaused(false);
      setIsWarning(false);
      setIsOver(true);
    }
  }, [timer]);


  const handleWarning = () => {
    setIsWarning(true);
  }


  const handleStart = () => {
    setIsActive(true);
    setIsPaused(true);
    countRef.current = setInterval(() => {
      setTimer(timer => timer - 1);
    }, 1000);
  };

  const handlePause = () => {
    setIsWarning(false)
    clearInterval(countRef.current);
    setIsPaused(false);
  };

  const handleResume = () => {
    setIsPaused(true);
    setIsWarning(false);
    countRef.current = setInterval(() => {
      setTimer(timer => timer - 1);
    }, 1000);
  };

  const handleReset = () => {
    clearInterval(countRef.current);
    setIsActive(false);
    setIsPaused(false);
    setTimer(0);
  };

  return {
    timer,
    isActive,
    isPaused,
    isOver,
    isWarning,
    handleStart,
    handlePause,
    handleResume,
    handleReset,
  };
};

export default useTimer;
