
export const AUTHENTICATED = 'AUTHENTICATED';
export const SIGNOUT = 'SIGNOUT';
export const SIGNUP = 'SIGNUP';
export const AUTH_TOKEN = 'auth_token'