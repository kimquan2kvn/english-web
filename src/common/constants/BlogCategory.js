export const categories = [
	{
		slug: "exam", title: "Exam"
	},
	{
		slug: "sharing", title: "Sharing"
	},
	{
		slug: "tip-tricks", title: "Tips & Tricks"
	},
	{
		slug: "tech", title: "Technology"
	},
	{
		slug: "lifestyle", title: "Lifestyle"
	},
]