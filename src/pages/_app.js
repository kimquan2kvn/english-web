import 'common/styles/globals.css'
import GlobalLayout from "../common/layouts/global-layout";
import {Provider} from "react-redux";
import store from "common/redux/store";
import {createWrapper} from 'next-redux-wrapper';
import NextNProgress from 'nextjs-progressbar';
import Head from 'next/head'
import LoadData from "common/boot/load-data/LoadData";
import {setCookiesData} from "common/services/StogareService";

function MyApp({Component, pageProps, router}) {
  const { route, asPath } = router;
  console.log(router)
  if(!['/auth/login', '/auth/register', '/auth/reset-password', '/error/404', '/error/403', '/auth/verify-email/[email]', '/auth/verify-email/check/[token]'].includes(route)) {
    setCookiesData('prePath', asPath, new Date().getTime() + 24 * 60 * 60 * 1000);
  }

  return (
    <>
      <Head>
        <link rel="stylesheet" href={"/css/index.css"}/>
        <title>English or Foolish</title>
      </Head>
      <NextNProgress
        height={4}
      />
      <Provider store={store}>
        <GlobalLayout>
          <LoadData>
            <Component {...pageProps} />
          </LoadData>
        </GlobalLayout>
      </Provider>
    </>
  );
}

const makeStore = () => store;
const wrapper = createWrapper(makeStore);

export default wrapper.withRedux(MyApp);
