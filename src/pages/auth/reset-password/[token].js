import React, {useState} from 'react';
import {Button, Card, Col, Form, Input, notification, Row} from "antd";
import {ROW_GUTTER} from "../../../common/constants/ThemeConstant";
import { useRouter } from 'next/router';
import axios from "axios";

const backgroundStyle = {
  backgroundImage: 'url(/img/others/img-17.jpg)',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover'
}

const ChangePassword = () => {
  const [form] = Form.useForm();
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const { token } = router.query;

  const requestChangePass = (password) => {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.post('/api/users/reset-password/' + token, {password});
        resolve(res);
      }
      catch(err) {
        reject(err);
      }
    })
  }

  const onChangePassword = async (values) => {
    const { password } = values;
    setLoading(true);
    try {
      const res = await requestChangePass(password);
      const { message } = res.data;
      await router.replace('/auth/login');
      notification.success({message});
    }
    catch(err) {
      const { message } = err.response.data;
      notification.error({message});
    }
    setLoading(false);
  }

  return (
    <div className="h-100" style={backgroundStyle}>
      <div className="container d-flex flex-column justify-content-center h-100">
        <Row gutter={ROW_GUTTER} justify="center">
          <Col xs={20} sm={20} md={20} lg={10}>
            <Card>
              <div className="my-4">
                <Row justify="center">
                  <Col xs={23} sm={23} md={23} lg={23}>
                    <div className="text-center mb-3">
                      <h3 className="font-weight-semibold">Change your password</h3>
                    </div>
                    <Form
                      layout="vertical"
                      form={form}
                      onFinish={onChangePassword}
                    >
                      <Form.Item
                        name="password"
                        hasFeedback
                        label="New password"
                        type={'password'}
                        rules={
                          [
                            {
                              required: true,
                              message: "Please input your email you registered account."
                            },
                            {
                              min: 6,
                            }
                          ]
                        }>
                        <Input className="w-100" placeholder="Input your email you registered account."/>
                      </Form.Item>
                      <Form.Item
                        type={'password'}
                        name="confirm-password"
                        label="Confirm password"
                        hasFeedback
                        rules={[
                          {
                            required: true,
                            message: 'Please confirm your password!',
                          },
                          ({getFieldValue}) => ({
                            validator(_, value) {
                              if (!value || getFieldValue('password') === value) {
                                return Promise.resolve();
                              }
                              return Promise.reject(new Error('The two passwords that you entered do not match!'));
                            },
                          }),
                        ]}
                      >
                        <Input className="w-100" placeholder="Input your email you registered account."/>
                      </Form.Item>
                      <div className="float-right">
                        <Button
                          type="primary"
                          onClick={() => form.submit()}
                          loading={loading}>{loading ? "Changing..." : "Change password"}</Button>
                      </div>
                    </Form>
                  </Col>
                </Row>
              </div>
            </Card>
          </Col>
        </Row>
      </div>
    </div>
  )
}

export default ChangePassword;