import React, {useState} from "react";
import {ROW_GUTTER} from "../../../common/constants/ThemeConstant";
import {Button, Card, Col, Form, Input, Modal, notification, Result, Row} from "antd";
import {useRouter} from "next/router";
import Image from "next/image";
import {useSelector} from "react-redux";
import Link from "next/link";
import axios from "axios";

const ForgotPassword = props => {
  const router = useRouter();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false)

  const sendEmail = async (values) => {
    const { email } = values;
    setLoading(true)
    try {
      const res = await axios.post('/api/email/reset-password', {email});
      const { message } = res.data;
      notification.success({message});
      setVisible(true);
    }
    catch(err) {
      const { message } = err.response.data;
      notification.error({message});
    }
    setLoading(false);
  }

  return (
    <div className="h-100" style={backgroundStyle}>
      <div className="container d-flex flex-column justify-content-center h-100">
        <Row gutter={ROW_GUTTER} justify="center">
          <Col xs={20} sm={20} md={20} lg={10}>
            <Card>
              <div className="my-4">
                <Row justify="center">
                  <Col xs={23} sm={23} md={23} lg={23}>
                    <div className="text-center mb-3">
                      <h3 className="font-weight-semibold">Forgot your password</h3>
                      <p>Do not worry! Resetting your password is easy! Just type your in the email you registered to the
                        English or Foolish.</p>
                    </div>
                    <Form
                      onFinish={sendEmail}
                      form={form}
                      layout="vertical"
                    >
                      <Form.Item
                        name="email"
                        label="Email"
                        rules={
                          [
                            {
                              required: true,
                              message: "Please input your email you registered account."
                            },
                            {
                              type: 'email',
                              message: 'Please enter a validations email!'
                            }
                          ]
                        }>
                        <Input className="w-100" placeholder="Input your email you registered account."/>
                      </Form.Item>
                      <div className="float-right">
                        <Button className="mr-2" disabled={loading} onClick={() => router.push("/auth/login")}>Not
                          you</Button>
                        <Button type="primary" onClick={() => form.submit()}
                                loading={loading}>{loading ? "Sending..." : "Send email"}</Button>
                      </div>
                    </Form>
                  </Col>
                </Row>
              </div>
            </Card>
          </Col>
        </Row>
      </div>
      {visible && (
        <Modal width={500} visible={visible} footer={null} onCancel={() => setVisible(false)}>
          <Result
            status="success"
            title={`Change password information has been sent to your email, Please check your mailbox`}
          />
        </Modal>
      )}
    </div>
  )
}

const backgroundStyle = {
  backgroundImage: 'url(/img/others/img-17.jpg)',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover'
}
export default ForgotPassword;
