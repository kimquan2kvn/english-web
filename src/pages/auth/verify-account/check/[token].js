import React, {useEffect} from 'react';
import {Col, notification, Row} from "antd";
import Image from "next/image";
import {LoadingOutlined} from "@ant-design/icons";
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import axios from "axios";
import {updateUser} from "../../../../common/redux/actions/User";

const backgroundStyle = {
  backgroundImage: 'url(/img/others/img-17.jpg)',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover'
}

const CheckTokenVerifyEmail = () => {
  const theme = useSelector(state => state.theme);
  const router = useRouter();
  const { token } = router.query;

  useEffect(() => {
    if(token) {
      (async () => {
        try {
          const res = await axios.post('/api/auth/verify-account/' + token);
          const { user } = res.data;
          updateUser(user);
          await router.replace('/');
          notification.success({message: `Welcome ${user.fullName}`});
        }
        catch(err) {
          setTimeout(() => {
            window.close();
          }, 1000)
          notification.error({message: 'Failed to verify'});
        }
      })()
    }
  }, [token]);


  return (
    <div className={`h-100 ${theme === 'light' ? 'bg-white' : ''}`}>
      <Row justify="center" className="align-items-stretch h-100">
        <Col xs={20} sm={20} md={24} lg={16}>
          <div className="container d-flex flex-column justify-content-center h-100 py-3">
            <div className='d-flex justify-content-center'>
              <div
                className='d-flex flex-column align-items-center'
              >
                <Image
                  src='/img/logo.png'
                  height={100}
                  width={130}
                />
                <div
                  style={{fontSize: 20 }}
                >
                  <LoadingOutlined className='mr-2'/>
                  <span>
                    Verifying email
                  </span>
                </div>
              </div>
            </div>
          </div>
        </Col>
        <Col xs={0} sm={0} md={0} lg={8}>
          <div className="d-flex flex-column justify-content-between h-100 px-4" style={backgroundStyle}>
            <div className="text-right">
              <img src="/img/logo-white.png" alt="logo"/>
            </div>
            <Row justify="center">
              <Col xs={0} sm={0} md={0} lg={20}>
                <img className="img-fluid mb-5" src="/img/others/img-19.png" alt=""/>
                <h1 className="text-white">Welcome to English of Foolish</h1>
                <p className="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ullamcorper
                  nisl erat, vel convallis elit fermentum pellentesque.</p>
              </Col>
            </Row>
            <div />
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default CheckTokenVerifyEmail;