import React, {useEffect, useState} from 'react';
import {Button, Col, notification, Row} from "antd";
import ApiService, {getLinkVerifyEmail} from "../../../common/services/ApiService";
import {useSelector} from "react-redux";
import Image from 'next/image';
import { useRouter } from 'next/router';
import {LoadingOutlined} from "@ant-design/icons";

const backgroundStyle = {
  backgroundImage: 'url(/img/others/img-17.jpg)',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover'
}

const VerifyAccount = () => {
  const router = useRouter();
  const [firstLoading, setFirstLoading] = useState(false);
  const theme = useSelector(state => state.theme.currentTheme);
  const [showResend, setShowResend] = useState(false);
  const [seconds, setSeconds] = useState(0);
  const { email } = router.query;

  const countDown = () => {
    if(seconds === 0) {
      setSeconds(120);
      const timer = setInterval(() => {
        setSeconds(state => {
          if(state > 0) {
            return state - 1;
          } else {
            clearInterval(timer);
            return 0;
          }
        })
      }, 1000);
    }else return;
  }

  const resendEmail = async () => {
    try {
      const res = await getLinkVerifyEmail(email);
      countDown();
    }
    catch(err) {
      const { message } = err.response.data;
      notification.error({message})
    }
  }

  useEffect(() => {
    if(email) resendEmail().then(_ => {});
  }, [email])

  return (
    <div className={`h-100 ${theme === 'light' ? 'bg-white' : ''}`}>
      <Row justify="center" className="align-items-stretch h-100">
        <Col xs={20} sm={20} md={24} lg={16}>
          <div className="container d-flex flex-column justify-content-center h-100 py-3">
            <div className='d-flex justify-content-center'>
              <div
                className='d-flex flex-column align-items-center'
              >
                <Image
                  src='/img/logo.png'
                  height={100}
                  width={130}
                />
                {
                  firstLoading
                    ?
                    <div>
                      <LoadingOutlined/>
                    </div>
                    :
                    <>
                      <p>
                        Check your email for account verification before logging in.
                      </p>
                      <div>
                        {
                          showResend
                            ?
                            <Button
                              size='small'
                              type='primary'
                              disabled={seconds > 0}
                              onClick={resendEmail}
                            >
                              Resend email
                              {seconds > 0 && ` after ${seconds} (s)`}
                            </Button>
                            :
                            <Button
                              type='link'
                              size='small'
                              onClick={() => {
                                setShowResend(true);
                              }}
                            >
                              Have not recieved the mail yet?
                            </Button>
                        }
                      </div>
                    </>
                }
              </div>
            </div>
          </div>
        </Col>
        <Col xs={0} sm={0} md={0} lg={8}>
          <div className="d-flex flex-column justify-content-between h-100 px-4" style={backgroundStyle}>
            <div className="text-right">
              <img src="/img/logo-white.png" alt="logo"/>
            </div>
            <Row justify="center">
              <Col xs={0} sm={0} md={0} lg={20}>
                <img className="img-fluid mb-5" src="/img/others/img-19.png" alt=""/>
                <h1 className="text-white">Welcome to English of Foolish</h1>
                <p className="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ullamcorper
                  nisl erat, vel convallis elit fermentum pellentesque.</p>
              </Col>
            </Row>
            <div />
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default VerifyAccount;