import React, {useState} from 'react'
import {BackTop, Button, Divider, Grid, Tabs} from "antd";
import {PageHeaderAlt} from "common/components/layout-components/PageHeaderAlt";
import ProfileCard from "common/components/profile-components/activity/profile-card";
import utils from "common/utils";
import ActivityTabs from "common/components/profile-components/activity/activity-tabs";
import CompleteExamTabs from "common/components/profile-components/complete-exam/complete-exam-tabs";
import UserInformationTabs from "common/components/profile-components/user-information-tabs";
import {AppLayout} from "common/layouts/app-layout";
import ActivityHistoryTabs from "common/components/profile-components/activity-log";
import {ArrowUpOutlined} from "@ant-design/icons";
import {useSelector} from "react-redux";

const {useBreakpoint} = Grid;
const Profile = props => {
  const user = useSelector(state => state.user);
  const {userData, completedExamData, recentActivitiesData} = props;
  const isMobile = !utils.getBreakPoint(useBreakpoint()).includes('md');
  const [tab, setTab] = useState('1');
  const avatarSize = 150;

  return (
    <AppLayout>
      <div className="profile">
        <PageHeaderAlt background="/img/others/img-12.jpg" cssClass="bg-primary" overlap>
          <div className="container text-center">
            <div className="py-5 my-md-5">
            </div>
          </div>
        </PageHeaderAlt>
        <div className="container">
          <ProfileCard
            userData={userData}
            avatarSize={avatarSize}
            completedExam={completedExamData?.result?.totalResults ?? 0}
            totalActions={recentActivitiesData.totalResults}
          />
          <div style={{marginTop: 30}} className={`tab-profile ${isMobile ? "tabs-mobile" : "tabs-desktop"}`}>
            <Tabs activeKey={tab} onChange={(tab) => setTab(tab)}>
              <Tabs.TabPane tab="Activity" key={1}>
                <ActivityTabs setTab={setTab} data={{user: userData, completedExamData}}/>
              </Tabs.TabPane>
              <Tabs.TabPane tab="Complete Exam" key={2}>
                <CompleteExamTabs data={completedExamData}/>
              </Tabs.TabPane>
              {
                user._id === userData._id && <Tabs.TabPane tab="Activity History" key={3}>
                  <ActivityHistoryTabs user={userData} data={recentActivitiesData}/>
                </Tabs.TabPane>
              }
              <Tabs.TabPane tab="User Information" key={4}>
                <UserInformationTabs data={{user: userData,}}/>
              </Tabs.TabPane>
            </Tabs>
          </div>
        </div>
        <BackTop>
          <Button
            shape='circle'
            type={'primary'}
          >
            < ArrowUpOutlined/>
          </Button>
        </BackTop>
      </div>
    </AppLayout>
  )
}

export const getServerSideProps = async (context) => {
  const auth = require("server/utils/auth");

  const {userId} = context.query;
  const {userService, examService, userActivityService} = require("server/services");
  let userData = null, completedExamData = null, recentActivitiesData = null;
  try {
    // const user = await auth(context);
    completedExamData = await examService.getDoneExams(userId, {}, {limit: 8, page: 1, sortBy: '-createdAt'});
    userData = await userService.getUserByFilter({username: userId});
    recentActivitiesData = await userActivityService.queryUserActivities(userData, {}, {
      page: 1,
      limit: 10,
      sortBy: '-createdAt',
    });
  } catch (err) {
    console.log(err)
    return {
      redirect: {
        destination: '/error/403',
        permanent: false,
      }
    }
  }
  const data = ({
    userData,
    completedExamData,
    recentActivitiesData,
  });
  return {
    props: JSON.parse(JSON.stringify(data))
  }
}

export default Profile;
