import React, {useEffect, useState} from "react";
import {Button, Card, Col, Row, Input, Grid, Empty, Select, Spin, BackTop, Skeleton} from "antd";
import Icon, {ArrowUpOutlined, QuestionCircleOutlined, SearchOutlined,} from "@ant-design/icons";
import {learningPeople} from "common/assets/svg/icon";
import utils from "common/utils";
import ExamCard from "common/components/exam-components/exam-card";
import {AppLayout} from "common/layouts/app-layout";
import RedirectConfig from "common/configs/RedirectConfig";
import {useExam} from "common/hooks/useRequest";
import {useSelector} from "react-redux";

const {useBreakpoint} = Grid;
const description = "Tuyển tập các đề thi THPT quốc gia môn tiếng Anh đã được cập nhật. Để làm quen với các dạng bài hay gặp trong đề thi, thử sức với các câu hỏi khó giành điểm 9 – 10 và có chiến lược thời gian làm bài thi phù hợp."

const Exam = props => {
  const user = useSelector(state => state["user"]);
  const isMobile = !utils.getBreakPoint(useBreakpoint()).includes('md')
  const [exams, setExams] = useState(props.exams || []);
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});
  const [options, setOptions] = useState({limit: 12, page: 1});
  const [shouldFetch, setShouldFetch] = useState(false);
  const {data, loading, error} = useExam(options, shouldFetch);
  const [value, setValue] = useState("name");

  useEffect(() => {
    if (data) {
      const {results, totalResults, totalPages, page} = data.data;
      const newExams = [...exams, ...results]
      setExams(newExams)
      setPageInfo({
        totalResults,
        hasNextPage: page < totalPages
      })
      setShouldFetch(false);
    }
    if (error) {
      setShouldFetch(false)
    }
  }, [options.page, data, error]) // eslint-disable-line react-hooks/exhaustive-deps

  const suffixSelector = (
    <Select
      value={value}
      onChange={(e) => setValue(e)}
    >
      <Select.Option value="name">Title</Select.Option>
      <Select.Option value="code">Code</Select.Option>
    </Select>
  );

  window.onscroll = function () {
    if (window.scrollY + 1 + document.body.offsetHeight >= document.body.scrollHeight) {
      if (exams.length > 0 && pageInfo.hasNextPage && !shouldFetch) {
        console.log('load')
        const nextPage = options.page + 1;
        setOptions({...options, page: nextPage})
        setShouldFetch(true)
      }
    }
  }

  return (
    <AppLayout>
      <div className="exam">
        <Card>
          <div className="d-flex justify-content-between">
            {!isMobile && (<div className="mr-4">
              <Icon component={learningPeople} style={{fontSize: 150}}/>
            </div>)}
            <div>
              <div className="mb-3 flex-1">
                <h3>Recent Exams</h3>
                <span>{description}</span>
              </div>
              <div className="d-flex justify-content-between flex-wrap">
                <div className="mb-3 mr-2">
                  <Button className="mr-3" type="primary" size="small"
                          icon={<QuestionCircleOutlined/>}>Tutorial</Button>
                  <Button type="default" size="small" icon={<SearchOutlined/>}>Join Exam</Button>
                </div>
                <Input.Search
                  size="small"
                  addonBefore={suffixSelector}
                  onChange={(e) => {
                    if (e.target.value === "") {
                      setShouldFetch(true);
                      setExams([]);
                      setOptions({limit: 12, page: 1})
                    }
                  }}
                  placeholder={value === "name" ? "Input exam title" : "Input exam code"}
                  onSearch={query => {
                    if (query === "") return;
                    setExams([])
                    setShouldFetch(true);
                    setOptions({page: 1, limit: 12, [value]: query})
                  }} enterButton style={{width: 250}}/>
              </div>
            </div>
          </div>
        </Card>
        {exams.length > 0 ? (
          <Row gutter={18}>
            {exams.map((exam, index) => (
              <Col xs={24} lg={12} xl={8} xxl={6} key={"exam" + index + exam._id}>
                <ExamCard exam={exam} key={exam._id}/>
              </Col>
            ))}
          </Row>
        ) : (
          <Spin spinning={loading && shouldFetch} tip="Loading...">
            <Card bordered={false}>
              <Empty/>
            </Card>
          </Spin>
        )}
        {shouldFetch && (exams.length > 0 && pageInfo.hasNextPage) && (
          <div>
            <p>Loading...</p>
            <Skeleton/>
          </div>
        )}
      </div>
    </AppLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {examService} = require("server/services");
  const connectDb = require('server/utils/connect-db');
  let exams = {}, user = null;
  try {
    if(ctx.req.cookies.hasOwnProperty('refresh_token')) {
      const data = await auth(ctx);
      user = data['user'];
    }else await connectDb();
    exams = await examService.queryExams({}, {page: 1, limit: 12}, user);

  } catch (err) {
    console.log(err)
    const {status} = err;
    return {
      redirect: RedirectConfig(status)
    }
  }
  if (!exams) return {props: {}}
  const {results, totalResults, totalPages, page} = exams
  return {
    props: {
      exams: JSON.parse(JSON.stringify(results)),
      pageInfo: {
        totalResults,
        hasNextPage: page < totalPages
      }
    }
  }
}

export default Exam;
