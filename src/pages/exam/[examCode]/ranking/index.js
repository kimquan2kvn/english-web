import React, {useEffect, useState} from "react";
import {Avatar, Button, Card, Pagination, Table, Tooltip, Grid, Spin} from "antd";
import Icon, {FlagOutlined, StarOutlined} from "@ant-design/icons";
import {AppLayout} from "common/layouts/app-layout";
import moment from "moment"
import RedirectConfig from "common/configs/RedirectConfig";
import {useRanking} from "common/hooks/useRequest";
import utils from "common/utils";
import CustomModal from "common/components/util-components/CustomModal";
import ReportResultCard from "common/components/exam-components/report-result-card";
import Link from "next/link"
import {onChangeShowPopup} from "../../../../common/redux/actions";
import {useSelector} from "react-redux";

const {useBreakpoint} = Grid
const description = "Table 1 shows the order of ranking of exam takers based on their scores and the time taken to take the exam."

const Ranking = props => {
  const user = useSelector(state => state.user);
  const [ranking, setRanking] = useState(props.ranking || 0);
  const {pageInfo = {}, examInfo = {}} = props
  const [shouldFetch, setShouldFetch] = useState(false);
  const isMobile = !utils.getBreakPoint(useBreakpoint()).includes('md');
  const [visible, setVisible] = useState(false);
  const [selectedResult, setSelectedResult] = useState({})
  const [options, setOptions] = useState({page: 1, limit: 10});
  const {data, error, loading} = useRanking(examInfo.code, options, shouldFetch);


  useEffect(() => {
    if (data && data.status === 200) {
      const {results = []} = data.data
      let newResult = results.map((result, index) => ({
        ...result,
        top: (options.page - 1) * options.limit + 1 + index
      }))
      setRanking(newResult)
    }
    if (error) {
      setShouldFetch(false)
    }
  }, [options.page, data, error]) // eslint-disable-line react-hooks/exhaustive-deps

  const columns = [
    {
      title: 'Top',
      dataIndex: "top",
      align: "center",
      render: record => {
        return <p className="mb-0">{record}</p>
      }
    },
    {
      title: 'Avatar',
      dataIndex: "user",
      align: "center",
      render: (record) =>  {
        console.log(record)
        return (
          (
            <Link href={"/profile/" + record.username}>
              <a className="cursor-pointer">
                <Avatar size={54} src={record.avatar} style={{
                  justifyContent: "center",
                }}/>
              </a>
            </Link>
          )
        )
      },
    },
    {
      title: 'Name',
      dataIndex: 'user',
      align: "center",
      render: record => {
        return (
          <>
            {record.displayName ? (
              <p className="mb-0">{record.displayName}</p>
            ) : <p className="mb-0">{record.fullName}</p>}
          </>
        )
      }
    },
    {
      title: 'User Name',
      dataIndex: 'user',
      align: "center",
      render: record => {
        return (
          <p className="mb-0">{record.username}</p>
        )
      }
    },
    {
      title: 'Correct/Total',
      dataIndex: 'correct',
      align: "center",
      // width: 200,
      render: (_, record) => {
        return (
          <p className="mb-0">{record?.nCorrect} / {record?.exam?.totalQuestions}</p>
        )
      }
    },
    {
      title: 'Duration',
      dataIndex: 'duration',
      align: "center",
      render: record => {
        let duration = moment().startOf('day').seconds(record).format("HH:mm:ss");
        return (
          <p className="mb-0">{duration}</p>
        )
      }
    },
    {
      title: 'Date',
      dataIndex: 'submittedAt',
      align: "center",
      render: record => {
        let submissionTime = moment(parseInt(record)).format("YYYY/MM/DD")
        return (
          <p className="mb-0">{submissionTime}</p>
        )
      }
    },
    {
      title: "Action",
      dataIndex: "Action",
      render: (_, record) => {
        return (
          <Tooltip>
            <Button
              size="small" icon={<FlagOutlined/>} danger
              onClick={() => {
                if(!user._id || user._id === '') return onChangeShowPopup(true);
                setSelectedResult(record);
                setVisible(true)
              }}
            />
          </Tooltip>
        )
      }
    }
  ];


  return (
    <AppLayout>
      <Card
        style={!isMobile ? {
          minHeight: 150,
          backgroundImage: `url(/img/others/ranking.jpg)`,
          backgroundSize: `40%`,
          backgroundRepeat: "no-repeat",
          backgroundPosition: "96% 45%"
        } : {}}
      >
        <div className="d-flex justify-content-between">
          <div>
            <div className="mb-3">
              <h3>Recent Exams</h3>
              <span>{description}</span>
            </div>
            <div className="d-flex">
              <Button disabled={!user._id || user._id === ''} className="mr-3" type="primary" size="small" icon={<StarOutlined/>}>Your ranking</Button>
            </div>
          </div>
        </div>
      </Card>
      <Card className="mt-3">
        <Spin spinning={loading && shouldFetch} tip="Loading....">
          <div className="table-responsive">
            <Table
              columns={columns} dataSource={ranking} pagination={false} rowKey='_id' footer={() => {
              return (
                <Pagination
                  showQuickJumper defaultCurrent={options?.page} total={pageInfo?.totalResults}
                  onChange={(page, pageSize) => {
                    setShouldFetch(true)
                    setOptions({...options, page: page});
                  }}
                />
              )
            }}
            />
          </div>
        </Spin>
      </Card>
      <CustomModal footer={null} title="Report result" visible={visible} onCancel={() => setVisible(false)}>
        <ReportResultCard
          selectedResult={selectedResult} onCancel={() => setVisible(false)}
        />
      </CustomModal>
    </AppLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const {examService} = require("server/services")
  const {Exam} = require("server/models");
  const connectDb = require('server/utils/connect-db');
  const {examCode} = ctx.query;
  let res = {};
  let examInfo = {};
  try {
    await connectDb();
    examInfo = await Exam.findOne({code: examCode}).catch(err => {
      throw ({
        status: 400,
        message: "Exam not found"
      })
    })
    res = await examService.getExamRanking(examCode, {}, {page: 1, limit: 10}).catch(err => {
      throw ({
        status: 400,
        message: "Exam not found"
      })
    })
  } catch (err) {
    console.log("err", err)
    const {status} = err
    return {
      redirect: RedirectConfig(status)
    }
  }
  if (!res || JSON.stringify(res) === "{}") return {props: {}}
  const {results = [], page, totalPages, totalResults} = res
  let newResult = results.map((result, index) => ({
    ...result["_doc"],
    top: index + 1
  }))
  return {
    props: {
      ranking: JSON.parse(JSON.stringify(newResult)),
      examInfo: JSON.parse(JSON.stringify(examInfo)),
      pageInfo: {
        totalResults,
        hasNextPage: page < totalPages
      }
    }
  }
}

export default Ranking;
