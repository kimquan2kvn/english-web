import React, {useState} from "react";
import {Avatar, Button, Col, Divider, notification, Row, Upload} from "antd";
import {UserOutlined, CloudUploadOutlined} from "@ant-design/icons";
import ImgCrop from "antd-img-crop";
import AccountSettingLayout from "../../../common/layouts/setting-account-layout";
import UserForm from "../../../common/components/ manage-components/user-form";
import Flex from "../../../common/components/shared-components/Flex";
import axios from "axios";
import {useSelector} from "react-redux";
import {updateUser} from "../../../common/redux/actions/User";
import RedirectConfig from "../../../common/configs/RedirectConfig";


const EditProfile = props => {
  const user = useSelector(state => state.user);
  const [loading, setLoading] = useState(false);

  return (
    <AccountSettingLayout>
      <div className="update-profile">
        <h4 className="mb-4">Edit User</h4>
        <Divider type="dashed" className="mb-4"/>
        <Row>
          <Col xs={24} sm={24} md={24} lg={8} xxl={8} className="mb-4">
            <Flex flexDirection="column" alignItems="center" mobileFlex={false} className="text-center text-md-left">
              <Avatar
                hape={"circle"}
                size={120}
                icon={<UserOutlined/>}
                src={user.avatar}
              />
              <div className="d-flex mt-4 justify-content-center">
                <ImgCrop rotate>
                  <Upload
                    showUploadList={false}
                    onChange={async (file) => {
                      if(loading) return;
                      setLoading(true);
                      try {
                        const formData = new FormData();
                        formData.append('file', file.file.originFileObj);
                        const res = await axios.post('/api/users/update-self-info', formData, {
                          headers: {
                            'Content-Type': 'multipart/form-data',
                          }
                        })
                        const {user} = res.data;
                        updateUser(user);
                        notification.success({message: 'Upload avatar successfully'});
                      } catch (err) {
                        console.log(err)
                        notification.error({message: 'Failed to upload avatar'})
                      }
                      setLoading(false);
                    }}
                  >
                    <Button
                      icon={<CloudUploadOutlined/>}
                      type="primary"
                      loading={loading}
                    >
                      Change Avatar
                    </Button>
                  </Upload>
                </ImgCrop>
              </div>
            </Flex>
          </Col>
          <Col xs={24} sm={24} md={24} lg={16} xxl={16}>
            <UserForm user={user} mode="EDIT"/>
          </Col>
        </Row>
      </div>
    </AccountSettingLayout>
  )
}

export const getServerSideProps = async (context) => {
  const auth = require("server/utils/auth");
  let user = {};
  try {
    user = ((await auth(context))).user._doc;
  } catch (err) {
    const {status, statusCode} = err;
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  return {
    props: {
      user: JSON.parse(JSON.stringify(user)),
    }
  }
}

export default EditProfile;
