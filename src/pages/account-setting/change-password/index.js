import React, {useState} from "react";
import {Col, Divider, Form, Row, Input, Button, notification, Spin} from "antd";
import AccountSettingLayout from "../../../common/layouts/setting-account-layout";
import ApiService from "../../../common/services/ApiService";
import {useSelector} from "react-redux";

const ChangePassword = props => {
  const user = useSelector(state => state.user);
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();

  const handleChangePassword = async (values) => {
    setLoading(true);
    const { password, new_password } = values;
    const data = {password: new_password, currentPassword: password};
    try {
      const res = await ApiService.changePassword(user.username, data);
      if(res.data) {
        notification.success({message: 'Changed password successfully'});
      }
    }
    catch(err) {
      notification.error({message: 'Failed to change password'});
    }
    setLoading(false);
    form.resetFields();
  }

  return (
    <AccountSettingLayout>
      <div className="change-password">
        <h4 className="mb-4">Edit User</h4>
        <Divider type="dashed" className="mb-4"/>
        <Row>
          <Col xs={24} sm={24} md={24} lg={14}>
            <Spin
              spinning={loading}
              tip='Changing password...'
            >
              <Form
                form={form}
                onFinish={handleChangePassword}
                name="changePasswordForm"
                layout="vertical"
              >
                <Form.Item
                  label="Current Password"
                  name="password"
                  rules={[{
                    required: true,
                    message: 'Please enter your current password!'
                  }]}
                >
                  <Input.Password/>
                </Form.Item>
                <Form.Item
                  label="New Password"
                  name="new_password"
                  rules={[{
                    required: true,
                    message: 'Please enter your new password!'
                  }]}
                >
                  <Input.Password/>
                </Form.Item>
                <Form.Item
                  label="Confirm Password"
                  name="confirmPassword"
                  rules={
                    [
                      {
                        required: true,
                        message: 'Please confirm your password!'
                      },
                      ({getFieldValue}) => ({
                        validator(rule, value) {
                          if (!value || getFieldValue('new_password') === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject('Password not matched!');
                        },
                      }),
                    ]
                  }
                >
                  <Input.Password/>
                </Form.Item>
                <Button type="primary" htmlType="submit">
                  Change password
                </Button>
              </Form>
            </Spin>
          </Col>
        </Row>
      </div>
    </AccountSettingLayout>
  )
}
export default ChangePassword;
