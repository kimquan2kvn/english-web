import React, {useEffect, useState} from "react";
import CommunityLayout from "common/layouts/community-layout";
import {Button, Card, Divider, Grid} from "antd";
import {PlusCircleOutlined} from "@ant-design/icons";
import QuestionList from "common/components/forum-components/question-list";
import ApiService from "common/services/ApiService";
import RedirectConfig from "common/configs/RedirectConfig";
import Utils from "../../common/utils";
import {useRouter} from "next/router";

const {useBreakpoint} = Grid;


const Community = (props) => {
  const router = useRouter();
  const {favoriteQuestions, popularQuestions, totalComQuestions} = props;
  const [totalQuestions, setTotalQuestions] = useState(0);
  const [questions, setQuestions] = useState([]);
  const [totalResults, setTotalResults] = useState(1);
  const [hasNextPage, setHasNextPage] = useState(false);
  const [loading, setLoading] = useState(false);
  const [loadedPage, setLoadedPage] = useState([1]);
  const [currentPage, setCurrentPage] = useState(1);
  const isMobile = !Utils.getBreakPoint(useBreakpoint()).includes('lg')

  useEffect(() => {
    const {results, totalPages, page, totalResults} = props.questionData;
    setQuestions(results);
    setHasNextPage(page < totalPages);
    if (page < totalPages) {
      setTotalResults(page * 10 + 10);
    }
    if (!loadedPage.includes(page)) setLoadedPage(state => [...state, page]);
    setTotalQuestions(totalResults);
  }, [props.questionData]) // eslint-disable-line react-hooks/exhaustive-deps

  const loadQuestions = async (nextPage) => {
    setLoading(true);
    try {
      const res = await ApiService.getQuestions({page: nextPage, limit: 10, sortBy: '-createdAt'});
      const {results, page, totalPages, totalResults} = res.data;
      const newQuestions = [...questions, ...results];
      setQuestions(newQuestions);
      setHasNextPage(page < totalPages);
      setLoadedPage(state => [...state, page]);
      if (page < totalPages) {
        setTotalResults(page * 10 + 10);
      }
      setTotalQuestions(totalResults);
    } catch (err) {
    }
    setLoading(false);
  }

  // useEffect(() => {
  //   loadQuestions(1).then(_ => {
  //   });
  // }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <CommunityLayout sideData={{popularQuestions, favoriteQuestions}} totalComQuestions={props.totalComQuestions}>
      <Card bordered={false} bodyStyle={{padding: "4px"}}>
        <div className="header-content mb-2">
          <div className="d-flex justify-content-between mb-4">
            <div className="d-flex align-items-center">
              <h2 className="mr-2 mb-0">All questions</h2>
              <p className="m-0">{`(${totalComQuestions})`}</p>
            </div>
            <Button onClick={() => {router.push('/forum/add-question')}} type="primary" size="small" icon={<PlusCircleOutlined/>}>{isMobile ? "Ask" : "Ask Question"}</Button>
          </div>
          <Divider type="dashed"/>
        </div>
        <div>
          <QuestionList
            {...props}
            setCurrentPage={setCurrentPage}
            currentPage={currentPage}
            setQuestions={setQuestions}
            questions={questions}
            totalResults={totalResults}
            hasNextPage={hasNextPage}
            loading={loading}
            loadedPage={loadedPage}
            loadQuestions={loadQuestions}
          />
        </div>
      </Card>
    </CommunityLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {communityQuestionService} = require("server/services");
  let data = {};
  try {
    const {
      favoriteQuestionsData,
      popularQuestionsData,
      totalComQuestions
    } = await Utils.getDataComQuestionLayout(auth, communityQuestionService, ctx);
    data['popularQuestions'] = popularQuestionsData?.results ?? [];
    data['favoriteQuestions'] = favoriteQuestionsData?.results ?? [];
    data['totalComQuestions'] = totalComQuestions?.totalResults ?? 0;
    data['questionData'] = await communityQuestionService.queryQuestions({deleted: {$ne: true}}, {
      limit: 10,
      page: 1,
      sortBy: '-createdAt'
    });
  } catch (err) {
    const {status, statusCode} = err;
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  const {favoriteQuestions, totalComQuestions, popularQuestions, questionData} = data;
  return {
    props: {
      totalComQuestions,
      favoriteQuestions,
      popularQuestions,
      questionData: JSON.parse(JSON.stringify(questionData))
    }
  }
}

export default Community;
