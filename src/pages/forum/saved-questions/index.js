import React, {useEffect, useState} from "react"
import {Button, Card, Divider} from "antd";
import {PlusCircleOutlined} from "@ant-design/icons";
import QuestionList from "../../../common/components/forum-components/question-list";
import CommunityLayout from "../../../common/layouts/community-layout";
import Utils from "../../../common/utils";
import RedirectConfig from "../../../common/configs/RedirectConfig";
import {useRouter} from "next/router";
import ApiService from "../../../common/services/ApiService";

const SavedQuestions = props => {
  const router = useRouter();
  const { favoriteQuestions, popularQuestions, questionData } = props;
  const [questions, setQuestions] = useState(questionData?.results ?? []);
  const [total, setTotal] = useState(questionData?.totalResults ?? 0);
  const [currentPage, setCurrentPage] = useState(1);
  const [loading, setLoading] = useState(false);

  const loadQuestions = async (nextPage) => {
    setLoading(true);
    try {
      const res = await ApiService.getSavedQuestion({limit: 10, sortBy: '-createdAt', page: nextPage});
      const {results, page, totalResults} = res.data;
      setCurrentPage(page);
      setQuestions(results);
      setTotal(totalResults);
    } catch (err) {

    }
    setLoading(false);
  }

  return (
    <CommunityLayout sideData={{popularQuestions, favoriteQuestions}} totalComQuestions={props.totalComQuestions}>
      <Card bordered={false} bodyStyle={{padding: "4px"}}>
        <div className="header-content mb-2">
          <div className="d-flex justify-content-between mb-4">
            <div className="d-flex align-items-center">
              <h2 className="mr-2 mb-0">{Utils.translate("label.my.saved.question")}</h2>
              <p className="m-0">{`(${props.totalComQuestions})`}</p>
            </div>
            <Button type="primary" size="small" icon={<PlusCircleOutlined/>} onClick={() => {
              router.push('/forum/add-question');
            }}>{Utils.translate("label.ask.question")}</Button>
          </div>
          <Divider type="dashed"/>
        </div>
        <div>
          <QuestionList
            {...props}
            setCurrentPage={setCurrentPage}
            currentPage={currentPage}
            setQuestions={setQuestions}
            loading={loading}
            questions={questions}
            totalResults={total}
            loadQuestions={loadQuestions}
          />
        </div>
      </Card>
    </CommunityLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {communityQuestionService} = require("server/services");
  let data = {};
  try {
    const userData = await auth(ctx);
    const { favoriteQuestionsData, popularQuestionsData, totalComQuestions } = await Utils.getDataComQuestionLayout(auth, communityQuestionService, ctx);
    data['popularQuestions'] = popularQuestionsData?.results ?? [];
    data['favoriteQuestions'] = favoriteQuestionsData?.results ?? [];
    data['totalComQuestions'] = totalComQuestions?.totalResults ?? 0;
    data['questionData'] = await communityQuestionService.queryQuestions({saved: true}, {limit: 10, page: 1, sortBy: '-createdAt'}, userData.user._id);
  } catch (err) {
    const {status, statusCode} = err;
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  const { favoriteQuestions, totalComQuestions, popularQuestions, questionData } = data;
  return {
    props: {
      totalComQuestions,
      favoriteQuestions,
      popularQuestions,
      questionData: JSON.parse(JSON.stringify(questionData))
    }
  }
}
export default SavedQuestions
