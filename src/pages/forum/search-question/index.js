import React, {useEffect, useState} from "react";
import QuestionList from "common/components/forum-components/question-list";
import {Button, Card, Divider, Input} from "antd";
import CommunityLayout from "common/layouts/community-layout";
import ApiService from "../../../common/services/ApiService";
import RedirectConfig from "../../../common/configs/RedirectConfig";
import Utils from "../../../common/utils";
import {useSelector} from "react-redux";

const SearchQuestion = props => {
  const { favoriteQuestions, popularQuestions } = props;
  const [total, setTotal] = useState(0);
  const [keyword, setKeyword] = useState('');
  const [loading, setLoading] = useState(false);
  const [questions, setQuestions] = useState([])
  const [currentPage, setCurrentPage] = useState(1);
  const {locale} = useSelector(state => state.theme)

  const loadQuestions = async (nextPage) => {
    setLoading(true);
    if (keyword !== '') {
      try {
        const res = await ApiService.getQuestions({limit: 10, sortBy: '-createdAt', page: nextPage, slug: Utils.slugify(keyword)});
        const {results, totalResults, page} = res.data;
        setCurrentPage(page);
        setQuestions(results);
        setTotal(totalResults);
      } catch (err) {
      }
    }
    setLoading(false);
  }


  return (
    <CommunityLayout sideData={{popularQuestions, favoriteQuestions}} totalComQuestions={props.totalComQuestions}>
      <div>
        <Card bordered={false} bodyStyle={{padding: "4px"}}>
          <div className="header-content">
            <div className="d-flex mb-4">
              <Input className="mr-2" placeholder={Utils.translate("placeholder.search")} onChange={(e) => {
                setKeyword(e.target.value);
              }}/>
              <Button type="primary" onClick={() => loadQuestions(1)}>{Utils.translate("label.search")}</Button>
            </div>
            <div className="counter-result">
              {
                keyword
                  ?
                  <p>{Utils.translate("message.count.result.search.question", {
                    count: total,
                    keyword: keyword
                  })}</p>
                  :
                  <p>{Utils.translate("message.tip.search.question")}</p>
              }
            </div>
            <Divider type="dashed"/>
          </div>
          <QuestionList
            {...props}
            setCurrentPage={setCurrentPage}
            currentPage={currentPage}
            setQuestions={setQuestions}
            questions={questions}
            totalResults={total}
            loadQuestions={loadQuestions}
            loading={loading}
          />
        </Card>
      </div>
    </CommunityLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {communityQuestionService} = require("server/services");
  const connectDb = require('server/utils/connect-db');
  let data = {};
  try {
    const { favoriteQuestionsData, popularQuestionsData, totalComQuestions } = await Utils.getDataComQuestionLayout(auth, communityQuestionService, ctx, connectDb);
    data['popularQuestions'] = popularQuestionsData?.results ?? [];
    data['favoriteQuestions'] = favoriteQuestionsData?.results ?? [];
    data['totalComQuestions'] = totalComQuestions?.totalResults ?? 0;
  } catch (err) {
    const {status, statusCode} = err;
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  const { favoriteQuestions, totalComQuestions, popularQuestions } = data;
  return {
    props: {
      totalComQuestions,
      favoriteQuestions,
      popularQuestions,
    }
  }
}

export default SearchQuestion;
