import React from "react";
import {Card} from "antd";
import QuestionForm from "common/components/forum-components/question-form";
import CommunityLayout from "common/layouts/community-layout";
import RedirectConfig from "../../../common/configs/RedirectConfig";
import Utils from "../../../common/utils";

const AddQuestion = props => {
  const { favoriteQuestions, popularQuestions } = props;
  return (
    <CommunityLayout sideData={{popularQuestions, favoriteQuestions}} totalComQuestions={props.totalComQuestions}>
      <div className="add-question">
        <Card bordered={false} bodyStyle={{padding: "4px"}}>
          <QuestionForm/>
        </Card>
      </div>
    </CommunityLayout>
  );
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {communityQuestionService} = require("server/services");
  const connectDb = require('server/utils/connect-db');
  let data = {};
  try {
    const { favoriteQuestionsData, popularQuestionsData, totalComQuestions } = await Utils.getDataComQuestionLayout(auth, communityQuestionService, ctx, connectDb);
    data['popularQuestions'] = popularQuestionsData?.results ?? [];
    data['favoriteQuestions'] = favoriteQuestionsData?.results ?? [];
    data['totalComQuestions'] = totalComQuestions?.totalResults ?? 0;
  } catch (err) {
    const {status, statusCode} = err;
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  const { favoriteQuestions, totalComQuestions, popularQuestions } = data;
  return {
    props: {
      totalComQuestions,
      favoriteQuestions,
      popularQuestions,
    }
  }
}

export default AddQuestion;
