import React from "react";
import {Divider} from "antd";
import ExamForm from "common/components/ manage-components/exam-form";
import ManageLayout from "common/layouts/manage-layout";
import Utils from "../../../../common/utils";

const AddExam = () => {

  return (
    <ManageLayout>
      <div className="add-exam">
        <h4 className="mb-4">{Utils.translate('label.add.exam')}</h4>
        <Divider type="dashed" className="mb-4"/>
        <ExamForm mode="ADD"/>
      </div>
    </ManageLayout>
  )
}
export default AddExam;


