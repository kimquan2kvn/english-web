import React, {useCallback, useEffect, useState} from "react";
import {Button, Card, Input, Modal, notification, Pagination, Select, Spin, Table, Tag, Tooltip} from "antd";
import {DeleteOutlined, EditOutlined, EyeOutlined, PlusCircleOutlined, UserAddOutlined} from "@ant-design/icons";
import moment from "moment"
import ManageLayout from "common/layouts/manage-layout";
import Link from "next/link"
import ApiService from "common/services/ApiService";
import RedirectConfig from "common/configs/RedirectConfig";
import {useExam} from "common/hooks/useRequest";
import Utils from "../../../common/utils";


const {Search} = Input;

const ExamList = props => {
  const [exams, setExams] = useState(props.exams || []);
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});
  const [shouldFetch, setShouldFetch] = useState(false);
  const [options, setOptions] = useState({page: 1, limit: 10});
  const {data, loading, error} = useExam(options, shouldFetch);
  const [value, setValue] = useState("name");
  const [currentPage, setCurrentPage] = useState(1)


  useEffect(() => {
    if (data && data.status === 200) {
      const {results, totalResults, page, totalPages} = data.data
      setExams(results);
      setPageInfo({totalResults: totalResults, hasNextPage: page < totalPages})
      setShouldFetch(false)
    }
    if (error) {
      setShouldFetch(false)
    }
  }, [options.page, data, error])

  const showDeleteConfirm = (examId) => {
    Modal.confirm({
      title: 'Are you sure delete this exam?',
      content: 'This action cannot be undone, are you sure you want to delete this exam?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      async onOk() {
        try {
          const res = await ApiService.deleteExam(examId);
          if (res.status === 200) {
            const newExams = [...exams].filter(exam => exam._id !== res.data.exam._id)
            setExams(newExams);
            notification.success({
              message: res.data.message
            })
          }
        } catch (err) {
          console.log(err)
        }
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }

  const tableColumns = [{
    title: Utils.translate('label.name'),
    dataIndex: 'name',
    align: 'center',
    width: 200,
    render: (_, record) => (<div><span>{record.name}</span></div>),
    sorter: {
      compare: (a, b) => {
        a = a.name.toLowerCase();
        b = b.name.toLowerCase();
        return a > b ? -1 : b > a ? 1 : 0;
      },
    },
  }, {
    title: Utils.translate('label.code'), dataIndex: 'code', align: "center", render: (_, record) => (<div>
      <span>{record.code}</span>
    </div>), sorter: {
      compare: (a, b) => {
        a = a.code.toLowerCase();
        b = b.code.toLowerCase();
        return a > b ? -1 : b > a ? 1 : 0;
      },
    },
  }, {
    title: Utils.translate('label.total.question'), dataIndex: 'totalQuestions', align: "center", sorter: {
      compare: (a, b) => a.totalQuestions - b.totalQuestions
    },
  }, {
    title: Utils.translate('label.time'), dataIndex: 'duration', align: "center", render: time => {
      return (<>
        {time ?
          <span>{moment.utc(moment.duration(time, 'seconds').as('milliseconds')).format('HH:mm:ss')}</span> : "No Data"}
      </>)
    }, sorter: (a, b) => moment(a["createAt"]).unix() - moment(b["createAt"]).unix()
  }, {
    title: Utils.translate('label.start.date'), dataIndex: 'startDate', align: "center", render: date => {
      return (<>{date ?
        <span>{moment(parseInt(date)).format("YYYY-MM-DD HH:ss")}</span> : "No Data"}</>)
    }, sorter: (a, b) => moment(a["createAt"]).unix() - moment(b["createAt"]).unix()
  }, {
    title: Utils.translate('label.end.date'), dataIndex: 'endDate', align: "center", render: date => {
      return (<>{date ? <span>{moment(parseInt(date)).format("YYYY-MM-DD HH:mm:ss")}</span> : "No Data"}</>)
    },
  }, {
    title: Utils.translate('label.status'),
    dataIndex: 'privacy',
    align: "center",
    render: status => (<Tag className="text-capitalize" color={status === "open" ? "cyan" : "red"}>{status}</Tag>),
    sorter: {
      compare: (a, b) => a.privacy.length - b.privacy.length,
    },
  }, {
    title: Utils.translate('label.options'),
    dataIndex: 'actions',
    align: "center",
    render: (_, record) => (<div className="text-right d-flex justify-content-center">
      <Tooltip title={Utils.translate('label.view')}>
        <Button type="primary" className="mr-2" icon={<EyeOutlined/>}
                onClick={() => {
                  if (typeof window !== "undefined") {
                    window.open("/exam/" + record?.code)
                  }
                }}
                size="small"/>
      </Tooltip>
      <Link href={`/manage/exam/edit/` + record['code']} passHref>
        <Button success="true" className="mr-2" icon={<EditOutlined/>} size="small"/>
      </Link>
      <Tooltip title={Utils.translate('label.delete')}>
        <Button
          danger
          icon={<DeleteOutlined/>}
          onClick={() => showDeleteConfirm(record['code'])}
          size="small"/>
      </Tooltip>
    </div>)
  }];


  const suffixSelector = (
    <Select
      value={value}
      onChange={(e) => setValue(e)}
    >
      <Select.Option value="name">{Utils.translate('label.name')}</Select.Option>
      <Select.Option value="code">{Utils.translate('label.code')}</Select.Option>
      <Select.Option value="privacy">{Utils.translate('label.status')}</Select.Option>
    </Select>
  );

  return (
    <ManageLayout>
      <Spin spinning={loading && shouldFetch} tip={Utils.translate('label.loading')}>
        <div className="exam-list">
          <div className="search-bar mb-4 d-flex justify-content-between flex-wrap">
            <Link href="/manage/exam/add" passHref>
              <Button className="mb-3 mr-3" type="primary" icon={<PlusCircleOutlined/>}>{Utils.translate('label.add.exam')}</Button>
            </Link>
            <Search
              onChange={(e) => {
                if (e.target.value === "") {
                  setShouldFetch(true)
                  setOptions({page: currentPage, limit: 10})
                }
              }}
              addonBefore={suffixSelector} placeholder={Utils.translate('label.input.search.text')}
              onSearch={query => {
                if (query === "") return;
                setShouldFetch(true)
                setOptions({page: 1, limit: 10, [value]: query})
              }} enterButton style={{width: 400}}
            />
          </div>
          <Card
            bodyStyle={{'padding': '8px'}}
          >
            <div className="table-responsive">
              <Table
                columns={tableColumns} pagination={false} dataSource={exams}
                rowKey='_id' footer={() => {
                return (
                  <Pagination
                    showQuickJumper
                    current={options.page}
                    total={pageInfo.totalResults}
                    onChange={async (page, pageSize) => {
                      setCurrentPage(page)
                      setOptions({...options, page: page});
                      setShouldFetch(true)
                    }}
                  />
                )
              }}/>
            </div>
          </Card>
        </div>
      </Spin>
    </ManageLayout>)
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {examService} = require("server/services");
  let exams = {};
  try {
    const {user} = await auth(ctx, ["MANAGE_ALL_EXAM", "GET_ALL_EXAM"])
    exams = await examService.queryExams({deleted: {$ne: true}}, {page: 1, limit: 10}, user);
  } catch (err) {
    const {status} = err;
    return {
      redirect: RedirectConfig(status)
    }
  }
  if (!exams || JSON.stringify(exams) === "{}") return {props: {}}
  const {results, totalResults, totalPages, page} = exams
  return {
    props: {
      exams: JSON.parse(JSON.stringify(results)),
      pageInfo: {
        totalResults,
        hasNextPage: page < totalPages
      }
    }
  }
}

export default ExamList;
