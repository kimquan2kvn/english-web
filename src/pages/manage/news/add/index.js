import React from "react";
import ManageLayout from "common/layouts/manage-layout";
import {Divider} from "antd";
import BlogForm from "common/components/ manage-components/blog-form";
import Utils from "../../../../common/utils";

const AddBlog = props => {
  return (
    <ManageLayout>
      <div className="add-exam">
        <h4 className="mb-4">{Utils.translate('label.add.new')}</h4>
        <Divider type="dashed" className="mb-4"/>
        <BlogForm />
      </div>
    </ManageLayout>
  )
}
export default AddBlog;
