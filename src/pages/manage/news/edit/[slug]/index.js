import React from "react";
import {Divider} from "antd";
import ManageLayout from "common/layouts/manage-layout";
import BlogForm from "common/components/ manage-components/blog-form";
import RedirectConfig from "common/configs/RedirectConfig";
import Utils from "../../../../../common/utils";


const EditBlog = props => {
  const {blog} = props
  return (<ManageLayout>
    <div className="add-exam">
      <h4 className="mb-4">{Utils.translate('label.edit.news')}</h4>
      <Divider type="dashed" className="mb-4"/>
      <BlogForm mode="EDIT" blog={blog}/>
    </div>
  </ManageLayout>)

}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {blogService} = require("server/services");
  const {slug} = ctx.query
  let blog = {};

  try {
    await auth(ctx, ["UPDATE_ALL_BLOG", "MANAGE_ALL_BLOG"])
    blog = await blogService.getBlogByFilter({slug: slug}).catch(err => {
      throw ({
        status: 400,
        message: "Blog not found."
      })
    })
  } catch (err) {
    const {status, statusCode} = err
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  if (!blog || JSON.stringify(blog) === "{}") return {props: {}}
  return {
    props: {
      blog: JSON.parse(JSON.stringify(blog))
    }
  }
}
export default EditBlog;
