import React, {useEffect, useState} from "react";
import moment from "moment";
import {Button, Card, Input, Modal, notification, Pagination, Select, Spin, Table, Tag, Tooltip} from "antd";
import {DeleteOutlined, EditOutlined, EyeOutlined, PlusCircleOutlined} from "@ant-design/icons";
import ManageLayout from "common/layouts/manage-layout";
import Link from 'next/link'
import RedirectConfig from "common/configs/RedirectConfig";
import {useBlogs} from "common/hooks/useRequest";
import ApiService from "common/services/ApiService";
import Utils from "../../../common/utils";

const {Search} = Input
const BlogList = props => {
  const [blogs, setBlogs] = useState(props.blogs || []);
  const [shouldFetch, setShouldFetch] = useState(false)
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});
  const [options, setOptions] = useState({page: 1, limit: 10});
  const {data, loading, error} = useBlogs(options, shouldFetch);
  const [current, setCurrent] = useState(1);
  const [value, setValue] = useState("title");

  useEffect(() => {
    if (data && data.status === 200) {
      const {results, totalPages, page, totalResults} = data.data
      setBlogs(results)
      setPageInfo({totalResults: totalResults, hasNextPage: page < totalPages})
      setShouldFetch(false)
    }
    if (error) {
      setShouldFetch(false)
    }
  }, [options.page, data, error])

  const tableColumns = [
    {
      title: Utils.translate('label.title'),
      dataIndex: "title",
      align: "center",
      render: record => {
        return (
          <span>{record}</span>
        )
      },
    },
    {
      title: Utils.translate('label.author'),
      dataIndex: "author",
      align: "center",
      render: record => {
        return (
          <span>{record.username}</span>
        )
      },
    },
    {
      title: Utils.translate('label.category'),
      dataIndex: "categories",
      align: "center",
      render: record => {
        return (
          <span>{record?.length > 0 ? (
            record?.map((category, index) => {
              return <Tag key={index}>{category}</Tag>
            })
          ) : (<span>Uncategorized</span>)}</span>
        )
      },
    },
    {
      title: Utils.translate('label.posted.at'),
      dataIndex: "createdAt",
      align: "center",
      render: record => {
        return (
          <>
            {record ? <span>{moment(parseInt(record)).format("YYYY-MM-DD")}</span> : "No Data"}
          </>
        )
      },
    },
    {
      title: Utils.translate('label.options'),
      dataIndex: 'actions',
      align: "center",
      render: (_, record) => {
        return (
          <div className="text-right d-flex justify-content-center">
            <Tooltip title={Utils.translate('label.view')}>
              <Button type="primary" className="mr-2" icon={<EyeOutlined/>}
                      onClick={() => {
                        if (typeof window !== "undefined") {
                          window.open("/news/" + record?.slug)
                        }
                      }}
                      size="small"/>
            </Tooltip>
            <Link href={`/manage/news/edit/${record["slug"]}`} passHref>
              <Button success="true" className="mr-2" icon={<EditOutlined/>} size="small"/>
            </Link>
            <Tooltip title={Utils.translate('label.delete')}>
              <Button danger icon={<DeleteOutlined/>}
                      onClick={() => showDeleteConfirm(record['_id'])}
                      size="small"/>
            </Tooltip>
          </div>
        )
      }
    }
  ]

  const showDeleteConfirm = (blogId) => {
    Modal.confirm({
      title: Utils.translate('label.warn.delete.new'),
      content: Utils.translate('label.warn.desc.delete.new'),
      okText: Utils.translate('label.yes'),
      okType: "danger",
      cancelText: Utils.translate('label.no'),
      async onOk() {
        try {
          const res = await ApiService.deleteBlog({blogs: [blogId]});
          if (res.status === 200) {
            const newBlogs = [...blogs].filter(blog => blog._id !== blogId)
            setBlogs(newBlogs)
            notification.success({
              message: res.data.message
            })
          }
        } catch (err) {
          console.log(err)
        }
      },
      onCancel() {
      }
    });
  }

  const suffixSelector = (
    <Select
      value={value}
      onChange={(e) => setValue(e)}
    >
      <Select.Option value="title">{Utils.translate('label.title')}</Select.Option>
      <Select.Option value="author">{Utils.translate('label.author')}</Select.Option>
    </Select>
  );

  return (
    <ManageLayout>
      <Spin spinning={(loading && shouldFetch)} tip="Loading...">
        <div className="blog-list">
          <div className="search-bar mb-4 d-flex justify-content-between flex-wrap">
            <Link href="/manage/news/add" passHref>
              <Button className="mb-3" type="primary" icon={<PlusCircleOutlined/>}>{Utils.translate('label.add.new')}</Button>
            </Link>
            <Search
              onChange={(e) => {
                if (e.target.value === "") {
                  setShouldFetch(true)
                  setOptions({page: current, limit: 10})
                }
              }}
              addonBefore={suffixSelector} placeholder={Utils.translate('label.input.search.text')}
              onSearch={query => {
                if (query === "") return;
                setShouldFetch(true)
                setOptions({page: 1, limit: 10, [value]: query})
              }}
              enterButton style={{width: 400}}
            />
          </div>
          <Card bodyStyle={{'padding': '8px'}}>
            <div className="table-responsive">
              <Table
                columns={tableColumns} dataSource={blogs} rowKey='_id'
                pagination={false} footer={() => {
                return (
                  <Pagination
                    showQuickJumper current={options.page} defaultCurrent={1} total={pageInfo?.totalResults}
                    onChange={(page, pageSize) => {
                      setCurrent(page);
                      setShouldFetch(true)
                      setOptions({
                        ...options,
                        page: page
                      })
                    }}
                  />
                )
              }}/>
            </div>
          </Card>
        </div>
      </Spin>
    </ManageLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {blogService} = require("server/services");
  let blogs = {};
  try {
    await auth(ctx, ["MANAGE_ALL_BLOG", "GET_ALL_BLOG"]);
    blogs = await blogService.queryBlogs({deleted: {$ne: true}}, {limit: 10, page: 1})
  } catch (err) {
    const {status} = err
    return {
      redirect: RedirectConfig(status)
    }
  }
  if (!blogs || JSON.stringify(blogs) === "{}") return {props: {}}
  const {results, totalPages, page, totalResults} = blogs
  return {
    props: {
      blogs: JSON.parse(JSON.stringify(results)),
      pageInfo: {
        totalResults,
        hasNextPage: page < totalPages,
      }
    }
  }
}
export default BlogList;
