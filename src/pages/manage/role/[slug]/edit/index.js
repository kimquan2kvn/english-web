import React, {useEffect} from "react";
import ManageLayout from "common/layouts/manage-layout";
import {Button, Checkbox, Col, Divider, Form, Input, notification, Row} from "antd";
import RedirectConfig from "common/configs/RedirectConfig";
import {ROW_GUTTER} from "common/constants/ThemeConstant";
import ApiService from "common/services/ApiService";
import Utils from "../../../../../common/utils";


const EditRole = (props) => {
  const {role = {}, permissions = []} = props;
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({...role})
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  const getNamePermission = (slug) => {
    const arr = slug.split('-');
    console.log(arr)
    if(arr.length > 1) {
      return `${arr[0].charAt(0).toUpperCase() + arr[0].slice(1)} ${arr[1]}`;
    } else {
      return  `${arr[0].charAt(0).toUpperCase() + arr[0].slice(1)}`;
    }
  }


  const onsubmit = (slug) => {
    form.validateFields().then(async () => {
      const values = form.getFieldsValue();
      try {
        const res = await ApiService.updateRole(slug, values)
        if (res.status === 200) {
          notification.success({
            message: res.data.message
          })
        }
      } catch (err) {
        console.log("err")
      }
    })
  }
  return (
    <ManageLayout>
      <div className="add-exam">
        <div className="d-flex justify-content-between">
          <h4 className="mb-4">{Utils.translate('label.edit.role')}</h4>
          <Button size="small" type="primary" onClick={() => onsubmit(role.slug)}>{Utils.translate('label.save.change')}</Button>
        </div>
        <Divider type="dashed" className="mb-4"/>
        <Form
          form={form}
          layout="vertical"
        >
          <Row gutter={ROW_GUTTER}>
            <Col xs={24} lg={12}>
              <Form.Item
                name="name"
                label={Utils.translate('label.name')}
                rules={[{required: true, message: Utils.translate('label.warn.input.role.name')}]}
              >
                <Input/>
              </Form.Item>
            </Col>
            <Col xs={24} lg={12}>
              <Form.Item
                name="slug"
                label="Slug"
                rules={[{required: true, message: Utils.translate('label.warn.input.slug')}]}
              >
                <Input disabled={true}/>
              </Form.Item>
            </Col>
          </Row>
          <Form.Item
            name="permissions"
          >
            <Checkbox.Group>
              {Object.keys(permissions).map((per, index) => console.log(per) || (
                <div key={per} className="w-100 mb-3">
                  <span className="d-block mb-2 text-capitalize font-weight-semibold">{getNamePermission(per)} permission</span>
                  <Row gutter={16}>
                    {permissions[per].map((p, i) => (
                      <Col xs={24} md={12} lg={12} xl={8} key={p + i}>
                        <Checkbox value={p} key={p + i}>
                          <span className="text-capitalize">{p.split("_").join(" ")}</span>
                        </Checkbox>
                      </Col>
                    ))}
                  </Row>

                </div>
              ))}
            </Checkbox.Group>
          </Form.Item>
        </Form>
      </div>
    </ManageLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {roleService} = require("server/services");
  const {slug} = ctx.query
  let role = {}
  let res = {};
  let permissions = {
    user: [],
    blog: [],
    role: [],
    comment: [],
    post: [],
    'exam-question': [],
    exam: [],
    'community-question': [],
    issue: [],
  };
  try {
    await auth(ctx, ["UPDATE_ALL_ROLE", "MANAGE_ALL_ROLE"])
    res = roleService.getPermissions()
    role = await roleService.getRoleBySlug(slug).catch(err => {
      throw ({
        status: 400,
        message: "Role not found."
      })
    })
  } catch (err) {
    role = {};
    const {status, statusCode} = err
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  if (res && res.length > 0) {
    res.forEach((per, index) => {
      if (per.includes("USER")) {
        permissions.user.push(per);
      } else if (per.includes("BLOG")) {
        permissions.blog.push(per);
      } else if (per.includes("ROLE")) {
        permissions.role.push(per);
      } else if (per.includes("COMMENT")) {
        permissions.comment.push(per);
      } else if (per.includes("POST")) {
        permissions.post.push(per);
      } else if (per.includes("EXAM_QUESTION")) {
        permissions['exam-question'].push(per);
      } else if (per.includes("EXAM")) {
        permissions.exam.push(per);
      } else if (per.includes("COMMUNITY_QUESTION")) {
        permissions['community-question'].push(per);
      } else if (per.includes("ISSUE")) {
        permissions.issue.push(per)
      } else {
        console.log("else")
      }
    })
  }
  if (JSON.stringify(role) === "{}") return {props: {}}
  return {
    props: {
      permissions,
      role: JSON.parse(JSON.stringify(role))
    }
  }

}
export default EditRole
