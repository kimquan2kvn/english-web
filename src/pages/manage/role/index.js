import React, {useState} from "react";
import RedirectConfig from "common/configs/RedirectConfig";
import {Button, Card, Input, Modal, notification, Pagination, Select, Table, Tag, Tooltip} from "antd";
import {DeleteOutlined, EditOutlined, EyeOutlined, PlusCircleOutlined, UserAddOutlined} from "@ant-design/icons";
import ManageLayout from "common/layouts/manage-layout";
import CustomModal from "common/components/util-components/CustomModal";
import ApiService from "common/services/ApiService";
import Link from "next/link"
import Utils from "../../../common/utils";

const {Search} = Input

const Role = (props) => {
  const [visible, setVisible] = useState(false)
  const [value, setValue] = useState("")
  const [roles, setRoles] = useState(props.roles || [])

  const {pageInfo} = props

  const tableColumns = [
    {
      title: Utils.translate('label.name'),
      dataIndex: "name",
      align: "center",
      render: record => {
        return (
          <div>
            <span>{record}</span>
          </div>
        )
      },
    },
    {
      title: "Slug",
      dataIndex: "slug",
      align: "center",
      render: record => {
        return (
          <div>
            <span>{record}</span>
          </div>
        )
      },
    },
    {
      title: Utils.translate('label.permission'),
      dataIndex: "permissions",
      // width: 120,
      align: "center",
      render: record => {
        return (
          <div>
            <Tag>{record?.length} permission(s)</Tag>
          </div>
        )
      },
    },
    {
      title: Utils.translate('label.options'),
      dataIndex: 'actions',
      align: "center",
      render: (_, record) => (
        <div className="text-right d-flex justify-content-center">
          <Tooltip title={Utils.translate('label.view')}>
            <Button type="primary" className="mr-2" icon={<EyeOutlined/>}
              // onClick={() => {showExamInfo(record)}}
                    size="small"/>
          </Tooltip>
          <Tooltip title={Utils.translate('label.edit')}>
            <Link href={`/manage/role/${record.slug}/edit`} passHref>
              <Button success={"true"} className="mr-2" icon={<EditOutlined/>} size="small"/>
            </Link>
          </Tooltip>
          <Tooltip title={Utils.translate('label.delete')}>
            <Button danger icon={<DeleteOutlined/>}
                    onClick={() => showDeleteConfirm(record['slug'])}
                    size="small"/>
          </Tooltip>
        </div>
      )
    }
  ]

  const onSubmit = async () => {
    try {
      const res = await ApiService.addRole({name: value})
      if (res.status === 200) {
        const newRoles = [...roles, res.data.role]
        setRoles(newRoles)
        setVisible(false)
      }
    } catch (err) {
      console.log(err)
    }
  }

  const showDeleteConfirm = (slug) => {
    Modal.confirm({
      title: Utils.translate('label.confirm.delete.role'),
      content: Utils.translate('label.warn.delete.role'),
      okText: Utils.translate('label.yes'),
      okType: "danger",
      cancelText: Utils.translate('label.no'),
      onOk: async () => {
        try {
          const res = await ApiService.deleteRole(slug)
          if (res.status === 200) {
            const newRole = [...roles].filter(role => role.slug !== slug)
            setRoles(newRole)
            notification.success({
              message: res.data.message
            })
          }
        } catch (err) {
          console.log(err)
        }
      },
      onCancel() {
        console.log('Cancel');
      }
    })
  }

  return (
    <ManageLayout>
      <div className="manage-issue">
        <div className="search-bar mb-4 d-flex justify-content-between flex-wrap">
          <Button type="primary" icon={<PlusCircleOutlined/>} className="mb-3 mr-3" onClick={() => setVisible(true)}>{Utils.translate('label.add.role')}</Button>
          <Search placeholder={Utils.translate('label.input.search.text')} onSearch={value => {
            //  Call API search
          }} enterButton style={{width: 400}}/>
        </div>
        <Card
          bodyStyle={{'padding': '8px'}}
        >
          <div className="table-responsive">
            <Table columns={tableColumns} pagination={false} dataSource={roles} rowKey='_id' footer={() => {
              return (
                <Pagination showQuickJumper defaultCurrent={pageInfo?.page ?? 1} total={pageInfo?.totalResults}
                            onChange={(page, pageSize) => {

                            }}/>
              )
            }}/>
          </div>
        </Card>
      </div>
      <CustomModal footer={null} width={500} centered={false} visible={visible} title={Utils.translate('label.add.role')}
                   onCancel={() => setVisible(false)}>
        <div>
          <span className="font-weight-semibold mb-2 d-block">{Utils.translate('label.name')}</span>
          <Input value={value} className="mb-4" onChange={(e) => {
            const {value} = e.target
            if (value === "") return;
            setValue(value)
          }}/>
          <div className="text-right">
            <Button className="mr-2" onClick={() => {
              setValue("")
              setVisible(false)
            }}>{Utils.translate('label.cancel')}</Button>
            <Button type="primary" disabled={value === ""} onClick={onSubmit}>{Utils.translate('label.ok')}</Button>
          </div>
        </div>
      </CustomModal>
    </ManageLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth")
  const {roleService} = require("server/services")
  let roles = {};
  try {
    await auth(ctx, ["MANAGE_ALL_ROLE", "GET_ALL_ROLE"])
    roles = await roleService.queryRoles({}, {page: 1, limit: 10, sortBy: "createdAt"}).catch(err => {
      throw ({
        status: 400,
        message: "Can not load data"
      })
    })
  } catch (err) {
    roles = {}
    const {status, statusCode} = err
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  if (JSON.stringify(roles) === "{}") return {props: {}}
  const {results, totalResults, page} = roles
  return {
    props: {
      roles: JSON.parse(JSON.stringify(results)),
      pageInfo: {
        totalResults,
        page
      }
    }
  }
}
export default Role;
