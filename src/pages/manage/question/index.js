import React, {useEffect, useState} from "react";
import {Button, Card, Input, Pagination, Table, Tooltip, Modal, Spin, notification, Select} from "antd";
import {DeleteOutlined, EditOutlined, EyeOutlined, PlusCircleOutlined, UserAddOutlined} from "@ant-design/icons";
import ManageLayout from "common/layouts/manage-layout";
import Link from "next/link"
import RedirectConfig from "common/configs/RedirectConfig";
import {useQuestion} from "common/hooks/useRequest";
import ApiService from "common/services/ApiService";
import Utils from "../../../common/utils";


const {Search} = Input
const QuestionList = props => {
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {})
  const [questions, setQuestions] = useState(props.questions || []);
  const [shouldFetch, setShouldFetch] = useState(false);
  const [options, setOptions] = useState({page: 1, limit: 10, sortBy: "-id"})
  const [value, setValue] = useState("id");
  const [currentPage, setCurrentPage] = useState(1)

  const {data, loading, error} = useQuestion(options, shouldFetch);

  useEffect(() => {
    if (data && data.status === 200) {
      const {results, totalResults, page, totalPages} = data.data
      setQuestions(results);
      setPageInfo({totalResults: totalResults, hasNextPage: page < totalPages})
      setShouldFetch(false)
    }
    if (error) {
      setShouldFetch(false)
    }
  }, [options.page, data, error]) // eslint-disable-line react-hooks/exhaustive-deps

  const showDeleteConfirm = (questionId) => {
    Modal.confirm({
      title: Utils.translate('label.warn.delete.question'),
      content: Utils.translate('label.warn.desc.delete.question'),
      okText: Utils.translate('label.yes'),
      okType: "danger",
      cancelText: Utils.translate('label.no'),
      async onOk() {
        try {
          const res = await ApiService.deleteExamQuestion(questionId);
          if (res.status === 200) {
            const newQuestion = [...questions].filter(question => question.id !== questionId);
            setQuestions(newQuestion);
            notification.success({
              message: res.data.message
            })
          }
        } catch (err) {
          console.log(err)
        }
      },
      onCancel() {
        console.log('Cancel');
      }
    })
  }

  const tableColumns = [
    {
      title: Utils.translate('label.question.id'),
      dataIndex: "id",
      align: "center",
    },
    {
      title: Utils.translate('label.question.type'),
      dataIndex: "type",
      align: "center",
    },
    {
      title: Utils.translate('label.question.children'),
      dataIndex: "number_children",
      align: "center",
    },
    {
      title: Utils.translate('label.options'),
      dataIndex: 'actions',
      align: "center",
      render: (_, record) =>console.log(record) || (
        <div className="text-right d-flex justify-content-center">
          <Tooltip title={Utils.translate('label.view')}>
            <Link href={"/manage/question/" + record.id + "/view/"} passHref>
              <Button type="primary" className="mr-2" icon={<EyeOutlined/>} size="small"/>
            </Link>
          </Tooltip>
          <Link href={"/manage/question/" + record.id + "/edit/"} passHref>
            <Button success="true" className="mr-2" icon={<EditOutlined/>} size="small"/>
          </Link>
          <Tooltip title={Utils.translate('label.delete')}>
            <Button danger icon={<DeleteOutlined/>}
                    onClick={() => showDeleteConfirm(record['id'])}
                    size="small"/>
          </Tooltip>
        </div>
      )
    }
  ]

  const suffixSelector = (
    <Select
      value={value}
      onChange={(e) => setValue(e)}
    >
      <Select.Option value="id">{Utils.translate('label.question.id')}</Select.Option>
      <Select.Option value="type">{Utils.translate('label.type')}</Select.Option>
    </Select>
  );

  return (
    <ManageLayout>
      <Spin spinning={loading && shouldFetch} tip="Loading...">
        <div className="question-list">
          <div className="search-bar mb-4 d-flex justify-content-between flex-wrap">
            <Link href="/manage/question/add" passHref>
              <Button className="mb-3 mr-3" type="primary" icon={<PlusCircleOutlined/>}>{Utils.translate('label.add.question')}</Button>
            </Link>
            <Search
              addonBefore={suffixSelector} placeholder={Utils.translate('label.input.search.text')}
              onChange={(e) => {
                if (e.target.value === "") {
                  setShouldFetch(true)
                  setOptions({page: currentPage, limit: 10, sortBy: "-id"})
                }
              }}
              onSearch={query => {
                if (query !== "") {
                  setShouldFetch(true)
                  setOptions({page: 1, limit: 10, sortBy: "-id", [value]: query})
                }
              }}
              enterButton style={{width: 400}}
            />
          </div>
          <Card
            bodyStyle={{'padding': '8px'}}
          >
            <div className="table-responsive">
              <Table columns={tableColumns}
                     dataSource={questions}
                     rowKey='id' pagination={false} footer={() => {
                return (
                  <Pagination
                    current={options.page || currentPage || 1} total={pageInfo.totalResults}
                    showQuickJumper
                    defaultCurrent={currentPage}
                    onChange={(page, pageSize) => {
                      setCurrentPage(page)
                      setOptions({...options, page: page});
                      setShouldFetch(true)
                    }}/>
                )
              }}/>
            </div>
          </Card>
        </div>
      </Spin>
    </ManageLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {questionService} = require("server/services")
  let questions = {};
  try {
    await auth(ctx, ["MANAGE_ALL_EXAM_QUESTION", "GET_ALL_EXAM_QUESTION"])
    questions = await questionService.queryQuestions({model: "Question"}, {limit: 10, page: 1, sortBy: "-id"})
  } catch (err) {
    const {status} = err
    console.log("err", err)
    return {
      redirect: RedirectConfig(status)
    }
  }
  if (!questions || JSON.stringify(questions) === "{}") return {props: {}}
  const {results, totalResults, totalPages, page} = questions
  return {
    props: {
      questions: JSON.parse(JSON.stringify(results)),
      pageInfo: {
        totalResults: totalResults,
        hasNextPage: page < totalPages
      }
    }
  }

}
export default QuestionList;
