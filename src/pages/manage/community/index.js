import React, {useEffect, useState} from "react";
import {Button, Card, Input, Modal, notification, Typography, Spin, Table, Tag, Tooltip, Popover} from "antd";
import {DeleteOutlined, EditOutlined, EyeOutlined, UserAddOutlined} from "@ant-design/icons";
import ManageLayout from "common/layouts/manage-layout";
import ApiService from "../../../common/services/ApiService";
import Link from 'next/link';
import moment from "moment";
import QuestionForm from "../../../common/components/forum-components/question-form";
import Utils from "../../../common/utils";

const {Search} = Input
const ManageCommunity = props => {
  const [keyword, setKeyword] = useState('');
  const [visible, setVisible] = useState(false);
  const [questions, setQuestions] = useState([]);
  const [total, setTotal] = useState(10);
  const [loadedPage, setLoadedPage] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [questionData, setQuestionData] = useState(null);
  const [searchData, setSearchData] = useState([]);
  const [totalResultSearch, setTotalResultSearch] = useState(10);
  const [loadedPageSearch, setLoadedPageSearch] = useState([]);

  useEffect(() => {
    if (props.questionData) {
      const {results, totalPages, page} = props.questionData;
      setQuestions(results);
      if (page < totalPages) setTotal(page * 10 + 10);
      setLoadedPage(state => [...state, page]);
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  const loadQuestions = async (nextPage) => {
    setLoading(true);
    try {
      const res = await ApiService.getQuestions({page: nextPage, limit: 10, sortBy: '-createdAt'});
      const {totalPages, page, results} = res.data;
      if(nextPage === 1) {
        setQuestions(results);
      } else {
        setQuestions(state => [...state, ...results]);
      }
      if (page < totalPages) {
        setTotal(page * 10 + 10);
      }
      setCurrentPage(page);
      setLoadedPage(state => [...state, page]);
    } catch (err) {
      setCurrentPage(1);
      notification.error({message: 'Failed to load questions'});
    }
    setLoading(false);
  }

  const tableColumns = [
    {
      title: Utils.translate('label.question.title'),
      align: "center",
      render: record => (
        <div className='d-flex'>
          <div className='m-auto'>
            <Link href={'/forum/question/' + record._id}>
              <a>
                <Typography.Paragraph
                  ellipsis={{
                    tooltip: record.title,
                    rows: 3
                  }}
                  className='under-line'
                  style={{maxWidth: 150, cursor: 'pointer'}}
                >
                  {Utils.decodeHtml(record.title)}
                </Typography.Paragraph>
              </a>
            </Link>
          </div>
        </div>
      )
    },
    {
      title: Utils.translate('label.author'),
      align: "center",
      render: record => (
        <span>{record.author?.displayName ?? ''}</span>
      )
    },
    {
      title: Utils.translate('label.time'),
      align: "center",
      render: record => (
        <Tag>{moment(record.createdAt).format('DD-MM-YYYY')}</Tag>
      )
    },
    {
      title: Utils.translate('label.tag'),
      align: "center",
      render: record => (
        <Popover
          content={
            <div className='d-flex'>
              {
                record.tags.map(x => {
                  return (
                    <Tag key={x.title} color='blue'>{x.title}</Tag>
                  )
                })
              }
            </div>
          }
        >
          <Tag style={{cursor: 'default'}}>{record.tags.length} {Utils.translate('label.tags')}</Tag>
        </Popover>
      )
    },
    {
      title: Utils.translate('label.options'),
      align: "center",
      render: (_, record) => {
        return (
          <div className="text-right d-flex justify-content-center">
            <Link href={'/forum/question/' + record.slug}>
              <a>
                <Tooltip title={Utils.translate('label.view')}>
                  <Button type="primary" className="mr-2" icon={<EyeOutlined/>}
                    // onClick={() => {showExamInfo(record)}}
                          size="small"/>
                </Tooltip>
              </a>
            </Link>
            <Tooltip title={Utils.translate('label.edit')}>
              <Button success="true" className="mr-2" icon={<EditOutlined/>}
                onClick={() => {
                  setVisible(true);
                  setQuestionData(record);
                }}
                      size="small"/>
            </Tooltip>
            <Tooltip title={Utils.translate('label.delete')}>
              <Button danger icon={<DeleteOutlined/>}
                      onClick={() => showDeleteConfirm(record['slug'])}
                      size="small"/>
            </Tooltip>
          </div>
        )
      }
    }
  ]

  const showDeleteConfirm = (communityId) => {
    Modal.confirm({
      title: Utils.translate("label.warn.delete.question"),
      content: Utils.translate("label.warn.desc.delete.question"),
      okText: Utils.translate('label.yes'),
      okType: "danger",
      cancelText: Utils.translate('label.no'),
      onOk: async () => {
        try {
          const res = await ApiService.deleteQuestion(communityId, {model: 'CommunityQuestion'});
          setQuestions(state => {
            return state.filter(x => x.slug !== communityId);
          })
          notification.success({message: 'Delete question successfully'});
        }
        catch(err) {
        }
      },
      onCancel() {
        console.log('Cancel');
      }
    })
  }

  const searchQuestion = async (nextPage) => {
    setLoading(true);
    try {
      const res = await ApiService.searchQuestion({slug: keyword, page: nextPage, limit: 10, sortBy: '-createdAt'});
      const { results, totalPages, page } = res.data;
      setCurrentPage(page);
      if(nextPage === 1) {
        setLoadedPageSearch([1]);
        setSearchData(results);
      } else {
        setSearchData(state => [...state, ...results]);
        setLoadedPageSearch(state => [...state, page]);
      }
      if(page < totalPages) {
        setTotalResultSearch(page * 10 + 10);
      }
      if(results.length === 0) {
        notification.error({message: 'Question not found'});
      }
    }
    catch(err) {
      console.log(err);
    }
    setLoading(false);
  }

  return (
    <ManageLayout>
      <div>
        <div className="search-bar mb-4 d-flex justify-content-end flex-wrap">
          <Search
            placeholder={Utils.translate('label.input.search.text')}
            onChange={(e) => {
              setTotalResultSearch(10);
              if(e.target.value === '') {
                setSearchData([]);
                setCurrentPage(1);
              }
              setKeyword(e.target.value);
            }}
            onSearch={value => {
            //  Call API search
              if(keyword === '') return;
              searchQuestion(1).then(_ => {});
          }} enterButton style={{width: 400}}/>
        </div>
        <Spin
          spinning={loading}
          tip={Utils.translate('label.loading.question')}
        >
          <Card
            type="inner"
            bodyStyle={{'padding': '8px'}}
          >
            <div className="table-responsive">
              <Table
                columns={tableColumns}
                pagination={{
                  current: currentPage,
                  pageSize: 10,
                  total: searchData.length > 0 ? totalResultSearch : total,
                  onChange: (page) => {
                    setCurrentPage(page);
                    if(searchData.length > 0) {
                      if(!loadedPageSearch.includes(page)) {
                        searchQuestion(page).then(_ => {});
                      }
                    } else {
                      if (!loadedPage.includes(page)) {
                        loadQuestions(page).then(_ => {});
                      }
                    }
                  }
                }}
                dataSource={searchData.length > 0 ? searchData : questions}
                rowKey='_id'
              />
            </div>
          </Card>
        </Spin>
        <Modal
          visible={visible}
          onCancel={() => {
            setVisible(false);
            setQuestionData(null);
          }}
          footer={false}
        >
          <QuestionForm setVisible={setVisible} setQuestions={setQuestions} setQuestionData={setQuestionData} questionData={questionData}/>
        </Modal>
      </div>
    </ManageLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {communityQuestionService} = require("server/services");
  let questionData = [];
  try {
    await auth(ctx, ["MANAGE_ALL_COMMUNITY_QUESTION", "GET_ALL_COMMUNITY_QUESTION"]);
    questionData = await communityQuestionService.queryQuestions({deleted: {$ne: true}}, {limit: 10, sortBy: '-createdAt', page: 1});
  } catch (err) {

  }
  return {
    props: {
      questionData: JSON.parse(JSON.stringify(questionData)),
    }
  }
}

export default ManageCommunity;
