import React, {useEffect, useState} from "react";
import {Avatar, Button, Col, Divider, notification, Row, Upload} from "antd";
import {CloudUploadOutlined, UserOutlined} from "@ant-design/icons";
import ImgCrop from "antd-img-crop";
import Flex from "common/components/shared-components/Flex";
import ManageLayout from "common/layouts/manage-layout";
import RedirectConfig from "common/configs/RedirectConfig";
import UserForm from "common/components/ manage-components/user-form";
import axios from "axios";

const EditUser = props => {
  const {user = {}, roles = []} = props;
  const [avatar, setAvatar] = useState(user?.avatar ?? '');
  const [loading, setLoading] = useState(false);

  return (
    <ManageLayout>
      <div className="add-user">
        <h4 className="mb-4">Edit User</h4>
        <Divider type="dashed" className="mb-4"/>
        <Row>
          <Col xs={24} sm={24} md={24} lg={8} xxl={8} className="mb-4">
            <Flex flexDirection="column" alignItems="center" mobileFlex={false} className="text-center text-md-left">
              <Avatar
                shape={"circle"}
                size={160}
                icon={<UserOutlined/>}
                src={avatar}
              />
              <div className="d-flex mt-4 justify-content-center">
                <ImgCrop rotate>
                  <Upload
                    showUploadList={false}
                    onChange={async (file) => {
                      if(loading) return;
                      setLoading(true);
                      try {
                        const formData = new FormData();
                        formData.append('file', file.file.originFileObj);
                        const res = await axios.post('/api/users/update/' + user.username, formData, {
                          headers: {
                            'Content-Type': 'multipart/form-data',
                          }
                        })
                        setAvatar(res.data.user.avatar);
                        notification.success({message: 'Upload avatar successfully'});
                      } catch (err) {
                        console.log(err)
                        notification.error({message: 'Failed to upload avatar'})
                      }
                      setLoading(false);
                    }}
                  >
                    <Button loading={loading} icon={<CloudUploadOutlined/>} type="primary">Change Avatar</Button>
                  </Upload>
                </ImgCrop>
              </div>
            </Flex>
          </Col>
          <Col xs={24} sm={24} md={24} lg={16} xxl={16}>
            <UserForm mode="EDIT" user={user} roles={roles}/>
          </Col>
        </Row>
      </div>
    </ManageLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {userService, roleService} = require("server/services")
  const {username} = ctx.query
  let user = {};
  let roles = []
  try {
    await auth(ctx, ["MANAGE_ALL_USER", "UPDATE_ALL_USER"]);
    user = await userService.getUserByFilter({username: username}, "").catch(err => {
      throw ({
        status: 400,
        message: "User not found"
      })
    })
    roles = await roleService.queryRoles({}, {page: 1, limit: 10, sortBy: "createdAt"});
  } catch (err) {
    console.log(err)
    user = {};
    roles = [];
    const {status} = err
    return {
      redirect: RedirectConfig(status)
    }
  }
  return {
    props: {
      user: JSON.parse(JSON.stringify(user)),
      roles: JSON.parse(JSON.stringify(roles.results))
    }
  }
}
export default EditUser
