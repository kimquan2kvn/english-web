import React, {useEffect, useState} from "react";
import ManageLayout from "common/layouts/manage-layout";
import {Avatar, Button, Card, Modal, Pagination, Table, Tooltip, Input, notification, Spin, Select} from "antd";
import {DeleteOutlined, EditOutlined, EyeOutlined, UserAddOutlined, UserOutlined} from "@ant-design/icons";
import Link from "next/link"
import ApiService from "common/services/ApiService";
import RedirectConfig from "common/configs/RedirectConfig";
import {useUser} from "common/hooks/useRequest";
import {query} from "winston";
import Utils from "../../../common/utils";
import {useSelector} from "react-redux";

const {Search} = Input
const ManageUser = props => {
  const theme = useSelector(state => state.theme);
  console.log('theme', theme)
  const [options, setOptions] = useState({page: 1, limit: 10, sortBy: "-id"});
  const [shouldFetch, setShouldFetch] = useState(false);
  const [users, setUsers] = useState(props.users || [])
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});
  const {data, loading, error} = useUser(options, shouldFetch);
  const [value, setValue] = useState("username");
  const [currentPage, setCurrentPage] = useState(1)

  useEffect(() => {
    if (data && data.status === 200) {
      const {results, totalResults, page, totalPages} = data.data
      setUsers(results)
      setPageInfo({totalResults: totalResults, hasNextPage: page < totalPages})
      setShouldFetch(false)
    }
    if (error) {
      setShouldFetch(false)
    }
  }, [options.page, data, error]) // eslint-disable-line react-hooks/exhaustive-deps

  const showDeleteConfirm = (username) => {
    Modal.confirm({
      title: Utils.translate('message.delete.user'),
      content: Utils.translate('warning.delete.user'),
      okText: Utils.translate('label.yes'),
      okType: "danger",
      cancelText: Utils.translate('label.no'),
      onOk: async () => {
        try {
          const res = await ApiService.deleteUser(username)
          if (res.status === 200) {
            const newUsers = [...users].filter(user => user.username !== res.data.user.username)
            setUsers(newUsers)
            notification.success({
              message: res.data.message
            })
          }
        } catch (err) {
          console.log(err)
        }
      },
      onCancel() {
        console.log('Cancel');
      }
    })
  }

  const tableColumns = [
    {
      title: Utils.translate('label.avatar'),
      dataIndex: "avatar",
      align: "center",
      render: (_, record) => {
        return (
          (
            <Link href={'/profile/' + record._id} passHref>
              <Avatar
                size={54}
                src={record.avatar}
                icon={<UserOutlined/>}
                style={{
                  justifyContent: "center",
                }}
              />
            </Link>
          )
        )
      },
    },
    {
      title: Utils.translate('label.full.name'),
      dataIndex: "fullName",
      align: "center",
    },
    {
      title: Utils.translate('label.username'),
      dataIndex: "username",
      align: "center",
      render: (record) => <span>{record}</span>
    },
    {
      title: Utils.translate('label.display.name'),
      dataIndex: "displayName",
      align: "center",
      render: (record) => {
        return (
          <>
            {record ? (
              <span>{record}</span>
            ) : <span>EOF User</span>}
          </>
        )
      }
    },
    {
      title: Utils.translate('label.role'),
      dataIndex: "role",
      align: "center",
      width: 200,
      render: (record) => {
        return (
          <span style={{marginRight: "8px"}}>{record.name}</span>
        )
      }
    },
    {
      title: Utils.translate('label.options'),
      dataIndex: 'actions',
      align: "center",
      render: (_, record) => {
        return (
          <div className="text-right d-flex justify-content-center">
            <Tooltip title={Utils.translate('label.view')}>
              <Button type="primary" className="mr-2" icon={<EyeOutlined/>}
                      onClick={() => {
                        if (typeof window !== "undefined") {
                          window.open("/profile/" + record?.username)
                        }
                      }}
                      size="small"/>
            </Tooltip>
            <Tooltip title={Utils.translate('label.edit')}>
              <Link href={`/manage/user/edit/` + record['username']} passHref>
                <Button success="true" className="mr-2" icon={<EditOutlined/>} size="small"/>
              </Link>
            </Tooltip>
            <Tooltip title={Utils.translate('label.delete')}>
              <Button danger icon={<DeleteOutlined/>}
                      onClick={() => showDeleteConfirm(record['username'])}
                      size="small"/>
            </Tooltip>
          </div>
        )
      }
    }
  ]

  const suffixSelector = (
    <Select
      value={value}
      onChange={(e) => setValue(e)}
    >
      <Select.Option value="username">{Utils.translate('label.username')}</Select.Option>
      <Select.Option value="fullName">{Utils.translate('label.full.name')}</Select.Option>
      <Select.Option value="displayName">{Utils.translate('label.display.name')}</Select.Option>
      <Select.Option value="role">{Utils.translate('label.role')}</Select.Option>
    </Select>
  );

  return (
    <ManageLayout>
      <Spin spinning={loading && shouldFetch} tip={Utils.translate('label.loading.data')}>
        <div className="user-list">
          <div className="search-bar mb-4 d-flex justify-content-between flex-wrap">
            <Link href="/manage/user/add" passHref>
              <Button type="primary" icon={<UserAddOutlined/>} className="mr-3 mb-3">{Utils.translate('label.add.user')}</Button>
            </Link>
            <Search
              addonBefore={suffixSelector}
              onChange={(e) => {
                if (e.target.value === "") {
                  setShouldFetch(true)
                  setOptions({page: currentPage, limit: 10, sortBy: "-id"})
                }
              }}
              placeholder={Utils.translate('label.input.search.text')}
              onSearch={query => {
                if (query === "") return;
                setShouldFetch(true)
                setOptions({page: 1, limit: 10, sortBy: "-id", [value]: query})
              }} enterButton style={{width: 400}}/>
          </div>
          <Card
            bodyStyle={{'padding': '8px'}}
          >
            <div className="table-responsive">
              <Table
                columns={tableColumns}
                pagination={false}
                dataSource={users}
                rowKey='_id'
                footer={() => {
                  return (
                    <Pagination
                      showQuickJumper
                      current={options.page}
                      defaultCurrent={currentPage}
                      total={pageInfo.totalResults}
                      onChange={(page, pageSize) => {
                        setCurrentPage(page)
                        setOptions({...options, page: page});
                        setShouldFetch(true);
                      }}
                    />
                  )
                }}/>
            </div>
          </Card>
        </div>
      </Spin>
    </ManageLayout>
  )
}

export const getServerSideProps = async (context) => {
  const auth = require("server/utils/auth");
  const {userService} = require("server/services");
  let users = {};
  try {
    await auth(context, ["MANAGE_ALL_USER", "GET_ALL_USER"]);
    users = await userService.queryUsers({}, {limit: 10, page: 1, sortBy: "-id"});
  } catch (err) {
    console.log(err)
    const {status, statusCode} = err;
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }

  if (!users || JSON.stringify(users) === "{}") return {props: {}}
  return {
    props: {
      users: JSON.parse(JSON.stringify(users.results)),
      pageInfo: {
        totalResults: users.totalResults,
        hasNextPage: users.page < users.totalPages
      }
    }
  }
}

export default ManageUser;
