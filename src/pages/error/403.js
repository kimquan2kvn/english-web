import React from "react";
import {AppLayout} from "../../common/layouts/app-layout";
import Link from "next/link";
import {Button, Card, Result} from "antd";

const NotAuthorized = (props) => {
  return (
    <AppLayout>
      <Card bordered={false}>
        <Result
          status="403"
          title="403"
          subTitle="Sorry, you are not authorized to access this page."
          extra={<Link href="/" passHref>
            <Button type="primary">Back Home</Button>
          </Link>}
        />
      </Card>
    </AppLayout>
  )
}
export default NotAuthorized
