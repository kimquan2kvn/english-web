import React from "react";
import {Button, Card, Result} from "antd";
import Link from "next/link"
import {AppLayout} from "../../common/layouts/app-layout";

const PageNotFound = () => {
  return (
    <AppLayout>
      <Card bordered={false}>
        <Result
          status="404"
          title="400"
          subTitle="Sorry, the page you visited does not exist."
          extra={<Link href="/" passHref>
            <Button type="primary">Back Home</Button>
          </Link>}
        />
      </Card>
    </AppLayout>
  )
}
export default PageNotFound
