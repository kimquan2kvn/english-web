import {connectDB, errorHandler} from "server/middleware";
import {authController} from "server/controllers";
import middlewares from "server";

const handler = (async (req, res) => {
  await middlewares(req, res);

  if (req.method === "POST") {
    try {
      await authController.refreshTokens(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);