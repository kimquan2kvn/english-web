import {connectDB, errorHandler, validate} from "server/middleware";
import {authController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {login} from "server/validations/auth.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  // await runMiddleware(req, res, validate(login));

  if (req.method === "POST") {
    try {
      await authController.login(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);