import {auth, connectDB, errorHandler} from "server/middleware";
import {examController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_EXAM", "ADD_ALL_EXAM"));

  if (req.method === "POST") {
    try {
      await examController.addExam(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);
