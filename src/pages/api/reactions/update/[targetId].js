import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {reactionController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {updateReaction} from "server/validations/reaction.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));
  await runMiddleware(req, res, validate(req, res, updateReaction));

  if (req.method === "POST") {
    try {
      await reactionController.updateReaction(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);