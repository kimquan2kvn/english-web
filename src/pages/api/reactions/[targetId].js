import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {reactionController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {getReactions} from "server/validations/reaction.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));
  await runMiddleware(req, res, validate(req, res, getReactions));

  if (req.method === "GET") {
    try {
      await reactionController.getReactions(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);