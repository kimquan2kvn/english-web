import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {blogController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {getBlogs} from "server/validations/blog.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  if (req.cookies.access_token || req.headers.authorization || req.body.access_token) {
    await runMiddleware(req, res, auth(req, res));
  }
  await runMiddleware(req, res, validate(req, res, getBlogs));

  if (req.method === "GET") {
    try {
      await blogController.getBlogs(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);