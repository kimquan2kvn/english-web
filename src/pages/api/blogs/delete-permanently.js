import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {blogController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {deleteBlogsPermanently} from "server/validations/blog.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_BLOG"));
  await runMiddleware(req, res, validate(req, res, deleteBlogsPermanently));

  if (req.method === "POST") {
    try {
      await blogController.deleteBlogsPermanently(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);