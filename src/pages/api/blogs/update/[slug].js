import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {blogController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {updateBlog} from "server/validations/blog.validation";
import uploadConfig from "server/config/upload.config";
import uploadData from "server/middleware/upload.middleware";

export const config = {
  api: {
    bodyParser: false
  }
};

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));
  await runMiddleware(req, res, validate(req, res, updateBlog));
  await runMiddleware(req, res, uploadData(req, res, uploadConfig.thumbnail.exts));

  if (req.method === "POST") {
    try {
      await blogController.updateBlog(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);