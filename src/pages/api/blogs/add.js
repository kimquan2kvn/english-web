import {auth, connectDB, validate, upload, errorHandler} from "server/middleware";
import {blogController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {createBlog} from "server/validations/blog.validation";
import uploadConfig from "server/config/upload.config";

export const config = {
  api: {
    bodyParser: false
  }
};

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_BLOG", "DELETE_ALL_BLOG"));
  await runMiddleware(req, res, validate(req, res, createBlog));
  await runMiddleware(req, res, upload(req, res, uploadConfig.thumbnail.exts));

  if (req.method === "POST") {
    try {
      await blogController.addBlog(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);