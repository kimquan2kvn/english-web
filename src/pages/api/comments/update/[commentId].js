import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {commentController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {updateComment} from "server/validations/comment.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));
  await runMiddleware(req, res, validate(req, res, updateComment));

  if (req.method === "POST") {
    try {
      await commentController.updateComment(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);