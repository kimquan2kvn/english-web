import connectDB from "server/middleware/mongodb.middleware";
import {Bot} from "server/bot/bot";
import MessengerProfile from "server/bot/lib/messenger-profile";
import Button from "server/bot/lib/button";

const handler = async (req, res) => {
  Bot.setCredentials(process.env.ACCESS_TOKEN);
  const profile = new MessengerProfile().setGetStarted({
    payload: "get_started"
  }).setPersistentMenu([
    {
      locale: "default",
      composer_input_disabled: false,
      call_to_actions: [
        new Button("postback", "Main menu").setPayload("main_menu"),
        new Button("postback", "About").setPayload("about"),
      ]
    },
    {
      locale: "vi_VN",
      composer_input_disabled: false,
      call_to_actions: [
        new Button("postback", "Menu chính").setPayload("main_menu"),
        new Button("postback", "Thông tin").setPayload("about"),
      ]
    }
  ]);
  Bot.setMessengerProfile(profile).then(r => {
    console.log("Profile was successfully set.")
  }).catch(err => {
    console.log("error", err);
  });

};

export default connectDB(handler);
