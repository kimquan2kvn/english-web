import {connectDB, errorHandler} from "server/middleware";
import middlewares from "server";
import {emailController} from "server/controllers";

const handler = (async (req, res) => {
  await middlewares(req, res);
  // await runMiddleware(req, res, auth(req, res));

  if (req.method === "POST") {
    try {
      await emailController.sendVerifyEmail(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);
