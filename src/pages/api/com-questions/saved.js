import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {communityQuestionController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));

  if (req.method === "GET") {
    try {
      await communityQuestionController.getSavedQuestion(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);