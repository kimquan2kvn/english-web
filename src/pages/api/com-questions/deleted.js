import {auth, connectDB, validate, errorHandler} from "server/middleware";
import {communityQuestionController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {getQuestions} from "server/validations/community-question.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));
  await runMiddleware(req, res, validate(req, res, getQuestions));

  if (req.method === "GET") {
    try {
      await communityQuestionController.getDeletedQuestions(req, res);
    } catch (err) {
      errorHandler(err, req, res)
    }
  }
});

export default connectDB(handler);