import {auth, connectDB, validate, errorHandler} from "server/middleware";
import {communityQuestionController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {getQuestion} from "server/validations/community-question.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  if (req.cookies.access_token || req.headers.authorization || req.body.access_token) {
    await runMiddleware(req, res, auth(req, res));
  }
  await runMiddleware(req, res, validate(req, res, getQuestion));

  if (req.method === "GET") {
    try {
      await communityQuestionController.getQuestion(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);