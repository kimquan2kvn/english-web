import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {questionController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {getQuestions} from "server/validations/question.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_EXAM_QUESTION", "DELETE_ALL_EXAM_QUESTION"));
  await runMiddleware(req, res, validate(req, res, getQuestions));

  if (req.method === "GET") {
    try {
      await questionController.getQuestions(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);
