import {auth, connectDB, errorHandler} from "server/middleware";
import {questionController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_EXAM_QUESTION", "DELETE_ALL_EXAM_QUESTION"));

  if (req.method === "POST") {
    try {
      await questionController.deleteQuestion(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);
