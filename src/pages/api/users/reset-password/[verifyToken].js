import {connectDB, errorHandler, validate} from "server/middleware";
import {resetPassword} from "server/validations/user.validation";
import {userController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, validate(req, res, resetPassword));

  if (req.method === "POST") {
    try {
      await userController.resetPassword(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);
