import {auth, connectDB, validate, errorHandler} from "server/middleware";
import {userController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {updateUser} from "server/validations/user.validation";
import uploadData from "server/middleware/upload.middleware";
import uploadConfig from "server/config/upload.config";

export const config = {
  api: {
    bodyParser: false
  }
};

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_USER", "UPDATE_ALL_USER"));
  await runMiddleware(req, res, validate(req, res, updateUser));
  await runMiddleware(req, res, uploadData(req, res, uploadConfig.avatar.exts));

  if (req.method === "POST") {
    try {
      await userController.updateUser(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);
