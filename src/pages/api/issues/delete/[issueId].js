import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {issueController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {deleteIssue} from "server/validations/issue.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_ISSUE"));
  await runMiddleware(req, res, validate(req, res, deleteIssue));

  if (req.method === "GET") {
    try {
      await issueController.deleteIssue(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);