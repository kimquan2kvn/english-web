import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {issueController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {restoreIssues} from "server/validations/issue.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_ISSUE"));
  await runMiddleware(req, res, validate(req, res, restoreIssues));

  if (req.method === "POST") {
    try {
      await issueController.restoreIssues(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);