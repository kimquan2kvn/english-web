import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {issueController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {updateIssue} from "server/validations/issue.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));
  await runMiddleware(req, res, validate(req, res, updateIssue));

  if (req.method === "POST") {
    try {
      await issueController.updateIssue(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);