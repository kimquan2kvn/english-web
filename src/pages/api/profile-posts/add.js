import {auth, connectDB, validate, upload, errorHandler} from "server/middleware";
import {profilePostController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {createPost} from "server/validations/profile-post.validation";
import uploadConfig from "server/config/upload.config";

export const config = {
  api: {
    bodyParser: false
  }
};

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));
  await runMiddleware(req, res, validate(req, res, createPost));
  await runMiddleware(req, res, upload(req, res, uploadConfig.thumbnail.exts));

  if (req.method === "POST") {
    try {
      await profilePostController.addPost(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);