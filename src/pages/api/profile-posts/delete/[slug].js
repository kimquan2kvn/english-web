import {auth, connectDB, errorHandler} from "server/middleware";
import {profilePostController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));
  // await runMiddleware(req, res, validate(req, res, deleteBlog));

  if (req.method === "POST") {
    try {
      await profilePostController.deletePost(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);