import React, {useEffect, useState} from "react";
import ApiService from "../common/services/ApiService";
import CommunityLayout from "../common/layouts/community-layout";
import {BackTop, Button, Card, Divider, Drawer, Grid} from "antd";
import QuestionList from "../common/components/forum-components/question-list";
import RedirectConfig from "../common/configs/RedirectConfig";
import Utils from "../common/utils";
import {
  ArrowUpOutlined,
  PlusCircleOutlined
} from "@ant-design/icons";
import {useSelector} from "react-redux";
import {useRouter} from "next/router";


const {useBreakpoint} = Grid;


const Community = (props) => {
  const {favoriteQuestions, popularQuestions, totalComQuestions} = props;
  const [totalQuestions, setTotalQuestions] = useState(0);
  const [questions, setQuestions] = useState([]);
  const [totalResults, setTotalResults] = useState(1);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const state = useSelector(state => state);
  const router = useRouter();


  useEffect(() => {
    const {results, totalResults} = props.questionData;
    setQuestions(results);
    setTotalResults(totalResults);
  }, [props.questionData]) // eslint-disable-line react-hooks/exhaustive-deps

  const loadQuestions = async (nextPage) => {
    setLoading(true);
    try {
      const res = await ApiService.getQuestions({page: nextPage, limit: 10, sortBy: '-createdAt'});
      const {results, page, totalPages, totalResults} = res.data;
      // const newQuestions = [...questions, ...results];
      setQuestions(results);
      setTotalResults(totalResults);
    } catch (err) {
    }
    setLoading(false);
  }

  return (
    <CommunityLayout sideData={{popularQuestions, favoriteQuestions}} totalComQuestions={totalComQuestions}>
      <Card bordered={false} bodyStyle={{padding: "4px"}}>
        <div className="header-content mb-2">
          <div className="d-flex justify-content-between mb-4 flex-wrap">
            <div className="d-flex align-items-center mb-2">
              <h2 className="mr-2 mb-0">{Utils.translate("label.all.questions")}</h2>
              <p className="m-0">{`(${totalComQuestions})`}</p>
            </div>
            <Button icon={<PlusCircleOutlined/>} onClick={() => {
              router.push('/forum/add-question');
            }} type="primary" size="small">{Utils.translate("label.ask.question")}</Button>
          </div>
          <Divider type="dashed"/>
        </div>
        <div>
          <QuestionList
            {...props}
            setCurrentPage={setCurrentPage}
            currentPage={currentPage}
            setQuestions={setQuestions}
            questions={questions}
            totalResults={totalResults}
            loading={loading}
            loadQuestions={loadQuestions}
          />
        </div>
      </Card>
    </CommunityLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const connectDb = require("server/utils/connect-db");
  const {communityQuestionService} = require("server/services");
  let data = {};
  try {
    const {
      favoriteQuestionsData,
      popularQuestionsData,
      totalComQuestions,
      userData
    } = await Utils.getDataComQuestionLayout(auth, communityQuestionService, ctx, connectDb);
    data['popularQuestions'] = popularQuestionsData?.results ?? [];
    data['favoriteQuestions'] = favoriteQuestionsData?.results ?? [];
    data['totalComQuestions'] = totalComQuestions?.totalResults ?? 0;
    data['questionData'] = await communityQuestionService.queryQuestions({deleted: {$ne: true}}, {
      limit: 10,
      page: 1,
      sortBy: '-createdAt'
    }, userData ? userData.user._id : null);
  } catch (err) {
    const {status, statusCode} = err;
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  const {favoriteQuestions, totalComQuestions, popularQuestions, questionData} = data;
  return {
    props: {
      totalComQuestions,
      favoriteQuestions,
      popularQuestions,
      questionData: JSON.parse(JSON.stringify(questionData))
    }
  }
}

export default Community;
