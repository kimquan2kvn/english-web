import {IncomingForm} from 'formidable'
import httpStatus from "http-status";
import ApiError from "server/utils/api-error";
import {errorHandler} from "server/middleware";
import {uploadFile} from "server/utils/google-storage";

const uploadData = (req, res, extFilter = [], limitedSize = 10 * 1024 * 1024) => {
  return new Promise((resolve, reject) => {
    const uniqueSuffix = Date.now() + '_' + Math.round(Math.random() * 1E9);
    const form = new IncomingForm();
    form.parse(req, async (err, fields, files) => {
      try {
        if (err) {
          throw new ApiError(httpStatus.BAD_REQUEST, "Failed to upload file");
        }
        req.body = fields;
        if (Object.keys(files).length <= 0) return resolve();

        const {newFilename, filepath, mimetype: rawMimetype, size} = files.file;
        const mimetype = rawMimetype.split("/")[1];
        const destFileName = `${newFilename}_${uniqueSuffix}.${mimetype}`

        const gFile = await uploadFile(filepath, {
          destination: destFileName,
        });

        if (!extFilter.includes(mimetype)) {
          throw new ApiError(httpStatus.BAD_REQUEST, "Invalid file type");
        }

        if (size > limitedSize) {
          throw new ApiError(httpStatus.BAD_REQUEST, "This file was to large to upload");
        }
        req.file = files.file;
        req.file.linkUrl = gFile[1]["mediaLink"];
        resolve();
      } catch (err) {
        errorHandler(err, req, res);
      }
    });
  });
}

export default uploadData;