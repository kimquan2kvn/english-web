import {communityTagService} from '../services';
import pick from "../utils/pick";

export const addTag = async (req, res) => {
  const tag = await communityTagService.createTag(req.body);
  res.json({
    message: "Created Tag successfully",
    tag
  });
};

export const getTags = async (req, res) => {
  const filter = pick(req.query, ['title', 'slug']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await communityTagService.queryTags(filter, options);
  res.json(result);
};

export const getTag = async (req, res) => {
  const result = await communityTagService.getTagByFilter({_id: req.params.tagId});
  res.json(result);
};


export const deleteTag = async (req, res) => {
  const tag = await communityTagService.deleteTag(req.params.tagId);
  res.json({
    message: "Deleted Tag successfully",
    tag
  });
};