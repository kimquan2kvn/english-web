import {examService, userActivityService} from '../services';
import pick from "../utils/pick";

export const addExam = async (req, res) => {
  req.body.creator = req.user._id;
  const exam = await examService.createExam(req.body);
  res.json({
    message: "Created Exam successfully",
    exam
  });
  await userActivityService.createUserActivity({
    message: `have added an exam`,
    detail: exam.name,
    target: exam._id,
    model: "Exam",
    user: req.user._id
  });
};

export const getExams = async (req, res) => {
  const filter = pick(req.query, ['name', 'code']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await examService.queryExams(filter, options, req.user);
  res.json(result);
};

export const getExam = async (req, res) => {
  let result = await examService.getExamByFilter({code: req.query.code}, req.user);
  res.json(result);
};

export const updateExam = async (req, res) => {
  const exam = await examService.updateExam(req.query.code, req.body);
  res.json({
    message: "Updated Exam successfully",
    exam
  });
  await userActivityService.createUserActivity({
    message: `have updated an exam`,
    target: exam._id,
    detail: exam.name,
    model: "Exam",
    user: req.user._id
  });
};

export const deleteExam = async (req, res) => {
  const exam = await examService.deleteExam(req.query.code);
  res.json({
    message: "Deleted Exam successfully",
    exam
  });
  await userActivityService.createUserActivity({
    message: `have deleted an exam`,
    detail: exam.name,
    target: exam._id,
    model: "Exam",
    user: req.user._id
  });
};

export const submitExam = async (req, res) => {
  const submitBody = req.body;
  const result = await examService.submitExam(req.query.code, req.user._id, submitBody);
  res.json({
    message: "Submit exam successfully",
    result
  });
  await userActivityService.createUserActivity({
    message: `have submitted an exam`,
    detail: result.exam.name,
    target: result.exam._id,
    model: "Exam",
    user: req.user._id
  });
};

export const getResult = async (req, res) => {
  const result = await examService.getExamResult(req.query.code, req.user._id);
  res.json(result);
};

export const getRanking = async (req, res) => {
  const filter = pick(req.query, [""]);
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const result = await examService.getExamRanking(req.query.code, filter, options);
  res.json(result);
};

export const getCompletedExams = async (req, res) => {
  const filter = pick(req.query, [""]);
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const result = await examService.getDoneExams(req.user.username, filter, options);
  res.json(result);
};
