import {profilePostService, userActivityService} from '../services';
import pick from "../utils/pick";

export const addPost = async (req, res) => {
  if (req.file) {
    req.body.thumbnail = req.file.linkUrl;
  }
  const post = await profilePostService.createPost(req.body, req.user._id);
  res.json({
    message: "Created post successfully",
    post
  });
  await userActivityService.createUserActivity({
    message: `has added a new post`,
    detail: post.content,
    target: post._id,
    model: "ProfilePost",
    user: req.user._id
  });
};

export const getPosts = async (req, res) => {
  const filter = pick(req.query, ['title', 'slug', 'author', "location"]);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  Object.assign(filter, {deleted: {$ne: true}});
  let userId = req.user ? req.user._id : null;
  const result = await profilePostService.queryPosts(filter, options, userId);
  res.json(result);
};

export const getPost = async (req, res) => {
  let userId = req.user ? req.user._id : null;
  const result = await profilePostService.getPostByFilter({slug: req.query.slug}, userId);
  res.json(result);
};

export const updatePost = async (req, res) => {
  const post = await profilePostService.updatePost(req.query.slug, req.body, (req.user.role.permissions.includes('MANAGE_ALL_BLOG') || req.user.role.permissions.includes('UPDATE_ALL_BLOG')), req.user._id, req.file);
  res.json({
    message: "Updated post successfully",
    post
  });
  await userActivityService.createUserActivity({
    message: `has updated a post`,
    detail: post.content,
    target: post._id,
    model: "ProfilePost",
    user: req.user._id
  });
};

export const deletePost = async (req, res) => {
  const post = await profilePostService.deletePost(req.query.slug, (req.user.role.permissions.includes('MANAGE_ALL_BLOG') || req.user.role.permissions.includes('DELETE_ALL_BLOG')), req.user._id);
  res.json({
    message: "Deleted post successfully",
    post
  });
  await userActivityService.createUserActivity({
    message: `has deleted a post`,
    detail: post.content,
    target: post._id,
    model: "ProfilePost",
    user: req.user._id
  });
};

export const getDeletedPosts = async (req, res) => {
  const filter = pick(req.query, ['title', 'slug']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  Object.assign(filter, {deleted: true});
  const result = await profilePostService.queryPosts(filter, options);
  res.json(result);
};

export const restorePosts = async (req, res) => {
  const result = await profilePostService.restorePosts(req.body);
  res.json({
    message: "Restore posts successfully",
    result
  });
};

export const deletePostsPermanently = async (req, res) => {
  const result = await profilePostService.deletePostsPermanently(req.body);
  res.json({
    message: "Delete posts successfully",
    result
  });
};