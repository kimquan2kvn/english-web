import {uploadToDiscord as upload} from "server/utils/discord-uploader";

export const uploadToDiscord = async (req, res) => {
    const data = await upload(req.body);
    res.json(data);
}