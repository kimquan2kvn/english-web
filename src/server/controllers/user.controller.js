import {tokenService, userService} from 'server/services';
import pick from "server/utils/pick";
import {default as defaultURL} from 'server/config/upload.config';
import {Role, Token} from "server/models";
import {tokenTypes} from "server/config/tokens.config";
import ApiError from "server/utils/api-error";
import httpStatus from "http-status";

const publicFields = "username email fullName dob gender avatar";

export const addUser = async (req, res) => {
  if (!req.file) {
    req.body.avatar = defaultURL.url;
  } else {
    req.body.avatar = req.file.linkUrl;
  }
  const user = await userService.createUser(req.body);
  res.json({
    message: "Created user successfully",
    user
  });
};

export const getUsers = async (req, res) => {
  const filter = pick(req.query, ['displayName', 'fullName', 'username', 'role', 'id']);
  if (filter.role) filter.role = (await Role.findOne({name: {$regex: filter.role, $options: 'i'}}))._id
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await userService.queryUsers(filter, options);
  res.json(result);
};

export const getPublicUsers = async (req, res) => {
  const filter = pick(req.query, ['displayName', 'fullName', 'username', 'role', 'id']);
  if (filter.role) filter.role = (await Role.findOne({name: {$regex: filter.role, $options: 'i'}}))._id;
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  Object.assign(options, {select: publicFields});
  const result = await userService.queryUsers(filter, options);
  res.json(result);
};

export const getUser = async (req, res) => {
  const result = await userService.getUserByFilter({username: req.query.username}, "");
  res.json(result);
};

export const getPublicUser = async (req, res) => {
  const result = await userService.getUserByFilter({username: req.query.username}, publicFields);
  res.json(result);
};

export const getSelfInfo = async (req, res) => {
  const result = await userService.getUserByFilter({_id: req.user._id});
  res.json(result);
};

export const updateUser = async (req, res) => {
  const user = await userService.updateUser({username: req.query.username}, req.body, req.file);
  res.json({
    message: "Updated user successfully",
    user
  });
};

export const updateSelfProfile = async (req, res) => {
  const user = await userService.updateUser({username: req.user.username}, req.body, req.file);
  res.json({
    message: "Updated user successfully",
    user
  });
};
export const resetPassword = async (req, res) => {
  const tokenDoc = await tokenService.verifyToken(req.query.verifyToken, tokenTypes.RESET_PASSWORD).catch(async _ => {
    await Token.deleteOne({token: req.query.verifyToken, type: tokenTypes.RESET_PASSWORD});
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "Your verification was expired! Please request for a new one")
  });
  const user = await userService.updateUser({
    _id: tokenDoc.user
  }, {password: req.body.password});
  res.json({
    message: "Reset password successfully"
  });
  console.log({token: req.query.verifyToken, user: user._id, type: tokenTypes.RESET_PASSWORD})
  await Token.deleteOne({token: req.query.verifyToken, user: user._id, type: tokenTypes.RESET_PASSWORD});
};

export const deleteUser = async (req, res) => {
  const user = await userService.deleteUser({username: req.query.username});
  res.json({
    message: "Deleted user successfully",
    user
  });
};
