import {reactionService, userActivityService} from '../services';

export const getReactions = async (req, res) => {
  const result = await reactionService.getReactions(req.query.targetId);
  res.json(result);
};

export const updateReaction = async (req, res) => {
  const result = await reactionService.updateReaction(req.query.targetId, req.user._id, req.body);
  res.json(result);
  if (result.message) {
    await userActivityService.createUserActivity({
      message: "has" +  result.message.slice("You have".length),
      detail: result.reaction.type,
      target: result.target,
      model: (!req.body.reactTo) ? req.body.model : req.body.reactTo,
      user: req.user._id
    });
  }
};