import catchAsync from '../utils/catch-async';
import {userActivityService} from '../services';
import pick from "../utils/pick";

export const addUserActivity = catchAsync(async (req, res) => {
  const userActivity = await userActivityService.createUserActivity(req.body);
  res.json({
    message: "Created activity successfully",
    userActivity
  });
});

export const getUserActivities = async (req, res) => {
  const filter = pick(req.query, [""]);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await userActivityService.queryUserActivities(req.user, filter, options);
  res.json(result);
};

export const deleteUserActivity = catchAsync(async (req, res) => {
  const userActivity = await userActivityService.deleteUserActivity(req.query.activityId);
  res.json({
    message: "Deleted userActivity successfully",
    userActivity
  });
});
