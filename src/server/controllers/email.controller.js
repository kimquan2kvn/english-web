import {content} from "server/config/email.config";
import {emailService, userService} from "server/services";

export const sendVerifyEmail = async (req, res) => {
  const user = await userService.getUserByFilter({email: req.body.email});
  const message = (user.isVerifiedEmail) ? "This email has been verified" : "A verify email was sent, you'll get an email with a link to verify your shortly.";
  await emailService.sendVerifyEmail(user);
  res.json({message});
}

export const sendResetPasswordEmail = async (req, res) => {
  const user = await userService.getUserByFilter({email: req.body.email});
  await emailService.sendResetPasswordEmail(user);
  res.json({message: "If your EoF account matches with the email, you'll get an email with a link to reset your password shortly."});
}