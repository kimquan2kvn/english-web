import {blogService, userActivityService} from '../services';
import pick from "../utils/pick";

export const addBlog = async (req, res) => {
  if (req && req.file) {
    req.body.thumbnail = req.file.linkUrl;
  }
  const blog = await blogService.createBlog(req.body, req.user._id);
  res.json({
    message: "Created news successfully",
    blog
  });
  await userActivityService.createUserActivity({
    message: `has added a new blog`,
    detail: blog.title,
    target: blog._id,
    model: "Blog",
    user: req.user._id
  });
};

export const getBlogs = async (req, res) => {
  const filter = pick(req.query, ['title', 'slug', 'author']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  Object.assign(filter, {deleted: {$ne: true}});
  let userId = req.user ? req.user._id : null;
  const result = await blogService.queryBlogs(filter, options, userId);
  res.json(result);
};

export const getBlog = async (req, res) => {
  let userId = req.user ? req.user._id : null;
  const result = await blogService.getBlogByFilter({slug: req.query.slug}, userId);
  res.json(result);
};

export const updateBlog = async (req, res) => {
  const blog = await blogService.updateBlog(req.query.slug, req.body, req.user.role.permissions.includes('MANAGE_ALL_BLOG'), req.user._id, req.file);
  res.json({
    message: "Updated news successfully",
    blog
  });
  await userActivityService.createUserActivity({
    message: `has updated a blog`,
    detail: blog.title,
    target: blog._id,
    model: "Blog",
    user: req.user._id
  });
};

export const deleteBlog = async (req, res) => {
  const blog = await blogService.deleteBlog(req.query.slug, req.user.role.permissions.includes('MANAGE_ALL_BLOG'), req.user._id);
  res.json({
    message: "Deleted news successfully",
    blog
  });
  await userActivityService.createUserActivity({
    message: `has deleted a blog`,
    detail: blog.title,
    target: blog._id,
    model: "Blog",
    user: req.user._id
  });
};

export const getDeletedBlogs = async (req, res) => {
  const filter = pick(req.query, ['title', 'slug']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  Object.assign(filter, {deleted: true});
  const result = await blogService.queryBlogs(filter, options);
  res.json(result);
};

export const restoreBlogs = async (req, res) => {
  const result = await blogService.restoreBlogs(req.body);
  res.json({
    message: "Restore Blogs successfully",
    result
  });
};

export const deleteBlogsPermanently = async (req, res) => {
  const result = await blogService.deleteBlogsPermanently(req.body);
  res.json({
    message: "Delete blogs successfully",
    result
  });
};
