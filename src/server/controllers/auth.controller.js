import httpStatus from 'http-status';
import {authService, userService, tokenService, emailService} from 'server/services';
import {removeCookies} from 'cookies-next';
import {tokenTypes} from "server/config/tokens.config";
import ApiError from "server/utils/api-error";
import {setAuthCookies} from "server/utils/set-auth-cookies";
import {Token} from "server/models";

export const register = async (req, res) => {
  const user = await userService.createUser(req.body);
  // const tokens = await tokenService.generateAuthTokens(user);
  res.status(httpStatus.CREATED).send({user/*, tokens*/});
  emailService.sendVerifyEmail(user).then();
};

export const login = async (req, res) => {
  const user = await authService.login(req.body);
  if (!user.isVerifiedEmail) return res.json(user);
  await setAuthCookies(req, res, user);
};

export const verifyAccount = async (req, res) => {
  const tokenDoc = await tokenService.verifyToken(req.query.verifyToken, tokenTypes.VERIFY_EMAIL).catch(async _ => {
    await Token.deleteOne({token: req.query.verifyToken, type: tokenTypes.VERIFY_EMAIL});
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "Your verification was expired! Please request for a new one")
  });

  const user = await userService.updateUser({
    _id: tokenDoc.user, isVerifiedEmail: {$ne: true}
  }, {isVerifiedEmail: true});

  await Token.deleteOne({token: req.query.verifyToken, user: user._id, type: tokenTypes.VERIFY_EMAIL});
  await setAuthCookies(req, res, user);
}

export const logout = async (req, res) => {
  if (req.body.refreshToken) await authService.logout(req.body.refreshToken);
  removeCookies("access_token", {req, res});
  removeCookies("refresh_token", {req, res});
  res.status(httpStatus.OK).send();
};

export const refreshTokens = async (req, res) => {
  try {
    const result = await authService.refreshAuth(req.body.refreshToken);
    res.json(result);
  } catch (e) {
    throw new Error("Refresh token failed");
  }
};

export const connectToFB = async (req, res) => {
  const data = await authService.connectToFB(req.body, req.user);
  res.json(data);
};

export const connectToGG = async (req, res) => {
  const data = await authService.connectToGG(req.body, req.user);
  res.json(data);
};
