import {questionService} from '../services';
import pick from "../utils/pick";

export const addQuestion = async (req, res) => {
  req.body.author = req.user._id;
  const questions = await questionService.createQuestion(req.body);
  res.json({
    message: `Created ${questions.length} question(s) successfully`,
    questions
  });
};

export const getQuestions = async (req, res) => {
  const filter = pick(req.query, ['id', 'type']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await questionService.queryQuestions(filter, options);
  res.json(result);
};

export const getQuestion = async (req, res) => {
  let result = await questionService.getQuestionByFilter({id: req.query.questionId});
  res.json(result);
};

export const updateQuestion = async (req, res) => {
  const question = await questionService.updateQuestion(req.query.questionId, req.body);
  res.json({
    message: "Updated Question successfully",
    question
  });
};

export const deleteQuestion = async (req, res) => {
  const question = await questionService.deleteQuestion(req.query.questionId);
  res.json({
    message: "Deleted Question successfully",
    question
  });
};
