import {commentService, userActivityService} from '../services';
import pick from "../utils/pick";

export const addComment = async (req, res) => {
  const comment = await commentService.createComment(req.body, req.user._id);
  res.json({
    message: "Created Comment successfully",
    comment
  });
  await userActivityService.createUserActivity({
    message: `has added a new comment`,
    target: comment.category._id,
    detail: comment.content,
    model: req.body.model,
    user: req.user._id
  });
};

export const getComments = async (req, res) => {
  const filter = pick(req.query, ['category', 'author', 'replyFor', 'level']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  let userId = req.user ? req.user._id : null;
  const result = await commentService.queryComments(filter, options, userId);
  res.json(result);
};

export const getComment = async (req, res) => {
  let userId = req.user ? req.user._id : null;
  const result = await commentService.getCommentByFilter({_id: req.query.commentId}, userId);
  res.json(result);
};

export const updateComment = async (req, res) => {
  const hasAuth = req.user.role.permissions.includes('MANAGE_ALL_COMMENT') || req.user.role.permissions.includes('UPDATE_ALL_COMMENT');
  req.body.author = req.user._id;
  const comment = await commentService.updateComment(req.query.commentId, req.body, hasAuth);
  res.json({
    message: "Updated Comment successfully",
    comment
  });
  await userActivityService.createUserActivity({
    message: `has updated a comment`,
    detail: comment.content,
    target: comment.category._id,
    model: req.body.model,
    user: req.user._id
  });
};

export const deleteComment = async (req, res) => {
  const deleteData = {commentId: req.query.commentId, author: req.user._id, model: req.body.model}
  const hasAuth = req.user.role.permissions.includes('MANAGE_ALL_COMMENT') || req.user.role.permissions.includes('DELETE_ALL_COMMENT');
  const comment = await commentService.deleteComment(deleteData, hasAuth);
  res.json({
    message: "Deleted Comment successfully",
    comment
  });
  await userActivityService.createUserActivity({
    message: `has delete a comment`,
    detail: comment.content,
    target: comment.category._id,
    model: req.body.model,
    user: req.user._id
  });
};