import TurnDown from "turndown";
export const turnDownService = new TurnDown();
export const parseHTML = (str) => turnDownService.turndown(str).replace(/\\/g, '');
