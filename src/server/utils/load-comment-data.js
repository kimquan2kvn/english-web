import {Comment} from "../models";

export const loadCommentData = (categoryArr) => {
  return new Promise(async (resolve, reject) => {
    try {
      for (const category of categoryArr) {
        Object.assign(category._doc, {
          nComments: await Comment.countDocuments({category: category._id}),
          nReactions: 0,
          isReacted: false
        });

        for (const comment of category.comments) {
          Object.assign(comment._doc, {
            nReplies: await Comment.countDocuments({replyFor: comment._id}),
            nReactions: 0,
            isReacted: false
          });
        }
      }
      resolve(categoryArr)
    } catch (e) {
      reject(e)
    }
  });
}