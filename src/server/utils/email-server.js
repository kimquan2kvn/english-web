import ApiError from "server/utils/api-error";
import httpStatus from "http-status";
import nodemailer from "nodemailer";
import smtpTransport from "nodemailer-smtp-transport";
import mailgun from "mailgun-js";
import {mailgun as mailgunConfig, env, email} from "server/config";
import logger from "server/config/logger.config";

const transporter = nodemailer.createTransport(smtpTransport({
  service: 'gmail', host: 'smtp.gmail.com', auth: {
    user: email.auth.user, pass: email.auth.pass
  }
}));

/**
 * Send email
 * @param {Object} mailData - Mail data
 * @param {string} mailData.subject
 * @param {string} mailData.contentHTML
 * @param {string} mailData.sendTo
 * @returns {Object}
 */
export const sendEmailFromNodeMailer = async (mailData) => {
  try {
    const info = await transporter.sendMail({
      from: 'quanganhlb2508@gmail.com', to: mailData.sendTo, subject: mailData.subject, html: mailData.contentHTML,
    });
    if (env !== "production") logger.info('Email sent: ' + info.response);
    return {
      message: 'A request was sent to your email'
    };
  } catch (e) {
    if (env !== "production") logger.error(e)
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "Cannot send email");
  }
}

const mg = mailgun({
  apiKey: mailgunConfig.apiKey, domain: mailgunConfig.domain
});

/**
 * Send email
 * @param {Object} mailData - Mail data
 * @param {string} mailData.subject
 * @param {string} mailData.contentHTML
 * @param {string} mailData.sendTo
 * @returns {Object}
 */
export const sendEmailFromMailGun = async (mailData) => {
  try {
    await mg.messages().send({
      from: `English or Foolish <noreply@${mailgunConfig.domain}>`,
      to: mailData.sendTo,
      subject: mailData.subject,
      html: mailData.contentHTML,
    });
    if (env !== "production") logger.info(`Email sent to ${mailData.sendTo}`);
    return {
      message: 'A request was sent to your email'
    };
  } catch (err) {
    if (env !== "production") logger.error(err);
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "Cannot send email");
  }
}