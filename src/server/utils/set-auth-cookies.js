import {tokenService} from 'server/services';
import {setCookies} from 'cookies-next';

export const setAuthCookies = async (req, res, user) => {
  const tokens = await tokenService.generateAuthTokens(user);
  const {access, refresh} = tokens;

  setCookies("access_token", access.token, {
    req, res, httpOnly: process.env.NODE_ENV === "production", secure: process.env.NODE_ENV === "production", // maxAge: (access.expires - new Date().getTime()) / 1000
  });
  setCookies("refresh_token", refresh.token, {
    req, res, httpOnly: process.env.NODE_ENV === "production", secure: process.env.NODE_ENV === "production", // maxAge: (refresh.expires - new Date().getTime()) / 1000
  });

  res.send({user, tokens});
}