import axios from "axios";
import {discord, env} from "server/config";

export const uploadToDiscord = (data) => new Promise(async (resolve, reject) => {
  try {
    const {data: res} = await axios.post("https://upload.eof.vn/api/upload", {...data, secret: discord.secret});
    const processedFiles = res.map(data => {
      return (env === "production") ? data.proxyURL.replace("media.discordapp.net/attachments", "media.eof.vn") : data.url;
    });
    resolve(processedFiles);
  } catch (err) {
    console.log(err)
    reject(err)
  }
});
