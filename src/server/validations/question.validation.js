import Joi from 'joi';
import {password, ObjectId} from "./custom.validations";

const addQuestion = {
  body: Joi.object().keys({
    // Questionname: Joi.string().required(),
    // email: Joi.string().email().required(),
    // password: Joi.string().custom(password).required(),
    // fullName: Joi.string(),
    // role: Joi.string(),
    // gender: Joi.string(),
    // phoneNumber: Joi.string(),
    // dob: Joi.alternatives().try(Joi.number(), Joi.string()),
    // bio: Joi.string(),
    // displayName: Joi.string(),
    // avatar: Joi.any(),
  })
};

const getQuestions = {
  query: Joi.object().keys({
    type: Joi.string(),
    id: Joi.number(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getQuestion = {
  query: Joi.object().keys({
    questionId: Joi.string().custom(ObjectId).required()
  })
};

const updateQuestion = {
  params: Joi.object().keys({
    QuestionId: Joi.string().custom(ObjectId).required()
  }),
  body: Joi.object().keys({
    Questionname: Joi.string(),
    email: Joi.string().email(),
    role: Joi.string(),
    fullName: Joi.string(),
    password: Joi.string().custom(password),
    currentPassword: Joi.string().custom(password),
    phoneNumber: Joi.string(),
    dob: Joi.alternatives().try(Joi.number(), Joi.string()),
    gender: Joi.string(),
    bio: Joi.string(),
    displayName: Joi.string(),
    avatar: Joi.any(),
  })
};

// extends current Joi object
const updateSelfProfileBody = updateQuestion.body.keys({currentPassword: Joi.string()})
const updateSelfProfile = {
  body: updateSelfProfileBody
};

const deleteQuestion = {
  params: getQuestion.params
}

export {
  addQuestion,
  getQuestions,
  getQuestion,
  updateQuestion,
  updateSelfProfile,
  deleteQuestion,
}