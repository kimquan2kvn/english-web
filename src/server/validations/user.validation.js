import Joi from 'joi';
import {password} from "./custom.validations";

const addUser = {
  body: Joi.object().keys({
    username: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().custom(password).required(),
    fullName: Joi.string(),
    role: Joi.string(),
    gender: Joi.string(),
    phoneNumber: Joi.string(),
    dob: Joi.alternatives().try(Joi.number(), Joi.string()),
    bio: Joi.string(),
    displayName: Joi.string(),
    avatar: Joi.any(),
  })
};

const getUsers = {
  query: Joi.object().keys({
    username: Joi.string(),
    displayName: Joi.string(),
    fullName: Joi.string(),
    role: Joi.string(),
    id: Joi.number(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getUser = {
  params: Joi.object().keys({
    username: Joi.string().required()
  })
};

const updateUser = {
  params: Joi.object().keys({
    username: Joi.string().required()
  }),
  body: Joi.object().keys({
    username: Joi.string(),
    email: Joi.string().email(),
    role: Joi.string(),
    fullName: Joi.string(),
    id: Joi.number(),
    password: Joi.string().custom(password),
    currentPassword: Joi.string().custom(password),
    phoneNumber: Joi.string(),
    dob: Joi.alternatives().try(Joi.number(), Joi.string()),
    gender: Joi.string(),
    bio: Joi.string(),
    displayName: Joi.string(),
    avatar: Joi.any(),
  })
};

// extends current Joi object
const updateSelfProfileBody = updateUser.body.keys({currentPassword: Joi.string()})
const updateSelfProfile = {
  body: updateSelfProfileBody
};

const deleteUser = {
  params: getUser.params
}

const resetPassword = {
  query: Joi.object().keys({
    verifyToken: Joi.string().required()
  }),
  body: Joi.object().keys({
    password: Joi.string().custom(password).required()
  })
}

export {
  addUser,
  getUsers,
  getUser,
  updateUser,
  updateSelfProfile,
  deleteUser,
  resetPassword
}