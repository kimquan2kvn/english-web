import Joi from 'joi';
import {ObjectId} from "./custom.validations";

const createQuestion = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    content: Joi.string().required(),
    privacy: Joi.string(),
    tags: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

const restoreQuestions = {
  body: Joi.object().keys({
    questions: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

const getQuestions = {
  query: Joi.object().keys({
    title: Joi.string(),
    slug: Joi.string(),
    tag: Joi.string(),
    privacy: Joi.string(),
    author: Joi.string().custom(ObjectId),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getQuestion = {
  params: Joi.object().keys({
    slug: Joi.string(),
  }),
};

const updateQuestion = {
  params: Joi.object().keys({
    slug: Joi.string(),
  }),
  body: Joi.object()
    .keys({
      title: Joi.string(),
      content: Joi.string(),
      privacy: Joi.string(),
      tags: Joi.array().items(Joi.string().custom(ObjectId)),
    })
    .min(1),
};

const deleteQuestion = {
  params: Joi.object().keys({
    slug: Joi.string(),
  }),
};

const deleteQuestionsPermanently = {
  body: Joi.object().keys({
    comments: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

export {
  createQuestion,
  getQuestions,
  getQuestion,
  updateQuestion,
  deleteQuestion,
  restoreQuestions,
  deleteQuestionsPermanently,
};