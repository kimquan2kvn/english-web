import Joi from 'joi';
import {ObjectId} from "./custom.validations";

const createIssue = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    content: Joi.string().required(),
    type: Joi.string(),
    target: Joi.string().custom(ObjectId),
    model: Joi.string().required(),
  }),
};

const getIssues = {
  query: Joi.object().keys({
    category: Joi.string().custom(ObjectId),
    user: Joi.string().custom(ObjectId),
    model: Joi.string(),
    slug: Joi.string(),
    title: Joi.string(),
    type: Joi.string(),
    state: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getIssue = {
  params: Joi.object().keys({
    issueId: Joi.string().custom(ObjectId),
  }),
};

const updateIssue = {
  params: Joi.object().keys({
    issueId: Joi.required().custom(ObjectId),
  }),
  body: Joi.object()
    .keys({
      model: Joi.string(),
      state: Joi.string()
    })
    .min(1),
};


const deleteIssue = {
  params: Joi.object().keys({
    issueId: Joi.string().custom(ObjectId),
  }),
};

const restoreIssues = {
  body: Joi.object().keys({
    issues: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

const deleteIssuesPermanently = {
  body: Joi.object().keys({
    issues: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

export {
  createIssue,
  getIssues,
  getIssue,
  updateIssue,
  deleteIssue,
  restoreIssues,
  deleteIssuesPermanently,
};
