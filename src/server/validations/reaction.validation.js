import Joi from 'joi';
import {ObjectId} from "./custom.validations";

const getReactions = {
  params: Joi.object().keys({
    targetId: Joi.string().custom(ObjectId),
  }),
};

const updateReaction = {
  params: Joi.object().keys({
    targetId: Joi.string().required().custom(ObjectId),
  }),
  body: Joi.object()
    .keys({
      type: Joi.string(),
      active: Joi.string(),
      model: Joi.string().required(),
      reactTo: Joi.string(),
      delete: Joi.boolean()
    })
    .min(1),
};

export {
  updateReaction,
  getReactions,
};