import Joi from 'joi';
import {ObjectId} from "./custom.validations";

const createBlog = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    content: Joi.string().required(),
    thumbnail: Joi.any(),
    categories: Joi.array(),
  }),
};

const getBlogs = {
  query: Joi.object().keys({
    title: Joi.string(),
    slug: Joi.string(),
    author: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getBlog = {
  params: Joi.object().keys({
    slug: Joi.string(),
  }),
};

const updateBlog = {
  params: Joi.object().keys({
    slug: Joi.string(),
  }),
  body: Joi.object()
    .keys({
      title: Joi.string().required(),
      content: Joi.string().required(),
      file: Joi.string(),
    })
    .min(1),
};

const deleteBlog = {
  params: Joi.object().keys({
    slug: Joi.string()
  }),
};

const restoreBlogs = {
  body: Joi.object().keys({
    blogs: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

const deleteBlogsPermanently = {
  body: Joi.object().keys({
    blogs: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

export {
  createBlog,
  getBlogs,
  getBlog,
  updateBlog,
  deleteBlog,
  restoreBlogs,
  deleteBlogsPermanently,
};