import Joi from 'joi';
import {ObjectId} from "./custom.validations";

const createTag = {
  body: Joi.object().keys({
    title: Joi.string().required(),
  }),
};

const getTags = {
  query: Joi.object().keys({
    title: Joi.string(),
    slug: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getTag = {
  params: Joi.object().keys({
    tagId: Joi.string().custom(ObjectId),
  }),
};

const deleteTag = {
  params: Joi.object().keys({
    tagId: Joi.string().custom(ObjectId),
  }),
};

export {
  createTag,
  getTags,
  getTag,
  deleteTag,
};