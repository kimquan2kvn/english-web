import Joi from 'joi';
import {ObjectId} from "./custom.validations";

const createPost = {
  body: Joi.object().keys({
    title: Joi.string(),
    content: Joi.string().required(),
    file: Joi.string(),
    privacy: Joi.string(),
    location: Joi.custom(ObjectId),
  }),
};

const getPosts = {
  query: Joi.object().keys({
    title: Joi.string(),
    slug: Joi.string(),
    author: Joi.custom(ObjectId),
    location: Joi.custom(ObjectId),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getPost = {
  params: Joi.object().keys({
    slug: Joi.string().required()
  }),
};

const updatePost = {
  params: Joi.object().keys({
    slug: Joi.string().required()
  }),
  body: Joi.object()
    .keys({
      title: Joi.string().required(),
      content: Joi.string().required(),
      file: Joi.string(),
      location: Joi.custom(ObjectId),
    })
    .min(1),
};

const deletePost = {
  params: Joi.object().keys({
    slug: Joi.string().required()
  }),
};

const restorePost = {
  body: Joi.object().keys({
    blogs: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

const deletePostPermanently = {
  body: Joi.object().keys({
    blogs: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

export {
  getPosts,
  getPost,
  createPost,
  updatePost,
  deletePost,
  deletePostPermanently,
  restorePost
}