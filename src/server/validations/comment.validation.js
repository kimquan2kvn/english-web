import Joi from 'joi';
import {ObjectId} from "./custom.validations";

const createComment = {
  body: Joi.object().keys({
    content: Joi.string().required(),
    replyFor: Joi.string().custom(ObjectId),
    level: Joi.number(),
    tag: Joi.string().custom(ObjectId),
    category: Joi.string().custom(ObjectId).required(),
    model: Joi.string(),
  }),
};

const getComments = {
  query: Joi.object().keys({
    category: Joi.string().custom(ObjectId),
    author: Joi.string().custom(ObjectId),
    replyFor: Joi.string().custom(ObjectId),
    level: Joi.number().integer(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getComment = {
  params: Joi.object().keys({
    commentId: Joi.string().custom(ObjectId),
  }),
};

const updateComment = {
  params: Joi.object().keys({
    commentId: Joi.required().custom(ObjectId),
  }),
  body: Joi.object()
    .keys({
      content: Joi.string().required(),
      model: Joi.string(),
    })
    .min(1),
};


const deleteComment = {
  params: Joi.object().keys({
    commentId: Joi.string().custom(ObjectId),
  }),
  body: Joi.object().keys({
    model: Joi.string()
  })
};

const restoreComments = {
  body: Joi.object().keys({
    comments: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

const deleteCommentsPermanently = {
  body: Joi.object().keys({
    comments: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

export {
  createComment,
  getComments,
  getComment,
  updateComment,
  deleteComment,
  restoreComments,
  deleteCommentsPermanently,
};
