import {BotGroup, BotUser} from "../../models";
import {Bot} from "server/bot/bot";
import Message from "server/bot/lib/message";

const botFindGroupChat = async (user, chatSize) => {
  const senderId = user.data.facebookId;
  // firstly, we will look for any group that available to join
  console.log("Finding group chat");
  try {
    const foundGroup = await BotGroup.findOne({
      size: chatSize,
      isFull: false
    });
    if (foundGroup) {
      await BotGroup.findOneAndUpdate({
        isFull: foundGroup.members.length + 1 >= chatSize,
        members: [
          ...foundGroup.members,
          senderId
        ]
      });
      await BotUser.findOneAndUpdate({
        _id: user.data._id
      }, {
        groupId: foundGroup._id,
      })
      await Bot.send(new Message(senderId).text("Bạn đã được ghép vào một nhóm! Chào hỏi mọi người đi nào ;)"));
      for (let person of foundGroup.members) {
        const personInfo = await BotUser.findOne({facebookId: person});
        Bot.send(new Message(personInfo.facebookId).text(`${personInfo.firstName} ơi, vừa có một người gia nhập nhóm chat của bạn. Hãy gửi lời chào tới nhau đi nhé ;)`));
      }
    } else {
      // Bot.send(new Message(senderId).text("Now create group!"));
      const group = await BotGroup.create({
        size: chatSize,
        creator: user.data._id,
        isFull: false,
        members: [
          senderId
        ]
      });
      await BotUser.findOneAndUpdate({
        _id: user.data._id
      }, {
        groupId: group._id,
      })
      await Bot.send(new Message(senderId).text("Nhóm chat mới đã được tạo, hiện tại chỉ có mình bạn ở đây. Bạn sẽ nhận được thông báo khi có người khác tham gia nhóm, nhớ để ý nhé ;)"));
    }
  } catch (e) {
    console.log(e);
  }
};

export default botFindGroupChat;
