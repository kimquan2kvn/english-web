import botSendQuestionTypeMenu from "./bot-send-question-type-menu";
import {Bot} from "server/bot/bot";
import Message from "server/bot/lib/message";
import {questionInstructions} from "server/config/question.config";
import {Question} from "server/models";
import QuickRepliesButton from "server/bot/lib/quick-replies-button";
import {parseHTML} from "../../utils/parse-html";

const botRandomQuestion = async (user) => {
  const questionType = user.getDataStoreValue("favoriteQuestionType", false);
  const senderId = user.data.facebookId;
  if (!questionType) {
    await botSendQuestionTypeMenu(user);
  } else {
    // user has already chosen a question type
    await Bot.send(new Message(senderId).text(questionInstructions[questionType]));
    let questions;
    if (questionType === "doc_hieu" || questionType === "dien_tu") {
      // get a paragraph
      const topicQuestions = await Question.aggregate([{
        $match: {
          type: questionType,
          parent: 0
        }
      }, {
        $sample: {size: 1}
      }]);
      const topicQuestionText = parseHTML(topicQuestions[0].data.question);
      const chunks = topicQuestionText.match(/(.|[\r\n]){1,2000}/g);
      for (let bodyPart of chunks) {
        await Bot.send(new Message(senderId).text(bodyPart));
      }
      questions = await Question.aggregate([{
        $match: {
          type: questionType,
          parent: topicQuestions[0]._id,
        }
      }, {
        $sample: {size: 1}
      }]);
    } else {
      questions = await Question.aggregate([{
        $match: {type: questionType}
      }, {
        $sample: {size: 1}
      }]);
    }
    if (questions) {
      const {data: questionData} = questions[0];
      const convertIndex = (index) => ['A', 'B', 'C', 'D', 'E', 'F', 'G'][index];
      let questionText = `[${questions[0].id}] ` + parseHTML(questionData.question);
      questionText += `\n\n`;
      questionText += questionData.answers.map(
        (choice, index) => convertIndex(index) + ". " + parseHTML(choice)
      ).join('\n');

      const questionMessage = new Message(senderId).text(questionText).quickReplies([
        ...questionData.answers.map(
          (choice, index) =>
            new QuickRepliesButton(convertIndex(index))
              .setPayload(JSON.stringify({
                action: "answer",
                question: questions[0]._id,
                choice: convertIndex(index).toLowerCase()
              })),
        )
      ]);
      await Bot.send(questionMessage);
    }
  }
};
export default botRandomQuestion;
