import {questionConfig} from "server/config/question.config";
import {Bot} from "server/bot/bot";
import Message from "server/bot/lib/message";
import QuickRepliesButton from "server/bot/lib/quick-replies-button";

const botSendQuestionTypeMenu = async (user) => {
  const senderId = user.data.facebookId;
  const availableTypes = questionConfig.type;
  await Bot.send(new Message(senderId)
    .text("Chọn một loại câu hỏi bạn muốn rèn luyện nhé!\nBạn có thể chọn lại loại câu hỏi trắc nghiệm trong phần cài đặt.")
    .quickReplies([
      ...availableTypes.values.map(
        (type, index) => new QuickRepliesButton(availableTypes.titles[index])
          .setPayload(JSON.stringify({
            action: "set_favorite_question_type",
            questionType: type
          }))
      )
    ]));
}

export default botSendQuestionTypeMenu;
