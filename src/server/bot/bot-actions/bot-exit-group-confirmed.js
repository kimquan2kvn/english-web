import {BotGroup, BotUser} from "../../models";
import {Bot} from "server/bot/bot";
import Message from "server/bot/lib/message";
import QuickRepliesButton from "server/bot/lib/quick-replies-button";

const botExitGroupConfirmed = async (user) => {
  const senderId = user.data.facebookId;
  try {
    const currentGroup = await BotGroup.findOne({
      _id: user.data.groupId
    });
    await BotUser.findOneAndUpdate({
      _id: user.data._id
    }, {
      groupId: null
    });
    if (currentGroup.members.length === 1) {
      await BotGroup.findOneAndDelete({
        _id: currentGroup._id
      });
    } else {
      const newMembers = currentGroup.members.filter(memberId => memberId !== senderId);
      await BotGroup.findOneAndUpdate({
        _id: currentGroup._id
      }, {
        isFull: false, // when someone leaves, always not full,
        members: newMembers
      })
      for (let person of newMembers) {
        Bot.send(new Message(person).text("Một người đã rời khỏi nhóm..."));
      }
    }
    await Bot.send(new Message(senderId).text("Bạn đã thoát khỏi nhóm chat.").quickReplies([
      new QuickRepliesButton("Menu chính").setPayload("main_menu"),
    ]));
  } catch (e) {
    console.error("Error when exit group", e);
  }
};

export default botExitGroupConfirmed;
