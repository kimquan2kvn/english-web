import Template from "server/bot/lib/template";
import TemplateElement from "server/bot/lib/template-element";
import Button from "server/bot/lib/button";
import {Bot} from "server/bot/bot";
import Message from "server/bot/lib/message";

const botInfo = async (user) => {
  const senderId = user.data.facebookId;
  const infoTemplate = new Template("generic").setElements([
    new TemplateElement("Thông tin", "Bạn tìm kiếm thông tin gì?").setButtons([
      new Button().setTitle("Về dự án").postback("about"),
      new Button().setTitle("Hướng dẫn sử dụng").postback("instruction"),
      new Button().setTitle("Nhóm Facebook").setType("web_url").setURL("https://eof.vn"),
    ])
  ]);
  await Bot.send(new Message(senderId).template(infoTemplate)).then(r => {
    // console.log("main menu was sent");
  });
};

export default botInfo;
