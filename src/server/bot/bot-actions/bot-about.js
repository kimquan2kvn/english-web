const {Bot} = require("server/bot/bot");
const Message = require("server/bot/lib/message");
const QuickRepliesButton = require("server/bot/lib/quick-replies-button");
const botAbout = async (user) => {
  const senderId = user.facebookId;
  await Bot.send(new Message(senderId).text("English or Foolish là một dự án phi lợi nhuận...").quickReplies([
    new QuickRepliesButton("Quay về").setPayload("info"),
    new QuickRepliesButton("Menu chính").setPayload("main_menu"),
  ]));
};

export default botAbout;
