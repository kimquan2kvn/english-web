import {BotUser} from "../models";
import {Bot} from "./bot";

class BotUserHandler {
  facebookId = '';
  data = null;

  constructor(facebookId) {
    this.facebookId = facebookId;
  }

  async init() {
    this.data = await BotUser.findOne({
      facebookId: this.facebookId
    });
    if (!this.data) {
      // create user in case not exist.
      let userInfo = {
        name: 'Bot User',
        firstName: 'User',
        lastName: 'Bot',
        avatar: '',
        isFacebookInfoFetched: false,
      };
      // try {
      //   userInfo = await Bot.getUserInfo(this.facebookId);
      // } catch (e) {
      //   userInfo = {
      //     name: 'Bot User',
      //     firstName: 'User',
      //     lastName: 'Bot',
      //     avatar: '',
      //     isFacebookInfoFetched: false,
      //   };
      // }
      await BotUser.create({
        facebookId: this.facebookId,
        name: userInfo.name,
        firstName: userInfo['first_name'],
        lastName: userInfo['last_name'],
        avatar: userInfo['profile_pic'],
        dataStore: {},
        isFacebookInfoFetched: !!userInfo.isFacebookInfoFetched
      });
      this.data = await BotUser.findOne({
        facebookId: this.facebookId
      });
    }
  }

  async updateUserDataStore(newDataStore = {}) {
    return BotUser.findOneAndUpdate({
      _id: this.data._id
    }, {
      dataStore: newDataStore
    });
  }

  async setDataStoreValue(field, value) {
    let newData = this.data.dataStore || {};
    newData[field] = value;
    this.data.dataStore = newData;
    return BotUser.findOneAndUpdate({
      _id: this.data._id
    }, {
      dataStore: newData
    });
  }

  getDataStoreValue(field, defaultValue) {
    if (typeof this.data.dataStore === "object") {
      return this.data.dataStore[field] || defaultValue;
    } else {
      console.log("dataStore not available");
      return defaultValue;
    }
  }
}

export default BotUserHandler;
