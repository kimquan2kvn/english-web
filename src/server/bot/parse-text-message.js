const {Bot} = require("server/bot/bot");
const Message = require("./lib/message");
const Template = require("./lib/template");
const TemplateElement = require("./lib/template-element");
const QuickRepliesButton = require("./lib/quick-replies-button");
const Button = require("./lib/button");
const parsePayload = require("./parse-payload");
const {BotGroup} = require("../models");

const parseTextMessage = async (messagingEntry, user) => {
  const sender = messagingEntry['sender'];
  const senderId = sender['id'];
  const msg = messagingEntry['message'];
  const msgText = msg['text'];
  try {
    if (msg['quick_reply']) {
      // quick reply response
      const quickReply = msg['quick_reply'];
      const quickReplyPayload = quickReply['payload'];
      await parsePayload(quickReplyPayload, senderId, user);
    } else {
      // normal message
      // await Bot.send(new Message(senderId).seen());
      // console.log(msg);
      const template = new Template("generic").setElements([
        new TemplateElement("Element", "Subtitle").setButtons([
          new Button("postback", "Ok").postback("Alright")
        ])
      ]);
      if (user.data.groupId) {
        // chat group
        const group = await BotGroup.findOne({_id: user.data.groupId});
        if (msg.attachments) {
          for (let file of msg.attachments) {
            for (let personId of group.members.filter(x => x !== senderId)) {
              Bot.send(new Message(personId).setAttachment(file.type, {
                url: file.payload.url
              }));
            }
          }
        } else {
          for (let personId of group.members.filter(x => x !== senderId)) {
            Bot.send(new Message(personId).text(msgText));
          }
        }
      } else {
        // normal message
      }
      // await Bot.send(new Message(senderId).template(template));
      // await Bot.send(new Message(senderId).text("Okay").quickReplies([
      //   new QuickRepliesButton("Hello").setPayload("okay"),
      //   new QuickRepliesButton("Lahlah").setPayload("okay").setImage("https://lh3.googleusercontent.com/3o0p6QmsCudBwUd0s4O0Ia0OQUa8lQatCK4mefVmNQAFpu4SlMrAbX-nIyijQ9y3az8"),
      // ]));
    }
  } catch (e) {
    console.log("error occurred");
    console.log(e);
  }
};

module.exports = parseTextMessage;
