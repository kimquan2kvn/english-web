import {BotGroup, BotUser, Question} from "server/models";
import mongoose from "mongoose";
import {questionConfig, questionInstructions} from "server/config/question.config";
import botSendQuestionTypeMenu from "server/bot/bot-actions/bot-send-question-type-menu";
import {parseHTML} from "server/utils/parse-html";
import botRandomQuestion from "server/bot/bot-actions/bot-random-question";
import botInfo from "./bot-actions/bot-info";
import botAbout from "./bot-actions/bot-about";
import botFindGroupChat from "./bot-actions/bot-find-group-chat";
import botExitGroupConfirmed from "./bot-actions/bot-exit-group-confirmed";

const Message = require("server/bot/lib/message");
const QuickRepliesButton = require("server/bot/lib/quick-replies-button");
const Template = require("server/bot/lib/template");
const TemplateElement = require("server/bot/lib/template-element");
const Button = require("server/bot/lib/button");
const {Bot} = require("server/bot/bot");

const parsePayload = async (payload, senderId, user) => {
  switch (payload) {
    case "get_started":
      await Bot.send(new Message(senderId).text("Chào mừng bạn đến với English or Foolish :3 Rất vui khi bạn đã đến với Page và hy vọng rằng bọn mình có thể giúp bạn cải thiện việc học tiếng Anh nhé <3"));
      break;
    case "main_menu":
      const menuTemplate = new Template("generic").setElements([
        new TemplateElement("Menu chính", `Chào ${user.data.firstName}, bạn muốn làm gì nào?`).setButtons([
          new Button().setTitle("Học tiếng Anh").postback("learn_english"),
          new Button().setTitle("Chat nhóm").postback("group_chat"),
          new Button().setTitle("Thiết lập").postback("settings"),
        ])
      ]);
      await Bot.send(new Message(senderId).template(menuTemplate)).then(r => {
        // console.log("main menu was sent");
      });
      break;
    case "settings":
      const settingsMenu = new Template("generic").setElements([
        new TemplateElement("Thiết lập", `Chỉnh sửa các cài đặt về trải nghiệm của bạn với chatbot EOF...`).setButtons([
          new Button().setTitle("Đổi loại câu hỏi").postback("change_question_type"),
          new Button().setTitle("Quyền riêng tư").postback("privacy_settings"),
          new Button().setTitle("Thông tin").postback("info"),
        ])
      ]);
      await Bot.send(new Message(senderId).template(settingsMenu)).then(r => {
        // console.log("main menu was sent");
      });
      break;
    case "change_question_type":
      await botSendQuestionTypeMenu(user);
      break;
    case "group_chat":
      const groupChatMenu = new Template("generic").setElements([
        new TemplateElement("Chat nhóm", `Tính năng chat nhóm...`).setButtons([
          user.data.groupId ?
            new Button().setTitle("Thoát nhóm").postback("exit_group") :
            new Button().setTitle("Tìm nhóm").postback("find_group_chat"),
          new Button().setTitle("Giới thiệu").postback("group_chat_info"),
        ])
      ]);
      await Bot.send(new Message(senderId).template(groupChatMenu)).then(r => {
        // console.log("main menu was sent");
      });
      break;
    case "exit_group":
      if (user.data.groupId) {
        await Bot.send(new Message(senderId).text("Bạn có chắc chắn muốn thoát khỏi nhóm chat không?").quickReplies([
          new QuickRepliesButton("Có").setPayload("exit_group_confirmed"),
          new QuickRepliesButton("Không").setPayload("exit_group_cancelled"),
        ]));
      } else {
        await Bot.send(new Message(senderId).text("Bạn đang không ở trong nhóm chat...").quickReplies([
          new QuickRepliesButton("Menu chính").setPayload("main_menu"),
        ]));
      }
      break;
    case "exit_group_confirmed":
      await botExitGroupConfirmed(user);
      break;
    case "exit_group_cancelled":
      await Bot.send(new Message(senderId).text("Bạn đã chọn không thoát khỏi nhóm hiện tại.").quickReplies([
        new QuickRepliesButton("Menu chính").setPayload("main_menu"),
      ]));
      break;
    case "find_group_chat":
      if (user.data.groupId) {
        await Bot.send(new Message(senderId).text("Bạn đang ở trong nhóm chat nên không thể tìm nhóm mới. Hãy thoát nhóm hiện tại trước nhé!").quickReplies([
          new QuickRepliesButton("Menu chính").setPayload("main_menu"),
        ]));
      } else {
        await Bot.send(new Message(senderId).text("Bạn muốn tìm nhóm bao nhiêu người?").quickReplies([
          ...[2, 3, 5].map(size => new QuickRepliesButton(size + " người").setPayload(JSON.stringify({
            action: "find_group_chat",
            size: size
          })))
        ]));
      }
      break;
    case "learn_english":
      const learnMenu = new Template("generic").setElements([
        new TemplateElement("Menu chính", "Học tiếng Anh với EOF <3").setButtons([
          new Button().setTitle("Trắc nghiệm").postback("random_question"),
          new Button().setTitle("Gặp giáo viên").postback("talk_with_experts"),
        ])
      ]);
      await Bot.send(new Message(senderId).template(learnMenu)).then(r => {
        // console.log("main menu was sent");
      });
      break;
    case "random_question":
      await botRandomQuestion(user);
      break;
    case "info":
      await botInfo(user);
      break;
    case "about":
      await botAbout(user)
      break;
    default:
      try {
        const data = JSON.parse(payload);
        // console.log(data);
        switch (data.action) {
          case "set_favorite_question_type":
            await user.setDataStoreValue("favoriteQuestionType", data.questionType);
            await Bot.send(new Message(senderId).text("Bạn đã hoàn tất thiết lập câu hỏi. Nhớ rằng bạn có thể thay đổi loại câu hỏi bất kỳ lúc nào trong Thiết lập nhé!").quickReplies([
              new QuickRepliesButton("Trắc nghiệm").setPayload("random_question"),
              new QuickRepliesButton("Menu chính").setPayload("main_menu")
            ]))
            break;
          case "answer":
            // answer a question
            // await Bot.send(new Message(senderId).text("Your choice is " + data.choice));
            const question = await Question.findOne({
              _id: data.question,
            });
            if (question) {
              let msgText = "";
              if (question.data.correct === data.choice) {
                msgText = "Bạn đã trả lời đúng!";
              } else {
                msgText = "Bạn đã trả lời sai!";
              }
              msgText += '\n\n' + parseHTML(question.data.explanation);
              await Bot.send(new Message(senderId).text(msgText).quickReplies([
                new QuickRepliesButton("Tiếp tục").setPayload("random_question"),
                new QuickRepliesButton("Menu chính").setPayload("main_menu"),
                new QuickRepliesButton("Đổi loại câu hỏi").setPayload("change_question_type"),
              ]));
            } else {
              await Bot.send(new Message(senderId).text("Câu hỏi không hợp lệ."));
            }
            break;
          case "find_group_chat":
            await botFindGroupChat(user, data.size);
            break;
          default:
            break;
        }
      } catch (e) {
        // data is not json

      }
      break;
  }
};

module.exports = parsePayload;
