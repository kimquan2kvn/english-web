class MessengerProfile {
  constructor() {
  }

  setGetStarted(obj) {
    this.get_started = obj;
    return this;
  }

  setGreeting(obj) {
    this.greeting = obj;
    return this;
  }

  setIceBreakers(arr) {
    this.ice_breakers = arr;
    return this;
  }

  setPersistentMenu(arr) {
    this.persistent_menu = arr;
    return this;
  }

  setWhitelistedDomains(arr) {
    this.whitelisted_domains = arr;
    return this;
  }

  setAccountLinkingUrl(url) {
    this.account_linking_url = url;
    return this;
  }

  setHomeUrl(url) {
    this.home_url = url;
    return this;
  }

  setTargetAudience(url) {
    this.target_audience = url;
  }

  setSubjectToNewEUPrivacyRules(state) {
    this.subject_to_new_eu_privacy_rules = state;
  }
}

module.exports = MessengerProfile;
