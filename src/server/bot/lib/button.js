class Button {
  constructor(type = "postback", title = "") {
    this.type = type;
    this.title = title;
  }
  postback(payload) {
    this.type = "postback";
    this.payload = payload;
    return this;
  }
  setType(type) {
    this.type = type;
    return this;
  }
  setTitle(title) {
    this.title = title;
    return this;
  }
  setURL(url) {
    this.url = url;
    return this;
  }
  setPayload(payload) {
    this.payload = payload;
    return this;
  }
}

module.exports = Button;
