class TemplateElement {
  constructor(title, subtitle) {
    this.title = title;
    this.subtitle = subtitle;
  }
  setSubtitle(text) {
    this.subtitle = text;
    return this;
  }
  setImage(url) {
    this.image_url = url;
    return this;
  }
  setDefaultAction(action) {
    this.default_action = action;
    return this;
  }
  setButtons(buttons) {
    this.buttons = buttons;
    return this;
  }
}

module.exports = TemplateElement;
