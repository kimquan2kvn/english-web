class Message {
  recipient = {};

  constructor(recipientId) {
    this.recipient = {
      id: recipientId
    }
  }

  text(text) {
    this.message = {
      ...this.message,
      text: text
    }
    return this;
  }

  typing(state) {
    this.sender_action = state ? "typing_on" : "typing_off";
    return this;
  }

  seen() {
    this.sender_action = "mark_seen";
    return this;
  }

  template(template) {
    this.setAttachment("template", template);
    return this;
  }

  setAttachment(type, payload) {
    this.message = {
      ...this.message,
      attachment: {
        type: type,
        payload: payload
      }
    };
    return this;
  }

  quickReplies(replies) {
    if (!this.message) this.message = {};
    this.message.quick_replies = replies;
    return this;
  }
}


module.exports = Message;
