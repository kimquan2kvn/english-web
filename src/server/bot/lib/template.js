class Template {
  template_type = "generic";
  elements = [];
  constructor(type = "generic") {
    this.template_type = type;
  }
  aspectRatio(ratio = "horizontal" | "square") {
    this.image_aspect_ratio = ratio;
    return this;
  }
  setElements(elements) {
    this.elements = elements;
    return this;
  }
}

module.exports = Template;
