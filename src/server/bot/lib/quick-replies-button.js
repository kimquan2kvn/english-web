class QuickRepliesButton {
  constructor(title, type = "text") {
    this.title = title;
    this.content_type = type;
  }

  setTitle(title) {
    this.title = title;
    return this;
  }

  setPayload(payload) {
    this.payload = payload;
    return this;
  }

  setImage(imgUrl) {
    this.image_url = imgUrl;
    return this;
  }
}

module.exports = QuickRepliesButton;
