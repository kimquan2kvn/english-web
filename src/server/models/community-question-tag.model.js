import mongoose from 'mongoose';
import {toJSON, paginate} from './plugins';

const qtSchema = new mongoose.Schema({
  question: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "CommunityQuestion"
  },
  tag: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "CommunityTag"
  },
}, {
  collection: "communityquestiontags",
  timestamps: true,
});

// add plugin that converts mongoose to json
qtSchema.plugin(toJSON);
qtSchema.plugin(paginate);

/**
 * @typedef CommunityQuestionTag
 */
const CommunityQuestionTag = mongoose.models.CommunityQuestionTag || mongoose.model('CommunityQuestionTag', qtSchema);

export default CommunityQuestionTag;
