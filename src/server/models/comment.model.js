import mongoose from 'mongoose';
import {paginate, toJSON} from './plugins';
import httpStatus from "http-status";
import Reaction from "./reaction.model";
import ApiError from "../utils/api-error";

const commentSchema = new mongoose.Schema({
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  content: {
    type: String,
    required: true
  },
  replyFor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Comment',
  },
  level: {
    type: Number,
    default: 1
  },
  tag: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
  },
  editedAt: {
    type: String,
  },
  editedTimes: {
    type: Number,
  }
}, {
  collection: "comments",
  timestamps: true
});

commentSchema.plugin(paginate);
commentSchema.plugin(toJSON);

commentSchema.pre("save", async function (next) {
  const comment = this;

  if (comment.replyFor) {
    const replyFor = await Comment.findOne({_id: comment.replyFor})
    if (!replyFor) {
      throw new ApiError(httpStatus.NOT_FOUND, "Comment not found")
    }
    if (replyFor.level < 3) {
      comment.level = ++replyFor.level
    }
    if(replyFor.level === 3) {
      comment.level = 3
    }
  }

  next();
});

// will call before remove method => cascading delete all blogs model references
commentSchema.pre("deleteOne", {document: true, query: false}, async function (next) {
  const comment = this;
  await Comment.deleteMany({replyFor: comment._id});
  await Reaction.deleteMany({target: comment._id});
  next();
});

/**
 * @typedef Comment
 */
const Comment = mongoose.models.Comment || mongoose.model('Comment', commentSchema);

export default Comment;


