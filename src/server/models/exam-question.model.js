import mongoose from "mongoose";
import {toJSON, paginate} from "./plugins";

const ExamQuestionSchema = new mongoose.Schema({
  exam: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Exam"
  },
  question: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Question"
  },
}, {
  collection: "examquestions",
  timestamps: true
});

ExamQuestionSchema.plugin(toJSON);
ExamQuestionSchema.plugin(paginate);

/**
 * @typedef ExamQuestion
 */
const ExamQuestion = mongoose.models.ExamQuestion || mongoose.model("ExamQuestion", ExamQuestionSchema);

export default ExamQuestion;