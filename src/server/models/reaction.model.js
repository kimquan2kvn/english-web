import mongoose from 'mongoose';
import {paginate, toJSON} from "./plugins";
import reactionConfig from "server/config/reaction.config"

const reactionSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  target: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  type: {
    type: String,
    enum: reactionConfig.type,
    default: reactionConfig.default,
  },
  active: {
    type: Boolean,
    default: true,
  }
}, {
  collection: 'reactions',
  timestamps: true
});

reactionSchema.plugin(paginate);
reactionSchema.plugin(toJSON);

/**
 * @typedef Reaction
 */
const Reaction = mongoose.models.Reaction || mongoose.model(
  "Reaction",
  reactionSchema
);

export default Reaction;