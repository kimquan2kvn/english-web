import mongoose from 'mongoose';
import {toJSON, paginate} from './plugins';
import {privacy, type} from "../config/exam.config";
import {randomString} from "../utils/randomString";
import UserActivity from "./user-activity.model";
import ExamQuestion from "./exam-question.model";

const ExamSchema = new mongoose.Schema({
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  name: {
    type: String,
    required: true,
    trim: true,
  },
  code: {
    type: String,
    trim: true,
  },
  duration: {
    type: Number,
    required: true,
    default: 3600,
    trim: true,
  },
  description: {
    type: String,
    required: true,
    trim: true,
  },
  privacy: {
    type: String,
    enum: privacy.values,
    default: privacy.default,
  },
  startDate: {
    type: String,
  },
  endDate: {
    type: String,
  },
  type: {
    type: String,
    enum: type.values,
    default: type.default
  },
  totalQuestions: {
    type: Number,
    default: 0
  },
  deleted: {
    type: Boolean,
    default: false,
    private: true
  },
}, {
  collection: "exams",
  toJSON: {virtuals: true},
  toObject: {virtuals: true},
  timestamps: true,
});

ExamSchema.virtual("examQuestions", {
  ref: "ExamQuestion",
  localField: "_id",
  foreignField: "exam"
});

ExamSchema.virtual("comments", {
  ref: "Comment",
  localField: "_id",
  foreignField: "category"
});

ExamSchema.virtual("results", {
  ref: "Result",
  localField: "_id",
  foreignField: "exam"
});

ExamSchema.pre('save', async function (next) {
  const exam = this;
  let isAvailable = false;
  let examCode = "";
  while (!isAvailable) {
    examCode = randomString(8).toUpperCase();
    isAvailable = ((await Exam.countDocuments({code: examCode})) === 0);
  }
  const currentDate = new Date().getTime();
  if (exam.startDate) {
    exam.privacy = (currentDate >= exam.startDate) ? "open" : "close";
  }

  if (exam.endDate) {
    exam.privacy = (currentDate < exam.startDate) ? "open" : "close";
  }

  if (!exam.code) exam.code = examCode;
  next();
});

// will call before remove method => cascading delete all blogs model references
ExamSchema.pre("deleteOne", {document: true, query: false}, async function (next) {
  const exam = this;
  await Comment.deleteMany({category: exam._id});
  await UserActivity.deleteMany({target: exam._id});
  await ExamQuestion.deleteMany({exam: exam._id});
  next();
});

// add plugin that converts mongoose to json
ExamSchema.plugin(toJSON);
ExamSchema.plugin(paginate);

/**
 * @typedef Exam
 */
const Exam = mongoose.models.Exam || mongoose.model('Exam', ExamSchema);

export default Exam;
