import mongoose from 'mongoose';
import slugify from "../utils/slugify";
import {paginate, toJSON} from "./plugins";
import Comment from "./comment.model";
import logger from "../config/logger.config";
import User from "server/models/user.model";

const postSchema = new mongoose.Schema({
  title: {
    type: String,
  },
  slug: {
    type: String
  },
  content: {
    type: String,
    required: true
  },
  privacy: {
    type: String,
    default: false,
    private: true
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  thumbnail: {
    type: String
  },
  location: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  deleted: {
    type: Boolean,
    default: false,
    private: true
  },
}, {
  collection: 'profileposts',
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true},
});

postSchema.virtual('comments', {
  ref: 'Comment',
  localField: '_id',
  foreignField: 'category'
});

postSchema.plugin(paginate);
postSchema.plugin(toJSON);

/**
 * Slug generator
 * @param {string} postName - The posts's name
 * @returns {Promise<string>}
 */
postSchema.statics.slugGenerator = async function (postName) {
  let newSlug = slugify(postName);
  let count = 0;
  while (await this.exists({slug: newSlug})) {
    newSlug = `${slugify(postName)}_${++count}`;
  }
  return newSlug;
};

postSchema.pre('save', async function (next) {
  const post = this;
  const {username} = await User.findOne({_id: post.author});
  post.slug = await ProfilePost.slugGenerator(username);
  next();
});

// will call before remove method => cascading delete all posts model references
postSchema.pre("deleteOne", {document: true, query: false}, async function (next) {
  const post = this;
  try {
    await Comment.deleteMany({category: post._id});
  } catch (e) {
    logger.error("Failed to delete thumbnail");
  }
  next();
});

/**
 * @typedef ProfilePost
 */
const ProfilePost = mongoose.models.ProfilePost || mongoose.model("ProfilePost", postSchema);

export default ProfilePost;