import mongoose from "mongoose";

const botUserSchema = new mongoose.Schema({
  avatar: String,
  facebookId: String,
  firstName: String,
  lastName: String,
  name: String,
  personaId: String,
  state: String,
  dataStore: mongoose.Schema.Types.Mixed,
  inGroupChat: Boolean,
  isFacebookInfoFetched: false,
  groupId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "BotGroup"
  }
}, {
  collection: 'BotUsers',
  timestamps: true
});

/**
 * @typedef BotUser
 */
const BotUser = mongoose.models.BotUser || mongoose.model("BotUser", botUserSchema);

export default BotUser;
