import mongoose from 'mongoose';
import {toJSON, paginate} from './plugins';

const ResultSchema = new mongoose.Schema({
  exam: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Exam"
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  submittedAt: {
    type: String,
    required: true,
  },
  duration: {
    type: Number,
    required: true,
  },
  nCorrect: {
    type: Number,
    default: 0,
    required: true,
  },
  deleted: {
    type: Boolean,
    default: false,
    private: true
  },
}, {
  collection: "results",
  timestamps: true,
});

// add plugin that converts mongoose to json
ResultSchema.plugin(toJSON);
ResultSchema.plugin(paginate);

/**
 * @typedef Result
 */
const Result = mongoose.models.Result || mongoose.model('Result', ResultSchema);

export default Result;
