import mongoose from "mongoose";
import {questionConfig} from "../config/question.config";
import {toJSON, paginate} from "./plugins";
import UserActivity from "./user-activity.model";
import ExamQuestion from "./exam-question.model";
import Issue from "./issue.model";
import Exam from "./exam.model";

const {type} = questionConfig;

const QuestionSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: type.values,
    default: type.default,
    required: true
  },
  id: {
    type: Number,
  },
  data: {
    type: mongoose.Schema.Types.Mixed
  },
  parent: {
    type: mongoose.Schema.Types.Mixed,
    ref: "Question"
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  number_children: {
    type: Number,
    default: 0
  },
  time: {
    type: Date
  },
  record: {
    type: Number,
    default: 0
  },
  deleted: {
    type: Boolean,
    default: false,
    private: true
  },
}, {
  collection: "questions",
  timestamps: true
});

QuestionSchema.pre("save", async function (next) {
  const question = this;
  question.id = (await Question.findOne({}).sort({id: -1})).id++;
  while (await Question.countDocuments({id: question.id}) > 0) {
    question.id++;
  }
  // console.log(question.id);
  question.parent = (question.data.answers.length > 1) ? 0 : question._id;
});

// will call before remove method => cascading delete all blogs model references
QuestionSchema.pre("deleteOne", {document: true, query: false}, async function (next) {
  const blog = this;
  await Issue.deleteMany({target: blog._id});
  const exams = await ExamQuestion.deleteMany({question: blog._id});
  // await Exam.deleteMany({target: news._id});
  next();
});

QuestionSchema.plugin(toJSON);
QuestionSchema.plugin(paginate);

/**
 * @typedef Question
 */
const Question = mongoose.models.Question || mongoose.model("Question", QuestionSchema);

export default Question;
