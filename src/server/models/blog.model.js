import mongoose from 'mongoose';
import slugify from "../utils/slugify";
import {paginate, toJSON} from "./plugins";
import Comment from "./comment.model";
import UserActivity from "./user-activity.model";
import Reaction from "./reaction.model";

const blogSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  slug: {
    type: String
  },
  categories: {
    type: Array,
  },
  content: {
    type: String,
    required: true
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  thumbnail: {
    type: String
  },
  deleted: {
    type: Boolean,
    default: false,
    private: true
  },
  editedAt: {
    type: String,
  }
}, {
  collection: 'blogs',
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true},
});

blogSchema.virtual('comments', {
  ref: 'Comment',
  localField: '_id',
  foreignField: 'category'
});

blogSchema.plugin(paginate);
blogSchema.plugin(toJSON);

/**
 * Slug generator
 * @param {string} blogName - The blogs's name
 * @returns {Promise<string>}
 */
blogSchema.statics.slugGenerator = async function (blogName) {
  let newSlug = slugify(blogName);
  let count = 0;
  while (await this.exists({slug: newSlug})) {
    newSlug = `${slugify(blogName)}_${++count}`;
  }
  return newSlug;
};

blogSchema.pre('save', async function (next) {
  const blog = this;
  if (blog.isModified("title")) {
    blog.slug = await Blog.slugGenerator(blog.title);
  }
  next();
});

// will call before remove method => cascading delete all blogs model references
blogSchema.pre("deleteOne", {document: true, query: false}, async function (next) {
  const blog = this;
  await Comment.deleteMany({category: blog._id});
  await UserActivity.deleteMany({target: blog._id});
  await Reaction.deleteMany({target: blog._id});
  next();
});

/**
 * @typedef Blog
 */
const Blog = mongoose.models.Blog || mongoose.model("Blog", blogSchema);

export default Blog;