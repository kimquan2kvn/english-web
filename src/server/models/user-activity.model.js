import mongoose from 'mongoose';
import {paginate, toJSON} from "./plugins";

const activitySchema = new mongoose.Schema({
  message: {
    type: String,
    required: true
  },
  detail: {
    type: String,
  },
  target: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  model: {
    type: String,
    enum: ["Blog", "Comment", "User", "CommunityQuestion", "Exam", "Result", "Reaction", "Question", "ProfilePost", "Issue"],
    required: true
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
}, {
  collection: 'useractivities',
  timestamps: true,
  toJSON: { virtuals: true },
  toObject: { virtuals: true },
});

activitySchema.plugin(paginate);
activitySchema.plugin(toJSON);

/**
 * @typedef UserActivity
 */
const UserActivity = mongoose.models.UserActivity || mongoose.model("UserActivity", activitySchema);

export default UserActivity;