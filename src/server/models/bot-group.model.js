import mongoose from "mongoose";

const botGroupSchema = new mongoose.Schema({
  size: Number,
  creator: mongoose.Types.ObjectId,
  isFull: Boolean,
  members: [{
    type: String,
  }]
}, {
  collection: 'BotGroups',
  timestamps: true
});

/**
 * @typedef BotGroup
 */
const BotGroup = mongoose.models.BotGroup || mongoose.model("BotGroup", botGroupSchema);

export default BotGroup;
