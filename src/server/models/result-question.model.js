import mongoose from "mongoose";
import {toJSON, paginate} from "./plugins";
import {answers} from "../config/exam.config";

const ResultQuestionSchema = new mongoose.Schema({
  result: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Exam"
  },
  question: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  answer: {
    type: String,
    enum: answers,
    required: true
  },
  isCorrect: {
    type: Boolean,
    default: false
  }
}, {
  collection: "resultquestions",
  timestamps: true
});

ResultQuestionSchema.plugin(toJSON);
ResultQuestionSchema.plugin(paginate);

/**
 * @typedef ResultQuestion
 */
const ResultQuestion = mongoose.models.ResultQuestion || mongoose.model("ResultQuestion", ResultQuestionSchema);

export default ResultQuestion;