import httpStatus from 'http-status';
import {CommunityTag} from '../models';
import ApiError from '../utils/api-error';

/**
 * Create a Tag
 * @param {Object} tagBody
 * @returns {Promise<tag>}
 */
export const createTag = async (tagBody) => {
  return CommunityTag.create(tagBody);
};

/**
 * Query for Tags
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
export const queryTags = async (filter, options) => {
  Object.assign(options, {lean: true});
  return CommunityTag.paginate(filter, options);
};

/**
 * Get tag by filter
 * @param {Object} filter
 * @returns {Promise<tag>}
 */
export const getTagByFilter = async (filter) => {
  const tag = await CommunityTag.findOne({...filter});
  if (!tag) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Tag not found');
  }
  return tag;
};

/**
 * Delete tag
 * @param {ObjectId} tagId
 * @return {Promise<tag>}
 */
export const deleteTag = async (tagId) => {
  const tag = await CommunityTag.findOne({_id: tagId});
  if (!tag || tag.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Tag not found');
  }
  await tag.deleteOne();
  return tag;
}
