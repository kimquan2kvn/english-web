import httpStatus from 'http-status';
import {Exam, ExamQuestion, Result, ResultQuestion, Question} from '../models';
import ApiError from '../utils/api-error';
import mongoose from "mongoose";
import logger from "../config/logger.config";
import {userService} from "server/services/index";

/**
 * Create a Exam
 * @param {Object} examBody
 * @returns {Promise<Exam>}
 */
export const createExam = async (examBody) => {
  let questions, subQuestions, pairs, loadedQuestions = [];
  let exam = await Exam.create(examBody);
  let totalQuestions = 0;
  for (const kind of examBody.questionMap) {
    if (kind.type === "dien_tu") {
      questions = await getRandomDocument("Question", {$and: [{type: kind.type}, {parent: 0}, {number_children: kind.count}]}, {record: 1});
      if (questions.length === 0) throw new ApiError(httpStatus.NOT_FOUND, `Currently, the number of questions with type '${kind.type}' is not available.`);
      subQuestions = (await Question.find({parent: questions[0]['_id']}).limit(kind.count).sort({"id": 1}).lean()).map(question => question._id);
      loadedQuestions.push(questions[0]._id, ...subQuestions);
      totalQuestions += subQuestions.length;
    } else if (kind.type === "doc_hieu") {
      questions = await getRandomDocument("Question", {$and: [{type: kind.type}, {parent: 0}, {number_children: kind.count}]}, {record: 1});
      if (questions.length !== 0) {
        const subQuestions = (await Question.find({parent: questions[0]['_id']}).limit(kind.count).sort({"id": 1}).lean()).map(question => question._id);
        loadedQuestions.push(questions[0]._id, ...subQuestions);
      } else {
        pairs = twoSum([3, 4, 5, 6, 7], kind.count)[0];
        questions = await getRandomDocument("Question", {$and: [{type: kind.type}, {parent: 0}, {number_children: {$gte: pairs.i}}]}, {record: 1});
        subQuestions = (await Question.find({parent: questions[0]['_id']}).limit(pairs.i).sort({"id": 1}).lean()).map(question => question._id);
        loadedQuestions.push(questions[0]._id, ...subQuestions);
        totalQuestions += subQuestions.length;

        questions = await getRandomDocument("Question", {$and: [{type: kind.type}, {parent: 0}, {number_children: {$gte: pairs.j}}]}, {record: 1});
        subQuestions = (await Question.find({parent: questions[0]['_id']}).limit(pairs.j).sort({"id": 1}).lean()).map(question => question._id);
        loadedQuestions.push(questions[0]._id, ...subQuestions);
        totalQuestions += subQuestions.length;
      }
    } else {
      questions = (await getRandomDocument("Question", {type: kind.type}, {record: 1}, 200, kind.count)).map(question => question._id);
      loadedQuestions.push(...questions);
      totalQuestions += questions.length;
    }
  }

  await insertLoadedQuestions(exam._id, loadedQuestions);
  exam = await Exam.findOneAndUpdate({_id: exam["_id"]}, {$inc: {totalQuestions: totalQuestions}}, {new: true});
  return exam;
};

/**
 * Query for Exams
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @param {Object} user
 * @returns {Promise<QueryResult>}
 */
export const queryExams = async (filter, options, user = null) => {
  Object.assign(filter, {deleted: {$ne: true}});
  Object.assign(options, {lean: true});
  let loadedExams = await Exam.paginate(filter, options), currentDate = new Date().getTime(), execute = [], privacy;
  loadedExams.results.map(exam => {
    if (exam.startDate) exam.startDate = new Date(parseInt(exam.startDate)).getTime();
    if (exam.endDate) exam.endDate = new Date(parseInt(exam.endDate)).getTime();
    switch (exam.privacy) {
      case "close":
        if (!exam.startDate && !exam.endDate) break;
        else if (!exam.endDate && exam.startDate && currentDate >= exam.startDate) privacy = "open";
        else if (!exam.startDate && exam.endDate && currentDate < exam.endDate) privacy = "open";
        else if (currentDate >= exam.startDate && currentDate < exam.endDate) privacy = "open";
        break;
      case "open":
        if (!exam.startDate && !exam.endDate) {
          break;
        } else if (exam.startDate && currentDate < exam.startDate) {
          privacy = "close";
          break;
        } else if (exam.endDate && currentDate > exam.endDate) {
          privacy = "close";
          break;
        }
        break;
      default:
        break;
    }
    execute.push({
      updateOne: {
        filter: {
          _id: exam._id,
        },
        update: {
          privacy
        }
      }
    });
  });
  await Exam.bulkWrite(execute);
  const hasAuthorities = (user) ? user.role.permissions.includes("MANAGE_ALL_EXAM") || user.role.permissions.includes("GET_ALL_EXAM") : false;
  Object.assign(filter, (!hasAuthorities) ? {privacy: "open"} : {});
  Object.assign(options, {
    populate: "creator,results", filter: {
      creator: {
        select: "username displayName avatar"
      },
      results: {
        select: "user duration nCorrect",
        populate: {
          path: "user",
          select: "username displayName avatar"
        }
      }
    },
  });

  const exams = await Exam.paginate(filter, options);
  for (const exam of exams.results) {
    exam.topParticipants = (exam.results.sort((a, b) => (a.nCorrect > b.nCorrect) ? -1 : ((a.nCorrect === b.nCorrect) ? ((a.duration > b.duration) ? -1 : 1) : 1))).slice(0, 5).map(data => {
      return {
        user: data.user,
        duration: data.duration,
        nCorrect: data.nCorrect,
      }
    });
    const [isDoneBefore, nParticipants] = await Promise.all([
      user ? Result.countDocuments({exam: exam._id, user: user._id}) : 0,
      Result.countDocuments({exam: exam._id})
    ]);
    exam.isDoneBefore = isDoneBefore > 0;
    exam.nParticipants = nParticipants;
    exam.createdAt = new Date(exam.createdAt).getTime();
    delete exam.results;
  }
  return exams;
}

/**
 * Get Exam by filter
 * @param {Object} filter
 * @param {Object} user
 * @returns {Promise<Exam>}
 */
export const getExamByFilter = async (filter, user = null) => {
  const hasAuthorities = (user) ? user.role.permissions.includes("MANAGE_ALL_EXAM") || user.role.permissions.includes("GET_ALL_EXAM") : false;
  Object.assign(filter, (hasAuthorities) ? {deleted: {$ne: true}} : {deleted: {$ne: true}, privacy: "open"});
  let exam = await Exam.findOne(filter).populate({
    path: "examQuestions",
    populate: {path: "question", select: "-data.correct -data.explanation"}
  }).lean();
  if (!exam || exam.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Exam not found');
  }
  if (user) exam.isDoneBefore = await Result.countDocuments({exam: exam._id, user: user._id}) > 0;
  const questions = exam.examQuestions.map(data => data.question);
  exam.nParticipants = await Result.countDocuments({exam: exam._id});
  delete exam.examQuestions;
  return {
    exam,
    questions
  };
};

/**
 * Update Exam by filter
 * @param {Object} code
 * @param {Object} updateBody
 * @returns {Promise<Exam>}
 */
export const updateExam = async (code, updateBody) => {
  const exam = await Exam.findOne({code});
  if (!exam || exam.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, "Exam not found");
  }
  Object.assign(exam, updateBody);
  await exam.save();
  return exam;
};

/**
 * Delete Exam
 * @param {Object} code
 * @return {Promise<Exam>}
 */
export const deleteExam = async (code) => {
  const exam = await Exam.findOne({code});
  if (!exam || exam.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Exam not found');
  }
  //await Exam.deleteOne();
  Object.assign(exam, {deleted: true});
  await exam.save();
  return exam;
};

/**
 * Submit Exam
 * @param {Object} code
 * @param {ObjectId} user
 * @param {Object} submitData
 * @return {Promise}
 */
export const submitExam = async (code, user, submitData) => {
  const exam = await Exam.findOne({code, deleted: {$ne: true}});
  if (!exam) throw new ApiError(httpStatus.NOT_FOUND, 'Exam not found');
  const examId = exam._id;
  if (await Result.countDocuments({
    exam: examId,
    user
  }) > 0) throw new ApiError(httpStatus.BAD_REQUEST, "Cannot submit an exam twice");

  const {nCorrect, answersData} = await checkAnswers(examId, submitData.answers);

  const result = await Result.create({
    exam: examId,
    user,
    nCorrect,
    duration: submitData.duration,
    submittedAt: submitData.submittedAt
  });

  answersData.forEach(data => {
    data.result = result._id
  });

  await ResultQuestion.insertMany(answersData.filter(data => !!data.answer));
  await result.populate([
    {path: "exam", model: "Exam", select: "name duration code description totalQuestions"},
    {path: "user", model: "User", select: "username fullName avatar"},
  ]);
  return result;
};

/**
 * Get exam result
 * @param {Object} code
 * @param {ObjectId} user
 * @return {Promise}
 */
export const getExamResult = async (code, user) => {
  const exam = await Exam.findOne({code, deleted: {$ne: true}});
  if (!exam) throw new ApiError(httpStatus.NOT_FOUND, 'Exam not found');
  const examId = exam._id;

  const result = await Result.findOne({exam: examId, user}).populate([
    {path: "exam", model: "Exam", select: "-creator -updatedAt -privacy"},
    {path: "user", model: "User", select: "username fullName avatar"},
  ]);
  if (!result || result.deleted) throw new ApiError(httpStatus.NOT_FOUND, 'Result not found');

  const answers = await ResultQuestion.find({result: result._id}).select("question answer").lean();

  const {answersData} = await checkAnswers(examId, answers);

  return {
    result,
    answersData
  };
};

/**
 * Get exam result
 * @param {String} username
 * @param {Object} filter
 * @param {Object} options
 * @return {Promise}
 */
export const getDoneExams = async (username, filter, options) => {
  const user = await userService.getUserByFilter({username});
  Object.assign(filter, {user: user._id});
  Object.assign(options, {
    populate: "exam,user", filter: {
      exam: {select: "-creator -updatedAt -privacy"},
      user: {select: "username fullName avatar"}
    },
    lean: true
  });
  const result = await Result.paginate(filter, options)
  return {
    result,
  };
};

/**
 * Submit Exam
 * @param {Object} code
 * @param {Object} filter
 * @param {Object} options
 * @return {Promise}
 */
export const getExamRanking = async (code, filter, options) => {
  const exam = await Exam.findOne({code, deleted: {$ne: true}});
  if (!exam) throw new ApiError(httpStatus.NOT_FOUND, 'Exam not found');
  const examId = exam._id;

  Object.assign(filter, {exam: examId});
  Object.assign(options, {
    populate: "exam,user", filter: {
      exam: {
        select: "-creator -updatedAt"
      },
      user: {
        select: "username fullName avatar"
      }
    },
    sortBy: (!options.sortBy) ? "-nCorrect" : options.sortBy
  });

  return Result.paginate(filter, options);
};

async function checkAnswers(examId, answerArr) {
  return new Promise(async (resolve, reject) => {
    const questions = (await ExamQuestion.find({exam: examId}).lean().populate({
      path: "question",
      model: "Question"
    })).map(data => data.question);

    let nCorrect = 0, answersData = [], checkQuestion = {};
    for (const question of questions) {
      checkQuestion = answerArr.find(data => {
        // console.log(typeof data.question, typeof question._id)
        return data.question.toString() === question._id.toString()
      });

      if (checkQuestion) {
        if (checkQuestion.answer === question.data.correct) {
          checkQuestion.isCorrect = true;
          answersData.push(checkQuestion);
          nCorrect++;
        }
        delete checkQuestion._id;
      } else checkQuestion = {question: question._id}

      Object.assign(checkQuestion, {correct: question.data.correct});
      Object.assign(checkQuestion, {explanation: question.data.explanation});
      answersData.push(checkQuestion);
    }
    resolve({
      nCorrect,
      answersData
    });
  });
}

/**
 * Create document from questionArr
 * @param {ObjectId} exam
 * @param {Array} questionArr
 * @return {Promise}
 */
async function insertLoadedQuestions(exam, questionArr) {
  const execute = [];
  for (const question of questionArr) {
    execute.push({
      insertOne: {
        document: {
          exam,
          question
        }
      }
    });
  }
  return ExamQuestion.bulkWrite(execute);
}

/**
 * Get random document from db
 * @param {String} collection Name of collection
 * @param {Object} match Query filter
 * @param {Object} sort Sort options
 * @param {Number} limit Range of documents
 * @param {Number} size Size of sample
 * @return {Promise<Array>} size Size of sample
 */
function getRandomDocument(collection, match = {}, sort = {}, limit = 200, size = 1) {
  return new Promise(async (resolve, reject) => {
    try {
      let result = await mongoose.model(collection).aggregate([{
        $match: match
      }, {
        $sort: sort
      }, {
        $limit: limit
      }, {
        $sample: {
          size: size
        }
      }]);
      if (!result || result.length === 0) logger.warn("No document found");
      resolve(result);
    } catch (e) {
      reject(e);
    }
  });
}

/**
 * Split target parameter into two which have the total equals to its value
 * @param {Array} arr values to be included when split
 * @param {Number} target Value to split
 */
function twoSum(arr, target) {
  let pairs = []
  let numObject = {};
  for (let i = 0; i < arr.length; i++) {
    let thisNum = arr[i];
    numObject[thisNum] = i;
  }
  for (let i = 0; i < arr.length; i++) {
    let diff = target - arr[i];
    if (numObject.hasOwnProperty(diff)) {
      pairs.push({i: arr[i], j: diff})
    }
  }
  return pairs
}
