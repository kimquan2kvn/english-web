import httpStatus from 'http-status';
import {UserActivity} from '../models';
import ApiError from '../utils/api-error';

/**
 * Create a UserActivity
 * @param {Object} userActivityBody
 * @returns {Promise<UserActivity>}
 */
export const createUserActivity = async (userActivityBody) => {
  return UserActivity.create(userActivityBody);
};

/**
 * Query for UserActivities
 * @param {Object} user - User
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
export const queryUserActivities = async (user, filter, options) => {
  Object.assign(filter, {user: user._id});
  const userActivities = await UserActivity.paginate(filter, options);
  for (const activity of userActivities.results) {
    await activity.populate({path: "target", model: activity.model, select: "slug username code id"});
    activity.target = activity.target.slug;
    activity.message = user.displayName + " " + activity.message;
  }
  return userActivities;
};

/**
 * Get UserActivity by filter
 * @param {Object} filter
 * @returns {Promise<UserActivity>}
 */
export const getUserActivityByFilter = async (filter) => {
  const userActivity = await UserActivity.findOne({...filter, deleted: {$ne: true}}).populate("user");
  if (!userActivity) {
    throw new ApiError(httpStatus.NOT_FOUND, 'UserActivity not found');
  }
  return userActivity;
};

/**
 * Delete UserActivity
 * @param {ObjectId} UserActivityId
 * @return {Promise<UserActivity>}
 */
export const deleteUserActivity = async (UserActivityId) => {
  const UserActivity = await getUserActivityByFilter({_id: UserActivityId});
  await UserActivity.deleteOne();
  return UserActivity;
}
