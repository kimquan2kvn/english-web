import httpStatus from 'http-status';
import {Issue} from '../models';
import ApiError from '../utils/api-error';
import mongoose from "mongoose";
import {models} from '../config/issue.config';

/**
 * Create a Issue
 * @param {Object} issueBody
 * @param {ObjectId} userId
 * @returns {Promise<Issue>}
 */
export const createIssue = async (issueBody, userId) => {
  Object.assign(issueBody, {user: userId});
  return Issue.create(issueBody);
};

/**
 * Get issues by filter
 * @param {Object} filter
 * @param {Object} options
 * @returns {Promise<question>}
 */
const getIssuesByFilter = async (filter, options) => {
  Object.assign(filter, {deleted: {$ne: true}});
  if (filter._id) filter._id = mongoose.Types.ObjectId(filter._id);
  if (filter.target) filter.target = mongoose.Types.ObjectId(filter.target);
  let lookup = models.map(m => {
    return {
      $lookup: {
        from: m.collection,
        let: {"target": "$target"},
        pipeline: [
          {$match: {"$expr": {"$eq": ["$_id", "$$target"]}}},
          ...m.nestedLookUp
        ],
        as: m.field,
      }
    }
  })
  let issues = await Issue.aggregate([
    {
      $match: {
        ...filter
      }
    },
    {
      $lookup: {
        from: 'users',
        localField: 'user',
        foreignField: '_id',
        as: 'user',
      }
    },
    {
      $unwind: "$user"
    },
    ...lookup,
    {
      $project: {
        "user.password": 0,
        "user.email": 0,
        "user.dob": 0,
        "user.gender": 0,
        "user.bio": 0,
        "user.isDeleted": 0,
        "user.role": 0,
        "user.facebookInfo": 0,
        "user.googleInfo": 0,
        "user.deleted": 0,
        "user.createdAt": 0,
        "user.updatedAt": 0,
        "user.__v": 0,
        "__v": 0,
        "updatedAt": 0,
        "deleted": 0,
      }
    },
    {
      $skip: options.skip || 0
    },
    {
      $sort: options.sort || {createdAt: -1}
    },
    {
      $limit: options.limit || 10
    },
  ]);
  for (let data of issues) {
    data.createdAt = new Date(data.createdAt).getTime();
    for (let model of models) {
      if (data[model.field].length > 0) {
        data.target = data[model.field][0];
        data.model = model.model;
      }
      delete data[model.field];
    }
    for (const field in data.target.user) {
      if (!["_id", "username", "avatar", "displayName", "fullName"].includes(field)) delete data.target.user[field];
    }
  }
  return issues;
};


/**
 * Query for Issues
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {ObjectId} userId
 * @param {boolean} hasPermission
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {object} [options.sort] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise}
 */
export const queryIssues = async (filter, options, hasPermission, userId) => {
  if (!hasPermission) filter.user = userId;
  options.sort = {};
  options.limit = options.limit && parseInt(options.limit, 10) > 0 ? parseInt(options.limit, 10) : 9;
  options.page = options.page && parseInt(options.page, 10) > 0 ? parseInt(options.page, 10) : 1;
  if (options.sortBy) (options.sortBy.includes("-")) ? options.sort[`${options.sortBy.split("-")[1]}`] = -1 : options.sort[`${options.sortBy}`] = 1;

  if (Object.keys(options.sort).length < 1) options.sort = null;
  Object.assign(options, {skip: (options.page - 1) * options.limit});
  return Promise.all([getIssuesByFilter(filter, options), Issue.count({...filter})]).then((values) => {
    let [questions, totalResults] = values;
    const totalPages = Math.ceil(totalResults / options.limit);
    const result = {
      results: questions,
      page: options.page,
      limit: options.limit,
      totalPages: totalPages,
      totalResults: totalResults,
    };
    return Promise.resolve(result);
  })
};

/**
 * Get Issue by filter
 * @param {Object} filter
 * @param {ObjectId} userId
 * @param {boolean} hasPermission
 * @returns {Promise<Issue>}
 */
export const getIssueByFilter = async (filter, hasPermission, userId) => {
  const issue = (await getIssuesByFilter(filter, {skip: 0, limit: 1}))[0];
  if (!issue) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Issue not found');
  }
  if (!hasPermission && issue.user._id.toString() !== userId.toString()) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  return issue;
};

/**
 * Update Issue by filter
 * @param {ObjectId} issueId
 * @param {Object} updateBody
 * @param {Boolean} hasPermission
 * @param {ObjectId} userId
 * @returns {Promise<Issue>}
 */
export const updateIssue = async (issueId, updateBody, hasPermission, userId) => {
  const issue = await Issue.findOne({_id: issueId});
  if (!hasPermission && issue.user._id !== userId) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  Object.assign(issue, updateBody);
  await issue.save();
  return issue;
};

/**
 * Delete Issue
 * @param {ObjectId} issueId
 * @param {Boolean} hasPermission
 * @param {ObjectId} userId
 * @return {Promise<Issue>}
 */
export const deleteIssue = async (issueId, hasPermission, userId) => {
  const issue = await Issue.findOne({_id: issueId});
  if (!issue || issue.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Issue not found');
  }
  if (!hasPermission && issue.user._id !== userId) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  Object.assign(issue, {deleted: true});
  await issue.save();
  return issue;
}

/**
 * Restore Issues
 * @param {Object} body
 * @returns {Promise}
 */
export const restoreIssues = async (body) => {
  if (!body.issues || body.issues.length <= 0) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  let issues = [...body.issues];
  let execute = issues.map(r => {
    return {
      updateOne: {
        filter: {
          _id: r,
        },
        update: {
          deleted: false,
        }
      }
    }
  })
  return Issue.bulkWrite(execute);
}

/**
 * Delete Issues Permanently
 * @param {Object} body
 * @returns {Promise}
 */
export const deleteIssuesPermanently = async (body) => {
  if (!body.issues || body.issues.length <= 0) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  let issues = [...body.issues];
  let execute = issues.map(r => {
    return {
      deleteOne: {
        filter: {
          _id: r,
        }
      }
    }
  })
  return Issue.bulkWrite(execute);
}

export default {
  createIssue,
  queryIssues,
  getIssueByFilter,
  updateIssue,
  deleteIssue,
  restoreIssues,
  deleteIssuesPermanently,
};