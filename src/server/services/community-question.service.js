import httpStatus from 'http-status';
import {CommunityQuestion, CommunityQuestionTag, CommunityTag, UserComQuestion} from '../models';
import ApiError from '../utils/api-error';
import mongoose from 'mongoose';

/**
 * Create a Question
 * @param {Object} questionBody
 * @returns {Object}
 */
export const createQuestion = async (questionBody) => {
  let tags = questionBody.tags ? [...questionBody.tags] : null;
  let question = await CommunityQuestion.create(questionBody);
  if (tags) {
    let exec = tags.map(t => {
      return {
        insertOne: {
          document: {
            question: question._id,
            tag: t
          }
        }
      }
    });
    await CommunityQuestionTag.bulkWrite(exec);
  }
  await question.populate({path: "author", select: "username displayName avatar"});
  return question;
};

/**
 * Get questions by filter
 * @param {Object} filter
 * @param {Object} options
 * @param {ObjectId} userId
 * @returns {Promise<question>}
 */
const getQuestionsByFilter = async (filter, options, userId = null) => {
  const objectIdFields = ["author"];
  const excludeFields = ["_id", "deleted"];

  for (let field in filter) {
    if (objectIdFields.includes(field) || (filter._id && typeof filter._id !== "object")) filter[field] = mongoose.Types.ObjectId(filter[field]);
    else if (excludeFields.includes(field)) continue;
    else filter[field] = {$regex: filter[field], $options: 'g'}
  }

  let questions = await CommunityQuestion.aggregate([
    {
      $match: {
        ...filter
      }
    },
    {
      $lookup: {
        from: 'users',
        localField: 'author',
        foreignField: '_id',
        as: 'author',
      }
    },
    {
      $unwind: "$author"
    },
    {
      $lookup: {
        from: 'roles',
        localField: 'author.role',
        foreignField: '_id',
        as: 'author.role',
      }
    },
    {
      $unwind: "$author.role"
    },
    {
      $lookup: {
        from: 'communityquestiontags',
        localField: '_id',
        foreignField: 'question',
        as: 'tags',
      }
    },
    {
      $unwind: {
        path: "$tags",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'communitytags',
        localField: 'tags.tag',
        foreignField: '_id',
        as: 'tags',
      }
    },
    {
      $unwind: {
        path: "$tags",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'comments',
        localField: '_id',
        foreignField: 'category',
        as: 'comments',
      }
    },
    {
      $lookup: {
        from: 'reactions',
        localField: '_id',
        foreignField: 'target',
        as: 'reactions',
      }
    },
    {
      $project: {
        "author._id": 1,
        "author.username": 1,
        "author.email": 1,
        "author.fullName": 1,
        "author.displayName": 1,
        "author.avatar": 1,
        "title": 1,
        "slug": 1,
        "privacy": 1,
        "content": 1,
        "state": 1,
        "reactions": 1,
        "createdAt": 1,
        "editedAt": 1,
        "editedTimes": 1,
        "comments": 1,
        "tags": 1,
        "reactionCount": {$size: "$reactions"},
        "commentCount": {$size: "$comments"}
      }
    },
    {
      $group: {
        _id: "$_id",
        title: {$first: "$title"},
        content: {$first: "$content"},
        author: {$first: "$author"},
        slug: {$first: "$slug"},
        privacy: {$first: "$privacy"},
        createdAt: {$first: "$createdAt"},
        editedAt: {$first: "$editedAt"},
        editedTimes: {$first: "$editedTimes"},
        reactionCount: {$first: "$reactionCount"},
        commentCount: {$first: "$commentCount"},
        reactions: {$first: "$reactions"},
        tags: {
          $push: "$tags"
        }
      }
    },
    {
      $sort: options.sort || {reactions: -1}
    },
    {
      $skip: options.skip || 0
    },
    {
      $limit: options.limit || 10
    },
  ]);
  for (const data of questions) {
    data.createdAt = new Date(data.createdAt).getTime();
    let reactions = [...data.reactions];
    data.reactions = reactions.reduce((previousValue, currentValue) => {
      return previousValue[currentValue.type] ? ++previousValue[currentValue.type] : previousValue[currentValue.type] = 1, previousValue;
    }, {})
    let userReaction = userId ? reactions.find(r => r.user.toString() === userId.toString()) : null;
    data.userReaction = userReaction ? userReaction.type : false;
    data.isSaved = userId ? await UserComQuestion.countDocuments({question: data, user: userId}) > 0: false
  }

  return questions;
};

/**
 * Query for Questions
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {ObjectId} userId
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @param {object} [options.sort] - Current page (default = 1)
 * @returns {Promise}
 */
export const queryQuestions = async (filter, options, userId = null) => {
  if (filter.tag) {
    if (await CommunityTag.countDocuments({slug: filter.tag}) <= 0) {
      throw new ApiError(httpStatus.NOT_FOUND, `Cannot find tag with name ${filter.tag}`);
    }
    const {_id: tag} = await CommunityTag.findOne({slug: filter.tag});
    Object.assign(filter, {
      _id: {
        $in: (await CommunityQuestionTag.find({tag})).map(data => data.question)
      }
    })
    delete filter.tag
  }

  if (filter.saved) {
    Object.assign(filter, {
      _id: {
        $in: (await UserComQuestion.find({user: userId})).map(data => data.question)
      }
    });
    delete filter.saved
  }

  options.sort = {};
  options.limit = options.limit && parseInt(options.limit, 10) > 0 ? parseInt(options.limit, 10) : 9;
  options.page = options.page && parseInt(options.page, 10) > 0 ? parseInt(options.page, 10) : 1;
  if (options.sortBy) {
    if (options.sortBy.includes("-")) options.sort[`${options.sortBy.split("-")[1]}`] = -1;
    else options.sort[`${options.sortBy}`] = 1;
  }
  if (Object.keys(options.sort).length < 1) options.sort = null;
  Object.assign(options, {skip: (options.page - 1) * options.limit});
  return Promise.all([getQuestionsByFilter(filter, options, userId), CommunityQuestion.countDocuments({...filter})]).then((values) => {
    let [questions, totalResults] = values;
    const totalPages = Math.ceil(totalResults / options.limit);
    const result = {
      results: questions,
      page: options.page,
      limit: options.limit,
      totalPages: totalPages,
      totalResults: totalResults,
    };
    return Promise.resolve(result);
  })
};

/**
 * Get question by filter
 * @param {Object} filter
 * @param {ObjectId} userId
 * @returns {Object}
 */
export const getQuestionByFilter = async (filter, userId = null) => {
  Object.assign(filter, {deleted: {$ne: true}});
  let question = await getQuestionsByFilter(filter, {skip: 0, limit: 1}, userId);
  if (question.length <= 0) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Question not found');
  }
  return question[0];
}

/**
 * Save question
 * @param {String} slug
 * @param {ObjectId} userId
 * @return {Promise<Object>}
 */
export const saveQuestion = async (slug, userId) => {
  const question = await CommunityQuestion.findOne({slug, deleted: {$ne: true}}).populate({path: 'author'});
  if (!question || question.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Question not found');
  }
  const savedQuestion = await UserComQuestion.findOne({question: question._id})
  let message = "Saved question successfully";
  if (savedQuestion) {
    await savedQuestion.deleteOne({});
    message = "Unsaved question successfully"
  } else {
    await UserComQuestion.create({
      question: question._id,
      user: userId
    });
  }
  return {question, message};
}

/**
 * Update question by filter
 * @param {String} slug
 * @param {Object} updateBody
 * @param {Boolean} hasPermission
 * @param {ObjectId} userId
 * @returns {Promise<blog>}
 */
export const updateQuestion = async (slug, updateBody, hasPermission, userId) => {
  const question = await CommunityQuestion.findOne({slug, deleted: {$ne: true}}).populate({path: 'author'});
  if (!question) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Question not found');
  }
  if (!hasPermission && question.author._id.toString() !== userId.toString()) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  if (updateBody.tags && updateBody.tags.length > 0) {
    let relations = await CommunityQuestionTag.find({question: question._id}).lean()
    let tags = relations.map(r => r.tag.toString());
    let added = updateBody.tags.filter(t => !tags.includes(t));
    let removed = tags.filter(t => !updateBody.tags.includes(t));
    let exec = [];
    for (let tag of added) {
      exec.push({
        insertOne: {
          document: {
            question: question._id,
            tag: tag
          }
        }
      })
    }
    for (let tag of removed) {
      exec.push({
        deleteOne: {
          filter: {
            question: question._id,
            tag: tag
          }
        }
      })
    }
    await CommunityQuestionTag.bulkWrite(exec);
  }
  Object.assign(question, updateBody);
  Object.assign(question, {editedAt: Date.now(), editedTimes: question.editedTimes ? ++question.editedTimes : 1});
  await question.save();
  return question;
};

/**
 * Delete question
 * @param {String} slug
 * @param {Boolean} hasPermission
 * @param {ObjectId} userId
 * @return {Promise<question>}
 */
export const deleteQuestion = async (slug, hasPermission, userId) => {
  const question = await CommunityQuestion.findOne({slug, deleted: {$ne: true}}).populate({path: 'author'});
  if (!question || question.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Question not found');
  }

  if (!hasPermission && question.author._id.toString() !== userId.toString()) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  Object.assign(question, {deleted: true});
  await question.save();
  return question;
}

/**
 * Restore questions
 * @param {Object} body
 * @returns {Promise}
 */
export const restoreQuestions = async (body) => {
  if (!body.questions || body.questions.length <= 0) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  let questions = [...body.questions];
  let execute = questions.map(r => {
    return {
      updateOne: {
        filter: {
          _id: r,
        },
        update: {
          deleted: false,
        }
      }
    }
  })
  return CommunityQuestion.bulkWrite(execute);
}

/**
 * Delete questions Permanently
 * @param {Object} body
 * @returns {Promise}
 */
export const deleteQuestionsPermanently = async (body) => {
  if (!body.question || body.questions.length <= 0) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  let questions = [...body.questions];
  let execute = questions.map(r => {
    return {
      deleteOne: {
        filter: {
          _id: r,
        }
      }
    }
  })
  return CommunityQuestion.bulkWrite(execute);
}
