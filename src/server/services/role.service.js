import httpStatus from "http-status";
import {User, Role} from "server/models"
import ApiError from "server/utils/api-error";
import {PERMISSIONS} from "server/config/role.config";

/**
 * Create new Role
 * @param {Object} roleBody - Role's object body
 * @return {Promise}
 */
export const createRole = async (roleBody) => {
  return Role.create(roleBody);
}

/**
 * Get users by role
 * @param {string} permission
 * @param {Object} filter
 * @param {Object} options
 * @param {string} options.sortBy
 * @param {number} options.limit
 * @param {number} options.page
 * @returns {Promise<QueryResult>}
 */
export const getUsers = async (permission, filter, options) => {
  const roles = (await Role.find({permissions: `${permission.toUpperCase()}`})).map(role => role._id);

  if (roles.length === 0) {
    throw new ApiError(httpStatus.NOT_FOUND, `Cannot find role with permission ${permission}`);
  }
  Object.assign(options, {populate: 'role', filter: filter ?? {}});
  if (!options.limit) options.limit = await User.countDocuments({role: {$in: roles}});
  return User.paginate({role: {$in: roles}}, options);
}

/**
 * Query roles
 * @param {Object} filter - Filter for find
 * @param {Object} options - Pagination options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
export const queryRoles = async (filter, options) => {
  return Role.paginate(filter, options);
}

/**
 * Get role by slug
 * @param {string} slug - Slug for finding
 * @returns {Promise<QueryResult>}
 */
export const getRoleBySlug = async (slug) => {
  return Role.findOne({slug: slug});
}

/**
 * Get role by id
 * @param {Object} filter - Filter for finding
 * @returns {Promise<Role>}
 */
export const getRoleByFilter = async (filter) => {
  let role = await Role.findOne(filter);
  if (!role) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Role not found');
  }
  return role;
}

/**
 * Update role
 * @param {String} slug
 * @param {Object} updateBody
 * @return {Promise<Role>}
 */
export const updateRole = async (slug, updateBody) => {
  const role = await getRoleByFilter({slug});
  Object.assign(role, updateBody);
  await role.save();
  return role;
};

/**
 * Delete role
 * @param {String} slug
 * @return {Promise<Role>}
 */
export const deleteRole = async (slug) => {
  const role = await Role.findOne({slug});
  if (!role) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Role not found');
  }

  await role.updateOne({deleted: true});
  return role;
}

export const getPermissions = () => {
  return PERMISSIONS;
}
