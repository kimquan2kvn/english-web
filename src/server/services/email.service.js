import {sendEmailFromMailGun} from "server/utils/email-server";
import {User} from "server/models";
import {content} from "server/config/email.config";
import {tokenService} from "server/services";
import logger from "server/config/logger.config";
import {env} from "server/config";
import {tokenTypes} from "server/config/tokens.config";

/**
 * Connect exist account to GG
 * @param {Object} user User login data
 * @return {Promise<String>}
 */
export const sendVerifyEmail = async (user) => {
  const verifyToken = await tokenService.verifyTokenFromEmail(user, tokenTypes.VERIFY_EMAIL);
  if (env !== "production") logger.info("Sending verify email...");
  return sendEmail({
    email: user.email, isVerifiedEmail: {$ne: true}
  }, {
    subject: "[EOF] Verify email",
    contentHTML: content.verifyEmail.replace("{verifyToken}", verifyToken),
    sendTo: user.email
  });
}

/**
 * Connect exist account to GG
 * @param {Object} user User login data
 * @return {Promise<String>}
 */
export const sendResetPasswordEmail = async (user) => {
  const verifyToken = await tokenService.verifyTokenFromEmail(user, tokenTypes.RESET_PASSWORD);
  if (env !== "production") logger.info("Sending reset password email...");
  return sendEmail({email: user.email}, {
    subject: "[EOF] Reset password",
    contentHTML: content.changePassword.replace("{verifyToken}", verifyToken),
    sendTo: user.email
  });
}

/**
 * Send email
 * @param {Object} filter
 * @param {Object} mailData - Mail data
 * @param {string} mailData.subject
 * @param {string} mailData.contentHTML
 * @param {string} mailData.sendTo
 * @returns {Object}
 */
export const sendEmail = async (filter, mailData) => {
  const user = await User.findOne(filter);
  if (user) {
    (!mailData.sendTo) ? mailData.sendTo = user.email : mailData;
    await sendEmailFromMailGun(mailData);
  }
  return "A request was sent to your email";
}

