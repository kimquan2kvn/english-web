import httpStatus from 'http-status';
import axios from "axios";
import ApiError from 'server/utils/api-error';
import {tokenService, userService, emailService} from 'server/services';
import {tokenTypes} from 'server/config/tokens.config';
import {User, Token} from "server/models";
import {env} from "server/config";
import logger from "server/config/logger.config";

/**
 * Login with username and password
 * @param {Object} loginData
 * @returns {Promise<User>}
 */
export const login = async (loginData) => {
  const {username, password, fbAccessToken, ggAccessToken} = loginData;
  let facebookId, googleId;

  if (fbAccessToken) {
    const {data} = await axios.get(`https://graph.facebook.com/me?access_token=${fbAccessToken}`).catch(e => {
      throw new ApiError(e.response.status, "Invalid access token");
    });
    facebookId = data.id;
  }

  if (ggAccessToken) {
    const {data} = await axios.get(`https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=${ggAccessToken}`).catch(e => {
      throw new ApiError(e.response.status, e.response.statusText);
    });
    googleId = data.user_id;
  }

  const user = await userService.getUserByFilter((googleId) ? {"googleInfo.googleId": googleId} : ((facebookId) ? {"facebookInfo.facebookId": facebookId} : {username}));

  if (!user) throw new ApiError(httpStatus.UNAUTHORIZED, 'User not found');

  if ((password && !(await user.isPasswordMatch(password)))) throw new ApiError(httpStatus.UNAUTHORIZED, 'Provided password is not correct');

  if (!user.isVerifiedEmail) {
    emailService.sendVerifyEmail(user).catch(err => {
      if (env !== "production") logger.warn(err.message)
    });
    return {
      isVerifiedEmail: user.isVerifiedEmail,
      email: user.email
    };
  }
  return user;
};

/**
 * Logout
 * @param {string} refreshToken
 * @returns {Promise}
 */
export const logout = async (refreshToken) => {
  const refreshTokenDoc = await Token.findOne({token: refreshToken, type: tokenTypes.REFRESH, blacklisted: false});
  if (!refreshTokenDoc) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  await refreshTokenDoc.remove();
};

/**
 * Refresh auth tokens
 * @param {string} refreshToken
 * @returns {Promise<Object>}
 */
export const refreshAuth = async (refreshToken) => {
  let user, refreshTokenDoc;
  try {
    refreshTokenDoc = await tokenService.verifyToken(refreshToken, tokenTypes.REFRESH);
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Please authenticate');
  }
  user = await userService.getUserByFilter({_id: refreshTokenDoc.user});
  const tokens = await tokenService.generateAuthTokens(user, false, refreshTokenDoc);
  return {
    user,
    ...tokens
  };
};

/**
 * Connect an exist account to FB
 * @param {String} connectData
 * @param {Object} user
 * @return {Promise}
 */
export const connectToFB = async (connectData, user) => {
  let {accessToken, facebookId, name, avatar} = connectData;

  if (accessToken) {
    const {data} = await axios.get(`https://graph.facebook.com/me?access_token=${accessToken}`).catch(e => {
      throw new ApiError(e.response.status, e.response.statusText);
    });

    facebookId = data.id
  }
  if (!facebookId) {
    throw new ApiError(httpStatus.NOT_FOUND, "Invalid Facebook id");
  }
  if (await User.countDocuments({"facebookInfo.facebookId": facebookId, _id: {$ne: user._id}}) > 0) {
    throw new ApiError(httpStatus.BAD_REQUEST, "This Facebook account has already been connected to another account");
  }

  Object.assign(user, {
    facebookInfo: (!user.facebookInfo) ? {
      facebookId: facebookId,
      name: name,
      avatar: avatar
    } : null
  });

  return user.save();
}

/**
 * Connect exist account to GG
 * @param {String} connectData
 * @param {Object} user
 * @return {Promise}
 */
export const connectToGG = async (connectData, user) => {
  let {accessToken, googleId, name, avatar} = connectData;

  if (accessToken) {
    const {data} = await axios.get(`https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=${accessToken}`).catch(e => {
      throw new ApiError(e.response.status, e.response.statusText);
    });

    googleId = data.user_id;
  }
  if (!googleId) {
    throw new ApiError(httpStatus.NOT_FOUND, "Invalid Google id");
  }

  if (await User.countDocuments({"googleInfo.googleId": googleId, _id: {$ne: user._id}}) > 0) {
    throw new ApiError(httpStatus.BAD_REQUEST, "This Google account has already been connected to another account");
  }

  Object.assign(user, {
    googleInfo: (user.googleInfo === null) ? {
      googleId: googleId,
      name: name,
      avatar: avatar
    } : null
  });

  return user.save();
}
