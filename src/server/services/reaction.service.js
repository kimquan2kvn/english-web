import httpStatus from 'http-status';
import {Reaction} from '../models';
import ApiError from '../utils/api-error';
import mongoose from "mongoose";

/**
 * Update reaction
 * @param {ObjectId} targetId
 * @param {ObjectId} userId
 * @param {Object} body
 * @returns {Object}
 */
export const updateReaction = async (targetId, userId, body) => {
  let target = await mongoose.models[body.model].findOne({_id: targetId}).populate({path: 'author'});
  if (!target || target.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  let reaction = await Reaction.findOne({user: userId, target: targetId});
  let message = `You have given a reaction to a ${(body.model === "CommunityQuestion") ? "forum question" : (body.model === "ProfilePost") ? "profile post" : `${body.model.toLowerCase()}`}`;

  if (!reaction) {
    reaction = await Reaction.create({
      user: userId,
      target: targetId,
      active: body.active,
      type: body.type,
    });
  } else if (reaction && body.delete) {
    await reaction.deleteOne({});
    message = ""/*`You have removed a reaction from a ${(body.model === "CommunityQuestion") ? "forum question" : (body.model === "ProfilePost") ? "profile post" : `${body.model.toLowerCase()}`}`;*/
  } else {
    reaction.active = body.active || true;
    reaction.type = body.type || "like";
    await reaction.save();
  }
  let likeCount = await Reaction.countDocuments({target: targetId, active: true});
  target = (body.model === "Comment" && body.reactTo) ? target.category : target._id;
  return {
    message,
    reaction,
    likeCount,
    target
  }
}

/**
 * Get Reactions by TargetId
 * @param {ObjectId} targetId
 * @returns {Object}
 */
export const getReactions = async (targetId) => {
  let reactions = await Reaction.find({target: targetId}).populate({
    path: "user",
    select: {_id: 1, avatar: 1, displayName: 1, fullName: 1, username: 1}
  }).lean();
  let result = {
    total: reactions.length
  };
  for (let reaction of reactions) {
    if (!result[reaction.type]) {
      result[reaction.type] = {
        users: [reaction.user],
        count: 1,
      }
    } else {
      result[reaction.type].users.push(reaction.user);
      result[reaction.type].count++;
    }
  }
  return result;
};
