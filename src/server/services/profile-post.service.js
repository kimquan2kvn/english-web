import httpStatus from 'http-status';
import {ProfilePost} from '../models';
import ApiError from '../utils/api-error';
import logger from '../config/logger.config';
import mongoose from "mongoose";
import {deleteFile} from "server/utils/google-storage";
import {userService} from "server/services";

/**
 * Create a ProfilePost
 * @param {Object} postBody
 * @param {ObjectId} authorId
 * @returns {Promise<ProfilePost>}
 */
export const createPost = async (postBody, authorId) => {
  const {_id: userId} = await userService.getUserByFilter({username: postBody.location});
  postBody.location = userId;
  Object.assign(postBody, {author: authorId});
  const post = await ProfilePost.create(postBody);
  await post.populate({path: "author", select: "username displayName avatar"});
  return post;
};

/**
 * Get ProfilePost by filter
 * @param {Object} filter
 * @param {Object} options
 * @param {ObjectId} userId
 * @returns {Object}
 */
const getPostsByFilter = async (filter, options, userId = null) => {
  const objectIdFields = ["author", "location"];
  const excludeFields = ["_id", "deleted"];

  for (let field in filter) {
    if (objectIdFields.includes(field) || (filter._id && typeof filter._id !== "object")) filter[field] = mongoose.Types.ObjectId(filter[field]);
    else if (excludeFields.includes(field)) continue
    else filter[field] = {$regex: filter[field], $options: 'g'}
  }

  let posts = await ProfilePost.aggregate([
    {
      $match: {
        ...filter
      }
    },
    {
      $lookup: {
        from: 'users',
        localField: 'author',
        foreignField: '_id',
        as: 'author',
      }
    },
    {$unwind: "$author"},
    {
      $lookup: {
        from: 'users',
        localField: 'location',
        foreignField: '_id',
        as: 'location',
      }
    },
    {$unwind: "$location"},
    {
      $lookup: {
        from: 'comments',
        localField: '_id',
        foreignField: 'category',
        as: 'comments',
      }
    },
    {
      $lookup: {
        from: 'reactions',
        localField: '_id',
        foreignField: 'target',
        as: 'reactions',
      }
    },
    {
      $project: {
        "author._id": 1,
        "author.username": 1,
        "author.email": 1,
        "author.fullName": 1,
        "author.displayName": 1,
        "author.avatar": 1,
        "location._id": 1,
        "location.username": 1,
        "location.email": 1,
        "location.fullName": 1,
        "location.displayName": 1,
        "location.avatar": 1,
        "title": 1,
        "slug": 1,
        "categories": 1,
        "content": 1,
        "thumbnail": 1,
        "reactions": 1,
        "createdAt": 1,
        "editedAt": 1,
        "editedTimes": 1,
        "reactionCount": {$size: "$reactions"},
        "commentCount": {$size: "$comments"}
      }
    },
    {
      $sort: options.sort || {createdAt: -1}
    },
    {
      $skip: options.skip || 0
    },
    {
      $limit: options.limit || 10
    },
  ]);
  for (let post of posts) {
    post.createdAt = new Date(post.createdAt).getTime();
    let reactions = [...post.reactions];
    post.reactions = reactions.reduce((previousValue, currentValue) => {
      return previousValue[currentValue.type] ? ++previousValue[currentValue.type] : previousValue[currentValue.type] = 1, previousValue;
    }, {})
    let userReaction = userId ? reactions.find(r => r.user.toString() === userId.toString()) : null;
    post.userReaction = userReaction ? userReaction.type : false;
  }
  return posts;
};

/**
 * Query for posts
 * @param {ObjectId} userId
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @param {object} [options.sort] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
export const queryPosts = async (filter, options, userId = null) => {
  options.sort = {};
  options.limit = options.limit && parseInt(options.limit, 10) > 0 ? parseInt(options.limit, 10) : 9;
  options.page = options.page && parseInt(options.page, 10) > 0 ? parseInt(options.page, 10) : 1;
  if (options.sortBy) {
    if (options.sortBy.includes("-")) options.sort[`${options.sortBy.split("-")[1]}`] = -1;
    else options.sort[`${options.sortBy}`] = 1;
  }
  if (Object.keys(options.sort).length < 1) options.sort = null;
  Object.assign(options, {skip: (options.page - 1) * options.limit});
  return Promise.all([getPostsByFilter(filter, options, userId), ProfilePost.count({...filter})]).then((values) => {
    let [questions, totalResults] = values;
    const totalPages = Math.ceil(totalResults / options.limit);
    const result = {
      results: questions,
      page: options.page,
      limit: options.limit,
      totalPages: totalPages,
      totalResults: totalResults,
    };
    return Promise.resolve(result);
  })
};

/**
 * Get posts by filter
 * @param {Object} filter
 * @param {ObjectId} userId
 * @returns {Promise<ProfilePost>}
 */
export const getPostByFilter = async (filter, userId = null) => {
  const posts = await getPostsByFilter({...filter, deleted: {$ne: true}}, {limit: 1}, userId);
  if (!posts || posts.length < 1) {
    throw new ApiError(httpStatus.NOT_FOUND, 'ProfilePost not found');
  }
  return posts[0];
};

/**
 * Update posts by filter
 * @param {String} slug
 * @param {Object} updateBody
 * @param {Boolean} hasPermission
 * @param {ObjectId} userId
 * @param {File} file
 * @returns {Promise<ProfilePost>}
 */
export const updatePost = async (slug, updateBody, hasPermission, userId, file = null) => {
  const post = await ProfilePost.findOne({slug, deleted: {$ne: true}});
  if (!hasPermission && post.author._id.toString() !== userId.toString()) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  if (file) {
    updateBody.thumbnail = file.linkUrl;
    try {
      if (post.thumbnail && post.avatar.startsWith("https://storage.googleapis.com/")) await deleteFile(post.thumbnail)
    } catch (e) {
      logger.error("Failed to delete thumbnail");
    }
  }
  Object.assign(post, updateBody);
  Object.assign(post, {editedAt: Date.now(), editedTimes: post.editedTimes ? ++post.editedTimes : 1});
  await post.save();
  return post;
};

/**
 * Delete posts
 * @param {String} slug
 * @param {Boolean} hasPermission
 * @param {ObjectId} userId
 * @return {Promise<ProfilePost>}
 */
export const deletePost = async (slug, hasPermission, userId) => {
  const post = await ProfilePost.findOne({slug, deleted: {$ne: true}}).populate({path: 'author'});
  if (!post || post.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'ProfilePost not found');
  }
  if (!hasPermission && post.author._id.toString() !== userId.toString()) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  Object.assign(post, {deleted: true});
  await post.save();
  return post;
}

/**
 * Restore posts
 * @param {Object} body
 * @returns {Promise}
 */
export const restorePosts = async (body) => {
  if (!body.posts || body.posts.length <= 0) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  let posts = [...body.posts];
  let execute = posts.map(r => {
    return {
      updateOne: {
        filter: {
          _id: r,
        },
        update: {
          deleted: false,
        }
      }
    }
  })
  return ProfilePost.bulkWrite(execute);
}

/**
 * Delete posts Permanently
 * @param {Object} body
 * @returns {Promise}
 */
export const deletePostsPermanently = async (body) => {
  if (!body.posts || body.posts.length <= 0) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  let posts = await ProfilePost.find({_id: {$in: [...body.posts]}});

  for (const post of posts) {
    if (user.avatar && user.avatar.startsWith("https://storage.googleapis.com/")) await deleteFile(user.avatar);
    await post.deleteOne({});
  }

  return posts;
}
