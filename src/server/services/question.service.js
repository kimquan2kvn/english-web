import httpStatus from 'http-status';
import {Question} from '../models';
import ApiError from '../utils/api-error';
import mongoose from "mongoose";

/**
 * Create a Question
 * @param {Object} questionBody
 * @returns {Promise}
 */
export const createQuestion = async (questionBody) => {
  const {question: questionData, author} = questionBody;
  let questions = [];
  for (const data of questionData.questions) {
    const question = await Question.create({
      author,
      type: questionData.type,
      number_children: questionData.questions.length - 1,
      data: {
        question: data.question,
        answers: data.choices,
        correct: data.correct,
        explanation: data.explanation,
      }
    });
    questions.push(question);
  }
  return questions;
};

/**
 * Query for Questions
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
export const queryQuestions = async (filter, options) => {
  Object.assign(filter, {deleted: {$ne: true}});
  return Question.paginate(filter, options);
};

/**
 * Get Question by filter
 * @param {Object} filter
 * @returns {Promise<Question>}
 */
export const getQuestionByFilter = async (filter) => {
  let question = await Question.findOne(filter);
  if (!question) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Question not found');
  }

  if (mongoose.isValidObjectId(question.parent)) {
    question = (await question.populate({path: "parent", model: "Question"})).parent;
    question._doc.children = await Question.find({parent: question._id});
  }
  return question;
};

/**
 * Update Question by filter
 * @param {Number} questionId
 * @param {Object} updateBody
 * @returns {Promise<Question>}
 */
export const updateQuestion = async (questionId, updateBody) => {
  const question = await getQuestionByFilter({id: questionId});

  const updateData = {
    type: updateBody.type,
    data: {
      question: updateBody.question,
      answers: updateBody.choices,
      correct: updateBody.correct,
      explanation: updateBody.explanation,
    }
  };
  await question.updateOne(updateData);
  return getQuestionByFilter({id: questionId});
};

/**
 * Delete Question
 * @param {Number} questionId
 * @return {Promise<Question>}
 */
export const deleteQuestion = async (questionId) => {
  const question = await getQuestionByFilter({id: questionId});
  // Object.assign(question, {deleted: true});
  // await Question.save();
  return question.deleteOne();
}
