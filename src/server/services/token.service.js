import jwt from 'jsonwebtoken';
import moment from 'moment';
import {jwt as jwtConfig, env} from 'server/config';
import {Token} from 'server/models';
import {tokenTypes} from 'server/config/tokens.config';
import logger from "server/config/logger.config";
import ApiError from "server/utils/api-error";
import httpStatus from "http-status";

/**
 * Generate token
 * @param {ObjectId} userId
 * @param {Moment} expires
 * @param {string} type
 * @param {string} [secret]
 * @returns {string}
 */
export const generateToken = (userId, expires, type, secret = jwtConfig.secret) => {
  const payload = {
    sub: userId, iat: moment().unix(), exp: expires.unix(), type,
  };
  return jwt.sign(payload, secret);
};

/**
 * Save a token
 * @param {string} token
 * @param {ObjectId} userId
 * @param {Moment} expires
 * @param {string} type
 * @param {boolean} [blacklisted]
 * @returns {Promise<Token>}
 */
export const saveToken = async (token, userId, expires, type, blacklisted = false) => {
  return Token.create({
    token, user: userId, expires: expires.toDate(), type, blacklisted,
  });
};

/**
 * Verify token and return token doc (or throw an error if it is not valid)
 * @param {string} token
 * @param {string} type
 * @returns {Promise<Token>}
 */
export const verifyToken = async (token, type) => {
  const payload = jwt.verify(token, jwtConfig.secret);
  const tokenDoc = await Token.findOne({token, type, user: payload.sub, blacklisted: false});
  if (!tokenDoc) {
    throw new Error('Token not found');
  }
  return tokenDoc;
};

/**
 * Verify token
 * @param {User} user
 * @param {boolean} remember
 * @param {Object} currentRefreshToken
 * @return {Promise<Object>}
 */
export const generateAuthTokens = async (user, remember = false, currentRefreshToken = null) => {
  const accessTokenExpires = (remember === true) ? moment().add(jwtConfig.accessExpirationMinutes - 23, 'days') : moment().add(jwtConfig.accessExpirationMinutes, 'minutes');
  const accessToken = generateToken(user._id, accessTokenExpires, tokenTypes.ACCESS);

  let refresh
  if (!currentRefreshToken || (currentRefreshToken && new Date(currentRefreshToken.expires).getTime() <= new Date().getTime())) {
    const refreshTokenExpires = moment().add(jwtConfig.refreshExpirationDays, 'days');
    const refreshToken = generateToken(user._id, refreshTokenExpires, tokenTypes.REFRESH);
    await saveToken(refreshToken, user._id, refreshTokenExpires, tokenTypes.REFRESH);
    refresh = {
      token: refreshToken, expires: refreshTokenExpires.format("x")
    }
  } else {
    refresh = {
      token: currentRefreshToken.token, expires: new Date(currentRefreshToken.expires).getTime()
    }
  }

  return {
    access: {
      token: accessToken, expires: accessTokenExpires.format("x")
    }, refresh
  }
}

/**
 * Verify token
 * @param {User} user
 * @param {string} type
 * @param {Number} resendDuration
 * @return {Promise<String>}
 */
export const verifyTokenFromEmail = async (user, type, resendDuration = 10 * 60 * 1000) => {
  let emailVerifyToken = await Token.findOne({user: user._id, type});
  if (emailVerifyToken) {
    let tokenDoc, {token} = emailVerifyToken;
    try {
      tokenDoc = await verifyToken(token, type);
    } catch (err) {
      if (env !== "production") logger.warn(err.message);
      await Token.deleteOne({token: token, user: user._id, type});
    }
    // console.log((resendDuration - (new Date().getTime() - new Date(tokenDoc.createdAt).getTime())) / (1000 * 60))
    if (tokenDoc) throw new ApiError(httpStatus.BAD_REQUEST, `Please try to resend verification to this email in ${Math.ceil((resendDuration - (new Date().getTime() - new Date(tokenDoc.createdAt).getTime())) / (1000 * 60))} minute(s)`);
  }
  const expiresTime = (type === tokenTypes.VERIFY_EMAIL) ? jwtConfig.verifyEmailExpirationMinutes : (type === tokenTypes.RESET_PASSWORD) ? jwtConfig.resetPasswordExpirationMinutes : 10;
  const verifyTokenExpires = moment().add((env === "production") ? expiresTime : expiresTime / 5, 'minutes');
  emailVerifyToken = generateToken(user._id, verifyTokenExpires, type);
  await saveToken(emailVerifyToken, user._id, verifyTokenExpires, type);
  return emailVerifyToken;
}