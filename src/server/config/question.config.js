export const questionConfig = {
  type: {
    values: ["doc_hieu", "dien_tu", "dong_nghia", "trai_nghia", "ngu_phap", "tim_loi_sai", "phat_am", "trong_am", "giao_tiep"],
    titles: ["Đọc hiểu", "Điền từ", "Đồng nghĩa", "Trái nghĩa", "Ngữ pháp", "Tìm lỗi sai", "Phát âm", "Trọng âm", "Giao tiếp"],
    default: "other"
  }
};

export const questionInstructions = {
  "doc_hieu": "Đọc đoạn văn dưới đây và trả lời câu hỏi.",
  "dien_tu": "Tìm từ phù hợp điền vào chỗ thiếu.",
  "dong_nghia": "Tìm từ/đoạn đồng nghĩa.",
  "trai_nghia": "Tìm từ/đoạn trái nghĩa.",
  "ngu_phap": "Câu hỏi ngữ pháp.",
  "tim_loi_sai": "Tìm lỗi sai.",
  "phat_am": "Tìm từ có đoạn gạch dưới (đánh dấu) phát âm khác so với phần còn lại.",
  "trong_am": "Tìm từ có trọng âm khác với phần còn lại.",
  "giao_tiep": "Câu hỏi giao tiếp."
}
