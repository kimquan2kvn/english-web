import {email} from "server/config"

export const content = {
  verifyEmail: `
<html>
<head>
<style>
.controls-section {
    text-align: center;
}
</style>
</head>
<body>
  <div
    style="
      padding: 10px;
      border-radius: 10px;
      border: 1px solid rgba(0, 0, 0, 0.1);
      width: 500px;
      box-shadow: 3px 4px 4px rgba(0, 0, 0, 0.05);
    "
  >
    <div
      style=""
    >
      <div style="margin-left: 210px">
          <img
            src="https://imgur.com/Uj2COMQ.png"
            height="50"
          />
      </div>
      <h2
        style="
          text-align: center;
          flex: 1;
          margin-bottom: 0;
          color: #1a3353;
          margin-left: 50%;
          transform: translateX(-50%);
        "
      >
        English or Foolish
      </h2>
    </div>
    <h3 style="text-align: center; margin-top: 16px">
      Click on the link below to verify your account!
    </h3>
    <div class="controls-section">
      <a href="${email.verifyAccountUrl}{verifyToken}">
        <button
          id="button-link"
          style="
            background-color: #3e79f7;
            color: white;
            padding: 10px;
            border-radius: 10px;
            cursor: pointer;
            border: none !important;
          "
        >
          Click to verify
        </button>
      </a>
    </div>
  </div>
</body>
</html>
`,
  changePassword: `
<html>
<head>
<style>
.controls-section {
    text-align: center;
}
</style>
</head>
<body>
  <div
    style="
      padding: 10px;
      border-radius: 10px;
      border: 1px solid rgba(0, 0, 0, 0.1);
      width: 500px;
      box-shadow: 3px 4px 4px rgba(0, 0, 0, 0.05);
    "
  >
      <div
        style="display: flex; justify-content: center"
      >
        <span style="margin-left: 210px">
          <img
            
            src="https://imgur.com/Uj2COMQ.png"
            height="50"
          />
        </span>

      </div>
      <h2
        style="
          text-align: center;
          flex: 1;
          margin-bottom: 0;
          color: #1a3353;
        "
      >
        English or Foolish
      </h2>
  
    <h3 style="
        text-align: center; 
        margin-top: 16px;
       
        "
       >
      Welcome to EOF mailer system, You can sign in to the system using this username:  Click on the link below to verify your account!
    </h3>
    <div class="controls-section">
     <a href="${email.resetPasswordUrl}{verifyToken}">
        <button
          style="
            background-color: #3e79f7;
            color: white;
            padding: 10px;
            border-radius: 10px;
            cursor: pointer;
            border: none !important;
          "
        >
          Click to verify
        </button>
      </a>
    </div>
  </div>
</body>
</html>
`,
}

