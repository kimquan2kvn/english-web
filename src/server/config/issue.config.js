module.exports = {
  type: {
    values: ['system', 'specifically'],
    default: 'system'
  },
  state: {
    values: ["unresolved", "resolved"],
    default: "unresolved"
  },
  models: [
    {
      model: 'Question',
      collection: 'questions',
      field: 'questionReported',
      hidden: '',
      nestedLookUp: []
    },
    {
      model: 'Result',
      collection: 'results',
      field: 'resultReported',
      hidden: '',
      nestedLookUp: [{
        $lookup: {
          from: "users",
          let: {"userId": "$user"},
          pipeline: [
            {$match: {"$expr": {"$eq": ["$_id", "$$userId"]}}}
          ], as: "user"
        }
      }, {
        $unwind: "$user"
      }, {
        $lookup: {
          from: "exams",
          let: {"examId": "$exam"},
          pipeline: [
            {$match: {"$expr": {"$eq": ["$_id", "$$examId"]}}}
          ], as: "exam"
        }
      }, {
        $unwind: "$exam"
      },]
    },
  ]
}