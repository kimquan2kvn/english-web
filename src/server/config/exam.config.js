module.exports = {
  privacy: {
    values: ["open", "close"],
    default: "close"
  },
  type: {
    values: ["forum", "class", "individual"],
    default: "forum"
  },
  status: {
    values: ["completed", "not-started"],
    default: "not-started"
  },
  answers: {
    values: ["a", "b", "c", "d"]
  }
}