export default {
	type: [
		"like",
		"haha",
		"wow",
		"love",
		"angry",
		"sad",
		"useful",
		"notUseful"
	],
	models: [
		"Blog",
		"Comment",
		"CommunityQuestion",
		"ProfilePost"
	],
	default: "like"
}