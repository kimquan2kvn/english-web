module.exports = {
  privacy: {
    values: ["public", "private", "friends"],
    default: "public"
  }
}