const authorities = {
  user: [],
  manager: [],
  admin: [
    'MANAGE_ALL_USER',
    'MANAGE_ALL_ROLE',
    'MANAGE_ALL_EXAM',
    'MANAGE_ALL_EXAM_QUESTION',
    'MANAGE_ALL_POST',
    'MANAGE_ALL_BLOG',
    'MANAGE_ALL_COMMENT',
    'MANAGE_ALL_REPORT',
    'MANAGE_ALL_ISSUE',
    'MANAGE_ALL_COMMUNITY_QUESTION'
  ]
}

const roleConfig = Object.keys(authorities);

const AUTHORITIES = new Map(Object.entries(authorities));
const PERMISSIONS = [
  // user permission
  'MANAGE_ALL_USER',
  'GET_ALL_USER',
  'ADD_ALL_USER',
  'UPDATE_ALL_USER',
  'DELETE_ALL_USER',

  // role permission
  'MANAGE_ALL_ROLE',
  'GET_ALL_ROLE',
  'ADD_ALL_ROLE',
  'UPDATE_ALL_ROLE',
  'DELETE_ALL_ROLE',

  //exam permission
  'MANAGE_ALL_EXAM',
  'GET_ALL_EXAM',
  'ADD_ALL_EXAM',
  'UPDATE_ALL_EXAM',
  'DELETE_ALL_EXAM',

  //question permission
  'MANAGE_ALL_EXAM_QUESTION',
  'GET_ALL_EXAM_QUESTION',
  'ADD_ALL_EXAM_QUESTION',
  'UPDATE_ALL_EXAM_QUESTION',
  'DELETE_ALL_EXAM_QUESTION',

  // post permission
  'MANAGE_ALL_POST',
  'GET_ALL_POST',
  'ADD_ALL_POST',
  'UPDATE_ALL_POST',
  'DELETE_ALL_POST',

  // news permission
  'MANAGE_ALL_BLOG',
  'GET_ALL_BLOG',
  'ADD_ALL_BLOG',
  'UPDATE_ALL_BLOG',
  'DELETE_ALL_BLOG',

  // comments permission
  'MANAGE_ALL_COMMENT',
  'GET_ALL_COMMENT',
  'ADD_ALL_COMMENT',
  'UPDATE_ALL_COMMENT',
  'DELETE_ALL_COMMENT',

  //issue permission
  'MANAGE_ALL_ISSUE',
  'GET_ALL_ISSUE',
  'UPDATE_ALL_ISSUE',
  'DELETE_ALL_ISSUE',

  // forum permission
  'MANAGE_ALL_COMMUNITY_QUESTION',
  'GET_ALL_COMMUNITY_QUESTION',
  'ADD_ALL_COMMUNITY_QUESTION',
  'UPDATE_ALL_COMMUNITY_QUESTION',
  'DELETE_ALL_COMMUNITY_QUESTION',
]

module.exports = {
  ROLES: roleConfig,
  AUTHORITIES,
  PERMISSIONS,
  authorities,
  TYPES: ['developer', 'human resource', 'content writer']
}
