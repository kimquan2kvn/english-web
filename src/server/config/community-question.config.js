module.exports = {
  privacy: {
    values: ["public", "private"],
    default: "public"
  }
}